<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

// localhost
if ($_SERVER['HTTP_HOST'] == 'suisselifescience.local')
{
    /** The name of the database for WordPress */
    define('DB_NAME', 'suisselifescience');
    /** MySQL database username */
    define('DB_USER', 'root');
    /** MySQL database password */
    define('DB_PASSWORD', 'root');
    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    define('WP_HOME','http://suisselifescience.local');
    define('WP_SITEURL','http://suisselifescience.local');
}

// localhost2
elseif ($_SERVER['HTTP_HOST'] == 'suisselifescience.loc')
{

    /** The name of the database for WordPress */
    define('DB_NAME', 'suisselifescience_db');
    /** MySQL database username */
    define('DB_USER', 'root');
    /** MySQL database password */
    define('DB_PASSWORD', '');
    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    define('WP_HOME','http://suisselifescience.loc');
    define('WP_SITEURL','http://suisselifescience.loc');
}

// Artwebit DEV
elseif ($_SERVER['HTTP_HOST'] == 'suisselifescience.artwebit.ru' || $_SERVER['HTTP_HOST'] == 'www.suisselifescience.artwebit.ru')
{
    /** The name of the database for WordPress */
    define('DB_NAME', 'suisse_db');
    /** MySQL database username */
    define('DB_USER', 'suisse_user');
    /** MySQL database password */
    define('DB_PASSWORD', '1XDhGHxqx2');
    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    define('WP_HOME','http://suisselifescience.artwebit.ru');
    define('WP_SITEURL','http://suisselifescience.artwebit.ru');
}

// suisselifescience.com DEV
elseif ($_SERVER['HTTP_HOST'] == 'suisselifescience.com')
{
    /** The name of the database for WordPress */
    define('DB_NAME', 'sls_database_seven_test');
    /** MySQL database username */
    define('DB_USER', 'root');
    /** MySQL database password */
    define('DB_PASSWORD', 'AzEc2SlsMysqlRoot@54321');
    /** MySQL hostname */
    define('DB_HOST', 'mysql-prod-rds.chmzx5kdp8sv.eu-central-1.rds.amazonaws.com');

    define('WP_HOME','http://suisselifescience.com/release_candidate');
    define('WP_SITEURL','http://suisselifescience.com/release_candidate');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');



define('DISABLE_WP_CRON', true);
ini_set('log_errors','On');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4g)OBQ3fG>gshf/V%H9eA$K<b$=2.ys6m{6$YR/0RbDNJk|cFl|Qk/m4a,;ytbC7');
define('SECURE_AUTH_KEY',  '@?_<BS(1yVS,VMB)_63Zig-gq+rU:c5xRM2D9I+6gb4<Ht9}w@J_;Er=~>#-x0xA');
define('LOGGED_IN_KEY',    'rOWx3YQ+|!V%Z%NM{LD_ +YnfLjWA`Usq.to/:]3N0N(/5aPvuy0]+W-,SJMhs(r');
define('NONCE_KEY',        't<Fx![F9Wml]V0QBTzeX9wZM>(ge2w|tRYs^?4S-FX-ve=p*Q6nkn4(997-%Jle|');
define('AUTH_SALT',        '890Z&0{]i!ZX8tvK!(9{F8nS+~=U`0=E?x]nEMFc]ndXk}Xm`gHx(ys7[e(`;>~t');
define('SECURE_AUTH_SALT', 'E}i`b3/A1xfo+$snn!rD{Rn3hwDN&?^yB-Q[T)d!k3H@~_KTZ?H14JQ~aGI_T&` ');
define('LOGGED_IN_SALT',   'vg;^XVVca hepBw&kG1wYk}o4)APh]|I`%t X95`iJG|4kpFx5 kkkuAuGJ,[sq-');
define('NONCE_SALT',       'Q:bp;_pJ0K%C1$DsU49nFo[]@-A-vj}YRbT5ueOb`jQC9<3Ck-v?>!~hZy2M8E4G');
define('FS_METHOD', 'direct');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
