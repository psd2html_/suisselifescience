<?php

$nebp = NewsletterEngineForBlog::instance();

$options = $nebp::options();
$options->update();

$list_id = $options->get('bebp_mailchimp_list_id');
$interest_category_id = $options->get('bebp_mailchimp_interest_category_id');

?>

<div class="wrap">
    <h1><?php echo __('Newsletter engine for blog', 'nebp'); ?></h1>

    <form method="post" action="" novalidate="novalidate">
        <?php wp_nonce_field('nebp_options'); ?>

        <div class="button-block">
            <h2><?php _e('Main Settings', 'bebp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="bebp-from-name"><?php _e('From name', 'bebp'); ?></label>
                        </th>
                        <td>
                            <input name="bebp_from_name" type="text" id="bebp-from-name"
                                value="<?php echo $options->get('bebp_from_name'); ?>">
                        </td>
                    </tr>

                    <tr>
                        <th scope="row">
                            <label for="bebp-reply-to"><?php _e('Reply to', 'bebp'); ?></label>
                        </th>
                        <td>
                            <input name="bebp_reply_to" type="text" id="bebp-reply-to"
                                value="<?php echo $options->get('bebp_reply_to'); ?>">
                        </td>
                    </tr>

                    <tr>
                        <th scope="row">
                            <label for="bebp-mailchimp-api-key"><?php _e('Template', 'bebp'); ?></label>
                        </th>
                        <td>
                            <?php wp_editor(stripslashes($options->get('bebp_template')), 'bebp_template', []); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="button-block">
            <h2><?php _e('MailChimp Settings', 'bebp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="bebp-mailchimp-api-key"><?php _e('MailChimp API key', 'bebp'); ?></label>
                        </th>
                        <td>
                            <input name="bebp_mailchimp_api_key" type="text" id="bebp-mailchimp-api-key"
                                value="<?php echo $options->get('bebp_mailchimp_api_key'); ?>">
                        </td>
                    </tr>

                    <?php if ($options->get('bebp_mailchimp_api_key')): ?>
                        <tr>
                            <th scope="row">
                                <label for="bebp-mailchimp-list-id"><?php _e('Mailchimp list', 'bebp'); ?></label>
                            </th>
                            <td>
                                <select name="bebp_mailchimp_list_id" id="bebp-mailchimp-list-id">
                                    <option value=""><?php echo __('Select list', 'bebp'); ?></option>

                                    <?php foreach ($nebp->getLists() as $list): ?>
                                        <?php if ($list_id == $list['id']): ?>
                                            <option value="<?php echo $list['id']; ?>"
                                                selected><?php echo $list['name']; ?></option>
                                        <?php else: ?>
                                            <option
                                                value="<?php echo $list['id']; ?>"><?php echo $list['name']; ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>

                        <?php if ($list_id): ?>
                            <tr>
                                <th scope="row">
                                    <label for="bebp-mailchimp-interest-category-id">
                                        <?php _e('Mailchimp interest category', 'bebp'); ?>
                                    </label>
                                </th>
                                <td>
                                    <select name="bebp_mailchimp_interest_category_id" id="bebp-mailchimp-interest-category-id">
                                        <option value=""><?php echo __('Select interest category', 'bebp'); ?></option>

                                        <?php foreach ($nebp->getInterestCategories($list_id) as $interest_category): ?>
                                            <?php if ($interest_category_id == $interest_category['id']): ?>
                                                <option value="<?php echo $interest_category['id']; ?>" selected>
                                                    <?php echo $interest_category['name']; ?>
                                                </option>
                                            <?php else: ?>
                                                <option value="<?php echo $interest_category['id']; ?>">
                                                    <?php echo $interest_category['name']; ?>
                                                </option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>


                            <?php if($interest_category_id): ?>
                                <?php $interest_values = $nebp->getInterestCategoriesValues($list_id, $interest_category_id) ?>

                                <tr>
                                    <th scope="row">
                                        <label for="bebp-interested-luxury-skin-care-value"><?php _e('Luxury skin care', 'bebp'); ?></label>
                                    </th>
                                    <td>
                                        <select name="bebp_interested_luxury_skin_care_value" id="bebp-interested-luxury-skin-care-value">
                                            <option value=""><?php echo __('Select interest', 'bebp');?></option>

                                            <?php foreach ($interest_values as $value): ?>
                                                <?php if($options->get('bebp_interested_luxury_skin_care_value') == $value['id']): ?>
                                                    <option value="<?php echo $value['id'];?>" selected><?php echo $value['name'];?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">
                                        <label for="bebp-interested-anti-aging-diet-value"><?php _e('Anti aging diet', 'bebp'); ?></label>
                                    </th>
                                    <td>
                                        <select name="bebp_interested_anti_aging_diet_value" id="bebp-interested-anti-aging-diet-value">
                                            <option value=""><?php echo __('Select interest', 'bebp');?></option>

                                            <?php foreach ($interest_values as $value): ?>
                                                <?php if($options->get('bebp_interested_anti_aging_diet_value') == $value['id']): ?>
                                                    <option value="<?php echo $value['id'];?>" selected><?php echo $value['name'];?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row">
                                        <label for="bebp-interested-sport-performance-value"><?php _e('Sport performance value', 'bebp'); ?></label>
                                    </th>
                                    <td>
                                        <select name="bebp_interested_sport_performance_value" id="bebp-interested-sport-performance-value">
                                            <option value=""><?php echo __('Select interest', 'bebp');?></option>

                                            <?php foreach ($interest_values as $value): ?>
                                                <?php if($options->get('bebp_interested_sport_performance_value') == $value['id']): ?>
                                                    <option value="<?php echo $value['id'];?>" selected><?php echo $value['name'];?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </select>
                                    </td>
                                </tr>
                            <?php endif;?>
                        <?php endif; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>

        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary"
                value="<?php echo __('Save', 'nebp'); ?>">
        </p>
    </form>
</div>