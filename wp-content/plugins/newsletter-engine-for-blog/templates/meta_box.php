<?php

$nebp = NewsletterEngineForBlog::instance();
$options = $nebp::options();
global $post;

?>

<div id="block-send-to-mailchimp">
    <p>
        <label for="nebp_tags">
            <strong>Select tags</strong>
        </label>
    </p>

    <p>
        <ul id="nebp_tags" class="form-no-clear">
            <li>
                <label class="selectit">
                    <input type="checkbox" name="nebp_tags[]" id="luxury-skin-care"
                        value="luxury_skin_care">
                    Luxury skin care
                </label>
            </li>
            <li>
                <label class="selectit">
                    <input type="checkbox" name="nebp_tags[]" id="anti-aging-diet"
                        value="anti_aging_diet">
                    Anti aging diet
                </label>
            </li>
            <li>
                <label class="selectit">
                    <input type="checkbox" name="nebp_tags[]" id="sport-performance"
                        value="sport_performance">
                    Sport performance
                </label>
            </li>
        </ul>
    </p>

    <p>
        <input name="send_to_mailchimp" type="button" class="button button-primary button-large" id="send-to-mailchimp"
            value="<?php echo __('Send to MailChimp'); ?>"
            data-post-id="<?php echo $post->ID; ?>"
            data-error-create="<?php echo __('You must saved your post!'); ?>"
            data-confirm="<?php echo __('Before you post in mailchimp, save it! You have saved your post?'); ?>">
    </p>
</div>

<script>
    jQuery(function($) {
        $('#send-to-mailchimp').on('click', function(e) {
            e.preventDefault();

            var btn = $(this),
                postId = parseInt(btn.data('post-id')),
                confirmMessage = btn.data('confirm'),
                errorCreateMessage = btn.data('error-create'),
                tags = [];

            if (!postId) {
                alert(errorCreateMessage);
                return false;
            }

            if (confirm(confirmMessage) === false) {
                return false;
            }

            $('[name="nebp_tags[]"]:checked').each(function() {
                tags.push(this.value);
            });

            $.post('/wp-admin/admin-ajax.php?action=send_post_to_mailchimp', {
                post_id: postId,
                tags: tags,
            }, function(json) {
                if (json.status === false) {
                    alert(json.error);
                    return;
                }

                alert(json.message);
            }, 'json');
        });
    });
</script>