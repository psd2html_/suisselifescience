<?php
/*
Plugin Name: Newsletter engine for blog
Plugin URI:
Description:
Version: 1.0.0
Author: Taras Bilohan [Seven]
Author URI:
*/

define('NEBP_PATH', dirname(__FILE__).'/');
define('NEBP_URL', plugin_dir_url(__FILE__));
define('NEBP_VERSION', '1.0.0');

require_once NEBP_PATH.'src/Options.php';
require_once NEBP_PATH.'src/NewsletterEngineForBlogOptions.php';
require_once NEBP_PATH.'src/NewsletterEngineForBlog.php';

\NewsletterEngineForBlog::make();
