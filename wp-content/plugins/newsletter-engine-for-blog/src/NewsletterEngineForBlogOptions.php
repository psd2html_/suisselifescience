<?php

use Seven\Tptshk\Options;

class NewsletterEngineForBlogOptions extends Options
{
    protected $key_prefix = 'nebp';

    protected $options = [
        'bebp_template' => [
            'default' => '',
        ],
        'bebp_from_name' => [
            'default' => '',
        ],
        'bebp_reply_to' => [
            'default' => '',
        ],
        'bebp_mailchimp_api_key' => [
            'default' => '',
        ],

        'bebp_mailchimp_list_id' => [
            'default' => '',
        ],
        'bebp_mailchimp_interest_category_id' => [
            'default' => '',
        ],
        'bebp_interested_luxury_skin_care_value' => [
            'default' => '',
        ],
        'bebp_interested_anti_aging_diet_value' => [
            'default' => '',
        ],
        'bebp_interested_sport_performance_value' => [
            'default' => '',
        ],
    ];
}
