<?php

namespace Seven\Tptshk;

/**
 * Class Options
 *
 * @package Seven\Tptshk
 */
abstract class Options
{
    /**
     * @var string
     */
    protected $key_prefix = '';

    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var array
     */
    protected $data = [];

    /**
     * Options constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     *
     */
    public function init()
    {
        $this->pull();
    }

    /**
     *
     */
    protected function pull()
    {
        foreach ($this->options as $key => $data) {
            $option_value = get_option($key);

            if ($option_value !== false) {
                $this->set($key, $option_value);
            } elseif (isset($data['default'])) {
                $this->set($key, $data['default']);
            } else {
                $this->set($key, null);
            }
        }
    }

    /**
     * @return bool
     */
    public function update()
    {
        $post = $_POST;

        if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($post['submit'])) {
            return false;
        }

        if (function_exists('current_user_can') && !current_user_can('manage_options')) {
            die(_e('Hacker?', 'nebp'));
        }

        if (function_exists('check_admin_referer')) {
            check_admin_referer($this->key_prefix.'_options');
        }

        foreach ($this->options as $key => $data) {
            if (isset($post[$key])) {
                update_option($key, $post[$key]);
            }
        }

        $this->pull();
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        if (!isset($this->data[$key])) {
            return $default;
        }

        return $this->data[$key];
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->options;
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        $this->data[$key] = $value;
    }
}
