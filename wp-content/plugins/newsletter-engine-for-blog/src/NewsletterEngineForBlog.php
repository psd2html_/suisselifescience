<?php

class NewsletterEngineForBlog
{
    private static $instance;

    private static $options;

    private $mailchimp;

    private function __construct()
    {
        self::$options = new \NewsletterEngineForBlogOptions();

        $this->mailchimp = new \VPS\MailChimp(self::$options->get('bebp_mailchimp_api_key'));

        add_action('wp_enqueue_scripts', [$this, 'registerScripts']);
        add_action('wp_enqueue_scripts', [$this, 'registerStyles']);

        add_action('admin_menu', [$this, 'registerSubMenu']);

        add_action('add_meta_boxes', [$this, 'registerMetaBox']);

        add_action('wp_ajax_send_post_to_mailchimp', [$this, 'sendToMailchimp']);
        add_action('wp_ajax_nopriv_send_post_to_mailchimp', [$this, 'mustLogin']);
    }

    protected function __clone()
    {
        //
    }

    static public function make()
    {
        return self::instance();
    }

    static public function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function options()
    {
        return self::$options;
    }

    public function registerScripts()
    {
        //
    }

    public function registerStyles()
    {
        //
    }

    public function loadTemplate()
    {
        //
    }

    public function registerSubMenu()
    {
        add_options_page(
            __('Newsletter engine for blog', 'nebp'),
            __('Newsletter engine for blog', 'nebp'),
            'manage_options',
            NEBP_PATH.'admin/options.php'
        );
    }

    public function registerMetaBox()
    {
        add_meta_box(
            'newsletter_engine',
            __('Newsletter engine', 'nebp'),
            [$this, 'newsletterEngineBox'],
            'post',
            'side',
            'default'
        );
    }

    public function newsletterEngineBox()
    {
        include NEBP_PATH.'templates/meta_box.php';
    }

    // ===============================================================
    //                               API
    // ===============================================================

    /**
     * Get available lists from MailChimp
     *
     * @return array
     */
    public function getLists()
    {
        $response = $this->mailchimp->get('lists');
        
        $lists = [];
        foreach ($response['lists'] as $template) {
            $lists[] = [
                'id' => $template['id'],
                'name' => $template['name'],
            ];
        }

        return $lists;
    }

    /**
     * @param $list_id
     *
     * @return array
     */
    public function getInterestCategories($list_id)
    {
        $response = $this->mailchimp->get('/lists/'.$list_id.'/interest-categories/');

        $interest_categories = [];
        foreach ($response['categories'] as $template) {
            $interest_categories[] = [
                'id' => $template['id'],
                'name' => $template['title'],
            ];
        }

        return $interest_categories;


        return [];
    }

    /**
     * @param $list_id
     * @param $interest_category_id
     *
     * @return array
     */
    public function getInterestCategoriesValues($list_id, $interest_category_id)
    {
        $response = $this->mailchimp->get('/lists/'.$list_id.'/interest-categories/'.$interest_category_id.'/interests');

        $values = [];
        foreach ($response['interests'] as $interest) {
            $values[] = [
                'id' => $interest['id'],
                'name' => $interest['name'],
            ];
        }

        return $values;


        return [];
    }

    /**
     * Send to MailChimp
     */
    public function sendToMailchimp()
    {
        // Check is admin
        if (!is_admin()) {
            wp_send_json([
                'status' => false,
                'error' => 'You must be admin!',
            ]);
        }

        // MailChimp info
        $list_id = self::$options->get('bebp_mailchimp_list_id');
        $interest_category_id = self::$options->get('bebp_mailchimp_interest_category_id');
        $interest_category_values = [
            'luxury_skin_care' => self::$options->get('bebp_interested_luxury_skin_care_value'),
            'anti_aging_diet' => self::$options->get('bebp_interested_anti_aging_diet_value'),
            'sport_performance' => self::$options->get('bebp_interested_sport_performance_value'),
        ];

        // Get post id
        if (isset($_POST['post_id'])) {
            $post_id = (int) $_POST['post_id'];
        } elseif (isset($_GET['post_id'])) {
            $post_id = (int) $_GET['post_id'];
        } else {
            $post_id = 0;
        }

        // Get tags
        if (isset($_POST['tags'])) {
            $tags = (array) $_POST['tags'];
        } elseif (isset($_GET['tags'])) {
            $tags = (array) $_GET['tags'];
        } else {
            $tags = [];
        }

        $tags = array_filter($tags, function($tag) use ($interest_category_values) {
            return isset($interest_category_values[$tag]);
        });

        if (!$tags) {
            wp_send_json([
                'status' => false,
                'error' => 'Please set tags!',
            ]);
        }

        $tags = array_map(function($tag) use ($interest_category_values) {
            return $interest_category_values[$tag];
        }, $tags);

        // Check if post exists
        if (!$post = get_post($post_id)) {
            wp_send_json([
                'status' => false,
                'error' => 'Post not found!',
            ]);
        }

        $mail_html = $this->getMailContentByPost($post);
        if ($mail_html === false) {
            wp_send_json([
                'status' => false,
                'error' => 'Error getting template letter!',
            ]);
        }

        // Create campaign
        $campaign = $this->mailchimp->post('/campaigns', [
            'type' => 'regular',
            'recipients' => [
                'list_id' => $list_id,
                'segment_opts' => [
                    'match' => 'all',
                    'conditions' => [
                        [
                            'condition_type' => 'Interests',
                            'op' => 'interestcontains',
                            'field' => 'interests-'.$interest_category_id,
                            'value' => $tags,
                        ],
                    ],
                ],
            ],
            'settings' => [
                'subject_line' => $post->post_title,
                'title' => 'Campaign '.date('Y/m/d H:i').' '.$post->post_title,
                'from_name' => self::$options->get('bebp_from_name'),
                'reply_to' => self::$options->get('bebp_reply_to'),
            ],
        ]);

        if (!isset($campaign['id'])) {
            wp_send_json([
                'status' => false,
                'error' => 'Error create campaign!',
            ]);
        }

        $campaign_id = $campaign['id'];

        // Set campaign content
        $campaign_content = $this->mailchimp->put('/campaigns/'.$campaign_id.'/content', [
            'html' => $mail_html,
        ]);

        if (isset($campaign_content['detail'])) {
            wp_send_json([
                'status' => false,
                'error' => $campaign_content['detail'],
            ]);
        }

        // Start campaign
        $campaign = $this->mailchimp->post('/campaigns/'.$campaign_id.'/actions/send');

        if (isset($campaign['detail'])) {
            wp_send_json([
                'status' => false,
                'error' => $campaign['detail'],
            ]);
        }

        wp_send_json([
            'status' => true,
            'message' => 'Successfully!',
        ]);
    }

    /**
     * @param WP_Post $post
     *
     * @return string
     */
    protected function getMailContentByPost(WP_Post $post)
    {
        $message = self::$options->get('bebp_template');

        $pattern = '/\[([a-zA-Z][a-zA-Z0-9\_\-]*)\]/';
        $message = preg_replace_callback($pattern, function ($data) use ($post) {
            $key = $data[1];

            if (!isset($post->$key)) {
                return $key;
            }

            return $post->$key;
        }, $message);

        return $message;
    }

    public function mustLogin()
    {
        wp_send_json([
            'status' => false,
            'errors' => 'You must login!',
        ]);
    }
}
