<?php
/*
Plugin Name: Form results
Plugin URI:
Description: Form results
Author: Taras Bilohan [Seven]
Author URI:
Version: 0.1
Plugin Slug: form_results
*/

add_action('init', 'frp_create_post_type');

function frp_create_post_type()
{
    register_post_type('form_results', [
        'labels' => [
            'name' => __('Form results', 'frp'),
            'singular_name' => __('Form results', 'frp'),
        ],
        'public' => true,
        'has_archive' => true,
    ]);
}

/**
 * Insert data from form to post type form_result
 *
 * @param array $data
 *
 * @return bool
 */
function frp_insert_form_result(array $data)
{
    if (isset($data['email']) && $email = trim($data['email'])) {
        $email = wp_slash($email);
    } else {
        return false;
    }

    if (isset($data['subject']) && $subject = trim($data['subject'])) {
        $subject = wp_slash($subject);
    } else {
        return false;
    }

    if (isset($data['message']) && $message = trim($data['message'])) {
        $message = wp_slash($message);
    } else {
        return false;
    }

    if (isset($data['page_title']) && $data['page_title']) {
        $page_title = wp_slash($data['page_title']);
    } else {
        $page_title = '';
    }

    if (isset($data['page_url']) && $data['page_url']) {
        $page_url = wp_slash($data['page_url']);
    } else {
        $page_url = '';
    }
    
    if (isset($_SERVER['REMOTE_ADDR'])) {
        $ip_address = wp_slash($_SERVER['REMOTE_ADDR']);
    } else {
        $ip_address = '';
    }

    if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $user_agent = wp_slash($_SERVER['HTTP_USER_AGENT']);
    } else {
        $user_agent = '';
    }

    $title_template = 'Form data from "%s"';
    $words_limit = 15;
    $title_end = '...';

    if ($page_title) {
        $title = sprintf($title_template, wp_trim_words($page_title, $words_limit, $title_end));
    } else if ($page_url) {
        $title = sprintf($title_template, wp_trim_words($page_url, $words_limit, $title_end));
    } else if ($ip_address) {
        $title = sprintf($title_template, wp_trim_words($ip_address, $words_limit, $title_end));
    } else {
        $title = sprintf($title_template, 'undefined source') ;
    }

    // Create an array of new data record
    $post_data = [
        'post_title' => $title,
        'post_content' => '',
        'post_status' => 'private',
        'post_author' => 1,
        'post_category' => [],
        'post_type' => 'form_results',
        'meta_input' => [
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
            'page_title' => $page_title,
            'page_url' => $page_url,
            'ip_address' => $ip_address,
            'user_agent' => $user_agent,
        ],
    ];

    $post_id = wp_insert_post($post_data);

    return $post_id === 0 ? false : true;
}