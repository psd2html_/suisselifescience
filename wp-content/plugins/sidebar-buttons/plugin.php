<?php
/*
Plugin Name: Sidebar buttons
Plugin URI:
Description:
Version: 1.0.0
Author: Taras Bilohan [Seven]
Author URI:
*/

define('SBP_PATH', dirname(__FILE__).'/');
define('SBP_URL', plugin_dir_url(__FILE__));
define('SBP_VERSION', '1.0.0');

require SBP_PATH.'src/SidebarButtons.php';

\SidebarButtons::make();


