<?php

SidebarButtons::updateOptions();

$options = SidebarButtons::getOptions();
?>

<style>
    .button-block {
        margin-bottom: 35px;
    }
    .button-block input[type=text],
    .button-block input[type=number] {
        width: 100%;
        max-width: 500px;
    }
</style>

<div class="wrap">
    <h1>Sidebar buttons settings</h1>

    <form method="post" action="" novalidate="novalidate">
        <?php wp_nonce_field('sbp_options'); ?>

        <div class="button-block">
            <h2><?php _e('Title template', 'sbp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="main-title-template]"><?php _e('Title template', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="main[title_template]" type="text" id="main-title-template"
                                value="<?php echo $options['main']['title_template']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="button-block">
            <h2><?php _e('Facebook', 'sbp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="facebook-status"><?php _e('Show in sidebar', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="facebook[status]" type="hidden" value="0">
                            <?php if ($options['facebook']['status'] == 1): ?>
                                <input name="facebook[status]" type="checkbox" value="1"
                                    id="facebook-status" checked>
                            <?php else: ?>
                                <input name="facebook[status]" type="checkbox" value="1"
                                    id="facebook-status">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="facebook-sort"><?php _e('Sort order', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="facebook[sort]" type="number" id="facebook-sort"
                                value="<?php echo $options['facebook']['sort']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="facebook-icon"><?php _e('Icon', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="facebook[icon]" type="text" id="facebook-icon"
                                value="<?php echo $options['facebook']['icon']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="button-block">
            <h2><?php _e('Twitter', 'sbp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="twitter-status"><?php _e('Show in sidebar', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="twitter[status]" type="hidden" value="0">
                            <?php if ($options['twitter']['status'] == 1): ?>
                                <input name="twitter[status]" type="checkbox" value="1"
                                    id="twitter-status" checked>
                            <?php else: ?>
                                <input name="twitter[status]" type="checkbox" value="1"
                                    id="twitter-status">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="twitter-sort"><?php _e('Sort order', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="twitter[sort]" type="number" id="twitter-sort"
                                value="<?php echo $options['twitter']['sort']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="twitter-icon"><?php _e('Icon', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="twitter[icon]" type="text" id="twitter-icon"
                                value="<?php echo $options['twitter']['icon']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="button-block">
            <h2><?php _e('LinkedIn', 'sbp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="linkedin-status"><?php _e('Show in sidebar', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="linkedin[status]" type="hidden" value="0">
                            <?php if ($options['linkedin']['status'] == 1): ?>
                                <input name="linkedin[status]" type="checkbox" value="1"
                                    id="linkedin-status" checked>
                            <?php else: ?>
                                <input name="linkedin[status]" type="checkbox" value="1"
                                    id="linkedin-status">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="linkedin-sort"><?php _e('Sort order', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="linkedin[sort]" type="number" id="linkedin-sort"
                                value="<?php echo $options['linkedin']['sort']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="linkedin-icon"><?php _e('Icon', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="linkedin[icon]" type="text" id="linkedin-icon"
                                value="<?php echo $options['linkedin']['icon']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="button-block">
            <h2><?php _e('Google plus', 'sbp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="google-status"><?php _e('Show in sidebar', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="google[status]" type="hidden" value="0">
                            <?php if ($options['google']['status'] == 1): ?>
                                <input name="google[status]" type="checkbox" value="1"
                                    id="google-status" checked>
                            <?php else: ?>
                                <input name="google[status]" type="checkbox" value="1"
                                    id="google-status">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="google-sort"><?php _e('Sort order', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="google[sort]" type="number" id="google-sort"
                                value="<?php echo $options['google']['sort']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="google-icon"><?php _e('Icon', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="google[icon]" type="text" id="google-icon"
                                value="<?php echo $options['google']['icon']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="button-block">
            <h2><?php _e('Cart', 'sbp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="cart-status"><?php _e('Show in sidebar', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[status]" type="hidden" value="0">
                            <?php if ($options['cart']['status'] == 1): ?>
                                <input name="cart[status]" type="checkbox" value="1"
                                    id="cart-status" checked>
                            <?php else: ?>
                                <input name="cart[status]" type="checkbox" value="1"
                                    id="cart-status">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-sort"><?php _e('Sort order', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[sort]" type="number" id="cart-sort"
                                value="<?php echo $options['cart']['sort']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-icon"><?php _e('Icon', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[icon]" type="text" id="cart-icon"
                                value="<?php echo $options['cart']['icon']; ?>">
                        </td>
                    </tr>
                    <tr><td><hr></td><td></td></tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-skin-care"><?php _e('Link for Skin care page', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_skin_care]" type="text" id="cart-page-skin-care"
                                value="<?php echo $options['cart']['page_skin_care']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-skin-care-redirect"><?php _e('Redirect link for Skin care page', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_skin_care_redirect]" type="text" id="cart-page-skin-care-redirect"
                                value="<?php echo $options['cart']['page_skin_care_redirect']; ?>">
                        </td>
                    </tr>
                    <tr><td><hr></td><td></td></tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-diet"><?php _e('Link for Diet page', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_diet]" type="text" id="cart-page-diet"
                                value="<?php echo $options['cart']['page_diet']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-diet-redirect"><?php _e('Redirect link for Diet page', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_diet_redirect]" type="text" id="cart-page-diet-redirect"
                                value="<?php echo $options['cart']['page_diet_redirect']; ?>">
                        </td>
                    </tr>
                    <tr><td><hr></td><td></td></tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-sport"><?php _e('Link for Sport page', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_sport]" type="text" id="cart-page-sport"
                                value="<?php echo $options['cart']['page_sport']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-sport-redirect"><?php _e('Redirect link for Sport page', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_sport_redirect]" type="text" id="cart-page-sport-redirect"
                                value="<?php echo $options['cart']['page_sport_redirect']; ?>">
                        </td>
                    </tr>
                    <tr><td><hr></td><td></td></tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-general-redirect"><?php _e('Redirect link for general pages', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_general_redirect]" type="text" id="cart-page-general-redirect"
                                value="<?php echo $options['cart']['page_general_redirect']; ?>">
                        </td>
                    </tr>
                    <tr><td><hr></td><td></td></tr>
                    <tr>
                        <th scope="row">
                            <label for="cart-page-auth-redirect"><?php _e('Redirect link for authorized users', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="cart[page_auth_redirect]" type="text" id="cart-page-auth-redirect"
                                value="<?php echo $options['cart']['page_auth_redirect']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="button-block">
            <h2><?php _e('Contact', 'sbp'); ?></h2><hr>

            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="contact-status"><?php _e('Show in sidebar', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="contact[status]" type="hidden" value="0">
                            <?php if ($options['contact']['status'] == 1): ?>
                                <input name="contact[status]" type="checkbox" value="1"
                                    id="contact-status" checked>
                            <?php else: ?>
                                <input name="contact[status]" type="checkbox" value="1"
                                    id="contact-status">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="contact-sort"><?php _e('Sort order', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="contact[sort]" type="number" id="contact-sort"
                                value="<?php echo $options['contact']['sort']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="contact-icon"><?php _e('Icon', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="contact[icon]" type="text" id="contact-icon"
                                value="<?php echo $options['contact']['icon']; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="contact-url"><?php _e('Url', 'sbp'); ?></label>
                        </th>
                        <td>
                            <input name="contact[url]" type="text" id="contact-url"
                                value="<?php echo $options['contact']['url']; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
        </p>
    </form>

</div>