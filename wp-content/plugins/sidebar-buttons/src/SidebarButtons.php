<?php

class SidebarButtons implements \IteratorAggregate
{
    private static $instance;

    protected $allowed_buttons = [
        'facebook' => 'formatFacebook',
        'twitter' => 'formatTwitter',
        'linkedin' => 'formatLinkedin',
        'google' => 'formatGoogle',
        'cart' => 'formatCart',
        'contact' => 'formatContact',
    ];

    protected static $default_options = [
        'main' => [
            'title_template' => '%title%',
        ],
        'facebook' => [
            'status' => 1,
            'sort' => 1,
            'icon' => 'fa fa-facebook',
        ],
        'twitter' => [
            'status' => 1,
            'sort' => 2,
            'icon' => 'fa fa-twitter',
        ],
        'linkedin' => [
            'status' => 1,
            'sort' => 3,
            'icon' => 'fa fa-linkedin',
        ],
        'google' => [
            'status' => 1,
            'sort' => 4,
            'icon' => 'fa fa-google-plus',
        ],
        'cart' => [
            'status' => 1,
            'sort' => 5,
            'icon' => 'fa fa-shopping-cart',
            'page_skin_care' => 'luxury-skin-care',
            'page_skin_care_redirect' => 'http://idna.works/programs/luxury-skin-care/',
            'page_diet' => 'anti-aging-diet',
            'page_diet_redirect' => 'http://idna.works/programs/anti-aging-diet/',
            'page_sport' => 'sport-performance',
            'page_sport_redirect' => 'http://idna.works/programs/sport-performance/',
            'page_general_redirect' => '#',
            'page_auth_redirect' => 'http://idna.works/shop/',
        ],
        'contact' => [
            'status' => 1,
            'sort' => 6,
            'icon' => 'fa fa-phone',
            'url' => null,
        ],
    ];

    protected static $options;

    const KEY_PREFIX = 'sbp_';

    protected $buttons_data = [];

    private function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'registerScripts']);
        add_action('wp_enqueue_scripts', [$this, 'registerStyles']);

        add_action('wp_footer', [$this, 'loadTemplate']);

        add_action('admin_menu', [$this, 'registerSubMenu']);

        self::initOptions();
    }

    protected function __clone()
    {
        //
    }

    static public function make()
    {
        self::instance();
    }

    static public function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function registerScripts()
    {
        wp_register_script('ssbp-script', SBP_URL.'res/scripts/script.js', ['jquery'], SBP_VERSION);
        wp_enqueue_script('ssbp-script');
    }

    public function registerStyles()
    {
        wp_register_style('ssbp-style', SBP_URL.'res/styles/style.css', ['bs'], SBP_VERSION);
        wp_enqueue_style('ssbp-style');
    }

    public function loadTemplate()
    {
        load_template(SBP_PATH.'templates/buttons.php');
    }

    public function registerSubMenu() {
        add_options_page(
            __('Sidebar buttons', 'sbp'),
            __('Sidebar buttons', 'sbp'),
            'manage_options',
            SBP_PATH.'admin/options.php'
        );
    }

    public static function initOptions()
    {
        $options = [];
        foreach (self::getDefaultOptions() as $key => $default) {
            $option_value = get_option(self::KEY_PREFIX.$key);

            if ($option_value !== false) {
                $options[$key] = $option_value;
            } elseif (isset($default_options[$key])) {
                $options[$key] = $default_options[$key];
            } else {
                $options[$key] = null;
            }
        }

        self::$options = $options;
    }

    public static function getDefaultOptions()
    {
        return self::$default_options;
    }

    public function getOptions()
    {
        if (is_null(self::$options)) {
            $this->initOptions();
        }

        return self::$options;
    }

    public function getButtonOptions($button)
    {
        $options = $this->getOptions();

        if (isset($options[$button])) {
            return $options[$button];
        }

        return [
            'name' => '',
            'icon' => '',
            'url' => '',
        ];
    }

    public static function updateOptions()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])) {
            if (function_exists('current_user_can') && !current_user_can('manage_options')) {
                die (_e('Hacker?', 'sbp'));
            }

            if (function_exists('check_admin_referer')) {
                check_admin_referer('sbp_options');
            }

            foreach (self::getDefaultOptions() as $key => $default) {
                if (isset($_POST[$key])) {
                    update_option(self::KEY_PREFIX.$key, $_POST[$key]);
                }
            }
        }

        self::initOptions();
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->filteredButtonKeys());
    }

    public function filteredButtonKeys()
    {
        $options = self::getOptions();

        $buttons = [];
        foreach ($options as $key => $data) {
            if ($data['status'] == 0) {
                continue;
            }

            $buttons[$key] = $data['sort'];
        }

        asort($buttons);

        return array_keys($buttons);
    }

    public function url($social)
    {
        if (!isset($this->allowed_buttons[$social])) {
            return $this->getCurrentUrl();
        }

        $data = $this->getFormattedButton($social);

        return $data['url'];
    }

    public function icon($social)
    {
        if (!isset($this->allowed_buttons[$social])) {
            return 'fa fa-heart-o';
        }

        $data = $this->getFormattedButton($social);

        return $data['icon'];
    }

    public function cssClass($social)
    {
        if (!isset($this->allowed_buttons[$social])) {
            return '';
        }

        $data = $this->getFormattedButton($social);

        if (isset($data['class'])) {
            return $data['class'];
        }

        return '';
    }

    public function name($social)
    {
        if (!isset($this->allowed_buttons[$social])) {
            return ucfirst($social);
        }

        $data = $this->getFormattedButton($social);

        return $data['name'];
    }

    protected function getFormattedButton($social)
    {
        if (!isset($this->buttons_data[$social])) {
            $this->buttons_data[$social] = call_user_func([$this, $this->allowed_buttons[$social]]);
        }

        return $this->buttons_data[$social];
    }

    public function getCurrentUrl()
    {
        if ($permalink = get_permalink()) {
            return $permalink;
        }

        return get_site_url();
    }

    public function getCurrentTitle()
    {
        $variables = [
            '/%title%/' => get_the_title(),
        ];

        $title = preg_replace(
            array_keys($variables),
            array_values($variables),
            self::$options['main']['title_template']
        );

        return urlencode(trim($title));
    }

    protected function formatFacebook()
    {
        $url = 'http://www.facebook.com/sharer.php?';
        // $url .= 's=100';
        if ($title = $this->getCurrentTitle()) {
            $url .= '&p[title]='.$title;
        }
        // $url .= '&p[summary]='.$this->getCurrentTitle();
        $url .= '&p[url]='.$this->getCurrentUrl();
        // $url .= '&p[images][0]=YOUR_IMAGE_TO_SHARE_OBJECT';

        $options = $this->getButtonOptions('facebook');

        return [
            'name' => __('Facebook', 'sbp'),
            'icon' => $options['icon'],
            'url' => $url,
        ];
    }

    protected function formatGoogle()
    {
        $url = 'https://plus.google.com/share?';
        $url .= 'url='.$this->getCurrentUrl();

        $options = $this->getButtonOptions('google');

        return [
            'name' => __('Google Plus', 'sbp'),
            'icon' => $options['icon'],
            'url' => $url,
        ];
    }

    protected function formatLinkedin()
    {
        $url = 'http://www.linkedin.com/shareArticle?';
        $url .= 'mini=true';
        if ($title = $this->getCurrentTitle()) {
            $url .= '&title='.$title;
        }
        $url .= '&url='.$this->getCurrentUrl();

        $options = $this->getButtonOptions('linkedin');

        return [
            'name' => __('LinkedIn', 'sbp'),
            'icon' => $options['icon'],
            'url' => $url,
        ];
    }

    protected function formatTwitter()
    {
        $url = 'https://twitter.com/intent/tweet?';
        if ($title = $this->getCurrentTitle()) {
            $url .= 'text='.$title;
        }
        $url .= '&url='.$this->getCurrentUrl();

        $options = $this->getButtonOptions('twitter');

        return [
            'name' => __('Twitter', 'sbp'),
            'icon' => $options['icon'],
            'url' => $url,
        ];
    }

    protected function formatCart()
    {
        $options = $this->getButtonOptions('cart');

        $current_url = $this->getCurrentUrl();
        // $current_url = 'http://idna.works/anti-aging-from-DNA/luxury-skin-care';
        // $current_url = 'http://idna.works/anti-aging-from-DNA/anti-aging-diet';
        // $current_url = 'http://idna.works/anti-aging-from-DNA/sport-performance';

        if (!is_user_logged_in()) {
            if (strpos($current_url, $options['page_skin_care'])) {
                $url = $options['page_skin_care_redirect'];
            } elseif (strpos($current_url, $options['page_diet'])) {
                $url = $options['page_diet_redirect'];
            } elseif (strpos($current_url, $options['page_sport'])) {
                $url = $options['page_sport_redirect'];
            } else {
                $url = $options['page_general_redirect'];
            }
        } else {
            $url = $options['page_auth_redirect'];
        }

        return [
            'name' => __('Cart', 'sbp'),
            'icon' => $options['icon'],
            'url' => $url,
        ];
    }

    protected function formatContact()
    {
        $options = $this->getButtonOptions('contact');

        return [
            'name' => __('Contact', 'sbp'),
            'icon' => $options['icon'],
            'url' => $options['url'],
            'class' => 'contact-chat',
        ];
    }
}
