jQuery(function($) {

  $(document).on('click', '.ssbp-block.contact-chat a', function(event) {
    event.preventDefault();

    if (!window.hasOwnProperty('Tawk_API')) {
      console.error('Tawk_API not install!');
      return;
    }

    Tawk_API.toggle();

    if (Tawk_API.isChatMinimized()) {
      Tawk_API.hideWidget();
    } else {
      Tawk_API.showWidget();
    }
  });

});