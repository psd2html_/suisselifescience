<?php $buttons = SidebarButtons::instance(); ?>

<div class="ssbp ssbp-right">

    <?php foreach ($buttons as $slug): ?>
        <div class="ssbp-block <?php echo $buttons->cssClass($slug); ?>">
            <a href="<?php echo $buttons->url($slug); ?>" target="_blank" alt="<?php echo $buttons->name($slug); ?>">
                <i class="<?php echo $buttons->icon($slug); ?>"></i>
            </a>
            <p>
                <?php echo $buttons->name($slug); ?>
            </p>
        </div>
    <?php endforeach;?>

</div>