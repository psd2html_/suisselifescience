jQuery(function($) {
  var pageTitle = $('head>title').text();

  if (!window.hasOwnProperty('ga')) {wp-content/themes/IDNA/js/contact_us_ajax_send.js
    console.error('Please set up Google Analytics! Set Universal Tracking Code in wp-google-analytics-events');
    return;
  }

  $('form').on('submit', function(e) {
    console.log('GA: Form submitted!');
    ga('send', 'event', 'Form', 'Submitted form on page: ' + pageTitle);
  });
});