<?php
/*
Plugin Name: WP Google Analytics Forms submit events
Plugin URI:
Description: Send event to Google Analytics when user submit form
Version: 1.0.0
Author: Taras Bilohan [Seven]
Author URI: no url
*/

if (!defined('WP_GA_FORMS_SUBMIT_EVENTS_PATH')) {
    define('WP_GA_FORMS_SUBMIT_EVENTS_PATH', dirname(__FILE__));
}

if (!defined('WP_GA_FORMS_SUBMIT_EVENTS_PLUGIN_PATH')) {
    define('WP_GA_FORMS_SUBMIT_EVENTS_PLUGIN_PATH', plugin_dir_url(__FILE__));
}

define('WP_GA_FORMS_SUBMIT_EVENTS_VERSION', '1.0.0');

function wp_ga_forms_submit_events_scripts()
{
    wp_register_script(
        'wp-ga-forms-submit-events-script',
        WP_GA_FORMS_SUBMIT_EVENTS_PLUGIN_PATH.'res/scripts/script.js',
        ['jquery'],
        WP_GA_FORMS_SUBMIT_EVENTS_VERSION
    );

    wp_enqueue_script('wp-ga-forms-submit-events-script');
}

// function wp_ga_forms_submit_events_styles()
// {
//     wp_enqueue_style(
//         'wp-ga-forms-submit-events-style',
//         WP_GA_FORMS_SUBMIT_EVENTS_PLUGIN_PATH.'res/styles/style.css',
//         ['bs'],
//         WP_GA_FORMS_SUBMIT_EVENTS_VERSION
//     );
// }

add_action('wp_enqueue_scripts', 'wp_ga_forms_submit_events_scripts');
// add_action('wp_enqueue_scripts', 'wp_ga_forms_submit_events_styles');
