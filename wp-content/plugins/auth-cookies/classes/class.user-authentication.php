<?php

class UserAuthentication {


    public static function add_hooks() {

        add_action( 'rest_api_init', array( __CLASS__, 'rest_api_init' ) );

    }

    public static function rest_api_init() {

        register_rest_route( 'wp/v2', 'auth/clear.gif', array(
            'methods'       => WP_REST_Server::READABLE,
            'callback'      => __CLASS__ . '::rest_clear_cookie'
        ));

        register_rest_route( 'wp/v2', 'auth/universal.gif', array(
            'methods'       => WP_REST_Server::READABLE,
            'callback'      => __CLASS__ . '::rest_set_cookie'
        ));

        register_rest_route( 'wp/v2', 'auth/save_cookie', array(
            'methods'       => WP_REST_Server::CREATABLE,
            'callback'      => __CLASS__ . '::rest_save_cookie'
        ));

        register_rest_route( 'wp/v2', 'auth/delete_cookie', array(
            'methods'       => WP_REST_Server::CREATABLE,
            'callback'      => __CLASS__ . '::rest_delete_cookie'
        ));

        register_rest_route( 'wp/v2', 'auth/cart.gif', array(
            'methods'       => WP_REST_Server::READABLE,
            'callback'      => __CLASS__ . '::rest_cart_session'
        ));
    }

    public static function rest_cart_session() {

        setcookie( 'session_cart_user',   $_GET['session_name'], $_GET['session_expiration'], COOKIEPATH, COOKIE_DOMAIN );

        ob_clean();

        $im = imagecreatetruecolor(1, 1);

        header('Content-Type: image/gif');

        imagegif($im);
        imagedestroy($im);
        exit;
    }

    public static function rest_clear_cookie() {

        $logged_in_name =  'wordpress_logged_in_' . COOKIEHASH;
        $woocommerce_session_name = 'wp_woocommerce_session_' . COOKIEHASH;

        setcookie( 'secure_iddna_login',   ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );
        setcookie( 'woocommerce_cart_hash', ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);
        setcookie( 'woocommerce_items_in_cart', ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);
        setcookie( $logged_in_name,   ' ', time() - YEAR_IN_SECONDS, COOKIEPATH,          COOKIE_DOMAIN );
        setcookie( $logged_in_name,   ' ', time() - YEAR_IN_SECONDS, SITECOOKIEPATH,      COOKIE_DOMAIN );
        setcookie( $woocommerce_session_name,   ' ', time() - YEAR_IN_SECONDS, SITECOOKIEPATH,      COOKIE_DOMAIN );

        ob_clean();

        $im = imagecreatetruecolor(1, 1);

        header('Content-Type: image/gif');

        imagegif($im);
        imagedestroy($im);
        exit;

    }

    public static function rest_set_cookie() {

        //$username = $_REQUEST['username'];
        $token = $_REQUEST['authToken'];

        if ( empty($token) ) {
            return;
        }

        $cookie_data = Auth_Cookie::get_instance( $token );

        if ( ! $cookie_data ) {
            return;
        }
        $logged_in_cookie = Auth::wp_generate_auth_cookie( $cookie_data->user_login, $cookie_data->expiration, 'logged_in', $cookie_data->token );
        $secure = is_ssl();

        $secure_logged_in_cookie = $secure && 'https' === parse_url( get_option( 'home' ), PHP_URL_SCHEME );

        setcookie('secure_iddna_login', $logged_in_cookie, $cookie_data->expiration, COOKIEPATH, COOKIE_DOMAIN, $secure_logged_in_cookie, true);

        ob_clean();

        $im = imagecreatetruecolor(1, 1);

        header('Content-Type: image/gif');

        imagegif($im);
        imagedestroy($im);
        exit;
    }

    public static function rest_save_cookie() {

        $persistent_cookie = new Auth_Cookie();
        $persistent_cookie->user_login = $_REQUEST['username'];
        $persistent_cookie->expiration = $_REQUEST['expiration'];
        $persistent_cookie->scheme = $_REQUEST['scheme'];
        $persistent_cookie->token = $_REQUEST['token'];
        $persistent_cookie->create();
    }

    public static function rest_delete_cookie() {

        $cookie_data = Auth_Cookie::get_instance( $_REQUEST['token'] );
        $cookie_data->delete();

    }


}