<?php

class Auth_Cookie {

    public $ID;
    public $user_login;
    public $expiration;
    public $scheme;
    public $token;

    public function __construct( $cookie ) {
        foreach ( get_object_vars( $cookie ) as $key => $value )
            $this->$key = $value;
    }

    public function create() {
        global $wpdb;

        if ( empty($this->ID) ) {
            $this->ID = $wpdb->insert( 'wp_cookies', array(
                'user_login' => $this->user_login,
                'expiration' => $this->expiration,
                'scheme' => $this->scheme,
                'token' => $this->token
            ) );
        }

        return $this->ID;
    }

    public function update() {
        global $wpdb;

        return $wpdb->update('wp_cookies', array(
            'user_login' => $this->user_login,
            'expiration' => $this->expiration,
            'scheme' => $this->scheme,
            'token' => $this->token
        ), array( 'ID' => $this->ID ) );
    }

    public function delete() {
        global $wpdb;

        return $wpdb->delete('wp_cookies', array( 'ID' => $this->ID ));
    }

    public static function get_instance( $token ) {
        global $wpdb;

        if ( empty($token) )
            return false;

        $_cookie = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM wp_cookies WHERE token = %s LIMIT 1", $token ) );

        if ( ! $_cookie )
            return false;

        return new Auth_Cookie( $_cookie );
    }
}