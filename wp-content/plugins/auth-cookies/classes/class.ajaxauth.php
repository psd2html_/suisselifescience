<?php


class AjaxAuth {

    public static function add_hooks() {
        add_action("wp_ajax_loginin_user", array( __CLASS__, "custom_loginin_user") );
        add_action("wp_ajax_nopriv_loginin_user", array( __CLASS__, "custom_loginin_user") );

        add_action("wp_ajax_logout_user", array( __CLASS__, "custom_logout_user") );
        add_action("wp_ajax_nopriv_logout_user", array( __CLASS__, "custom_logout_user") );

        add_action("wp_ajax_registration_user", array( __CLASS__, "registration_user") );
        add_action("wp_ajax_nopriv_registration_user", array( __CLASS__, "registration_user") );

    }

    public static function custom_loginin_user() {


       // $headers = array (
       //     'Authorization' => 'Basic ' . base64_encode( 'admin' . ':' . '12345' ),
       // );

        $url = get_option('secure_iddna_url') . "/wp-json2/wp/v2/auth/login";//rest_url( 'wp/v2/posts/1' );

        $data = array(
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'host' => $_SERVER['HTTP_HOST']
        );

        $error = new WP_Error();

        foreach ( $data as $field_name => $field_value ) {
            if ( empty($field_value) )
                $error->add('error_' . $field_name, __("<strong>ERROR</strong>: The $field_name field is empty."));
        }

        if( ! empty( $error->errors) ) {
            wp_send_json(array(
                    'errors' => $error->errors)
            );
        }

        $response = wp_remote_post( $url, array (
            'method'  => 'POST',
            //'headers' => $headers,
            'body'    =>  $data,
          //  'cookies' => $_COOKIE,
        ) );

        $cookies = $response['cookies'];
        foreach( $cookies as $cookie ) {
            setcookie( $cookie->name, $cookie->value, $cookie->expires, $cookie->path, COOKIE_DOMAIN, false, true);
        }

        if ( isset( $response['body']) ) {

            $_response_data = json_decode( $response['body'] );

            if ( isset ( $_response_data->errors ) ) {

                foreach ( $_response_data->errors as $error_name => $error_description ) {
                    $error->add( $error_name, $error_description[0] );
                }

                wp_send_json( array (
                        'errors' => $error->errors
                    )
                );
            }

            $cookie_data = $_response_data;

            $logged_in_cookie = Auth::wp_generate_auth_cookie( $data['username'], $cookie_data->expiration, 'logged_in', $cookie_data->token );

            $secure = is_ssl();

            $secure_logged_in_cookie = $secure && 'https' === parse_url( get_option( 'home' ), PHP_URL_SCHEME );

            setcookie('secure_iddna_login', $logged_in_cookie, $cookie_data->expiration, COOKIEPATH, COOKIE_DOMAIN, $secure_logged_in_cookie, true);

            $persistent_cookie = new Auth_Cookie();
            $persistent_cookie->user_login = $data['username'];
            $persistent_cookie->expiration = $cookie_data->expiration;
            $persistent_cookie->scheme = $cookie_data->scheme;
            $persistent_cookie->token = $cookie_data->token;
            $persistent_cookie->create();


            $hosts = array();
            if ( isset($cookie_data->hosts) ) {
                foreach ( $cookie_data->hosts as $host ) {
                    $hosts[] = array(
                        'host' => $host,
                        'username' => $data['username'],
                        'token' => $cookie_data->token
                    );
                }

                echo json_encode( array(
                    'hosts' => $hosts,
                    'redirect_to' => get_option('secure_iddna_redirect_url')
                ) );
            }

            exit;
        }

        exit;
    }


    public static function custom_logout_user() {

      //  $headers = array (
      //      'Authorization' => 'Basic ' . base64_encode( 'admin' . ':' . '12345' ),
      //  );

        $url = get_option('secure_iddna_url') . "/wp-json2/wp/v2/auth/logout";


        $cookie_elements = wp_parse_auth_cookie( $_COOKIE['secure_iddna_login'], 'logged_in' );

        $data = array(
            'username' => $cookie_elements['username'],
            'token' => $cookie_elements['token'],
            'host' => $_SERVER['HTTP_HOST']
        );

        $auth_cookie = Auth_Cookie::get_instance( $cookie_elements['token'] );
        $auth_cookie->delete();

        $logged_in_name =  'wordpress_logged_in_' . md5( get_option('secure_iddna_url') );
        $woocommerce_session_name = 'wp_woocommerce_session_' . md5( get_option('secure_iddna_url') );

        setcookie( 'secure_iddna_login',   ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );
        setcookie( 'woocommerce_cart_hash', ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);
        setcookie( 'woocommerce_items_in_cart', ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);
        setcookie( $logged_in_name,   ' ', time() - YEAR_IN_SECONDS, COOKIEPATH,          COOKIE_DOMAIN );
        setcookie( $logged_in_name,   ' ', time() - YEAR_IN_SECONDS, SITECOOKIEPATH,      COOKIE_DOMAIN );
        setcookie( $woocommerce_session_name,   ' ', time() - YEAR_IN_SECONDS, SITECOOKIEPATH,      COOKIE_DOMAIN );

        $response = wp_remote_post( $url, array (
            'method'  => 'POST',
            //'headers' => $headers,
            'body'    =>  $data
        ) );

        echo $response['body'];

        exit;
    }

    public static function registration_user() {

        if (
            ! isset( $_POST['cross-domain-register-nonce'] )
            || ! wp_verify_nonce( $_POST['cross-domain-register-nonce'], 'registration_user' )
        ) {
            wp_send_json( 'Sorry, your nonce did not verify.' );
        }

        $data = array(
            'username'      => $_POST['username'],
            'password'      => $_POST['password'],
            'email'         => $_POST['email'],
            'host'          => $_SERVER['HTTP_HOST'],
            'zip'           => $_POST['zip_code'],
            'first_name'    => $_POST['first_name'],
            'last_name'     => $_POST['last_name'],
            'country_code'  => Seven_Geolocation::geolocate_ip( '', true, true )
        );

        $buy = false;

        if( isset($_REQUEST['submit_and_buy']) && isset($_POST['product_id'])) {
            $data['product_id'] = $_POST['product_id'];
            $buy = true;
        }

        $error = new WP_Error();


        /*
        foreach ( $data as $field_name => $field_value ) {
            if ( empty($field_value) )
                $error->add('error_' . $field_name, __("<strong>ERROR</strong>: The $field_name field is empty."));
        }
        */

        if( ! empty( $error->errors) ) {
            wp_send_json(array(
                    'errors' => $error->errors)
            );
        }

        $headers = array (
            'Authorization' => 'Basic ' . base64_encode( get_option('secure_iddna_user_login') . ':' . get_option('secure_iddna_user_pass') ),
        );

        $response = wp_remote_post(
            get_option('secure_iddna_url') . "/wp-json2/wp/v2/auth/registration",
            array (
                'method'  => 'POST',
                'headers' => $headers,
                'body'    =>  $data,
                //  'cookies' => $_COOKIE,
            )
        );

        //var_dump($response);

        self::handle_response( $response, $data, $buy );
        //wp_send_json( $response );
    }

    public static function handle_response( $response, $data, $buy ) {

        //var_dump( json_decode($response['body']) );

        $error = new WP_Error();

        $cookies = $response['cookies'];
        foreach( $cookies as $cookie ) {
            setcookie( $cookie->name, $cookie->value, $cookie->expires, $cookie->path, COOKIE_DOMAIN, false, true);
        }

        if ( isset( $response['body']) ) {
            $_response_data = json_decode( $response['body'] );

            if ( isset ( $_response_data->errors ) ) {

                foreach ( $_response_data->errors as $error_name => $error_description ) {
                    $error->add( $error_name, $error_description[0] );
                }

                wp_send_json( array (
                        'errors' => $error->errors
                    )
                );
            }

            //var_dump($_response_data);

            $cookie_data = $_response_data;

            $cookies = $response['cookies'];

            $woocommerce_items_in_cart = 0;

            if ( ! empty ( $cookies ) ) {
                foreach( $cookies as $cookie ) {
                    if ('woocommerce_items_in_cart' == $cookie->name) {
                        $woocommerce_items_in_cart = $cookie->value;
                    }
                    setcookie( $cookie->name, $cookie->value, $cookie->expires, $cookie->path, COOKIE_DOMAIN, false, true);
                }
            }

            $logged_in_cookie = Auth::wp_generate_auth_cookie( $cookie_data->username, $cookie_data->expiration, 'logged_in', $cookie_data->token );

            $secure = is_ssl();

            $secure_logged_in_cookie = $secure && 'https' === parse_url( get_option( 'home' ), PHP_URL_SCHEME );

            setcookie('secure_iddna_login', $logged_in_cookie, $cookie_data->expiration, COOKIEPATH, COOKIE_DOMAIN, $secure_logged_in_cookie, true);

            $persistent_cookie = new Auth_Cookie();
            $persistent_cookie->user_login = $cookie_data->username;
            $persistent_cookie->expiration = $cookie_data->expiration;
            $persistent_cookie->scheme = $cookie_data->scheme;
            $persistent_cookie->token = $cookie_data->token;
            $persistent_cookie->create();

            $cookie_data->hosts = json_decode(json_encode($cookie_data->hosts),true);

            //var_dump( $cookie_data->hosts);

            $hosts = array();
            if ( isset($cookie_data->hosts) ) {
                foreach ( $cookie_data->hosts as $host ) {
                    $hosts[] = array(
                        'host' => $host,
                        'username' => $cookie_data->username,
                        'token' => $cookie_data->token
                    );
                }

                $response = [
                    'hosts' => $hosts,
                ];

                if ( $buy ) {
                    $response['redirect_to'] = get_option('woo_secure_iddna_cart_url');
                } else {
                    $response['redirect_to'] = get_option('secure_iddna_redirect_url');
                }

                echo json_encode( $response );
            }

            exit;
        }
    }
}
