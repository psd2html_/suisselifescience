<?php

class Auth {

    public static function add_hooks() {
        add_filter( 'determine_current_user', array( __CLASS__, 'validate_logged_in_cookie'), 25 );
    }

    public static function validate_logged_in_cookie( $user ) {

        global $is_secure_user_login;

        if ( isset($_COOKIE['secure_iddna_login'])) {
            $is_secure_user_login = self::wp_validate_auth_cookie($_COOKIE['secure_iddna_login'], 'logged_in');
        }
        return $user;
    }

    public static function wp_validate_auth_cookie($cookie = '', $scheme = 'auth' ) {
        if ( ! $cookie_elements = wp_parse_auth_cookie($cookie, $scheme) ) {
            /**
             * Fires if an authentication cookie is malformed.
             *
             * @since 2.7.0
             *
             * @param string $cookie Malformed auth cookie.
             * @param string $scheme Authentication scheme. Values include 'auth', 'secure_auth',
             *                       or 'logged_in'.
             */
            do_action( 'auth_cookie_malformed', $cookie, $scheme );
            return false;
        }

        $scheme = $cookie_elements['scheme'];
        $username = $cookie_elements['username'];
        $hmac = $cookie_elements['hmac'];
        $token = $cookie_elements['token'];
        $expired = $expiration = $cookie_elements['expiration'];

        // Allow a grace period for POST and AJAX requests
        if ( defined('DOING_AJAX') || 'POST' == $_SERVER['REQUEST_METHOD'] ) {
            $expired += HOUR_IN_SECONDS;
        }

        // Quick check to see if an honest cookie has expired
        if ( $expired < time() ) {
            /**
             * Fires once an authentication cookie has expired.
             *
             * @since 2.7.0
             *
             * @param array $cookie_elements An array of data for the authentication cookie.
             */
            do_action( 'auth_cookie_expired', $cookie_elements );
            return false;
        }

        if ( empty($username) ) {
            /**
             * Fires if a bad username is entered in the user authentication process.
             *
             * @since 2.7.0
             *
             * @param array $cookie_elements An array of data for the authentication cookie.
             */
            do_action( 'auth_cookie_bad_username', $cookie_elements );
            return false;
        }

        //$pass_frag = substr($user->user_pass, 8, 4);

        $key = wp_hash( $username . '|' . $expiration . '|' . $token, $scheme );

        // If ext/hash is not present, compat.php's hash_hmac() does not support sha256.
        $algo = function_exists( 'hash' ) ? 'sha256' : 'sha1';
        $hash = hash_hmac( $algo, $username . '|' . $expiration . '|' . $token, $key );

        if ( ! hash_equals( $hash, $hmac ) ) {
            /**
             * Fires if a bad authentication cookie hash is encountered.
             *
             * @since 2.7.0
             *
             * @param array $cookie_elements An array of data for the authentication cookie.
             */
            do_action( 'auth_cookie_bad_hash', $cookie_elements );
            return false;
        }

        $manager = Auth_Cookie::get_instance( $token );
        if ( ! $manager ) {
            do_action( 'auth_cookie_bad_session_token', $cookie_elements );
            return false;
        }

        // AJAX/POST grace period set above
        if ( $expiration < time() ) {
            $GLOBALS['login_grace_period'] = 1;
        }

        /**
         * Fires once an authentication cookie has been validated.
         *
         * @since 2.7.0
         *
         * @param array   $cookie_elements An array of data for the authentication cookie.
         * @param WP_User $user            User object.
         */
        do_action( 'auth_cookie_valid', $cookie_elements, $username );

        return true;
    }

    public static function wp_generate_auth_cookie( $username, $expiration, $scheme = 'auth', $token = '' ) {

        $key = wp_hash( $username . '|' . $expiration . '|' . $token, $scheme );

        // If ext/hash is not present, compat.php's hash_hmac() does not support sha256.
        $algo = function_exists( 'hash' ) ? 'sha256' : 'sha1';
        $hash = hash_hmac( $algo, $username. '|' . $expiration . '|' . $token, $key );

        $cookie = $username . '|' . $expiration . '|' . $token . '|' . $hash;

        /**
         * Filter the authentication cookie.
         *
         * @since 2.5.0
         *
         * @param string $cookie     Authentication cookie.
         * @param int    $user_id    User ID.
         * @param int    $expiration Authentication cookie expiration in seconds.
         * @param string $scheme     Cookie scheme used. Accepts 'auth', 'secure_auth', or 'logged_in'.
         * @param string $token      User's session token used.
         */
        return apply_filters( 'auth_cookie', $cookie, $username, $expiration, $scheme, $token );
    }
}