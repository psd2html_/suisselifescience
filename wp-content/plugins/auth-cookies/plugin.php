<?php
/*
 * Plugin Name: Auth cookies
 *
 */

require_once( dirname( __FILE__ ) . '/classes/class.auth-cookie.php');
require_once( dirname( __FILE__ ) . '/classes/class.auth.php');
require_once( dirname( __FILE__ ) . '/classes/class.ajaxauth.php');
require_once( dirname( __FILE__ ) . '/classes/class.user-authentication.php');

Auth::add_hooks();
UserAuthentication::add_hooks();

add_action( 'init', 'my_init_ajax_auth' );
function my_init_ajax_auth() {
    AjaxAuth::add_hooks();
}

function seven_cross_domain_menu() {
    add_menu_page(
        __('Cross Domain Auth', 'seven'),
        __('Cross Domain Auth', 'seven'),
        'manage_options',
        'cross_domain',
        'seven_cross_domain_settings_page',
        'dashicons-share-alt2'
    );
}
add_action('admin_menu', 'seven_cross_domain_menu');

function seven_cross_domain_settings_page() {

    $options = array(
        'secure_iddna_url' => '',
        'secure_iddna_user_login' => '',
        'secure_iddna_user_pass' => '',
        "secure_iddna_redirect_url" => '',
        'sls_url' => ''
    );

    $updated = false;

    foreach($options as $key => $option ) {
        $value = isset($_POST[ $key ]) ? $_POST[ $key ] : '';

        if( empty( $value ) ) {
            $options[ $key ] = get_option( $key, $option);
        } else {
            $option = get_option( $key, $options[ $key ]);
            if ( $option != $value ) {
                $options[ $key ] = $value;
                update_option( $key, $value );
            } else {
                $options[ $key ] = $option;
            }

            $updated = true;
        }
    }

    ?>

    <div class="wrap">

        <div id="header">
            <h1><?php _e('Cross Domain Auth', 'seven'); ?></h1>
        </div>

        <?php
        if ( $updated ) {
            add_settings_error('general', 'settings_updated', __('Settings saved.'), 'updated');
        }
        settings_errors();
        ?>

        <form name="social-media-setting-form" method="post" action="">

            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row"><label for="secure_iddna_url"><?php _e('secure_iddna_url', 'seven'); ?></label></th>
                    <td><input required name="secure_iddna_url" type="text" id="secure_iddna_url" value="<?php echo $options['secure_iddna_url']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="secure_iddna_user_login"><?php _e('secure_iddna_user_login', 'seven'); ?></label></th>
                    <td><input required name="secure_iddna_user_login" type="text" id="secure_iddna_user_login" value="<?php echo $options['secure_iddna_user_login']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="secure_iddna_user_pass"><?php _e('secure_iddna_user_pass', 'seven'); ?></label></th>
                    <td><input required name="secure_iddna_user_pass" type="text" id="secure_iddna_user_pass" value="<?php echo $options['secure_iddna_user_pass']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="secure_iddna_redirect_url"><?php _e('secure_iddna_redirect_url', 'seven'); ?></label></th>
                    <td><input required name="secure_iddna_redirect_url" type="text" id="secure_iddna_redirect_url" value="<?php echo $options['secure_iddna_redirect_url']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="sls_url"><?php _e('sls_url', 'seven'); ?></label></th>
                    <td><input required name="sls_url" type="text" id="sls_url" value="<?php echo $options['sls_url']; ?>" class="regular-text"></td>
                </tr>
                </tbody>
            </table>

            <?php submit_button(); ?>
        </form>

    </div>

    <?php
}