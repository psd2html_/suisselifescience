<?php

class RegisterSecureUsers
{
    private static $instance;

    protected $fields_map = [
        'First_name' => 'first_name',
        'First name' => 'first_name',
        'Last_name' => 'last_name',
        'Last name' => 'last_name',
        'Email' => 'email',
        'Phone' => 'phone',
        'Name_of_practice/company' => 'company_name',
        'Name of practice/company' => 'company_name',
        'Company_name' => 'company_name',
        'Company name' => 'company_name',
        'Street_address' => 'street_address',
        'Street address' => 'street_address',
        'City' => 'city',
        'Country' => 'country',
        'State' => 'state',
        'ZIP/postal_code' => 'zip',
        'ZIP/postal code' => 'zip',
        'Website' => 'website',
        'Distribution_channels' => 'distribution_channels',
        'Distribution channels' => 'distribution_channels',
    ];

    protected $revers_fields_map = [];

    private function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'registerScripts']);

        add_action('wp_ajax_register_secure_user', [$this, 'register_secure_user']);
        add_action('wp_ajax_nopriv_register_secure_user', [$this, 'register_secure_user']);
    }

    protected function __clone()
    {
        //
    }

    static public function make()
    {
        self::instance();
    }

    static public function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function registerScripts()
    {
        wp_register_script('rsu-script', RSU_URL.'res/scripts/script.js', ['jquery'], RSU_VERSION);
        wp_enqueue_script('rsu-script');
    }

    public function getApiUrl()
    {
        $api_url = get_option('secure_iddna_url');

        if (substr($api_url, -1) !== '/') {
            $api_url .= '/';
        }

        return $api_url.'wp-json2/wp/v2/auth/registration/secure';
    }

    public function getApiLogin()
    {
        return get_option('secure_iddna_user_login');
    }

    public function getApiPassword()
    {
        return get_option('secure_iddna_user_pass');
    }

    public function register_secure_user()
    {
        $post = $_POST;

        // Verify post data
        if (
            !isset($post['cross-domain-register-nonce'])
            || !wp_verify_nonce($post['cross-domain-register-nonce'], 'registration_secure_user')
        ) {
            wp_send_json(['error' => 'Sorry, your nonce did not verify.']);
        }

        // Convert post data
        $converted_post_data = $this->convertKeys($post);

        // Format url for post
        $post_url = $this->getApiUrl();

        // // Format headers for post
        $api_login = $this->getApiLogin();
        $api_pass = $this->getApiPassword();
        $headers = ['Authorization' => 'Basic '.base64_encode($api_login.':'.$api_pass)];

        // Format post data
        $post_data = [
            'method' => 'POST',
            'headers' => $headers,
            'body' => $converted_post_data,
        ];

        // Get results from API
        $response = wp_remote_post($post_url, $post_data);

        try {
            $response = json_decode($response['body'], true);

            if (is_null($response)) {
                $response = ['errors' => []];
            }
        } catch (Exception $exception) {
            $response = ['errors' => []];
        }

        if (isset($response['errors'])) {
            $response['errors'] = $this->reversKeys($response['errors']);
        } elseif (isset($response['user_id'])) {
            // Send data to API v3
            $old_data = [];
            $old_data['form_data'] = $post_data;
            $old_data['user_id'] = $response['user_id'];
            wp_remote_post('http://staging.idna.works/wp-json/v3/prospect_signup', $old_data);
        }

        wp_send_json($response);
    }

    protected function convertKeys($data)
    {
        return $this->mapKeys($data, $this->fields_map);
    }

    protected function reversKeys($data)
    {
        return $this->mapKeys($data, $this->revers_fields_map);
    }

    protected function mapKeys($data, $map)
    {
        $this->revers_fields_map = [];

        $converted_data = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                continue;
            }

            if (isset($map[$key])) {
                $new_key = $map[$key];
            } else {
                $new_key = $key;
            }

            $converted_data[$new_key] = $value;

            $this->revers_fields_map[$value] = $new_key;
        }

        return $converted_data;
    }
}
