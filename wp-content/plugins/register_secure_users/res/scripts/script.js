jQuery(function($) {

  $('#ajax_register_provider, #ajax_register_distributor, #ajax_register_distributor_inq').on('submit', function(e) {
    e.preventDefault();

    var form = $(this);
    var data = form.serializeArray();
    var notificationBlock = form.find('.notification');
    console.log('notificationBlock', notificationBlock);

    data.push({ name: 'action', value: 'register_secure_user' });

    function notification(action, message){
      message = message || '';

      notificationBlock
        .removeClass('alert-success')
        .removeClass('alert-warning')
        .removeClass('alert-danger')
        .removeClass('alert-danger')
        .removeClass('hidden')
        .html('');

      if (action === 'start') {
        notificationBlock.addClass('alert-warning')
          .html('<i class="fa fa-spinner fa-spin"></i> Loading ... ' + message);
      } else if (action === 'success') {
        notificationBlock.addClass('alert-success')
          .html('<strong>Success!</strong> ' + message);
      } else if (action === 'error') {
        notificationBlock.addClass('alert-danger')
          .html('<strong>Errors!</strong> ' + message);
      } else {
        notificationBlock.addClass('hidden');
      }
    }

    console.log('sendJson: ', data);

    $.when(sendAjaxToCurrentServer($, form, true)).then(function(statusReturn) {
        console.log('Form status: ', statusReturn);
    });

    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      type: 'post',
      data: data,
      dataType: 'json',
      beforeSend: function() {
        notification('start');
        $('.my_form_input span.validation-error').remove();
      },
      success: function(json) {
        form.find("button").removeAttr("disabled");
        console.log('Returned: ', json);
        json = json || {};


        if (json.hasOwnProperty('errors')) {
          var key, error, query, input, inputBlock,
            searchFields = ['input', 'select', 'textarea'];

          for(key in json.errors) {
            error = json.errors[key];

            query = searchFields.map(function(field) {
              return field+'[name="'+key+'"]';
            }).join(', ');

            input = $(query);
            inputBlock = input.closest('.my_form_input');

            input.addClass('user-error').removeClass('user-success');
            inputBlock.append('<span class="validation-error">'+error+'</span>')
//             inputBlock.find('span.valid').remove();
          }

          notification('error');
        } else if (json.hasOwnProperty('redirect_url') && json.redirect_url) {
          // Added notification
          var message = 'Account successfully created. After a few seconds you will be taken to a page to set the password.';
          notification('success', message);

          setTimeout(function() {
            window.location.href = json.redirect_url;
          }, 2000);
        } else {
          notification('error', 'Error in API! Please contact the site administrator!');
        }

        return;
      },

      error: function(e) {
        notification('error', e.responseText);
        console.log('form', form);
        form.find("button").removeAttr("disabled");
      }
    });
  });

});
