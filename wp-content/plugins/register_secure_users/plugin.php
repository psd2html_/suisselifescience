<?php
/*
Plugin Name: Register secure users
Plugin URI:
Description:
Version: 1.0.0
Author: Taras Bilohan [Seven]
Author URI:
*/

define('RSU_PATH', dirname(__FILE__).'/');
define('RSU_URL', plugin_dir_url(__FILE__));
define('RSU_VERSION', '1.0.0');

require RSU_PATH.'src/RegisterSecureUsers.php';

\RegisterSecureUsers::make();
