jQuery(function($) {
  var copyProtectJqueryModal = $('#copy-protect-jquery-modal');
  var blockAllowed = [
    '[data-allow-copy]',
    'input',
    'textarea',
  ];

  $(document).on('copy', function(event) {
    if($(event.target).closest(blockAllowed.join(', ')).length) {
      return;
    }

    event.preventDefault();

    copyProtectJqueryModal.modal();
  });
});