<?php
/*
Plugin Name: WP-CopyProtect-jQuery
Plugin URI:
Description: This plug-in will protect your content
Version: 1.0.0
Author: Taras Bilohan [Seven]
Author URI: no url
*/

if (!defined('WP_COPY_PROTECT_JQUERY_PATH')) {
    define('WP_COPY_PROTECT_JQUERY_PATH', dirname(__FILE__));
}

if (!defined('WP_COPY_PROTECT_JQUERY_PLUGIN_PATH')) {
    define('WP_COPY_PROTECT_JQUERY_PLUGIN_PATH', plugin_dir_url(__FILE__));
}

define('WP_COPY_PROTECT_JQUERY_VERSION', '1.0.0');

function wp_copy_protect_jquery_register_scripts()
{
    wp_register_script(
        'wp-copy-protect-jquery-script',
        WP_COPY_PROTECT_JQUERY_PLUGIN_PATH.'res/scripts/script.js',
        ['jquery'],
        WP_COPY_PROTECT_JQUERY_VERSION
    );

    wp_enqueue_script('wp-copy-protect-jquery-script');
}

function wp_copy_protect_jquery_register_styles()
{
    wp_enqueue_style(
        'wp-copy-protect-jquery-style',
        WP_COPY_PROTECT_JQUERY_PLUGIN_PATH.'res/styles/style.css',
        ['bs'],
        WP_COPY_PROTECT_JQUERY_VERSION
    );
}

function wp_copy_protect()
{
    // get modals template
    load_template(WP_COPY_PROTECT_JQUERY_PATH.'/templates/modal.php');
}

add_action('wp_enqueue_scripts', 'wp_copy_protect_jquery_register_scripts');
add_action('wp_enqueue_scripts', 'wp_copy_protect_jquery_register_styles');
add_action('wp_footer', 'wp_copy_protect');
