<div class="modal fade" tabindex="-1" role="dialog" id="copy-protect-jquery-modal" data-allow-copy>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h4 class="modal-title">
                    <?php echo __('This content is copyright protected', 'wp-copy-protect-jquery') ?>
                </h4>
            </div>

            <div class="modal-body">
                <p>
                    <?php echo __('However, if you would like to share the information in this article, you may use the headline and link below', 'wp-copy-protect-jquery') ?>
                </p>

                <?php if ($title = trim(get_the_title())){ ?>
                    <p class="content-link">
                        <a href=""><?php echo $title; ?></a>
                    </p>
                <?php } elseif ($permalink = get_permalink()) { ?>
                    <p class="content-link">
                        <a href=""><?php echo $permalink; ?></a>
                    </p>
                <?php } else { ?>
                    <p class="content-link">
                        <a href=""><?php echo esc_html($_SERVER['HTTP_HOST']); ?></a>
                    </p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>