<?php
/**
 * Geolocation class
 *
 * Adapted from https://wordpress.org/plugins/woocommerce/.
 */

class Seven_Geolocation {

    private static $ip_lookup_apis = array(
        'icanhazip'         => 'http://icanhazip.com',
        'ipify'             => 'http://api.ipify.org/',
        'ipecho'            => 'http://ipecho.net/plain',
        'ident'             => 'http://ident.me',
        'whatismyipaddress' => 'http://bot.whatismyipaddress.com',
        'ip.appspot'        => 'http://ip.appspot.com'
    );

    /** @var array API endpoints for geolocating an IP address */
    private static $geoip_apis = array(
        'freegeoip'         => 'https://freegeoip.net/json/%s',
        //'telize'            => 'http://www.telize.com/geoip/%s', // not working
        //'geoip-api.meteor'  => 'http://geoip-api.meteor.com/lookup/%s', // not working
        'ip-api.com'        => 'http://ip-api.com/json'
    );



    /**
     * Get current user IP Address.
     * @return string
     */
    public static function get_ip_address() {
        if ( isset( $_SERVER['X-Real-IP'] ) ) {
            return $_SERVER['X-Real-IP'];
        } elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
            // Proxy servers can send through this header like this: X-Forwarded-For: client1, proxy1, proxy2
            // Make sure we always only send through the first IP in the list which should always be the client IP.
            return trim( current( explode( ',', $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) );
        } elseif ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return '';
    }
    /**
     * Get user IP Address using an external service.
     * This is used mainly as a fallback for users on localhost where
     * get_ip_address() will be a local IP and non-geolocatable.
     * @return string
     */
    public static function get_external_ip_address() {
        $external_ip_address = self::get_ip_address();
        if ( false === $external_ip_address || '127.0.0.1' == $external_ip_address) {
            $external_ip_address     = '0.0.0.0';
            $ip_lookup_services      = self::$ip_lookup_apis;
            $ip_lookup_services_keys = array_keys( $ip_lookup_services );
            shuffle( $ip_lookup_services_keys );
            foreach ( $ip_lookup_services_keys as $service_name ) {
                $service_endpoint = $ip_lookup_services[ $service_name ];
                $response         = wp_safe_remote_get( $service_endpoint, array( 'timeout' => 2 ) );
                if ( ! is_wp_error( $response ) && $response['body'] ) {
                    $external_ip_address = seven_clean( $response['body'] );
                    break;
                }
            }
        }
        return $external_ip_address;
    }
    /**
     * Geolocate an IP address.
     * @param  string $ip_address
     * @param  bool   $fallback If true, fallbacks to alternative IP detection (can be slower).
     * @param  bool   $api_fallback If true, uses geolocation APIs if the database file doesn't exist (can be slower).
     * @return array
     */
    public static function geolocate_ip( $ip_address = '', $fallback = true, $api_fallback = true ) {

        $country_code = '';

        // If GEOIP is enabled in CloudFlare, we can use that (Settings -> CloudFlare Settings -> Settings Overview)
        if ( ! empty( $_SERVER['HTTP_CF_IPCOUNTRY'] ) ) {
            $country_code = sanitize_text_field( strtoupper( $_SERVER['HTTP_CF_IPCOUNTRY'] ) );
        } else {
            $ip_address = $ip_address ? $ip_address : self::get_ip_address();
            if ( self::is_IPv6( $ip_address ) ) {
                $database = self::get_local_database_path( 'v6' );
            } else {
                $database = self::get_local_database_path();
            }
            if ( file_exists( $database ) ) {
                $country_code = self::geolocate_via_db( $ip_address );
            } elseif ( $api_fallback ) {
                $country_code = self::geolocate_via_api( $ip_address );
            } else {
                $country_code = '';
            }

            if ( ! $country_code && $fallback ) {
                // May be a local environment - find external IP
                return self::geolocate_ip( self::get_external_ip_address(), false, $api_fallback );
            }
        }

        return $country_code;
    }
    /**
     * Get coordinates from IP address.
     * @param  string $ip_address
     * @param  bool   $fallback If true, fallbacks to alternative IP detection (can be slower).
     * @param  bool   $api_fallback If true, uses geolocation APIs if the database file doesn't exist (can be slower).
     * @return array  Latitude & Longitude.
     */
    public static function geolocate_coordinates( $ip_address = '', $fallback = true, $api_fallback = true ) {

        $lat_lon = array();

        $ip_address = $ip_address ? $ip_address : self::get_ip_address();

        if ( $api_fallback ) {
            $lat_lon = self::geolocate_lat_lon( $ip_address );
        }
        if ( empty($lat_lon) && $fallback ) {
            // May be a local environment - find external IP
            return self::geolocate_coordinates( self::get_external_ip_address(), false, $api_fallback );
        }

        return $lat_lon;
    }
    /**
     * Path to our local db.
     * @param  string $version
     * @return string
     */
    public static function get_local_database_path( $version = 'v4' ) {
        $version    = ( 'v4' == $version ) ? '' : 'v6';
        return SEVEN_GEOLOCATION_PLUGIN_PATH . 'geoip/GeoIP' . $version . '.dat';
    }

    /**
     * Use MAXMIND GeoLite database to geolocation the user.
     * @param  string $ip_address
     * @return string
     */
    private static function geolocate_via_db( $ip_address ) {
        if ( ! class_exists( 'Seven_Geo_IP' ) ) {
            include_once( 'class-s-geo-ip.php' );
        }
        $gi = new Seven_Geo_IP();
        if ( self::is_IPv6( $ip_address ) ) {
            $database = self::get_local_database_path( 'v6' );
            $gi->geoip_open( $database, 0 );
            $country_code = $gi->geoip_country_code_by_addr_v6( $ip_address );
        } else {
            $database = self::get_local_database_path();
            $gi->geoip_open( $database, 0 );
            $country_code = $gi->geoip_country_code_by_addr( $ip_address );
        }
        $gi->geoip_close();
        return sanitize_text_field( strtoupper( $country_code ) );
    }
    /**
     * Use APIs to Geolocate the user.
     * @param  string $ip_address
     * @return string|bool
     */
    private static function geolocate_via_api( $ip_address ) {
        $country_code = get_transient( 'geoip_' . $ip_address );
        if ( false === $country_code ) {
            $geoip_services      = self::$geoip_apis;
            $geoip_services_keys = array_keys( $geoip_services );
            shuffle( $geoip_services_keys );
            foreach ( $geoip_services_keys as $service_name ) {
                $service_endpoint = $geoip_services[ $service_name ];
                $response         = wp_safe_remote_get( sprintf( $service_endpoint, $ip_address ), array( 'timeout' => 2 ) );
                if ( ! is_wp_error( $response ) && $response['body'] ) {
                    switch ( $service_name ) {
                        case 'geoip-api.meteor' :
                            $data         = json_decode( $response['body'] );
                            $country_code = isset( $data->country ) ? $data->country : '';
                            break;
                        case 'freegeoip' :
                        case 'telize' :
                            $data         = json_decode( $response['body'] );
                            $country_code = isset( $data->country_code ) ? $data->country_code : '';
                            break;
                        case 'ip-api.com' :
                            $data         = json_decode( $response['body'] );
                            $country_code = isset( $data->countryCode ) ? $data->countryCode : '';
                            break;
                        default :
                            $country_code = apply_filters( 'seven_geolocation_geoip_response_' . $service_name, '', $response['body'] );
                            break;
                    }
                    $country_code = sanitize_text_field( strtoupper( $country_code ) );
                    if ( $country_code ) {
                        break;
                    }
                }
            }
            set_transient( 'geoip_' . $ip_address, $country_code, WEEK_IN_SECONDS );
        }
        return $country_code;
    }

    /**
     * Use APIs to get coordinates the user.
     * @param  string $ip_address
     * @return string|bool
     */
    private static function geolocate_lat_lon( $ip_address ) {
        $latitude   = get_transient( 'geoip_lat_' . $ip_address );
        $longitude  = get_transient( 'geoip_lon_' . $ip_address );
        $state      = get_transient( 'geoip_state_' . $ip_address );

        if ( (empty($longitude) && empty($latitude)) || (empty($state)) ) {
            $geoip_services      = self::$geoip_apis;
            $geoip_services_keys = array_keys( $geoip_services );
            shuffle( $geoip_services_keys );
            foreach ( $geoip_services_keys as $service_name ) {
                $service_endpoint = $geoip_services[ $service_name ];
                $response         = wp_safe_remote_get( sprintf( $service_endpoint, $ip_address ), array( 'timeout' => 2 ) );
                if ( ! is_wp_error( $response ) && $response['body'] ) {
                    switch ( $service_name ) {
                        case 'geoip-api.meteor' :
                            break;
                        case 'freegeoip' :
                        case 'telize' :
                            $data       = json_decode( $response['body'] );
                            $latitude   = isset( $data->latitude ) ? $data->latitude : '';
                            $longitude  = isset( $data->longitude ) ? $data->longitude : '';
                            $state      = isset( $data->region_name ) ? $data->region_name : '';
                            break;
                        case 'ip-api.com' :
                            $data       = json_decode( $response['body'] );
                            $latitude   = isset( $data->lat ) ? $data->lat : '';
                            $longitude  = isset( $data->lon ) ? $data->lon : '';
                            $state      = isset( $data->regionName ) ? $data->regionName : '';
                            break;
                        default :
                            break;
                    }
                }
            }
            set_transient( 'geoip_lat_' . $ip_address, $latitude, WEEK_IN_SECONDS );
            set_transient( 'geoip_lon_' . $ip_address, $longitude, WEEK_IN_SECONDS );
            set_transient( 'geoip_state_' . $ip_address, $state, WEEK_IN_SECONDS );
        }
        $lat_lon = array(
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'state'     => $state,
        );
        return $lat_lon;
    }
    /**
     * Test if is IPv6.
     *
     * @since  2.4.0
     *
     * @param  string $ip_address
     * @return bool
     */
    private static function is_IPv6( $ip_address ) {
        return false !== filter_var( $ip_address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 );
    }
}

new Seven_Geolocation();
