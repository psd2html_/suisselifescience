<?php
/**
Plugin Name: Seven Geolocation
Version: 1.0
*/

define( 'SEVEN_GEOLOCATION_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

/**
 * Clean variables using sanitize_text_field. Arrays are cleaned recursively.
 * Non-scalar values are ignored.
 * @param string|array $var
 * @return string|array
 */
function seven_clean( $var ) {
    if ( is_array( $var ) ) {
        return array_map( 'seven_clean', $var );
    } else {
        return is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
    }
}

include_once 'includes/class-s-geo-ip.php';
include_once 'includes/class-s-geolocation.php';