<?php

/**
 * Plugin Name: Wocommerce REST API Client
 */

require __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/classes/class.cross-woo-cart.php';
require_once __DIR__ . '/classes/class.admin-options.php';

Cross_WC_Cart::add_hooks();
Cross_Admin_Options::add_hooks();

add_action('wp_enqueue_scripts','register_cross_woo_scripts');

function register_cross_woo_scripts(){
    wp_register_script( 'jquery.cookie', plugins_url( '/js/jquery-cookie/jquery.cookie.js', __FILE__ ), array('jquery') , '', true );
}

?>