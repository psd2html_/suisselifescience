<?php


class Cross_WC_Cart {

    public static function add_hooks() {

        add_action("wp_ajax_cross_add_to_cart", array( __CLASS__, "cross_add_to_cart") );
        add_action("wp_ajax_nopriv_cross_add_to_cart", array( __CLASS__, "cross_add_to_cart"));
    }

    public static function cross_add_to_cart() {
        $product_id        = absint( $_POST['product_id'] );
        $quantity          = absint( $_POST['quantity'] );

        try {
            //$headers = array (
            //    'Authorization' => 'Basic ' . base64_encode( 'admin' . ':' . '12345' ),
            //);

            $url = get_option('woo_secure_iddna_url') . "/wp-json2/wp/v2/cart/add_to_cart";//rest_url( 'wp/v2/posts/1' );

            $data = array(
                'product_id' => $product_id,
                'quantity' => $quantity
            );

            $_cookies = $_COOKIE;

            if (isset($cookies['PHPSESSID'])) {
                unset($cookies['PHPSESSID']);
            }

            $response = wp_remote_post($url, array(
                'method' => 'POST',
                //'headers' => $headers,
                'body' => $data,
                'cookies' => $_cookies,
                'timeout' => 45
            ));

            //var_dump($response);

            $cookies = $response['cookies'];

            $woocommerce_items_in_cart = 0;

            $woo_user_cart_session = '';
            $woo_user_cart_session_expiration = '';

            if (!empty ($cookies)) {
                foreach ($cookies as $cookie) {
                    if ('woocommerce_items_in_cart' == $cookie->name) {
                        $woocommerce_items_in_cart = $cookie->value;
                    }
                    if (strpos($cookie->name, 'wp_woocommerce_session') !== false) {
                        $session = explode("||", $cookie->value);
                        $woo_user_cart_session = $session[0];
                        $woo_user_cart_session_expiration = $session[1];
                    }
                    setcookie($cookie->name, $cookie->value, $cookie->expires, $cookie->path, COOKIE_DOMAIN, false, true);
                }
            }

            $cookies_data = self::get_session_from_cookies($cookies);

            //var_dump($cookies);

            echo json_encode(array(
                    "cart_page" => get_option('woo_secure_iddna_cart_url'),
                    "host" => get_option('woo_secure_iddna_host_name'),
                    "cookie_data" => $cookies_data,
                    "items_in_cart" => $woocommerce_items_in_cart,
                    "woo_user_cart_session" => array(
                        "host" => get_option('sls_url'),
                        'value' => $woo_user_cart_session,
                        'expires' => $woo_user_cart_session_expiration
                    )
                )
            );
        } catch ( Exception $e ) {
            var_dump( $e );
        }
        exit;
    }

    public static function get_session_from_cookies( $cookies ) {
        foreach ( $cookies as $cookie ) {
            if ( strpos($cookie->name, 'woocommerce_session_') !== false ) {
                $cookie_data = self::get_session_cookie( $cookie->value );

                return $cookie_data;
            }
        }

        return false;
    }

    public static function get_session_cookie( $woo_session_cookie ) {

        if ( empty( $woo_session_cookie ) || ! is_string( $woo_session_cookie ) ) {
            return false;
        }

        list( $customer_id, $session_expiration, $session_expiring, $cookie_hash ) = explode( '||', $woo_session_cookie );

        return array(
            'customer_id' => $customer_id,
            'session_expiration' => $session_expiration,
            'session_expiring' => $session_expiring,
            'cookie_hash' => $cookie_hash
        );
    }

}