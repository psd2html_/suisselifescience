<?php

class Cross_Admin_Options {
    
    public static function add_hooks() {

        add_action("admin_menu", array( __CLASS__, "seven_cross_woo_cart_menu") );
    
    }

    public static function seven_cross_woo_cart_menu() {
        add_menu_page(
            __('Cross Woo Cart Setting', 'seven'),
            __('Cross Woo Cart Setting', 'seven'),
            'manage_options',
            'cross_woo_cart',
            __CLASS__ . '::seven_cross_woo_cart_settings_page',
            'dashicons-cart'
        );
    }

    public static function seven_cross_woo_cart_settings_page() {

        $options = array(
            'woo_secure_iddna_url' => '',
            'woo_secure_iddna_cart_url' => '',
            'woo_secure_iddna_host_name' => '',
            'woo_secure_iddna_consumer_key' => '',
            'woo_secure_iddna_consumer_secret' => '',
        );

        $updated = false;

        foreach($options as $key => $option ) {
            $value = isset($_POST[ $key ]) ? $_POST[ $key ] : '';

            if( empty( $value ) ) {
                $options[ $key ] = get_option( $key, $option);
            } else {
                $option = get_option( $key, $options[ $key ]);
                if ( $option != $value ) {
                    $options[ $key ] = $value;
                    update_option( $key, $value );
                } else {
                    $options[ $key ] = $option;
                }

                $updated = true;
            }
        }

        ?>

        <div class="wrap">

            <div id="header">
                <h1><?php _e('Cross Woo Cart Setting', 'seven'); ?></h1>
            </div>

            <?php
            if ( $updated ) {
                add_settings_error('general', 'settings_updated', __('Settings saved.'), 'updated');
            }
            settings_errors();
            ?>

            <form name="cross-woo-cart-setting-form" method="post" action="">

                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row"><label for="woo_secure_iddna_url"><?php _e('woo_secure_iddna_url', 'seven'); ?></label></th>
                        <td><input required name="woo_secure_iddna_url" type="text" id="woo_secure_iddna_url" value="<?php echo $options['woo_secure_iddna_url']; ?>" class="regular-text"></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="woo_secure_iddna_cart_url"><?php _e('woo_secure_iddna_cart_url', 'seven'); ?></label></th>
                        <td><input required name="woo_secure_iddna_cart_url" type="text" id="woo_secure_iddna_cart_url" value="<?php echo $options['woo_secure_iddna_cart_url']; ?>" class="regular-text"></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="woo_secure_iddna_host_name"><?php _e('woo_secure_iddna_host_name', 'seven'); ?></label></th>
                        <td><input required name="woo_secure_iddna_host_name" type="text" id="woo_secure_iddna_host_name" value="<?php echo $options['woo_secure_iddna_host_name']; ?>" class="regular-text"></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="woo_secure_iddna_consumer_key"><?php _e('woo_secure_iddna_consumer_key', 'seven'); ?></label></th>
                        <td><input required name="woo_secure_iddna_consumer_key" type="text" id="woo_secure_iddna_consumer_key" value="<?php echo $options['woo_secure_iddna_consumer_key']; ?>" class="regular-text"></td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="woo_secure_iddna_consumer_secret"><?php _e('woo_secure_iddna_consumer_secret', 'seven'); ?></label></th>
                        <td><input required name="woo_secure_iddna_consumer_secret" type="text" id="woo_secure_iddna_consumer_secret" value="<?php echo $options['woo_secure_iddna_consumer_secret']; ?>" class="regular-text"></td>
                    </tr>
                    </tbody>
                </table>

                <?php submit_button(); ?>
            </form>

        </div>

        <?php
    }
}