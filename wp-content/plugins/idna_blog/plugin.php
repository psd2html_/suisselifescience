<?php
/**
 * Plugin Name: IDNA posts widgets 
 * Description: Get posts from IDNA web-site and add widget to display it on pages.
 * Author: Konstantin Slupko [Seven]
 * Author URI: https://github.com/linux0uid
 * Version: 0.3
 * Plugin Slug: idna_blog
 */

if (!defined('BASE_URI_API')) {
    $base_uri = get_option('api_iddna_url');
    define('BASE_URI_API', $base_uri);
}

function get_idna_posts($count = 1)
{
    $slug = 'posts?per_page=' . $count;
    $result = get_idna_request($slug);

    return $result;
}

function get_idna_categories($post_id)
{
    $slug = 'categories?post=' . $post_id;
    $result = get_idna_request($slug);

    return $result;
}

function get_idna_media($media_id)
{
    $slug = 'media/' . $media_id;
    $result = get_idna_request($slug);

    return $result;
}

function get_idna_request($slug)
{
    $args = array(
        'timeout' => 120,
        'httpversion' => '1.1',
    );

    $response = wp_remote_get(BASE_URI_API . '/wp-json/wp/v2/' . $slug, $args);
    if (is_wp_error($response)) return false;
    $body = (string) $response['body'];
    $result = json_decode($body);

    return $result;
}

function get_idna_last_post()
{
    $args = array(
        'timeout' => 120,
        'httpversion' => '1.1',
    );

    $response = wp_remote_get(BASE_URI_API . '/wp-json/get-posts/v1/type/blog_post/count/1', $args);
    if (is_wp_error($response)) return false;
    if (!isset($response['body'])) return false;
    $bodyObj = json_decode($response['body']);
    $lastPost = array_shift(array_values($bodyObj));

    $uri = ($lastPost->uri) ? $lastPost->uri : '';

    return $uri;
}

function get_idna_blog_last_post( $atts ){
    if (function_exists( 'get_idna_last_post' )) return get_idna_last_post();
	return false;
}
add_shortcode( 'idna_blog', 'get_idna_blog_last_post' );
