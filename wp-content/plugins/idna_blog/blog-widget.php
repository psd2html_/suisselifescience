<?php

add_action("widgets_init", function () {
    register_widget("Blog_Widget");
});

class Blog_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct("blog_widget", "Blog IDNA widget",
            array("description" => "Widget display last post from IDNA blog"));
    }

    public function form($instance)
    {
        $title = "";

        if (!empty($instance)) {
            $title = $instance["title"];
        }

        $tableId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");

        echo '<label for="' . $tableId . '">Title</label><br>';
        echo '<input id="' . $tableId . '" type="text" name="' .
        $tableName . '" value="' . $title . '"><br>';
    }
    
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);

        return $values;
    }

    public function widget($args, $instance) {
        $title = $instance["title"];
        $post = get_posts_idna()[0];        
    }
}
