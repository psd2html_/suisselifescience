<?php

/**
 * Class Message
 * @author linux0uid
 */
class EditMessage
{
    
    /**
     * Body of message
     *
     * @var mixed
     */
    private $body;

    /**
     * replacement
     *
     * @var mixed
     */
    private $replacement;

    /**
     * subject
     *
     * @var mixed
     */
    private $subject;

    /**
     * headers
     *
     * @var mixed
     */
    private $headers;

    /**
     * data
     *
     * @var array
     */
    private $data = array();


    /**
     * Magic call methods
     *
     * @param mixed $name
     * @param mixed $value
     *
     * @return this
     */
    public function __call($method, $args)
    {
        if (0 === strpos($method, 'add')) {
            foreach ($args as $key=>$arg) {
                $this->data[substr($method, 3)][$key][] = $arg;
            }
        }

        return $this;
    }

    /**
     * replace
     *
     * @param mixed $subject
     *
     * @return string
     */
    private function replace($subject, $count = -1)
    {
        if (!empty($replaces = $this->data['Replace'])) {
            if (++$count < count($replaces[0])) {
                $search = $replaces[0][$count];
                $replace = $replaces[1][$count];
                $result = str_replace($search, $replace, $subject);
                $subject = $this->replace($result, $count);
            }
        }

        return $subject;
    }

    /**
     * Clearing all not used shortcodes
     *
     * @param mixed $subject
     *
     * @return string
     */
    private function clearShortcodes($subject)
    {
        $pattern = '/\{.*\}/';
        $result = preg_replace($pattern, '', $subject);

        return $result;
    }

    /**
     * Body setter
     *
     * @param mixed $val
     *
     * @return this
     */
    public function setBody($val)
    {
        $this->body = (string)$val;
        return $this;
    }
    
    /**
     * Subject setter
     *
     * @param mixed $val
     *
     * @return this
     */
    public function setSubject($val)
    {
        $this->subject = (string)$val;
        return $this;
    }

    /**
     * Headers setter
     *
     * @param mixed $val
     *
     * @return this
     */
    public function setHeaders($val)
    {
        $this->headers = (string)$val;
        return $this;
    }

    /**
     * Sending email
     *
     * @param mixed $email
     *
     * @return this
     */
    public function send($email)
    {
        $subject = $this->replace($this->subject);
        $subject = $this->clearShortcodes($subject);
        $body = $this->replace($this->body);
        $body = $this->clearShortcodes($body);

        $bodyHeader = file_get_contents(PLUGIN_CUSTOM_SEND_EMAIL_PATH . 'html' . DIRECTORY_SEPARATOR . 'mail_header.php');
        $bodyFoot = file_get_contents(PLUGIN_CUSTOM_SEND_EMAIL_PATH . 'html' . DIRECTORY_SEPARATOR . 'mail_foot.php');
        $body = $bodyHeader . $body . $bodyFoot;

        if (!wp_mail($email, $subject, $body, $this->headers)) {
            throw new Exception("We can't deliver email");
        }

        return $this;
    }

}
