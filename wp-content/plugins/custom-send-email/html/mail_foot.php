</div>
        <!--</section>--><footer style="margin-top: 50px; clear: both;">
        <div class="footerLinks" style="font-size: 13px; clear: both; margin: 0 auto; width: 47%; text-align: center;"><a style="color: #169ec8; text-decoration: none;" href="https://secure.idna.works" target="_blank">Account</a> <img style="margin: 1px 12px;" src="https://secure.idna.works/wp-content/themes/5.5.2/image/email/icon.png" width="4" /> <a style="color: #169ec8; text-decoration: none;" href="http://idna.works/iddna-anti-aging" target="_blank">Blog</a> <img style="margin: 1px 12px;" src="https://secure.idna.works/wp-content/themes/5.5.2/image/email/icon.png" width="4" /> <a style="color: #169ec8; text-decoration: none;" href="http://idna.works/anti-aging-from-DNA/luxury-anti-aging-contact-us" target="_blank">Support</a></div>
        <div style="text-align: center; clear: both; padding: 20px 0; font-size: 13px;">
          <p style="margin: 0;">© 2016 Suisse Life Science S.A. via Zorzi 4, 6900 Lugano, Switzerland</p>
          <p style="margin: 0;"><a style="text-decoration: none; color: black;">All Rights Reserved</a>/<a style="text-decoration: none; color: black;" href="http://suisselifescience.com/terms-and-conditions/" target="_blank">Terms and Conditions</a>, <a style="text-decoration: none; color: black;" href="http://suisselifescience.com/privacy-policy/" target="_blank">Privacy Policy</a></p>
        </div>
        <div style="font-size: 10px; text-align: center;">
          <p>To ensure you receive your iDDNA® emails, please add noreply@suisselifescience.com to your address book.</p>
          <p>This is an iDDNA® transactional email regarding your iDDNA® program membership. Even if you are opted out of iDDNA® marketing, you may still receive iDDNA® transactional emails on occasion regarding updates or changes to the iDDNA® program.</p>
        </div>
        </footer></div>
      <div style="clear: both; text-align: center; margin-top: 50px;"><img src="https://secure.idna.works/wp-content/themes/5.5.2/image/email/sussie.png" alt="" /></div>
    </div>
    <!--body-->
    <p>&nbsp;</p>
  </body>
</html>
