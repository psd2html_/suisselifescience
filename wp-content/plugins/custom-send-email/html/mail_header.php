<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>EMAIL TEMPLATE</title>
  </head>
  <body style="background-color: #f5f5f5;" bgcolor=”#f5f5f5”>
    <div class="body" style="background-color: #f5f5f5; padding: 50px 0;">
      <div class="main" style="box-shadow: 5px 5px 5px #d4d4d4; font-family: Verdana; color: black; background-color: white; margin: 0 auto; max-width: 700px; padding: 50px 35px; min-height: 100px;"><header style="clear: both;">
        <div class="logo" style="float: left;"><img src="https://secure.idna.works/wp-content/themes/5.5.2/image/email/logo.png" alt="" /></div>
        <div class="getApp" style="float: right; min-width: 130px; padding-right: 5px; padding-top: 34px;">
          <div class="icon" style="float: left; padding-right: 5px;"><img src="https://secure.idna.works/wp-content/themes/5.5.2/image/email/getapp.png" alt="" /></div>
          <div class="AppLink" style="padding: 6px 0;">
            <h5 style="color: #bbbdc0; margin: 0; font-size: 13px; font-weight: 400;">Get the App</h5>
            <h6 style="color: #bbbdc0; margin: 0; font-size: 9.4px;"><a style="color: #bbbdc0; margin: 0; font-weight: bold; text-decoration: none;" href="https://itunes.apple.com/us/app/iddna-anti-aging/id1081741460?mt=8" target="_blank">IOS</a> | <a style="color: #bbbdc0; margin: 0; font-weight: bold; text-decoration: none;" href="https://play.google.com/store/apps/details?id=com.susilifescience.iddna&amp;hl=en" target="_blank">ANDROID</a></h6>
          </div>
        </div>
        </header><!--<section style="clear: both; padding: 60px 0; margin: 0 39px; border-bottom: 1px solid black;"><h1 style="/*font-size: 28px; */color: #199ec8; font-size: 31px; font-weight: 100;">YOUR PASSWORD HAS BEEN CHANGED</h1>
        -->
        <div style="clear: both; color: #464646; padding: 60px 0; margin: 45px 39px; border-bottom: 1px solid black;">
