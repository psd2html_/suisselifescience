<?php
/**
 * Plugin Name: Custom send email
 * Description: Organize and sending email from form.
 * Author: Konstantin Slupko [Seven]
 * Author URI: https://github.com/linux0uid
 * Version: 0.1
 * Plugin Slug: email_forms
 */

spl_autoload_register(function ($class) {
    $file = plugin_dir_path( __FILE__ ) . 'classes'
        . DIRECTORY_SEPARATOR . strtolower($class) . '.class.php';
    if ( false !== strpos( $class, 'Message' ) ) {
        if (file_exists($file)) {
            require_once $file;
        } else {
            throw new Exception($file . ' dosn`t exist!');
        }
    }
});

if (!defined('PLUGIN_CUSTOM_SEND_EMAIL_PATH')) {
    define('PLUGIN_CUSTOM_SEND_EMAIL_PATH', plugin_dir_path( __FILE__ ));
}
