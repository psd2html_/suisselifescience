<?php 

$regions = getAllRegions(); 
$languages = getLanguages();

?>
<?php //var_dump($regions);exit; ?>

<div class="container">
    <div class="row">
        <?php if (isset($_GET['message'])) { ?>
            <div class="col-md-12">
                <div class="message_wrapper">
                    <span class="message">
                        <?php if ($_GET['message'] == 1) { ?>
                            Region was succsesfully added.
                        <?php } elseif ($_GET['message'] == 'error') {?>
                            Error. Something went wrong.
                        <?php } ?>
                    </span>
                </div>
            </div>
        <?php } ?>
        <h4><?php echo translate("List of regions"); ?>
        <table class="wp-list-table">
        <thead>
            <tr>
                <th>
                    <?php echo translate("ID"); ?>
                </th>
                <th>
                    <?php echo translate("Region"); ?>
                </th>
                <th>
                    <?php echo translate("Languages"); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($regions as $region) { ?>
                <tr>
                    <td><?php echo $region->id; ?></td>
                    <td><?php echo $region->name; ?></td>
                    <td>
                        <?php $regionLanguages = decodeLanguages($region->languages); ?>
                        <?php if ($regionLanguages) { ?>
                            <?php foreach ($regionLanguages as $language) { ?>
                                <?php echo getLanguage($language)[0]->english_name . ", "; ?>
                            <?php } ?>
                        <?php } else { ?>
                            <?php echo translate("No languages avalaibel for this region"); ?>
                        <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
        </table>
        <div class="col-md-10">
            <h4>Add regions form</h4>
            <form name="region_edit" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST" enctype="application/x-www-form-urlencoded">
                <label for="region">
                    <?php echo translate("Region"); ?>
                </label>
                <input type="text" name="region" placeholder="Region">
                <input type="hidden" name="action" value="region_form" required>
                <button type="submit"><?php echo translate("Submit"); ?></button>
            </form>

            <h4>Add language form</h4>
            <form name="language_edit" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="POST" enctype="application/x-www-form-urlencoded"> 
                <label for="region">
                    <?php echo translate("Region"); ?>
                </label>
                <select name="region" required>
                    <option value=""><?php echo translate("Select region"); ?></option>
                    <?php foreach ($regions as $region) { ?>
                        <option value="<?php echo $region->id; ?>"><?php echo $region->name; ?></option>
                    <?php } ?>
                </select>                <br>
                <label for="language">
                    <?php echo translate("Select language"); ?>
                </label>
                <select name="language" required>
                    <option value=""><?php echo translate("Select language"); ?></option>
                    <?php foreach ($languages as $language) { ?>
                        <option value="<?php echo $language->id; ?>"><?php echo $language->english_name; ?></option>
                    <?php } ?>
                </select>
                <input type="hidden" name="action" value="language_form">
                <button type="submit"><?php echo translate("Submit"); ?></button>
            </form>
        </div>
    </div>
</div>