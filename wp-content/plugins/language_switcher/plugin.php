<?php
/*
Plugin Name: Group language switcher
Description: Plugin adds language switcher grouped by region and allows to add there or delete languages.
Author: Vasyl Tarasov
Version: 0.1
Plugin Slug: language_switcher
*/


register_activation_hook( __FILE__, 'install_language_groups' );
function install_language_groups()
{
    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();

    $table_regions_name = $wpdb->prefix . 'language_regions';
    $table_languages_name = $wpdb->prefix . 'regions_languages';

    $create_regions_table_sql = "CREATE TABLE $table_regions_name (
      id mediumint(2) NOT NULL AUTO_INCREMENT,
      name tinytext NOT NULL,
      languages mediumtext,
      UNIQUE KEY id (id)
    ) $charset_collate;";

    $create_languages_table_sql = "CREATE TABLE $table_languages_name (
        id mediumint(2) NOT NULL AUTO_INCREMENT,
        language mediumint(3) NOT NULL,
        language_region mediumint(3) NOT NULL,
        UNIQUE KEY id (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    dbDelta( $create_languages_table_sql);
    dbDelta( $create_regions_table_sql );
}


register_uninstall_hook( __FILE__, 'uninstall_language_groups' );
function uninstall_language_groups()
{
    global $wpdb;

    $table_languages_name = $wpdb->prefix . "regions_languages";

    $wpdb->query("DROP TABLE IF EXISTS $table_languages_name");
}


add_action( 'admin_menu', 'register_menu_pages' );
function register_menu_pages() {
    add_submenu_page(
        'options-general.php',
        __( 'Language groups', 'textdomain' ),
        __( 'Language groups', 'textdomain' ),
        'manage_options',
        'language_switcher/templates/language-groups-admin.php',
        '',
        plugins_url( 'myplugin/images/icon.png' ),
        6
    );
}


add_action( 'wp_enqueue_scripts', 'wp_plugin_styles' );
function wp_plugin_styles()
{
    wp_enqueue_style('switcher', plugins_url() . '/language_switcher/css/style.css');
}

add_action( 'wp_footer', 'wp_plugin_script');
function wp_plugin_script(){
    wp_enqueue_script('switcher', plugins_url() . '/language_switcher/js/script.js', array('jquery'));
}

add_action( 'admin_post_region_form', 'region_form_handler' );
function region_form_handler()
{
    global $wpdb;
    $name = filter_input(INPUT_POST, 'region', FILTER_SANITIZE_STRING);
    try {
        $wpdb->insert($wpdb->prefix . 'language_regions', array(
            'name' => $name
            ));        
    } catch (Exception $e) {
        var_dump($e);exit;
          wp_redirect( admin_url('options-general.php?page=language_switcher%2Ftemplates%2Flanguage-groups-admin.php') . "?message=error");  
          exit;
    }
}


add_action( 'admin_post_language_form', 'language_form_handler' );
function language_form_handler()
{
    global $wpdb;

    $language = filter_input(INPUT_POST, 'language', FILTER_SANITIZE_STRING);
    $region   = filter_input(INPUT_POST, 'region', FILTER_SANITIZE_STRING);
    $languages = json_decode(getRegionById($region)[0]->languages);
    $languages[] = $language;
    $languages = json_encode($languages);

    try {
        $wpdb->update($wpdb->prefix . 'language_regions', 
            array(
                'languages' => $languages,
            ),
            array(
                'id' => $region
        ));  

    } catch (Exception $e) {
          wp_redirect( admin_url('options-general.php?page=language_switcher%2Ftemplates%2Flanguage-groups-admin.php') . "?message=error");  
          exit;
    }
}


/* One of main function of plugin, to detect user, user agent and so on */
add_action('init', 'detect_user_language');
function detect_user_language()
{
    /* Get user agent */
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        $user_language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    } else {
        $user_language = get_user_language_by_ip(get_user_ip());
    }

    $site_url = get_site_by_language($user_language);
    if (isset($_COOKIE['redirected']) && $_COOKIE) {
        setcookie('redirected', true, 3600);
        wp_redirect($site_url);    
        exit;
    }
}

/* Using WP plugin integrated with GeoIP get user language */

function get_user_language_by_ip($ip)
{
    $geo_ip = new Seven_Geo_IP();

    $country_code = $geo_ip->geoip_country_code_by_addr($ip);

    return $country_code;
}

function get_site_by_language($language)
{
    global $wpdb;

    $languages = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str');
    $site_url = $languages[$language]['url'];

    if ($site_url == 'str') {
        return get_site_url() . '/';
    }

    return $site_url;
}

/* Get IP of user by PHP */

function get_user_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

function getAllRegions()
{
    global $wpdb;

    $table_regions_name = $wpdb->prefix . "language_regions";

    $query = "SELECT * FROM $table_regions_name;";

    return $wpdb->get_results($query);
}

function getRegionById($id)
{
    global $wpdb;

    $table_regions_name = $wpdb->prefix . "language_regions";

    $query = "SELECT * FROM $table_regions_name WHERE id = $id;";

    return $wpdb->get_results($query);
}

function decodeLanguages($string)
{
    return json_decode($string);
}

function getLanguages()
{
    global $wpdb;

    $table_languages_name = $wpdb->prefix . "icl_languages";

    $query = "SELECT * FROM $table_languages_name WHERE $table_languages_name.active = 1;";

    return $wpdb->get_results($query);

}

function getLanguage($id)
{
    global $wpdb;

    $table_languages_name = $wpdb->prefix . "icl_languages";

    $query = "SELECT * FROM $table_languages_name WHERE $table_languages_name.active = 1 AND $table_languages_name.id = $id;";

    return $wpdb->get_results($query);

}
function getLanguagesForRegion($id)
{
    global $wpdb;

    $wpml_languages = $wpdb->prefix . "icl_languages";

    $langues_ids =json_decode($id);
    $languages_ids_array = $langues_ids;
    if (! $languages_ids_array) {
        return false;
    }
    $languages_ids_string = implode(', ', $languages_ids_array);


    $query = "SELECT * FROM $wpml_languages WHERE $wpml_languages.active = 1 AND $wpml_languages.id IN ($languages_ids_string);";

    return $wpdb->get_results($query);
}

function getImgSrcByRegion($name)
{
    $name = trim($name);
    $url = get_template_directory_uri() . '/img/language/';;

    switch ($name) {
        case 'Asia':
            return $url . 'reg-asia-south-pacific.png';
            break;

        case 'Europe':
            return $url . 'reg-europe.png';
            break;
        
        case 'Middle East':
            return $url . 'reg-middle-east.png';
            break;
        case 'USA and Canada':
            return $url . 'reg-north-south-america.png';
            break;
        
        case 'South America':
            return $url . 'reg-north-south-america.png';
            break;

        case 'Africa':
            return $url . 'reg-africa.png';
            break;
        
        default:
            return false;
    }
}