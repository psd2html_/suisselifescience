<?php

class WCC_Rewrite_Rule
{
    public $url_part;
    public $var;
    public $template_path;

    public function __construct($url_part, $var, $template_path)
    {

        $this->url_part = $url_part;
        $this->var = $var;
        $this->template_path = $template_path;

        $this->add_hooks();
    }

    public function add_hooks()
    {

        add_filter( 'query_vars', array( $this, 'prefix_register_query_var'), 0 );

        add_action( 'init', array( $this, 'prefix_movie_rewrite_rule') );

        add_filter( 'template_redirect', array( $this, 'products_plugin_display'), 0 );
    }

    public function prefix_movie_rewrite_rule() {
        add_rewrite_rule( "^" . $this->url_part . "/([^/]*)", 'index.php?pagename=' . $this->url_part . '&' . $this->var .'=$matches[1]', 'top');
    }

    public function prefix_register_query_var( $vars ) {

        $vars[] = $this->var;

        return $vars;
    }

    public function products_plugin_display() {

        $pagename = get_query_var('pagename');

        if ( $this->url_part == $pagename ) {
            return get_template_directory() . $this->template_path;
        }
    }
}