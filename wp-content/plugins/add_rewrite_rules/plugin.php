<?php
/*
 * Plugin Name: WCC Rewrite Rule
 */

include_once dirname( __FILE__ ) . '/classes/class-wcc-rewrite-rule.php';

$buy_page = new WCC_Rewrite_Rule(
    get_option('wcc_buy_page_slug', 'buy'),
    'current_category',
     '/template-page-buy-products.php'
);