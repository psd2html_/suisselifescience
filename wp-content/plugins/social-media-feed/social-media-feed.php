<?php
/*
Plugin Name: Social Media Feed
Version: 1.0
Author: Volodymyr Vovchok [Seven]
*/

require 'vendor/autoload.php';

use MetzWeb\Instagram\Instagram;

function seven_social_media_menu() {
    add_menu_page(
        __('Social Media', 'seven'),
        __('Social Media', 'seven'),
        'manage_options',
        'social-media-feed',
        'seven_social_media_settings_page',
        'dashicons-share-alt2'
    );
}
add_action('admin_menu', 'seven_social_media_menu');

function seven_social_media_settings_page() {

    $options = array(
        'facebook_app_id' => '',
        'facebook_app_secret' => '',
        'facebook_object_id' => '',
        'instagram_client_id' => '',
        'instagram_client_secret' => '',
        'instagram_access_token' => '',
        'instagram_user_id' => ''
    );

    $updated = false;

    foreach($options as $key => $option ) {
        $value = isset($_POST[ $key ]) ? $_POST[ $key ] : '';

        if( empty( $value ) ) {
            $options[ $key ] = get_option( $key, $option);
        } else {
            $option = get_option( $key, $options[ $key ]);
            if ( $option != $value ) {
                $options[ $key ] = $value;
                update_option( $key, $value );
            } else {
                $options[ $key ] = $option;
            }

            $updated = true;
        }
    }

    $page_url = add_query_arg(array(
        'page' => 'social-media-feed'
    ), get_site_url() . $_SERVER['PHP_SELF']);

    ?>

    <div class="wrap">

        <div id="header">
            <h1><?php _e('Social Media Settings', 'seven'); ?></h1>
        </div>

        <?php
            if ( $updated ) {
                add_settings_error('general', 'settings_updated', __('Settings saved.'), 'updated');
            }
            settings_errors();
        ?>

        <?php $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'facebook'; ?>

        <form name="social-media-setting-form" method="post" action="<?php echo add_query_arg('tab', $active_tab, $page_url); ?>">

            <h2 class="nav-tab-wrapper">
                <a href="<?php echo add_query_arg('tab', 'facebook', $page_url); ?>" class="nav-tab <?php echo $active_tab == 'facebook' ? 'nav-tab-active' : ''; ?>"><?php _e('Facebook App', 'instagram-feed'); ?></a>
                <a href="<?php echo add_query_arg('tab', 'instagram', $page_url); ?>" class="nav-tab <?php echo $active_tab == 'instagram' ? 'nav-tab-active' : ''; ?>"><?php _e('Instagram Client', 'instagram-feed'); ?></a>
                </h2>

            <?php if( $active_tab == 'facebook' ) { //Start Configure tab ?>

            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row"><label for="facebook_app_id"><?php _e('App ID', 'seven'); ?></label></th>
                    <td><input required name="facebook_app_id" type="text" id="facebook_app_id" value="<?php echo $options['facebook_app_id']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="facebook_app_secret"><?php _e('App Secret', 'seven'); ?></label></th>
                    <td><input required name="facebook_app_secret" type="text" id="facebook_app_secret" value="<?php echo $options['facebook_app_secret']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="facebook_object_id"><?php _e('facebook_object_id', 'seven'); ?></label></th>
                    <td><input required name="facebook_object_id" type="text" id="facebook_object_id" value="<?php echo $options['facebook_object_id']; ?>" class="regular-text"></td>
                </tr>
                </tbody>
            </table>

            <?php } // End tab ?>
            <?php if( $active_tab == 'instagram' ) { //Start tab ?>

            <?php

                if( isset($_GET['code']) ) {

                    $callback = add_query_arg(array(
                        'tab' => 'instagram'
                    ), $page_url);

                    $instagram= new Instagram( array(
                        'apiKey' => $options['instagram_client_id'],
                        'apiSecret' => $options['instagram_client_secret'],
                        'apiCallback' => $callback
                    ));

                    $options['instagram_access_token'] = $instagram->getOAuthToken($_GET['code'], true);
                }

            ?>

            <table class="form-table">
                <tbody>
                <tr>
                    <th scope="row"><label for="instagram_client_id"><?php _e('CLIENT ID', 'seven'); ?></label></th>
                    <td><input required name="instagram_client_id" type="text" id="instagram_client_id" value="<?php echo $options['instagram_client_id']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="instagram_client_secret"><?php _e('CLIENT SECRET', 'seven'); ?></label></th>
                    <td><input required name="instagram_client_secret" type="text" id="instagram_client_secret" value="<?php echo $options['instagram_client_secret']; ?>" class="regular-text"></td>
                </tr>
                <tr>
                    <th scope="row"><label for="instagram_access_token"><?php _e('access_token', 'seven'); ?></label></th>
                    <td><input name="instagram_access_token" type="text" id="instagram_access_token" value="<?php echo $options['instagram_access_token']; ?>" class="regular-text">
                        <?php

                        $scope = array('basic', 'likes', 'comments', 'relationships', 'public_content');

                        $instagram_url = "https://api.instagram.com/oauth/authorize/?client_id=";
                        $instagram_url .= $options['instagram_client_id'];
                        $instagram_url .= "&redirect_uri=";
                        $instagram_url .= urlencode(add_query_arg(array(
                            'tab' => 'instagram'
                        ), $page_url));
                        $instagram_url .= '&scope=' . implode('+', $scope);
                        $instagram_url .= "&response_type=code";
                        ?>

                        <p class="description">To receive an access_token <a href="<?php echo $instagram_url; ?>">click</a>.</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="instagram_user_id"><?php _e('user id', 'seven'); ?></label></th>
                    <td><input required name="instagram_user_id" type="text" id="instagram_user_id" value="<?php echo $options['instagram_user_id']; ?>" class="regular-text"></td>
                </tr>

                </tbody>
            </table>

            <?php } // End tab ?>

            <?php submit_button(); ?>
        </form>

    </div>

    <?php
}

function seven_compare_created_date($a, $b) {
    return $a->created_time <= $b->created_time ? $a->created_time : false;
}

function seven_get_social_media_feed($limit = 2) {

    $posts = array();

    $posts = array_merge($posts, seven_get_facebook_media_feed( $limit ));
    $posts = array_merge($posts, seven_get_instagram_media_feed( $limit ));

    if (empty($posts)) {
        return $posts;
    }

    usort($posts, "seven_compare_created_date");

    return array_slice($posts, 0, $limit);
}

function seven_get_facebook_media_feed( $limit = 2 ) {

    $posts = array();

    $facebook_app_id = get_option('facebook_app_id');
    $facebook_app_secret = get_option('facebook_app_secret');
    $user_id = get_option('facebook_object_id');//'219511151775633';


    if (!empty($facebook_app_id) &&
        !empty($facebook_app_secret) &&
        !empty($user_id)) {

        $fb = new \Facebook\Facebook([
            'app_id' => $facebook_app_id,
            'app_secret' => $facebook_app_secret,
            'default_graph_version' => 'v2.6',
            'default_access_token' => "$facebook_app_id|$facebook_app_secret"
        ]);

        try {
            $response = $fb->get("/$user_id/posts?fields=id,created_time,link,place,message,picture,name,attachments&limit=$limit");

            $fposts = $response->getDecodedBody();

            foreach( $fposts['data'] as $item) {
                $attachment = $item['attachments']['data'][0];
                $post = new StdClass;
                $object_item_data = explode('_', $item['id']);
                $object_item_id = $object_item_data[0];
                $object_item_post_id = $object_item_data[1];
                $post->created_time = strtotime($item['created_time']);
                $post->link = "https://www.facebook.com/$object_item_id/posts/$object_item_post_id";
                $post->text = isset($attachment['description']) ? $attachment['description'] : $attachment['title'];
                //if ( 'photo' == $attachment['type'] ) {
                $post->image = $item['picture'];
                //}
                $posts[] = $post;
            }
            // exit; //redirect, or do whatever you want
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            //  echo $e->getMessage();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            //  echo $e->getMessage();
        }
    }

    return $posts;
}

function seven_get_instagram_media_feed( $limit = 2 ) {

    $posts = array();

    $instagram_client_id = get_option('instagram_client_id');
    $instagram_client_secret = get_option('instagram_client_secret');
    $instagram_access_token = get_option('instagram_access_token');
    $instagram_user_id = get_option('instagram_user_id');

    if( ! empty($instagram_client_id) &&
        ! empty($instagram_client_secret) &&
        ! empty($instagram_access_token) &&
        ! empty($instagram_user_id)) {


        $instagram= new Instagram( array(
            'apiKey' => $instagram_client_id,
            'apiSecret' => $instagram_client_secret,
            'apiCallback' => ''
        ));

        $instagram->setAccessToken( $instagram_access_token ); //1838308032 self

        $media = $instagram->getUserMedia( $instagram_user_id, $limit);

        if (!empty($media->data)) {
            foreach($media->data as $item) {
                $post = new StdClass();
                $post->link = $item->link;
                $post->created_time = (int) $item->caption->created_time;
                $post->text = $item->caption->text;
                $post->image = $item->images->thumbnail->url;

                $posts[] = $post;
            }
        }
    }

    return $posts;
}