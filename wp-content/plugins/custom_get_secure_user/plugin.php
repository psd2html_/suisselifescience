<?php
/**
 * Plugin Name: Get Users from Secure API
 * Description: Find closest distributor. Update the list of centers from CPA.
 * Author: Konstantin Slupko [Seven]
 * Author URI: https://github.com/linux0uid
 * Version: 0.1
 * Plugin Slug: centers
 */

if (!defined('BASE_SECURE_URI_API')) {
    $base_uri = get_field('api_secure_uri');
    define('BASE_SECURE_URI_API', $base_uri);
}

if (!defined('SECURE_CENTERS_TABLE')) {
    global $wpdb;
    $db_table_name = $wpdb->prefix . 'secure_centers';
    define('SECURE_CENTERS_TABLE', $db_table_name);
}


/**
 * Create table
 *
 */
function custom_get_secure_user_activation() {
    require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
    global $wpdb;
    $db_table_name = SECURE_CENTERS_TABLE;
    if( $wpdb->get_var( "SHOW TABLES LIKE '$db_table_name'" ) != $db_table_name ) {
        if ( ! empty( $wpdb->charset ) )
            $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
        if ( ! empty( $wpdb->collate ) )
            $charset_collate .= " COLLATE $wpdb->collate";

        $sql = "CREATE TABLE `" . $db_table_name . "` (
            `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            'secure_user_id' INT(11) NULL DEFAULT NULL,
            `first_name` varchar(100) NULL DEFAULT NULL,
            `last_name` varchar(100) NULL DEFAULT NULL,
            `user_email` varchar(100) NULL DEFAULT NULL,
            `zip` varchar(100) NULL DEFAULT NULL,
            `user_role` varchar(100) NULL DEFAULT NULL,
            `company_name` varchar(100) NULL DEFAULT NULL,
            `company_desc` longtext NULL DEFAULT NULL,
            `street_address` varchar(100) NULL DEFAULT NULL,
            `town_city` varchar(100) NULL DEFAULT NULL,
            `state` varchar(100) NULL DEFAULT NULL,
            `country` varchar(100) NULL DEFAULT NULL,
            `country_code` varchar(100) NULL DEFAULT NULL,
            `company_phone` varchar(100) NULL DEFAULT NULL,
            `company_website` varchar(100) NULL DEFAULT NULL,
            `company_logo` VARCHAR(100) NULL DEFAULT NULL,
            `latitude` DECIMAL(18, 12) NOT NULL,
            `longitude` DECIMAL(18, 12) NOT NULL,
            `created` TIMESTAMP DEFAULT now(),
	    `booking_email` varchar(100) NULL DEFAULT NULL,
	    `company_email` varchar(100) NULL DEFAULT NULL,
            `brand_name` varchar(100) NULL DEFAULT NULL,
	    `center_name` varchar(100) NULL DEFAULT NULL,
            `status` varchar(11) NULL DEFAULT NULL,
            PRIMARY KEY (`id`),
            INDEX `role_countr` (`user_role`, `country`),
            INDEX (`state`),
            INDEX (`country_code`)
        ) $charset_collate;";
        dbDelta( $sql );
        get_all_centers_from_secure();
    }

    if ( ! wp_next_scheduled( 'get_centers_from_secure' ) ) {
      wp_schedule_event( time(), 'hourly', 'get_centers_from_secure' );
    }
}

register_activation_hook(__FILE__, 'custom_get_secure_user_activation');

add_action( 'get_centers_from_secure', 'get_all_centers_from_secure' );


/**
 * get_all_centers_from_secure
 * cron function
 *
 */
function get_all_centers_from_secure() {
    global $wpdb;
    $db_table_name = SECURE_CENTERS_TABLE;

    $args = array(
        'timeout' => 120,
        'httpversion' => '1.1',
    );

    $table_columns = array(
        'first_name',
        'last_name',
        'user_email',
        'zip',
        'user_role',
        'company_name',
        'company_desc',
        'street_address',
        'town_city',
        'state',
        'country',
        'company_phone',
        'company_website',
        'company_logo',
        'latitude',
        'longitude',
	'booking_email',
	'company_email',
        'secure_user_id',
         'brand_name',
	'center_name',
        'status',
    );

    $response = wp_remote_get(BASE_SECURE_URI_API . '/wp-json/v3/findcenter', $args);
    if (is_wp_error($response)) return false;
    $body = (string) $response['body'];
    $centers = json_decode($body);

    $wpdb->query('START TRANSACTION');
    try {
        $wpdb->query("TRUNCATE TABLE `{$db_table_name}`");
        foreach ($centers->data as $center) {
            $new_line = array();
            foreach ($center as $key=>$column) {
                if (in_array($key, $table_columns)) {
                    $new_line[$key] = $column;
                }
                if ($key === 'user_id') {                   
                    $new_line['secure_user_id'] = $column;                   
                }
            }
            $wpdb->insert($db_table_name, $new_line);
        }
    } catch (Exception $e) {
        $wpdb->query('ROLLBACK');
        return false;
    }
    $wpdb->query('COMMIT');

    return true;
}

/**
 * get_users_from_secure
 *
 * @param mixed $slug
 *
 * @return array
 */
function get_users_from_secure($slug)
{
    $args = array(
        'timeout' => 120,
        'httpversion' => '1.1',
    );
    $slug = rawurlencode($slug);

    $response = wp_remote_get(BASE_SECURE_URI_API . '/wp-json2/users-api/v1/country/' . $slug, $args);
    if (is_wp_error($response)) return false;
    $body = (string) $response['body'];
    $result = json_decode($body);

    return $result;
}

/**
 * Select user from DB closest Partner with specific role
 *
 * @param array $lat_lon    Latitude & Longitude
 * @param string $country   Users country
 * @param string $state     Users state of country
 * @param string $role      Role of needed center
 *
 * @return array
 */
function select_closest_distributors_email($lat_lon = array(), $country = '', $state = '', $role = 'distributor')
{
    global $wpdb;

    $latitude   = $lat_lon['latitude'];
    $longitude  = $lat_lon['longitude'];

    $db_table_name = SECURE_CENTERS_TABLE;
    $earthRadius = '3963.0'; //in miles.
    $sql = "
        SELECT
          ROUND(
            {$earthRadius} * ACOS(
                SIN({$latitude} * PI() / 180) * SIN(latitude * PI() / 180) + COS({$latitude} * PI() / 180)
                * COS(latitude * PI() / 180) * COS((longitude * PI() / 180) - ({$longitude} * PI() / 180))
              ),
              1
          ) AS DISTANCE,
          id,
          user_email,
          latitude,
          longitude,
	  booking_email
        FROM
          `{$db_table_name}`
        WHERE
            `user_role`='{$role}'
            AND `country`='{$country}'
    ";
    if (!empty($state)) $sql .= " AND `state`='{$state}' ";
    $sql .= "
        ORDER BY
          DISTANCE
        LIMIT 0, 1
    ;";

    $result = $wpdb->get_results($sql);

    if (empty($result)) {
        if ($role === 'master-distributor') {
            return array();
        } else {
            $result = select_closest_distributors_email($lat_lon, $country, $state, 'master-distributor');
        }
    }

    return $result;
}

/**
 * select_closest_distributors
 * If our user from Canada or US we must first of all find center in his state.
 *
 * @param array $lat_lon
 * @param string $country
 *
 * @return array
 */
function select_closest_distributors($lat_lon = array(), $country = '')
{
    $result = null;
    switch ($country) {
        case 'UnitedStates':
        case 'Canada':
            $state = $lat_lon['state'];
            $result = select_closest_distributors_email($lat_lon, $country, $state);
            if (!empty($result)) break;
        default:
            $result = select_closest_distributors_email($lat_lon, $country);
            break;
    }

    return $result;
}
