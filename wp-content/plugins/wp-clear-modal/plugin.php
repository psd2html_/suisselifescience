<?php
/*
Plugin Name: WP-Clear-Modal
Plugin URI:
Description: This plug-in will protect your content
Version: 1.0.0
Author: Konstantin Slupko [Seven]
Author URI: no url
*/

if (!defined('WP_CLEAR_MODAL_PATH')) {
    define('WP_CLEAR_MODAL_PATH', dirname(__FILE__));
}

define('WP_CLEAR_MODAL_VERSION', '1.0.0');

function wp_clear_modal()
{
    // get modals template
    load_template(WP_CLEAR_MODAL_PATH.'/templates/modal.php');
}

add_action('wp_footer', 'wp_clear_modal');
