<?php /* Template Name: Carrier */ ?>
<?php get_header();

$fields = get_fields( get_the_ID() );

?>
	<?php if( empty($fields['section_1_background_image']) ) : ?>
		<section class="cover carrier_img">
	<?php else : ?>
		<section class="cover " style="background: rgba(0, 0, 0, 0) url('<?php echo $fields['section_1_background_image']; ?>') repeat scroll 0 0; padding-top: 33%">
	<?php endif; ?>
    </section>
    <section>
    	<div class="container">
	    		<div class="col-lg-12">
	    			<div class="row">
		    			<h2 class="text-uppercase m-t-0"><?php echo $fields['section_2_title']; ?></h2>
		    			<p class="m-b-20"><?php echo $fields['section_2_description']; ?></p>
		    			<h4 class="m-t-0 m-b-40 text-blue text-uppercase letter-sapce-4"><?php echo $fields['section_2_sub_title']; ?></h4>
		    		</div>
		    	</div>
    		<div class="row">
				<?php $left_list_items =  $fields['section_2_left_components']; ?>
	    		<div class="col-lg-5">
	    			<div class="blue_text_section m-b-5 text-right text-uppercase text-white">
	    				<h4>
							<?php echo $left_list_items[0]['title']; ?>
						</h4>

	    			</div>
	    			<div class="blue_text_section m-b-5 text-right text-uppercase text-white">
	    				<h4>
							<?php echo $left_list_items[1]['title']; ?>
						</h4>
	    			</div>
	    			<div class="blue_text_section m-b-0 text-right text-uppercase text-white">
	    				<h4>
							<?php echo $left_list_items[2]['title']; ?>
						</h4>
	    			</div>

	    		</div>
	    		<div class="col-lg-6 col-lg-offset-1">
	    			<h3 class="mobile-top-p text-uppercase m-b-25 m-t-0"><?php echo $fields['section_2_right_components_title'] ?></h3>
					<?php $right_list_items =  $fields['section_2_right_components']; ?>
	    			<ul class="carrier_list blue_ul">
						<?foreach ( $right_list_items as $item ) : ?>
	    				<li><?php echo $item['title']; ?></li>
						<?php endforeach; ?>
	    			</ul>
	    		</div>
    		</div>    		
    	</div>
    </section>
	<?php if( empty($fields['section_2_background_image']) ) : ?>
		<section class="cover">
	<?php else : ?>
		<section class="cover mobile_baner baner_h630" style="background: rgba(0, 0, 0, 0) url('<?php echo $fields['section_2_background_image']; ?>') repeat scroll 0 0;">
	<?php endif; ?>
        <div class="container">            
            <div class="col-md-8 carrier_last_blc">
                <h3 class="m-b-35 m-t-0 text-uppercase"><?php echo $fields['section_3_title'] ?></h3>
				<h4 class="text-uppercase">
					<?php echo $fields['section_3_description'] ?>
				</h4>
				<a href="mailto:info@suisselifescience.com?subject=JOB APPLICATIONS" class="nbtn nbtn-inverse text-uppercase m-t-30"><?php echo $fields['section_3_button_value'] ?></a>
            </div>            
        </div>
    </section>

<?php get_footer(); ?>