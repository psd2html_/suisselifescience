<?php
/**
 * Запись в цикле (loop.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */ 
?>

<article class="m-b-20 p-b-20 b-b" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php $post_meta_description = get_post_meta( get_the_ID(), '_yoast_wpseo_metadesc', true );?>
    <?php $post_description = get_the_content(get_the_ID());?>
    <h3 class="m-t-10 m-b-10"><a href="<?php the_permalink(); ?>"><?php echo custom_highlight_text(get_the_title(), $keywords); ?></a></h3>

    <p class="m-b-10"><?php echo custom_highlight_text((!empty($post_description) ? $post_description : $post_meta_description), $keywords); ?></p>
    <a href="<?php the_permalink(); ?>">Read more ></a>
    
</article>
