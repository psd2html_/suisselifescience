<?php /* Template Name: How it works  */ ?>
<?php get_header();

    $fields = get_fields( get_the_ID() );

?>
      <section class="cover " style="background: rgba(0, 0, 0, 0) url('/wp-content/themes/sls/img/How-it-works.jpg') repeat scroll 0 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="upper_plash up_white thin f-52 text-white m-t-50 m-b-0">ASK YOUR DNA</h2>
                    <h2 class="bold f-52 text-white m-t-0">UNLOCK YOUR<br> 
                        ANTI-AGING POTENTIAL</h2>
                    <p class="m-t-200 text-white light">Simple, exclusive, long-lasting natural results</p>
                    
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="col-lg-12 m-b-60">
                <h2 class="upper_plash up_black thin f-52 m-t-50 m-b-0 ">HOW IT WORKS</h2>
                <h2 class="bold f-52  m-t-0">YOUR LUXURY ANTI-AGING</h2>
            </div>
            <div class="col-lg-12 block_with_blue_arrow wow fadeInLeft step1 m-b-30" data-wow-duration="0.2s" data-wow-delay="0.2s" >
                <h2 class="text-uppercase bold m-t-10">Step 1</h2>
                <div class="b_w_b_a_img_blc m-t-45">
                    <div class="m-b-25 pull-left">
                        <img src="/wp-content/themes/sls/img/sport_klava.png">
                    </div>
                    <div class="pull-left text-near-img">
                        <p class="bold m-b-5">JOIN NOW.</p>
                        <p>Purchase your <short>i</short>DDNA<sup>®</sup> lifelong membership with your iDDNA® kit.<p>
                    </div>
                    <div class="clearfix"></div>         
                </div>
            </div>
            <div class="col-lg-12 block_with_blue_arrow wow fadeInLeft step2 m-b-30" data-wow-duration="0.2s" data-wow-delay="0.7s" >
                <h2 class="text-uppercase bold m-t-10">Step 2</h2>
                <div class="b_w_b_a_img_blc m-t-45">
                    <div class="m-b-25 pull-left">
                        <img src="/wp-content/themes/sls/img/sport_ruka.png">
                    </div>
                    <div class="pull-left text-near-img">
                        <p class="bold m-b-5">RECEIVE YOUR <short>i</short>DDNA<sup>®</sup> KIT AND SIGN UP FOR YOUR<br> 
                            PRIVATE CONCIERGE AREA TO ACTIVATE IT.</p>
                        <p>As an <short>i</short>DDNA<sup>®</sup> exclusive member, you can enjoy a personal concierge designed to assist you for your <short>i</short>DDNA<sup>®</sup><br>and make your lifestyle solutions more enjoyable.<p>
                    </div>
                    <div class="clearfix"></div>         
                </div>
            </div>
            <div class="col-lg-12 block_with_blue_arrow wow fadeInLeft step3 m-b-30" data-wow-duration="0.2s" data-wow-delay="1.2s" >
                <h2 class="text-uppercase bold m-t-10">Step 3</h2>
                <div class="b_w_b_a_img_blc m-t-45">
                    <div class="m-b-25 pull-left">
                        <img src="/wp-content/themes/sls/img/sport_ruka.png">
                    </div>                    
                     <div class="pull-left text-near-img">
                        <p class="bold m-b-5">TELL US A BIT ABOUT YOUR LIFESTYLE.</p>
                        <p>Fill out our epigenetics online survey.<p>
                    </div>
                    <div class="clearfix"></div>         
                </div>
            </div>
            <div class="col-lg-12 block_with_blue_arrow wow fadeInLeft step4 m-b-30" data-wow-duration="0.2s" data-wow-delay="1.7s" >
                <h2 class="text-uppercase bold m-t-10">Step 4</h2>
                <div class="b_w_b_a_img_blc m-t-45">
                    <div class="m-b-25 pull-left">
                        <img src="/wp-content/themes/sls/img/sport_ruka.png">
                    </div>
                    <div class="pull-left text-near-img">
                        <p class="bold m-b-5">GET YOUR DNA SAMPLE COLLECTED.</p>
                        <p>Fast and painless, it takes  2 minutes. Just ship back the  pre-labelled envelope.<p>
                    </div>
                    <div class="clearfix"></div>         
                </div>
            </div>
            <div class="col-lg-12 block_with_blue_arrow wow fadeInLeft step5 m-b-30" data-wow-duration="0.2s" data-wow-delay="2.2s" >
                <h2 class="text-uppercase bold m-t-10">Step 5</h2>
                <div class="b_w_b_a_img_blc m-t-45">
                    <div class="m-b-25 pull-left">
                        <img src="/wp-content/themes/sls/img/sport_ruka.png">
                    </div>
                    <div class="pull-left text-near-img">
                        <p class="bold m-b-5">EXPERIENCE YOUR <short>i</short>DDNA<sup>®</sup> ultra-custom, LUXURY ANTI-AGING.</p>
                        <p>In 2/3 weeks from when we receive your de-identified sample, you will receive – directly to your door – an elegant box of your anti-aging treatment made to measure from your DNA, using the highest quality of active ingredients at the right concentrations, with your name engraved inside. Your very personal profile will be activated in <short>i</short>DDNA<sup>®</sup> Artificial Intelligence platform, providing you concierge mobile health & wellness advice, for all life.<p>
                    </div>
                    <div class="clearfix"></div>         
                </div>
            </div>
        </div>
    </section>
    <section style="background: rgba(0, 0, 0, 0) url('/wp-content/themes/sls/img/moloda_baba.jpg') repeat scroll 50% 0;" class="cover">
        <div class="container page-sport">
            <div class="col-md-4 col-lg-6"></div>
            <div class="col-md-8 col-lg-6 ">
                <h2 class="upper_plash bold"><short>i</short>DDNA<sup>®</sup> is about you.</h2>
                <p class="m-t-40"> Finally, a smart anti-aging that knows you, understands you<br> 
                    and follows the changes of your life. </p>
                <p class="">Think of <short>i</short>DDNA<sup>®</sup> as your new, discreet life companion that helps you <br> 
                    with your passion to look and feel your best. Your own personal <br> 
                    Anti-Aging, Health and Wellness coach, all in one.</p>
                <div class="buttons_div m-t-50">                   
                   <a href="/centers/" class="nbtn nbtn-inverse">TALK TO US<i class="ico arrow arrow_white"></i></a>
                   <a href="/centers/" class="nbtn ">JOIN <short>i</short>DDNA<sup>®</sup><i class="ico arrow arrow_white"></i></a>                   
               </div>
            </div>

    </section> 
        <?php get_footer(); ?>