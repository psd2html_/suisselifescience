<?php
get_header();
?>
<section>
	<div class="container">
		<div class="row">
			<div class="<?php content_class_by_sidebar(); ?>">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h1><?php the_title(); ?></h1>
						<div class="meta">
							<p><?php echo __("Published:","sls-theme")?> <?php the_time('F j, Y в H:i'); ?></p>
							<p><?php echo __("Author:","sls-theme")?>  <?php the_author_posts_link(); ?></p>
							<p><?php echo __("Categories:","sls-theme")?> <?php the_category(',') ?></p>
							<?php the_tags('<p>' . __("Tags:","sls-theme") . ' ' , ',', '</p>'); ?>
						</div>
						<?php the_content(); ?>
					</article>
				<?php endwhile; ?>
				<?php previous_post_link('%link', __('<- Previous Post: %title', 'sls-theme'), TRUE); ?>
				<?php next_post_link('%link', __('Next post: %title ->', 'sls-theme'), TRUE); ?>
				<?php if (comments_open() || get_comments_number()) comments_template('', true); ?>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
