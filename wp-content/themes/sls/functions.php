<?php

define('TEMPLATE_DIRECTORY', get_template_directory());

$global_center_countries = array(
        "AF" => "Afghanistan",
	"AX" => "Aland Islands",
	"AL" => "Albania",
	"DZ" => "Algeria",
	"AS" => "American Samoa",
	"AD" => "Andorra",
	"AO" => "Angola",
	"AI" => "Anguilla",
	"AQ" => "Antarctica",
	"AG" => "Antigua And Barbuda",
	"AR" => "Argentina",
	"AM" => "Armenia",
	"AW" => "Aruba",
	"AU" => "Australia",
	"AT" => "Austria",
	"AZ" => "Azerbaijan",
	"BS" => "Bahamas",
	"BH" => "Bahrain",
	"BD" => "Bangladesh",
	"BB" => "Barbados",
	"BY" => "Belarus",
	"BE" => "Belgium",
	"BZ" => "Belize",
	"BJ" => "Benin",
	"BM" => "Bermuda",
	"BT" => "Bhutan",
	"BO" => "Bolivia",
	"BA" => "Bosnia And Herzegovina",
	"BW" => "Botswana",
	"BV" => "Bouvet Island",
	"BR" => "Brazil",
	"IO" => "British Indian Ocean Territory",
	"BN" => "Brunei Darussalam",
	"BG" => "Bulgaria",
	"BF" => "Burkina Faso",
	"BI" => "Burundi",
	"KH" => "Cambodia",
	"CM" => "Cameroon",
	"CA" => "Canada",
	"CV" => "Cape Verde",
	"KY" => "Cayman Islands",
	"CF" => "Central African Republic",
	"TD" => "Chad",
	"CL" => "Chile",
	"CN" => "China",
	"CX" => "Christmas Island",
	"CC" => "Cocos (Keeling) Islands",
	"CO" => "Colombia",
	"KM" => "Comoros",
	"CG" => "Congo",
	"CD" => "Congo, Democratic Republic",
	"CK" => "Cook Islands",
	"CR" => "Costa Rica",
	"CI" => "Cote DIvoire",
	"HR" => "Croatia",
	"CU" => "Cuba",
	"CY" => "Cyprus",
	"CZ" => "Czech Republic",
	"DK" => "Denmark",
	"DJ" => "Djibouti",
	"DM" => "Dominica",
	"DO" => "Dominican Republic",
	"EC" => "Ecuador",
	"EG" => "Egypt",
	"SV" => "El Salvador",
	"GQ" => "Equatorial Guinea",
	"ER" => "Eritrea",
	"EE" => "Estonia",
	"ET" => "Ethiopia",
	"FK" => "Falkland Islands (Malvinas)",
	"FO" => "Faroe Islands",
	"FJ" => "Fiji",
	"FI" => "Finland",
	"FR" => "France",
	"GF" => "French Guiana",
	"PF" => "French Polynesia",
	"TF" => "French Southern Territories",
	"GA" => "Gabon",
	"GM" => "Gambia",
	"GE" => "Georgia",
	"DE" => "Germany",
	"GH" => "Ghana",
	"GI" => "Gibraltar",
	"GR" => "Greece",
	"GL" => "Greenland",
	"GD" => "Grenada",
	"GP" => "Guadeloupe",
	"GU" => "Guam",
	"GT" => "Guatemala",
	"GG" => "Guernsey",
	"GN" => "Guinea",
	"GW" => "Guinea-Bissau",
	"GY" => "Guyana",
	"HT" => "Haiti",
	"HM" => "Heard Island & Mcdonald Islands",
	"VA" => "Holy See (Vatican City State)",
	"HN" => "Honduras",
	"HK" => "Hong Kong",
	"HU" => "Hungary",
	"IS" => "Iceland",
	"IN" => "India",
	"ID" => "Indonesia",
	"IR" => "Iran, Islamic Republic Of",
	"IQ" => "Iraq",
	"IE" => "Ireland",
	"IM" => "Isle Of Man",
	"IL" => "Israel",
	"IT" => "Italy",
	"JM" => "Jamaica",
	"JP" => "Japan",
	"JE" => "Jersey",
	"JO" => "Jordan",
	"KZ" => "Kazakhstan",
	"KE" => "Kenya",
	"KI" => "Kiribati",
	"KR" => "Korea",
	"KW" => "Kuwait",
	"KG" => "Kyrgyzstan",
	"LA" => "Lao Peoples Democratic Republic",
	"LV" => "Latvia",
	"LB" => "Lebanon",
	"LS" => "Lesotho",
	"LR" => "Liberia",
	"LY" => "Libyan Arab Jamahiriya",
	"LI" => "Liechtenstein",
	"LT" => "Lithuania",
	"LU" => "Luxembourg",
	"MO" => "Macao",
	"MK" => "Macedonia",
	"MG" => "Madagascar",
	"MW" => "Malawi",
	"MY" => "Malaysia",
	"MV" => "Maldives",
	"ML" => "Mali",
	"MT" => "Malta",
	"MH" => "Marshall Islands",
	"MQ" => "Martinique",
	"MR" => "Mauritania",
	"MU" => "Mauritius",
	"YT" => "Mayotte",
	"MX" => "Mexico",
	"FM" => "Micronesia, Federated States Of",
	"MD" => "Moldova",
	"MC" => "Monaco",
	"MN" => "Mongolia",
	"ME" => "Montenegro",
	"MS" => "Montserrat",
	"MA" => "Morocco",
	"MZ" => "Mozambique",
	"MM" => "Myanmar",
	"NA" => "Namibia",
	"NR" => "Nauru",
	"NP" => "Nepal",
	"NL" => "Netherlands",
	"AN" => "Netherlands Antilles",
	"NC" => "New Caledonia",
	"NZ" => "New Zealand",
	"NI" => "Nicaragua",
	"NE" => "Niger",
	"NG" => "Nigeria",
	"NU" => "Niue",
	"NF" => "Norfolk Island",
	"MP" => "Northern Mariana Islands",
	"NO" => "Norway",
	"OM" => "Oman",
	"PK" => "Pakistan",
	"PW" => "Palau",
	"PS" => "Palestinian Territory, Occupied",
	"PA" => "Panama",
	"PG" => "Papua New Guinea",
	"PY" => "Paraguay",
	"PE" => "Peru",
	"PH" => "Philippines",
	"PN" => "Pitcairn",
	"PL" => "Poland",
	"PT" => "Portugal",
	"PR" => "Puerto Rico",
	"QA" => "Qatar",
	"RE" => "Reunion",
	"RO" => "Romania",
	"RU" => "Russian Federation",
	"RW" => "Rwanda",
	"BL" => "Saint Barthelemy",
	"SH" => "Saint Helena",
	"KN" => "Saint Kitts And Nevis",
	"LC" => "Saint Lucia",
	"MF" => "Saint Martin",
	"PM" => "Saint Pierre And Miquelon",
	"VC" => "Saint Vincent And Grenadines",
	"WS" => "Samoa",
	"SM" => "San Marino",
	"ST" => "Sao Tome And Principe",
	"SA" => "Saudi Arabia",
	"SN" => "Senegal",
	"RS" => "Serbia",
	"SC" => "Seychelles",
	"SL" => "Sierra Leone",
	"SG" => "Singapore",
	"SK" => "Slovakia",
	"SI" => "Slovenia",
	"SB" => "Solomon Islands",
	"SO" => "Somalia",
	"ZA" => "South Africa",
	"GS" => "South Georgia And Sandwich Isl.",
	"ES" => "Spain",
	"LK" => "Sri Lanka",
	"SD" => "Sudan",
	"SR" => "Suriname",
	"SJ" => "Svalbard And Jan Mayen",
	"SZ" => "Swaziland",
	"SE" => "Sweden",
	"CH" => "Switzerland",
	"SY" => "Syrian Arab Republic",
	"TW" => "Taiwan",
	"TJ" => "Tajikistan",
	"TZ" => "Tanzania",
	"TH" => "Thailand",
	"TL" => "Timor-Leste",
	"TG" => "Togo",
	"TK" => "Tokelau",
	"TO" => "Tonga",
	"TT" => "Trinidad And Tobago",
	"TN" => "Tunisia",
	"TR" => "Turkey",
	"TM" => "Turkmenistan",
	"TC" => "Turks And Caicos Islands",
	"TV" => "Tuvalu",
	"UG" => "Uganda",
	"UA" => "Ukraine",
	"AE" => "United Arab Emirates",
	"GB" => "United Kingdom",
	"US" => "United States",
	"UM" => "United States Outlying Islands",
	"UY" => "Uruguay",
	"UZ" => "Uzbekistan",
	"VU" => "Vanuatu",
	"VE" => "Venezuela",
	"VN" => "Viet Nam",
	"VG" => "Virgin Islands, British",
	"VI" => "Virgin Islands, U.S.",
	"WF" => "Wallis And Futuna",
	"EH" => "Western Sahara",
	"YE" => "Yemen",
	"ZM" => "Zambia",
	"ZW" => "Zimbabwe",
    );

$global_center_zones = array(
            "AD"=> "Europe",
            "AE"=> "Asia",
            "AF"=> "Asia",
            "AG"=> "North America",
            "AI"=> "North America",
            "AL"=> "Europe",
            "AM"=> "Asia",
            "AN"=> "North America",
            "AO"=> "Africa",
            "AQ"=> "Antarctica",
            "AR"=> "South America",
            "AS"=> "Australia",
            "AT"=> "Europe",
            "AU"=> "Australia",
            "AW"=> "North America",
            "AZ"=> "Asia",
            "BA"=> "Europe",
            "BB"=> "North America",
            "BD"=> "Asia",
            "BE"=> "Europe",
            "BF"=> "Africa",
            "BG"=> "Europe",
            "BH"=> "Asia",
            "BI"=> "Africa",
            "BJ"=> "Africa",
            "BM"=> "North America",
            "BN"=> "Asia",
            "BO"=> "South America",
            "BR"=> "South America",
            "BS"=> "North America",
            "BT"=> "Asia",
            "BW"=> "Africa",
            "BY"=> "Europe",
            "BZ"=> "North America",
            "CA"=> "North America",
            "CC"=> "Asia",
            "CD"=> "Africa",
            "CF"=> "Africa",
            "CG"=> "Africa",
            "CH"=> "Europe",
            "CI"=> "Africa",
            "CK"=> "Australia",
            "CL"=> "South America",
            "CM"=> "Africa",
            "CN"=> "Asia",
            "CO"=> "South America",
            "CR"=> "North America",
            "CU"=> "North America",
            "CV"=> "Africa",
            "CX"=> "Asia",
            "CY"=> "Asia",
            "CZ"=> "Europe",
            "DE"=> "Europe",
            "DJ"=> "Africa",
            "DK"=> "Europe",
            "DM"=> "North America",
            "DO"=> "North America",
            "DZ"=> "Africa",
            "EC"=> "South America",
            "EE"=> "Europe",
            "EG"=> "Africa",
            "EH"=> "Africa",
            "ER"=> "Africa",
            "ES"=> "Europe",
            "ET"=> "Africa",
            "FI"=> "Europe",
            "FJ"=> "Australia",
            "FK"=> "South America",
            "FM"=> "Australia",
            "FO"=> "Europe",
            "FR"=> "Europe",
            "GA"=> "Africa",
            "GB"=> "Europe",
            "GD"=> "North America",
            "GE"=> "Asia",
            "GF"=> "South America",
            "GG"=> "Europe",
            "GH"=> "Africa",
            "GI"=> "Europe",
            "GL"=> "North America",
            "GM"=> "Africa",
            "GN"=> "Africa",
            "GP"=> "North America",
            "GQ"=> "Africa",
            "GR"=> "Europe",
            "GS"=> "Antarctica",
            "GT"=> "North America",
            "GU"=> "Australia",
            "GW"=> "Africa",
            "GY"=> "South America",
            "HK"=> "Asia",
            "HN"=> "North America",
            "HR"=> "Europe",
            "HT"=> "North America",
            "HU"=> "Europe",
            "ID"=> "Asia",
            "IE"=> "Europe",
            "IL"=> "Asia",
            "IM"=> "Europe",
            "IN"=> "Asia",
            "IO"=> "Asia",
            "IQ"=> "Asia",
            "IR"=> "Asia",
            "IS"=> "Europe",
            "IT"=> "Europe",
            "JE"=> "Europe",
            "JM"=> "North America",
            "JO"=> "Asia",
            "JP"=> "Asia",
            "KE"=> "Africa",
            "KG"=> "Asia",
            "KH"=> "Asia",
            "KI"=> "Australia",
            "KM"=> "Africa",
            "KN"=> "North America",
            "KP"=> "Asia",
            "KR"=> "Asia",
            "KW"=> "Asia",
            "KY"=> "North America",
            "KZ"=> "Asia",
            "LA"=> "Asia",
            "LB"=> "Asia",
            "LC"=> "North America",
            "LI"=> "Europe",
            "LK"=> "Asia",
            "LR"=> "Africa",
            "LS"=> "Africa",
            "LT"=> "Europe",
            "LU"=> "Europe",
            "LV"=> "Europe",
            "LY"=> "Africa",
            "MA"=> "Africa",
            "MC"=> "Europe",
            "MD"=> "Europe",
            "ME"=> "Europe",
            "MG"=> "Africa",
            "MH"=> "Australia",
            "MK"=> "Europe",
            "ML"=> "Africa",
            "MM"=> "Asia",
            "MN"=> "Asia",
            "MO"=> "Asia",
            "MP"=> "Australia",
            "MQ"=> "North America",
            "MR"=> "Africa",
            "MS"=> "North America",
            "MT"=> "Europe",
            "MU"=> "Africa",
            "MV"=> "Asia",
            "MW"=> "Africa",
            "MX"=> "North America",
            "MY"=> "Asia",
            "MZ"=> "Africa",
            "NA"=> "Africa",
            "NC"=> "Australia",
            "NE"=> "Africa",
            "NF"=> "Australia",
            "NG"=> "Africa",
            "NI"=> "North America",
            "NL"=> "Europe",
            "NO"=> "Europe",
            "NP"=> "Asia",
            "NR"=> "Australia",
            "NU"=> "Australia",
            "NZ"=> "Australia",
            "OM"=> "Asia",
            "PA"=> "North America",
            "PE"=> "South America",
            "PF"=> "Australia",
            "PG"=> "Australia",
            "PH"=> "Asia",
            "PK"=> "Asia",
            "PL"=> "Europe",
            "PM"=> "North America",
            "PN"=> "Australia",
            "PR"=> "North America",
            "PS"=> "Asia",
            "PT"=> "Europe",
            "PW"=> "Australia",
            "PY"=> "South America",
            "QA"=> "Asia",
            "RE"=> "Africa",
            "RO"=> "Europe",
            "RS"=> "Europe",
            "RU"=> "Europe",
            "RW"=> "Africa",
            "SA"=> "Asia",
            "SB"=> "Australia",
            "SC"=> "Africa",
            "SD"=> "Africa",
            "SE"=> "Europe",
            "SG"=> "Asia",
            "SH"=> "Africa",
            "SI"=> "Europe",
            "SJ"=> "Europe",
            "SK"=> "Europe",
            "SL"=> "Africa",
            "SM"=> "Europe",
            "SN"=> "Africa",
            "SO"=> "Africa",
            "SR"=> "South America",
            "ST"=> "Africa",
            "SV"=> "North America",
            "SY"=> "Asia",
            "SZ"=> "Africa",
            "TC"=> "North America",
            "TD"=> "Africa",
            "TF"=> "Antarctica",
            "TG"=> "Africa",
            "TH"=> "Asia",
            "TJ"=> "Asia",
            "TK"=> "Australia",
            "TM"=> "Asia",
            "TN"=> "Africa",
            "TO"=> "Australia",
            "TR"=> "Asia",
            "TT"=> "North America",
            "TV"=> "Australia",
            "TW"=> "Asia",
            "TZ"=> "Africa",
            "UA"=> "Europe",
            "UG"=> "Africa",
            "US"=> "North America",
            "UY"=> "South America",
            "UZ"=> "Asia",
            "VC"=> "North America",
            "VE"=> "South America",
            "VG"=> "North America",
            "VI"=> "North America",
            "VN"=> "Asia",
            "VU"=> "Australia",
            "WF"=> "Australia",
            "WS"=> "Australia",
            "YE"=> "Asia",
            "YT"=> "Africa",
            "ZA"=> "Africa",
            "ZM"=> "Africa",
            "ZW"=> "Africa",
        );


/*
 * Redux framework
 */
define('REDUX_CORE_FILE_PATH', TEMPLATE_DIRECTORY . '/admin/ReduxCore/framework.php');
define('REDUX_CONFIG_FILE_PATH', TEMPLATE_DIRECTORY . '/admin/admin-config.php');
if( !class_exists( 'Redux' ) && file_exists( REDUX_CORE_FILE_PATH ) ) {
    require_once( REDUX_CORE_FILE_PATH );
    require_once( REDUX_CONFIG_FILE_PATH );
}

include_once TEMPLATE_DIRECTORY . '/admin/includes/class-sls-walker-nav-menu.php';
include_once TEMPLATE_DIRECTORY . '/admin/includes/class-sls-login-menu-navwalker.php';
include_once TEMPLATE_DIRECTORY . '/admin/includes/class-sls-sidebar.php';
include_once TEMPLATE_DIRECTORY . '/admin/includes/class-sls-walker-before-top-nav-menu.php';

//include_once TEMPLATE_DIRECTORY . '/include/acf.php';




new SLS_Sidebars();

function typical_title() {
	global $page, $paged; 
	wp_title('', true, 'right');
	$site_description = get_bloginfo('description', 'display');
	if ($site_description && (is_home() || is_front_page())) echo " | $site_description";
	if ($paged >= 2 || $page >= 2) echo ' | '.sprintf(__( 'Страница %s'), max($paged, $page));
}

register_nav_menus(array(
	'top' => __('Top', 'sls-theme'),
	'bottom' => __('Bottom', 'sls-theme'),
	'before-top' => __('Before top', 'sls-theme'),
    'login-top' => __('Login top', 'sls-theme')
));

add_theme_support('post-thumbnails');
set_post_thumbnail_size(250, 150);
add_image_size('big-thumb', 400, 400, true);

class clean_comments_constructor extends Walker_Comment {
	public function start_lvl( &$output, $depth = 0, $args = array()) {
		$output .= '<ul class="children">' . "\n";
	}
	public function end_lvl( &$output, $depth = 0, $args = array()) {
		$output .= "</ul><!-- .children -->\n";
	}
    protected function comment( $comment, $depth, $args ) {
    	$classes = implode(' ', get_comment_class()).($comment->comment_author_email == get_the_author_meta('email') ? ' author-comment' : '');
        echo '<li id="comment-'.get_comment_ID().'" class="'.$classes.' media">'."\n";
    	echo '<div class="media-left">'.get_avatar($comment, 64, '', get_comment_author(), array('class' => 'media-object'))."</div>\n";
    	echo '<div class="media-body">';
    	echo '<span class="meta media-heading">Автор: '.get_comment_author()."\n";
    	//echo ' '.get_comment_author_email();
    	echo ' '.get_comment_author_url();
    	echo ' Добавлено '.get_comment_date('F j, Y в H:i')."\n";
    	if ( '0' == $comment->comment_approved ) echo '<br><em class="comment-awaiting-moderation">Ваш комментарий будет опубликован после проверки модератором.</em>'."\n";
    	echo "</span>";
        comment_text()."\n";
        $reply_link_args = array(
        	'depth' => $depth,
        	'reply_text' => 'Ответить',
			'login_text' => 'Вы должны быть залогинены'
        );
        echo get_comment_reply_link(array_merge($args, $reply_link_args));
        echo '</div>'."\n";
    }
    public function end_el( &$output, $comment, $depth = 0, $args = array() ) {
		$output .= "</li><!-- #comment-## -->\n";
	}
}

function pagination() {
	global $wp_query;
	$big = 999999999;
	$links = paginate_links(array(
		'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))),
		'format' => '?paged=%#%',
		'current' => max(1, get_query_var('paged')),
		'type' => 'array',
		'prev_text'    => __('Prev', 'sls-theme'),
    	'next_text'    => __('Next', 'sls-theme'),
		'total' => $wp_query->max_num_pages,
		'show_all'     => false,
		'end_size'     => 15,
		'mid_size'     => 15,
		'add_args'     => false,
		'add_fragment' => '',
		'before_page_number' => '',
		'after_page_number' => ''
	));
 	if( is_array( $links ) ) {
	    echo '<ul class="pagination">';
	    foreach ( $links as $link ) {
	    	if ( strpos( $link, 'current' ) !== false ) echo "<li class='active'>$link</li>";
	        else echo "<li>$link</li>"; 
	    }
	   	echo '</ul>';
	 }
}

add_action('wp_footer', 'add_scripts');
function add_scripts() {
    if(is_admin()) return false;
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery','//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js','','',true);
	wp_enqueue_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js','','',true);
	wp_enqueue_script('polyfiller', get_template_directory_uri().'/js/polyfiller.min.js', array('jquery'),'',true);
	wp_enqueue_script('cookies', get_template_directory_uri().'/js/jquery.cookie.js', array('jquery'),'',true);
	wp_enqueue_script('geocodeByGoogle', get_template_directory_uri().'/js/geocodeByGoogle.js', array('jquery'),'',true);
	wp_enqueue_script('lightslider', get_template_directory_uri().'/js/lightslider.js','','',true);
	wp_enqueue_script('wow', get_template_directory_uri().'/js/wow.min.js', array('jquery'),'',true);
	wp_enqueue_script('owl', get_template_directory_uri().'/js/owl.carousel.min.js', array('jquery'),'',true);
	wp_enqueue_script('main', get_template_directory_uri().'/js/main.js', array('jquery'),'',true);
	wp_enqueue_script('heapbox', get_template_directory_uri().'/js/heapbox.js','','',true);
	wp_enqueue_script('jqueryArcticmodal', get_template_directory_uri().'/js/jquery.arcticmodal-0.3.min.js','','',true);
	wp_enqueue_script('jqueryForm', get_template_directory_uri().'/js/jquery.form.js','','',true);
	wp_enqueue_script('mailajax', get_template_directory_uri().'/js/mailajax.js','','',true);
	wp_enqueue_script('jqueryUi', '//code.jquery.com/ui/1.11.4/jquery-ui.min.js','','',true);
	wp_enqueue_script('jquery.select-to-autocomplete', get_template_directory_uri().'/js/jquery.select-to-autocomplete.js','','',true);
	wp_enqueue_script('country-select', get_template_directory_uri().'/js/country-select.js','','',true);
    	wp_enqueue_script('auth', get_template_directory_uri().'/js/login.js', array('jquery'),'',true);
 	wp_enqueue_script('custom', get_template_directory_uri().'/js/custom.js','','',true);
    /*wp_enqueue_script(
		'google-map',
		"https://maps.googleapis.com/maps/api/js?key=AIzaSyA55HS4C36lVMwdXSHgeY-BiVwcsfvsa20"
	);*/
	wp_enqueue_script('underscore');
	
    
    
	//if(is_taxonomy( "zones" )|| is_page("anti-aging-centers")) {
	//	wp_enqueue_script('centers', get_template_directory_uri().'/js/centers.js',array('jquery'),'',true);
	//	wp_enqueue_script('googlemap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA55HS4C36lVMwdXSHgeY-BiVwcsfvsa20&callback=initMap','','',true);
	//}
}

add_action('wp_print_styles', 'add_styles');
function add_styles() {
    if(is_admin()) return false;
    wp_enqueue_style( 'bs', get_template_directory_uri().'/css/bootstrap.min.css' );
	wp_enqueue_style( 'ls', get_template_directory_uri().'/css/lightslider.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri().'/css/animate.css' );
	wp_enqueue_style( 'owl', get_template_directory_uri().'/css/owl.carousel.css' );
	wp_enqueue_style( 'main', get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'jqueryArcticmodal', get_template_directory_uri().'/css/jquery.arcticmodal-0.3.css' );
	wp_enqueue_style( 'custom', get_template_directory_uri().'/css/custom.css' );
}

function content_class_by_sidebar() {
    if (is_active_sidebar( 'sidebar' )) {
        echo 'col-sm-9';
    } else {
        echo 'col-sm-12';
    }
}

function get_permalink_current_language( $post_id )
{
	$language = ICL_LANGUAGE_CODE;

    $lang_post_id = icl_object_id( $post_id , 'page', true, $language );

    $url = "";
    if($lang_post_id != 0) {
        $url = get_permalink( $lang_post_id );
    }else {
        // No page found, it's most likely the homepage
        global $sitepress;
        $url = $sitepress->language_url( $language );
    }

    return $url;
}

add_action( 'after_setup_theme', 'register_footer_nav_menu' );
function register_footer_nav_menu() {
    register_nav_menu( 'footer-horizontal', __('Footer Horizontal Menu', 'sls-theme'));
}

function sls_mime_types( $mimes ) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'sls_mime_types');



//Adding the necessary actions
add_action('init', 'sls_register_team_manager' );

//register the custom post type for the team manager
function sls_register_team_manager() {

	$labels = array(
		'name' => _x( 'Team', 'sls-theme' ),
		'singular_name' => _x( 'Team Member', 'sls-theme' ),
		'add_new' => _x( 'Add New Member', 'sls-theme' ),
		'add_new_item' => _x( 'Add New ', 'sls-theme' ),
		'edit_item' => _x( 'Edit Team Member ', 'sls-theme' ),
		'new_item' => _x( 'New Team Member', 'sls-theme' ),
		'view_item' => _x( 'View Team Member', 'sls-theme' ),
		'search_items' => _x( 'Search Team Members', 'sls-theme' ),
		'not_found' => _x( 'Not found any Team Member', 'sls-theme' ),
		'not_found_in_trash' => _x( 'No Team Member found in Trash', 'sls-theme' ),
		'parent_item_colon' => _x( 'Parent Team Member:', 'sls-theme' ),
		'menu_name' => _x( 'Team', 'sls-theme' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array( 'title', 'thumbnail','editor','page-attributes'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-groups',
		'rewrite' => array( 'slug' => 'teams' )

	);

	register_post_type( 'team_member', $args );

	//register custom category for the team manager

	$labels = array(
		'name'                       => _x( 'Groups', 'sls-theme' ),
		'singular_name'              => _x( 'Group', 'sls-theme' ),
		'search_items'               => _x( 'Search Groups', 'sls-theme' ),
		'popular_items'              => _x( 'Popular Groups', 'sls-theme' ),
		'all_items'                  => _x( 'All Groups', 'sls-theme' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => _x( 'Edit Group', 'sls-theme' ),
		'update_item'                => _x( 'Update Group', 'sls-theme' ),
		'add_new_item'               => _x( 'Add New Group', 'sls-theme' ),
		'new_item_name'              => _x( 'New Group Name', 'sls-theme' ),
		'separate_items_with_commas' => _x( 'Separate Groups with commas', 'sls-theme' ),
		'add_or_remove_items'        => _x( 'Add or remove Groups', 'sls-theme' ),
		'choose_from_most_used'      => _x( 'Choose from the most used Groups', 'sls-theme' ),
		'not_found'                  => _x( 'No Groups found.', 'sls-theme' ),
		'menu_name'                  => _x( 'Team Groups', 'sls-theme' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'team-groups' ),
	);

	register_taxonomy( 'team_groups', 'team_member', $args );

}


//Adding the necessary actions
add_action('init', 'sls_register_locations' );

//register the custom post type for the team manager
function sls_register_locations() {

	$labels = array(
		'name' => _x( 'Locations', 'sls-theme' ),
		'singular_name' => _x( 'Location', 'sls-theme' ),
		'add_new' => _x( 'Add New Location', 'sls-theme' ),
		'add_new_item' => _x( 'Add New ', 'sls-theme' ),
		'edit_item' => _x( 'Edit Location ', 'sls-theme' ),
		'new_item' => _x( 'New Location', 'sls-theme' ),
		'view_item' => _x( 'View Location', 'sls-theme' ),
		'search_items' => _x( 'Search Locations', 'sls-theme' ),
		'not_found' => _x( 'Not found any Location', 'sls-theme' ),
		'not_found_in_trash' => _x( 'No Location found in Trash', 'sls-theme' ),
		'parent_item_colon' => _x( 'Parent Location:', 'sls-theme' ),
		'menu_name' => _x( 'Locations', 'sls-theme' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array( 'title', 'thumbnail','editor','page-attributes'),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-location-alt',
		'rewrite' => array( 'slug' => 'locations' )

	);

	register_post_type( 'locations', $args );

}

include_once TEMPLATE_DIRECTORY . '/widgets/sls-socials.php';
include_once TEMPLATE_DIRECTORY . '/widgets/class-sls-nav-menu-widget.php';

add_action( 'widgets_init', 'sls_widgets_init' );
function sls_widgets_init() {

	register_widget( 'SLS_Widget_Socials' );
	register_widget( 'SLS_Nav_Menu_Widget' );

}

function get_centers($args = array()) {

	global $sls_data;

	$centers_url = $sls_data['iddna_centers_api_url'] . '?filter[posts_per_page]=-1';

	$args = array('per_page' => '-1');

	$request = wp_remote_get($centers_url, $args);

	if( $request instanceof WP_Error) {
		//$request->get_error_messages();
		return array();
	} else {
		$data = json_decode($request['body']);
	}

	return $data;
}
add_action( 'wp_ajax_sls_find_centers', 'sls_find_centers');
add_action( 'wp_ajax_nopriv_sls_find_centers', 'sls_find_centers');

function sls_find_centers() {

	global $sls_data;

	$search = $_REQUEST['search'];

	$country = Seven_Geolocation::geolocate_ip( '', true, true );

	$response = wp_remote_get( $sls_data['iddna_ajax_api_url'] . "?action=find_center_by_czc&search=" . $search . '&country='.$country );

	$centers = json_decode($response['body']);

	$markers = array();
	foreach ( $centers as $center ) {
		//  var_dump($center);
		$markers[] = array(
			'id' => $center->ID,
			'location' => $center->acf->location,
			'company_name' => $center->post_title,
			'company_phone' => $center->acf->company_phone,
			'city' => $center->acf->city,
			'zip' => $center->acf->zip,
			'state' => $center->acf->state,
			'address' => $center->acf->address,
			'role' => $center->acf->role,
			'country' => $center->acf->country
		);
	}

	echo json_encode( array( 'centers' => $markers ) );
	exit;
}


add_action( 'wp_ajax_sls_find_distributors', 'sls_find_distributors');
add_action( 'wp_ajax_nopriv_sls_find_distributors', 'sls_find_distributors');

function sls_find_distributors() {

	global $sls_data;

	$search = $_REQUEST['search'];

	$country = Seven_Geolocation::geolocate_ip( '', true, true );

	$response = wp_remote_get( $sls_data['iddna_ajax_api_url'] . "?action=find_center_by_czc&search=" . $search . '&country='.$country );

	$centers = json_decode($response['body']);

	$markers = array();
	foreach ( $centers as $center ) {

		if ($center->acf->role == 'Reseller') {
			continue;
		}
		//  var_dump($center);
		$markers[] = array(
			'id' => $center->ID,
			'location' => $center->acf->location,
			'company_name' => $center->post_title,
			'company_phone' => $center->acf->company_phone,
			'city' => $center->acf->city,
			'zip' => $center->acf->zip,
			'state' => $center->acf->state,
			'address' => $center->acf->address,
			'role' => $center->acf->role,
			'country' => $center->acf->country
		);
	}

	echo json_encode( array( 'centers' => $markers ) );
	exit;
}

function prefix_movie_rewrite_rule() {
	add_rewrite_rule('centers/?([^/]*)', 'index.php?pagename=centers&center=$matches[1]', 'top');
}
add_action( 'init', 'prefix_movie_rewrite_rule' );

function prefix_register_query_var( $vars ) {
	$vars[] = 'center';

	return $vars;
}

add_filter( 'query_vars', 'prefix_register_query_var', 10, 1 );


function products_plugin_display() {

	$pagename = get_query_var('pagename');
	$search_input = get_query_var('center');
	if ('centers' == $pagename ) {
		return TEMPLATE_DIRECTORY . '/template-page-centers.php';
	}

}

//register plugin custom pages display
add_filter('template_redirect', 'products_plugin_display');

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme Forms',
		'menu_title'	=> 'Theme Forms',
		'menu_slug' 	=> 'theme-forms',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


function get_secure_username() {
    $username = get_username_from_cookie($_COOKIE['secure_iddna_login']);

    return $username;
}


function get_username_from_cookie( $cookie, $divider = '|' ) {

    if( empty( $cookie ) ) {
        return false;
    }

    $cookie_elements = explode($divider, $cookie);
    if ( count( $cookie_elements ) !== 4 ) {
        return false;
    }

    return $cookie_elements[0];
}


function woo_cart_quantity_user() {

    $args = [
        'store_url' => get_option('woo_secure_iddna_url')	,
        'consumer_key' => get_option('woo_secure_iddna_consumer_key'),
        'consumer_secret' => get_option('woo_secure_iddna_consumer_secret')
    ];

    $woocommerce = new Automattic\WooCommerce\Client(
        $args['store_url'],
        $args['consumer_key'],
        $args['consumer_secret'],
        [
            'version' => 'wc/v1',
            'wp_api' => true
        ]
    );

    //$woocommerce_session_name = 'wp_woocommerce_session_' . md5( get_option('secure_iddna_url') );

    if ( ! isset($_COOKIE['session_cart_user'] ) ) {
        return [];
    }

    $user_name = $_COOKIE['session_cart_user'];

    try {
        $_response = $woocommerce->get("cartquantity", [
            'username' => $user_name
        ]);
    } catch ( Exception $e ) {

        return [];
    }

    return $_response;
}

function woo_cart_quantity_user_res() {

    $res = woo_cart_quantity_user();

    if ( empty( $res) ) {
        return 0;
    }

    $res_decode = unserialize($res['cart']);

    $res_qua = array();

    foreach($res_decode as $res_q){
        $res_qua[] = $res_q['quantity'];
    }

    $quantity = array_sum($res_qua);

    return $quantity;

}

if ( !function_exists( 'is_theme_plugin_active' ) ) {
    function is_theme_plugin_active( $plugin ) {
        return in_array($plugin, (array)get_option('active_plugins', array()));
    }
}

 // ------------------------------------------------------------------
 // Add all your sections, fields and settings during admin_init
 // ------------------------------------------------------------------
 //
 
 function api_settings_api_init() {
 	// Add the section to reading settings so we can add our
 	// fields to it
 	add_settings_section(
		'api_setting_section',
		'API Section',
		'api_setting_section_callback_function',
		'reading'
	);
 	
 	// Add the field with the names and function to use for our new
 	// settings, put it in our new section
 	add_settings_field(
		'api_iddna_url',
		'Url for IDDNA server',
		'eg_setting_callback_function',
		'reading',
		'api_setting_section'
	);
 	
 	// Register our setting so that $_POST handling is done for us and
 	// our callback function just has to echo the <input>
 	register_setting( 'reading', 'api_iddna_url' );
 } // api_settings_api_init()
 
 add_action( 'admin_init', 'api_settings_api_init' );
 
  
 // ------------------------------------------------------------------
 // Settings section callback function
 // ------------------------------------------------------------------
 //
 // This function is needed if we added a new section. This function 
 // will be run at the start of our section
 //
 
 function api_setting_section_callback_function() {
 	echo '<p>Intro text for our settings section</p>';
 }
 
 // ------------------------------------------------------------------
 // Callback function for our example setting
 // ------------------------------------------------------------------
 //
 // creates a checkbox true/false option. Other types are surely possible
 //
 
 function eg_setting_callback_function() {
    echo '<input type="text" name="api_iddna_url" value="' . esc_attr( get_option('api_iddna_url') ) . '" />';
 }
// =================================
// Thank you short code
// =================================
function thank_you_short_code()
{
    if (isset($_GET['source']) && $_GET['source']) {
        $source_key = $_GET['source'];
    } else {
        $source_key = 'form_11_thank_you_html';
    }

    $content = get_field($source_key, 'option');

    echo $content;
}

add_shortcode('thank-you', 'thank_you_short_code');
// =================================
// End thank you short code
// =================================

function woo_store_available_countries() {

	$args = [
		'store_url' => get_option('woo_secure_iddna_url')	,
		'consumer_key' => get_option('woo_secure_iddna_consumer_key'), //'ck_4cceaf150ebf1eb29a011f7cf5703866b7d8545a',
		'consumer_secret' => get_option('woo_secure_iddna_consumer_secret') //'cs_b9dc4b9ab60e21fce9cd6cc3579f1a6d4681c142',
	];

	$woocommerce = new Automattic\WooCommerce\Client(
		$args['store_url'],
		$args['consumer_key'],
		$args['consumer_secret'],
		[
			'version' => 'wc/v1',
			'wp_api' => true,
			'timeout'     => 45,
		]
	);

	try {
		$_response = $woocommerce->get("store");
	} catch ( Exception $e ) {
		// wait to long...
		return [];
	}

	return $_response['countries'];
}

function is_store_available() {

	global $is_iddna_store_available;

	if ( isset($is_iddna_store_available) ) {
		return $is_iddna_store_available;
	}

        $countries = woo_store_available_countries();
        
        if($_COOKIE['test_country']){
            $current_country = $_COOKIE['test_country'];
        }else{
            $current_country = Seven_Geolocation::geolocate_ip( '', true, true );  
        }

	if ( in_array( $current_country, $countries ) ) {
		$is_iddna_store_available = true;
		return true;
	}
	$is_iddna_store_available = false;

	return false;
}

function custom_highlight_text($str, $keywords = array()) {
    $strLen = mb_strlen($keywords[0]) + 500;

    $str = strip_tags($str);
    $position = stripos($str, $keywords[0]);
    $position = ($position < 200) ? 0 : $position - 200;
    $str = mb_substr($str, $position, $strLen);
    if ($position > 0) $str = '...' . $str;
    foreach ($keywords as $keyword) {
        $str = preg_replace("/\p{L}*?".preg_quote($keyword)."\p{L}*/ui", "<strong>$0</strong>", $str);
    }

    return $str;
}

function get_products_from_store( $product_barcodes = [] ) {

	if ( empty($product_barcodes) ) {
		return false;
	}

	$args = [
		'store_url' => get_option('woo_secure_iddna_url'),
		'consumer_key' => get_option('woo_secure_iddna_consumer_key'), //'ck_4cceaf150ebf1eb29a011f7cf5703866b7d8545a',
		'consumer_secret' => get_option('woo_secure_iddna_consumer_secret') //'cs_b9dc4b9ab60e21fce9cd6cc3579f1a6d4681c142',
	];

	$woocommerce = new Automattic\WooCommerce\Client(
		$args['store_url'],
		$args['consumer_key'],
		$args['consumer_secret'],
		[
			'version' => 'wc/v1',
			'wp_api' => true,
			'timeout'     => 45,
		]
	);

	$parameters = [
		'barcodes' => implode( ',', $product_barcodes )
	];

	if ( isset($_COOKIE['secure_iddna_login'])) {
		$username = get_username_from_cookie($_COOKIE['secure_iddna_login']);

		if (!empty($username)) {
			$parameters['username'] = $username;
		}
	}

	$country_code = Seven_Geolocation::geolocate_ip( '', true, true );

	$parameters['country_code'] = $country_code;
	if ( isset($_COOKIE['test_country']) ) {
		$parameters['test_country'] = $_COOKIE['test_country'];
	}

	try {
		$_response = $woocommerce->get("products/", $parameters);
	} catch ( Exception $e ) {
		//to long waiting...
		//var_dump( $e );
		return [];
	}

	return $_response;
}


function get_country_name_by_code($country_code)
{       
     if(empty($country_code)){
       return ""; 
     }
    global $global_center_countries;
    if(array_key_exists($country_code, $global_center_countries)){
        return $global_center_countries[$country_code];
    } else {
        return "";
    }    
}


function get_country_zone_by_code($country_code)
{    
    if(empty($country_code)){
       return ""; 
    }
    global $global_center_zones;
    if(array_key_exists($country_code, $global_center_zones)){
       return $global_center_zones[$country_code];
    } else {
        return "";
    }    
    
}

function get_country_zone_by_name($country_name)
{         
    if(empty($country_name)){
       return ""; 
    }
    global $global_center_countries;
    $country_code = array_search(strtolower($country_name), array_map('strtolower',$global_center_countries));
    $country_zone = get_country_zone_by_code($country_code);    
    return $country_zone;
        
}

function get_zone_countries($country_zone){
    
     $zone_countries = array();
     if(empty($country_zone)){
       return $zone_countries; 
     }
     global $global_center_zones;
     global $global_center_countries;
       
        foreach ($global_center_zones as $country_code=>$zone){  
            if($country_zone == $zone){
                $country_name = $global_center_countries[$country_code];
                if($country_name){
                    $zone_countries[] = $country_name;
                } 
            }           
        }        
      return $zone_countries;
}

