<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Widget: Socials
 *
 * Class SLS_Widget_Socials
 */
class SLS_Widget_Socials extends WP_Widget {

    public function __construct() {

        $this->fields = array(
            'facebook' => array(
                'heading' => 'Facebook',
                'std' => '',
            ),
            'twitter' => array(
                'type' => 'textfield',
                'heading' => 'Twitter',
                'std' => '',
            ),
            'instagram' => array(
                'heading' => 'Instagram',
                'std' => '',
            ),
            'pinterest' => array(
                'heading' => 'Pinterest',
                'std' => '',
            ),
            'linkedin' => array(
                'heading' => 'LinkedIn',
                'std' => '',
            ),
            'youtube' => array(
                'heading' => 'Youtube',
                'std' => '',
            ),
            'google-plus' => array(
                'heading' => 'Google-plus',
                'std' => '',
            )
        );

        $widget_ops = array(
            'classname' => 'widget_socials',
            'description' => __( 'Social Links', 'sls-theme' ),
            'customize_selective_refresh' => true,

        );
        $control_ops = array( 'width' => 400, 'height' => 350 );
        parent::__construct( 'socials-links', __( '(SLS) Social Links', 'sls-theme' ), $widget_ops, $control_ops );
    }

    public function widget( $args, $instance ) {

       // $title = empty( $instance['title'] ) ? '' : $instance['title'];

        echo $args['before_widget'];
       // if ( ! empty( $title ) ) {
       //     echo $args['before_title'] . $title . $args['after_title'];
      //  } ?>
        <div class="footer_soc_block text-center">
            <?php foreach($this->fields as $key => $field) : ?>
                <div class="social-icon social-big"><a href="<?php echo $instance[$key]; ?>" target="_blank" title=""><i class="fa fa-<?php echo $key?>"></i></a></div>
            <?php endforeach; ?>
        </div>
        <?php
        echo $args['after_widget'];
    }


    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        //if ( current_user_can( 'unfiltered_html' ) ) {
        //    $instance['text'] = $new_instance['text'];
        //} else {
        //    $instance['text'] = wp_kses_post( $new_instance['text'] );
        //}
        //$instance['filter'] = ! empty( $new_instance['filter'] );

        foreach($this->fields as $key => $field) {
            $instance[$key] = $new_instance[$key];
        }

        return $instance;
    }

    public function form( $instance ) {

        $default_values = array('title' => '');
        foreach($this->fields as $key => $field) {
            $default_values[$key] = $field['std'];
        }

        $instance = wp_parse_args( (array) $instance, $default_values );
        //$filter = isset( $instance['filter'] ) ? $instance['filter'] : 0;
        $title = sanitize_text_field( $instance['title'] );

        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
        <?php

        foreach($this->fields as $key => $field) {
            ?>
            <p><label for="<?php echo $this->get_field_id($key); ?>"><?php _e($field['heading']); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id($key); ?>" name="<?php echo $this->get_field_name($key); ?>" type="text" value="<?php echo esc_attr($instance[$key]); ?>" /></p>
            <?php
        }
    }
}

