<?php /* Template Name: Science */ ?>
<?php get_header();

$fields = get_fields( get_the_ID() );

wp_enqueue_script(
  'touchSwipe',
  get_template_directory_uri() . "/js/jquery.touchSwipe.min.js",
  array('jquery'),
  false,
  true
  );

  ?>

  <div class="science-page">
    <!-- section 1 -->
    <section class="sls-baner mobile-baner" style="background-image: url('<?php echo get_template_directory_uri()?>/img/science-baner-bg.jpg');">
      <img class="mobile-baner-img" src="<?php echo get_template_directory_uri()?>/img/science-banner.jpg" alt="">
      <div class="container">
        <h2>Anti-aging <span>science</span></h2>
        <div class="baner-white-block">
          <h3><short>i</short>DDNA<sup>®</sup> is age management at a genetic level:</h3>
          <ul>
            <li>Safe...</li>
            <li>Painless ...</li>
            <li>non-invasive ...</li>
            <li>regenerative  ...</li>
            <li>long-lasting, natural results ...</li>
          </ul>
        </div>
      </div>
    </section>

    <!-- section 2 -->
    <section>
      <div class="container">
        <h2 class="title-h2">Your dna is your cellular identity</h2>
        <p>
          <short>i</short>DDNA<sup>®</sup> is an exclusive new Swiss anti-aging technology, and the culmination of    almost a decade of international research and development, in the field of biomedical, genetical and cellular age-management therapies. By understanding the molecular basis of your aging, you can now unlock your full anti-aging potential with natural, biologically active treatments.
        </p>
        <div class="identity-animation">
          <?php echo file_get_contents('wp-content/themes/sls/img/HIW_info.svg'); ?>
        </div>
      </div>
    </section>
    <!-- section 3 -->
    <section class="bg-grey">
      <div class="container">
        <div class="clients-fingertips">
          <div class="from-to">
            <div class="from">
              <img src="<?php echo get_template_directory_uri();?>/img/ic_man.png" alt="">
              <div class="from--text">
                <h2 class="title-h2">From</h2>
                <p>The power of </p>
                <p>human genetic divercity</p>
                <img src="<?php echo get_template_directory_uri();?>/img/ic_arr_down.png" alt="" class="arrow-down">
              </div>
            </div>
            <div class="to">
              <img src="<?php echo get_template_directory_uri();?>/img/ic_phone_dna.png" alt="">
              <div class="to--text">
                <h2 class="title-h2">To</h2>
                <p>premium luxury </p>
                <p>artificial intelligence </p>
                <p>refined used experience</p>
              </div>
            </div>
          </div>
          <img src="<?php echo get_template_directory_uri();?>/img/mac_iphone.png" alt="" class="mac-image">
          <div class="fingerprints">
            <h2 class="title-h2">
              Everything <span>at your</span> <span>client's</span> <span>fingerprints</span>
            </h2>
            <ul class="blue-list-rect">
              <li>Natural</li>
              <li>Non-invasive</li>
              <li>Regenerative</li>
              <li>Long-lasting results</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- section 4 -->
    <section>
      <div class="container">
        <div class="row chinese">
          <div class="col-sm-6">
            <img src="<?php echo get_template_directory_uri();?>/img/chinese-img1.jpg" alt="">
          </div>
          <div class="col-sm-6">
            <img src="<?php echo get_template_directory_uri();?>/img/chinese-img2.jpg" alt="">
          </div>
        </div>
      </div>
    </section>
    <?php /* <section  class="mobile_baner baner_h630" style="background: url('<?php echo   $fields['section_3_background_image']; ?>'); background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-lg-offset-6 col-md-8 col-md-offset-4 col-sm-12 col-smoffset-0">
            <h2 class="f-38 m-t-0 text-uppercase font-normal m-b-30 letter-spacing"><?php echo $fields['section_3_title']; ?></h2>
            <?php echo $fields['section_3_description']; ?>
          </div>
        </div>
      </div>
    </section> */ ?>

    <!-- section 5 -->
    <section style="background-image: url('<?php echo $fields['section_4_background_image']; ?>'); background-size: cover;" class="experience">
      <div class="container">
        <h2 class="title-h2 text-white">
          <short>i</short>DDNA<sup>®</sup> is the first-ever epigenetic
          <span>anti-aging program <b>made to measure</b></span>
          <span><b>from DNA</b> designed to work on:</span>
        </h2>
        <ul class="blue-list-rect text-white">
          <li>TELOMERE PROTECTION</li>
          <li>SIRTUINS</li>
          <li>PROGERIN</li>
          <li>DNA PROTECTION</li>
          <li>DNA REPAIR</li>
        </ul>
      </div>

    </section>
    <?php /*<section style="background-image: url('<?php echo $fields['section_4_background_image']; ?>'); background-size: cover;" class="experience">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="font-normal text-center letter-spacing m-t-0"><?php echo $fields['section_4_title']; ?></h2>
            <h3 class="bold text-uppercase text-center"><?php echo $fields['section_4_description_1']; ?></h3>
            <h3 class="font-light text-uppercase letter-spacing m-b-0 m-t-60 text-center"><?php echo $fields['section_4_title_2']; ?></h3>
            <div class="col-sm-12">
              <?php $benefits = $fields['section_4_benefits']; ?>
              <?php foreach( $benefits as $benefit ) :?>
                <div class="col-sm-3 m-t-80">
                  <img class="center-block" src="<?php echo $benefit['image']; ?>" alt="<?php echo $benefit['image_alt']; ?>">
                  <?php echo $benefit['description']; ?>
                </div>
              <?php endforeach; ?>
            </div>
            <div class="col-sm-12">
              <p class="m-t-60 text-center"><?php echo $fields['section_4_description_2']; ?></p>
            </div>
          </div>
        </div>
      </div>
    </section> */?>

    <!-- section 6 -->
    <section>
      <div class="container">
        <div class="own-genes">
          <h2 class="title-h2">
            OWN YOUR GENES WITH YOUR <short>i</short>DDNA<sup>®</sup>:
          </h2>
          <ul class="blue-list-rect">
            <li><p>A natural (biologically active), non-invasive, regenerative solution to counteract cell senescence at a molecular level</p></li>
            <li><p>A super-premium set-and-forget anti-aging: it learns and evolves from the client’s epigenetics adapting the following protocol to threats that you do not need to see.</p></li>
          </ul>
        </div>
        <div class="dna-swiss">
          <h2 class="title-h2">DNA: a discovery</h2>
          <p>Many believe that American biologist James Watson and English physicist Francis Crick discovered DNA in the 1950s. In reality, DNA was first identified in the late 1860s by Swiss chemist Friedrich Miescher.</p>
          <div class="dna-swiss--timeline">
            <div class="bottom">
              <span>
                <span class="date">1860</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/fra.png" alt="">
                <p>DNA first indetified</p>
              </div>
            </div>
            <div class="top">
              <span>
                <span class="date">1930</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/fra.png" alt="">
                <p>Swiss cell theraphy</p>
              </div>
            </div>
            <div class="bottom long">
              <span>
                <span class="date">1962</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/genome_icon.png" alt="">
                <p> <b>Nobel prize winning research</b> Discovery of the molecular structure</p>
              </div>
            </div>
            <div class="top long">
              <span>
                <span class="date">1968</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/genome_icon.png" alt="">
                <p><b>Nobel prize winning research</b> Genetic code and its role in protein production</p>
              </div>
            </div>
            <div class="bottom">
              <span>
                <span class="date">2003</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/genome_icon.png" alt="">
                <p><b>Human genome project</b> Human genome decoded</p>
              </div>
            </div>
            <div class="top">
              <span>
                <span class="date">2007</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/fra.png" alt="">
                <p><short>i</short>DDNA<sup>®</sup> first release</p>
              </div>
            </div>
            <div class="bottom">
              <span>
                <span class="date">2009</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/genome_icon.png" alt="">
                <p><b>Nobel prize winning research</b> How chromosomes are <br> protected by telomeres</p>
              </div>
            </div>
            <div class="top">
              <span>
                <span class="date">2015</span>
              </span>
              <div class="dna-swiss--timeline__text">
                <img src="<?php echo get_template_directory_uri();?>/img/goes_global_icon.png" alt="">
                <p><short>i</short>DDNA<sup>®</sup> goes global</p>
              </div>
            </div>
          </div>
          <div class="timeline-img">
            <img src="<?php echo get_template_directory_uri();?>/img/timeline.jpg" alt="">
          </div>
          <p class="title-like-p">To this day the field of genetics remains an active area of ongoing research, but several scientific landmarks are the foundation of <short>i</short>DDNA<sup>®</sup></p>
          <ul>
            <li><b>The Free Radical Theory of Aging 1956</b> (Harman)</li>
            <li><b>The Immunologic Theory of Aging 1969</b> (Walford)</li>
            <li><b>The Cross-Linkage Theory of Aging 1941</b> (Bjorksten)</li>
            <li><b>The Genetic Control Theory of Aging 1998</b> (Bengston, Schaie)</li>
          </ul>
        </div>
      </div>
    </section>
    <!-- section 7 -->
    <section class="bg-grey">
      <div class="container">
        <div class="row">
          <div class="cell-therapy col-lg-7 col-md-7 col-sm-12">
            <h2 class="title-h2">Swiss cell therapy</h2>
            <p>The origins of cell therapy date to the early 1900s in Switzerland, when Dr. Paul Niehans researched using animal cells to reduce the effects of aging. Since then, anti-aging cellular therapies have evolved, offering great advancement in regenerative medicine.</p>
            <p>For decades, the world’s elite have traveled to Switzerland to undergo cell therapies using revitalization and regeneration.</p>
            <p>More than 5 million published studies on DNA.</p>
            <h5>ANTI-AGING PROGRAM THAT PUTS THE SCIENCE TO WORK.</h5>
          </div>
          <div class=" col-lg-5 col-md-5 col-sm-12 text-center">
            <img class=" responsive_image" src="<?php echo $fields['section_8_dright_image']; ?>" alt="<?php echo $fields['section_8_dright_image_alt']; ?>">
          </div>
        </div>
      </div>
    </section>
    <!-- section 8 -->
    <section>
      <div class="container">
        <div class="studies-area">
          <h2 class="title-h2">Studies area</h2>
          <h3 class="title-h3">Sirtuins</h3>
          <p>The key role of sirtuins as “longevity proteins” is becoming well-accepted and new data confirms the clear and strong expression of SIRT1 in human skin. As has been seen, recent studies also suggest that SIRT1 plays an additional protective role against environmental stresses, such as UV, and other factors that cause the skin to age. </p>
          <p>Moreover, at the molecular level, the fundamental role of sirtuins in important cellular pathways demonstrates that they could be explored as key regulators in many areas of dermatological study, including skin repair and antisenescence research.
          New research suggests that SIRT1 expression optimization is an important strategy in the fight against aging. Its importance can be compared to other leading antiaging strategies such as DNA protection or the use of anti-free radicals, due to the central role that sirtuins play in cellular protection and repair.</p>
            <a class="more">more >>></a>
            <h3 class="title-h3">Telomerase</h3>
            <p>Telomerase is an enzyme which adds DNA sequence repeats “TTAGGG” to the 3 end of DNA strands in the telomere regions at the ends of chromosomes. This region of repeated nucleotide called telomeres contains noncoding DNA and functions to prevent the loss of important DNA from chromosome ends. As a result, every time the chromosome is copied only 100–200 nucleotides are lost, which causes no damage to the organism’s DNA. Telomerase activity and telomere length was found to be associated with cellular senescence. Very few somatic cell types express sufficient quantity of telomerase to overcome the Hayflick limit to proliferation. However, embryonic stem cells are found to have the high expression of telomerase. This overexpression of telomerase permit embryonic stem cells to escape senescence. Exogenous bio- engineered telomerase has been adopted as a means to immortalize cells.</p>
            <a class="more">more >>></a>
            <h3 class="title-h3">Progerin</h3>
            <p>Progerin is one of biomarkers, which is used in studies on natural aging. This protein is expressed as a mutant form of the LMNA gene. Because of that it causes many defects in the area of the nuclear envelope. It also influences a level of reactive oxygen species and antioxidant enzymes. It decreases the activity of proteasomes, causes epigenetic changes, problems with regulation of replication and transcription. That leads to cell dysfunction, apoptosis and results in senescence of organism.</p>
            <p> Telomerase expression is independent of age and regulated by sexual hormones. Benson et al. revealed that progerin expression in human diploid fibroblasts causes telomere aggregation, telomere DNA damage and chromosome aberrations. They revealed that progerin rapidly induces telomeres dysfunction and that leads to appearing of serious DNA damages in cell structures. Influence of progerin on telomeres does not depend on telomere DNA shortening.</p>
            <a class="more">more >>></a>
            <h3 class="title-h3">Turning gene expression up & down</h3>
            <p>
              <b>Gene expression can be turned on and off like a switch, or it can be finely adjusted,</b> as with a volume control knob.
              Dr Garth Ilsley, research scientist in Prof. Nick Luscombe's unit at the Okinawa Institute of Science and Technology Graduate University (OIST), has developed a mathematical model that shows how to predictably tune gene expression. This study, published in Nature Genetics, has important implications in cellular and developmental biology, with potential applications in stem cell reprogramming and regenerative medicine.</p>
              <a class="more">more >>></a>
            </div>
          </div>
        </section>
        <!-- section 9 -->
        <section>
          <div class="container">
            <div class="names-list">
              <p>Genetics and the functional approach to aging remain an active area of ongoing research, <span>but <short>i</short>DDNA<sup>®</sup> anti-aging is based only on robust and not controversial peer-reviewed studies published in the top scientific and medical journals.</span></p>
              <div class="science-lists">
                <ul>

                  <li style="font-size: 10px;">PLoS one</li>

                  <li style="font-size: 10px;">Cell</li>

                  <li style="font-size: 10px;">J Cell Physiol</li>

                  <li style="font-size: 10px;">Exp. Cell. Res</li>

                  <li style="font-size: 10px;">Nat. Rev. Mol. Cell. Biol.</li>

                  <li style="font-size: 10px;">Nature Cell Biol.</li>

                  <li style="font-size: 10px;">J Biol Chem</li>

                  <li style="font-size: 10px;">Biochem J.</li>

                  <li style="font-size: 10px;">Int J Biochem Cell Biol</li>

                  <li style="font-size: 10px;">Int J Biol Sci</li>

                  <li style="font-size: 10px;">Nature</li>

                  <li style="font-size: 10px;">Nature Reviews Genetics</li>

                  <li style="font-size: 10px;">Nature Genetics</li>

                  <li style="font-size: 10px;">Sci. Amer.</li>

                  <li style="font-size: 10px;">Proc Natl Acad Sci USA</li>

                  <li style="font-size: 10px;">Int J Mol Med</li>

                  <li style="font-size: 10px;">J Immunology</li>

                  <li style="font-size: 10px;">Ann Intern Med.</li>

                  <li style="font-size: 10px;">JID</li>

                  <li style="font-size: 10px;">Mol Cell.</li>

                  <li style="font-size: 10px;">Am J Pathol.</li>

                  <li style="font-size: 10px;">Br J Pharmacol.</li>

                  <li style="font-size: 10px;">New scientist</li>

                  <li style="font-size: 10px;">Scientific american</li>

                  <li style="font-size: 10px;">BMC Medicine</li>

                  <li style="font-size: 10px;">PloS Genet</li>

                  <li style="font-size: 10px;">FASEB J</li>

                  <li style="font-size: 10px;">JAMA</li>

                  <li style="font-size: 10px;">Aging Res.</li>

                  <li style="font-size: 10px;">Aging</li>

                  <li style="font-size: 10px;">Rejuvenation research</li>

                  <li style="font-size: 10px;">Molecular Aspects of Medicine</li>

                  <li style="font-size: 10px;">Life Sciences</li>

                  <li style="font-size: 10px;">Nature Reviews Cancer</li>

                  <li style="font-size: 10px;">ChemBioChem</li>

                  <li style="font-size: 10px;">Oxidative Medicine and Cellular Longevity</li>

                  <li style="font-size: 10px;">Mech Ageing Dev</li>
                </ul>
                <ul>

                  <li style="font-size: 10px;">Cell Death Differ.</li>

                  <li style="font-size: 10px;">Eur J Hum Genet.</li>

                  <li style="font-size: 10px;">Biochemical Medicine and Metabolic Biology</li>

                  <li style="font-size: 10px;">Cell Stem Cell</li>

                  <li style="font-size: 10px;">Journal of Environmental and Public Health</li>

                  <li style="font-size: 10px;">Oncotarget</li>

                  <li style="font-size: 10px;">Environmental science &amp; techno</li>

                  <li style="font-size: 10px;">Science</li>

                  <li style="font-size: 10px;">N. Engl. J. Med</li>

                  <li style="font-size: 10px;">Genome Biology</li>

                  <li style="font-size: 10px;">Ame J Hum Gen</li>

                  <li style="font-size: 10px;">Gene</li>

                  <li style="font-size: 10px;">Curr Pharm Biotechnol.</li>

                  <li style="font-size: 10px;">Aging Cell</li>

                  <li style="font-size: 10px;">Matrix Biol.</li>

                  <li style="font-size: 10px;">J Gen Microbiol.</li>

                  <li style="font-size: 10px;">J Am Chem Soc.</li>

                  <li style="font-size: 10px;">Glycobiology</li>

                  <li style="font-size: 10px;">Int J Biochem Cell Biol</li>

                  <li style="font-size: 10px;">Curr Opin Cell Biol.</li>

                  <li style="font-size: 10px;">J Theor Biol.&nbsp;</li>

                  <li style="font-size: 10px;">J Appl Toxicol</li>

                  <li style="font-size: 10px;">American Society for Microbiology</li>

                  <li style="font-size: 10px;">N. Engl. J. Med</li>

                  <li style="font-size: 10px;">J Clin Invest</li>

                  <li style="font-size: 10px;">J Intern Med</li>

                  <li style="font-size: 10px;">Photochem Photobiol Sci</li>

                  <li style="font-size: 10px;">Physiol Rev.</li>

                  <li style="font-size: 10px;">Cells Tissues Organs</li>

                  <li style="font-size: 10px;">Br Med J</li>

                  <li style="font-size: 10px;">J Med Microbiol</li>

                  <li style="font-size: 10px;">Journal of the American Medical Association</li>

                  <li style="font-size: 10px;">Physiological Genomics</li>

                  <li style="font-size: 10px;">Exp Gerontol</li>

                  <li style="font-size: 10px;">Biogerontology</li>

                  <li style="font-size: 10px;">Mech. aging Dev.</li>

                  <li style="font-size: 10px;">J Gerontol</li>
                </ul>
                <ul>

                  <li style="font-size: 10px;">Microvascular Research</li>

                  <li style="font-size: 10px;">Oxid Med Cell Longev.</li>

                  <li style="font-size: 10px;">Current Pharmaceutical Design</li>

                  <li style="font-size: 10px;">Journal of Toxicology</li>

                  <li style="font-size: 10px;">Inflammation Research</li>

                  <li style="font-size: 10px;">Journal of Pharmaceutical and Biomedical Analysis</li>

                  <li style="font-size: 10px;">Journal of Inorganic Biochemistry</li>

                  <li style="font-size: 10px;">Advances in Experimental Medicine and Biology</li>

                  <li style="font-size: 10px;">Nanotoxicology</li>

                  <li style="font-size: 10px;">J Dermatol Sci.</li>

                  <li style="font-size: 10px;">J Invest Dermatol</li>

                  <li style="font-size: 10px;">Int. J. Dermatol.</li>

                  <li style="font-size: 10px;">Br. J. Dermatol.</li>

                  <li style="font-size: 10px;">Eur J Dermatol.</li>

                  <li style="font-size: 10px;">Exp Dermatol</li>

                  <li style="font-size: 10px;">Clin Cosmet Investig Dermatol</li>

                  <li style="font-size: 10px;">Cosmet Dermatol.</li>

                  <li style="font-size: 10px;">Arch Dermatol Res</li>

                  <li style="font-size: 10px;">Dermatoendocrinol</li>

                  <li style="font-size: 10px;">Am J Contact Dermat</li>

                  <li style="font-size: 10px;">Clin Dermat.</li>

                  <li style="font-size: 10px;">J Am Acad Dermatol.</li>

                  <li style="font-size: 10px;">J Drugs Dermatol.</li>

                  <li style="font-size: 10px;">J Eur Acad Dermatol Venereol</li>

                  <li style="font-size: 10px;">Int J Cosmet Sci</li>

                  <li style="font-size: 10px;">Austr Dermatol</li>

                  <li style="font-size: 10px;">Semin Cutan Med Surg</li>

                  <li style="font-size: 10px;">Archives of Facial Plastic Surgery</li>

                  <li style="font-size: 10px;">Free Radical Biology &amp; Medicine</li>

                  <li style="font-size: 10px;">Free Radical Research</li>

                  <li style="font-size: 10px;">Medicine and science in sports and exercise</li>

                  <li style="font-size: 10px;">J. Biomech</li>

                  <li style="font-size: 10px;">Journal of Athletic Training</li>

                  <li style="font-size: 10px;">American Medical Athletic Association Journal</li>

                  <li style="font-size: 10px;">Journal of Strength Conditioning Research</li>

                  <li style="font-size: 10px;">Sports Health</li>

                  <li style="font-size: 10px;">Am J Psychiatry</li>
                </ul>
                <ul>

                  <li style="font-size: 10px;">Am J Respir Crit Care Med</li>

                  <li style="font-size: 10px;">The Journal of Neuroscience</li>

                  <li style="font-size: 10px;">World Rev Nutr Diet</li>

                  <li style="font-size: 10px;">Journal of Nitric Oxide, Biology and Chemistry,</li>

                  <li style="font-size: 10px;">Journal of Exercise Nutrition and Biochemistry</li>

                  <li style="font-size: 10px;">Journal of Science and Medicine in Sport</li>

                  <li style="font-size: 10px;">Physiology</li>

                  <li style="font-size: 10px;">British Jrnl of Nutrition</li>

                  <li style="font-size: 10px;">J Am Coll Nutr</li>

                  <li style="font-size: 10px;">Am J Clin Nutr</li>

                  <li style="font-size: 10px;">Nutrition</li>

                  <li style="font-size: 10px;">Obesity</li>

                  <li style="font-size: 10px;">J Nutr Biochem</li>

                  <li style="font-size: 10px;">Nutrition Research</li>

                  <li style="font-size: 10px;">The Journal of Nutrition</li>

                  <li style="font-size: 10px;">Aliment Pharmacol Ther</li>

                  <li style="font-size: 10px;">Nutrition &amp; Metabolism</li>

                  <li style="font-size: 10px;">Arch Biochem Biophys</li>

                  <li style="font-size: 10px;">MJ.Nutr Rev</li>

                  <li style="font-size: 10px;">Cardiovasc Diabetol</li>

                  <li style="font-size: 10px;">Food Chem Toxicol.</li>

                  <li style="font-size: 10px;">J Med Food.</li>

                  <li style="font-size: 10px;">The Journal of the American Nutraceutical Association</li>

                  <li style="font-size: 10px;">J Agric Food Chem.</li>

                  <li style="font-size: 10px;">Nutrition journal</li>

                  <li style="font-size: 10px;">Nutr Res.</li>

                  <li style="font-size: 10px;">Free Radic. Biol. Med.</li>

                  <li style="font-size: 10px;">Eur J Clin Nutr.</li>

                  <li style="font-size: 10px;">Gastroenterology</li>

                  <li style="font-size: 10px;">Nutrients</li>

                  <li style="font-size: 10px;">Am J Gastroenterology</li>

                  <li style="font-size: 10px;">Neurogastroenterol Motil</li>

                  <li style="font-size: 10px;">Chinese Journal of Epidemiology</li>

                  <li style="font-size: 10px;">Scand J Gastroenteroly</li>

                  <li style="font-size: 10px;">Gut</li>

                  <li style="font-size: 10px;">J Clin Gastroenterol</li>

                  <li style="font-size: 10px;">Aliment Pharmacol Ther</li>
                </ul>
                <ul>

                  <li style="font-size: 10px;">The Journal of Nutrition</li>

                  <li style="font-size: 10px;">Curr Opin Endocrinol Diabetes Obes.</li>

                  <li style="font-size: 10px;">Physiology &amp; Behaviour</li>

                  <li style="font-size: 10px;">Diabetes/Metab Res Rev</li>

                  <li style="font-size: 10px;">Crit Rev Food Sci Nutr</li>

                  <li style="font-size: 10px;">Mol Nutr Food Res</li>

                  <li style="font-size: 10px;">Curr Opin Lipidol.</li>

                  <li style="font-size: 10px;">J Athl Train.</li>

                  <li style="font-size: 10px;">Med Sci Sports Exerc</li>

                  <li style="font-size: 10px;">Ann Intern Med.</li>

                  <li style="font-size: 10px;">Ann Med Exp Biol Fenn</li>

                  <li style="font-size: 10px;">Journal of Sports Medicine</li>

                  <li style="font-size: 10px;">Journal of Applied Physiology</li>

                  <li style="font-size: 10px;">Eur J Cardiovasc Prev Rehabil.</li>

                  <li style="font-size: 10px;">Respiration Physiology</li>

                  <li style="font-size: 10px;">Journal of Science and Medicine in Sports,</li>

                  <li style="font-size: 10px;">Strength and Conditioning Journal</li>

                  <li style="font-size: 10px;">Psychiatric News</li>

                  <li style="font-size: 10px;">Sports and Exercise</li>

                  <li style="font-size: 10px;">British Journal of Sports Medicine</li>

                  <li style="font-size: 10px;">Med Sci Sports Exerc</li>

                  <li style="font-size: 10px;">J. Am. Coll. Cardiol</li>

                  <li style="font-size: 10px;">International Journal of Sports Nutrition and Exercise Metabolism</li>

                  <li style="font-size: 10px;">Best Pract Res Clin Endocrinol Metab</li>

                  <li style="font-size: 10px;">Journal of the International Society of Sports Nutrition</li>

                  <li style="font-size: 10px;">European Journal of Applied Physiology</li>

                  <li style="font-size: 10px;">Journal of the International Society of Sports Nutrition</li>

                  <li style="font-size: 10px;">Phytotherapy Research</li>

                  <li style="font-size: 10px;">Journal of Nutrition</li>

                  <li style="font-size: 10px;">American Journal of Physiology</li>

                  <li style="font-size: 10px;">European Journal of Science</li>

                  <li style="font-size: 10px;">Bioscience, Biotechnology, and Biochemistry</li>

                  <li style="font-size: 10px;">Scandinavian Journal of Medicine and Science in Sports</li>

                  <li style="font-size: 10px;">Allergy, Asthma &amp; Clinical Immunology</li>
                </ul>
              </div>
            </div>
          </div>
        </section>
<?php /*<section class="p-t-0">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h3 class="light letter-spacing m-b-30 m-t-0"><?php echo $fields['section_6_title']; ?></h3>
            <?php echo $fields['section_6_description']; ?>
            <h4 class="text-blue bold letter-spacing text-uppercase"><?php echo $fields['section_6_list_title']; ?></h4>
            <?php $section_5_list_items = $fields['section_6_list_items']; ?>
            <ul class="blue_ul inside_ul m-t-30">
              <?php foreach( $section_5_list_items as $item ) :?>
                <li><?php echo $item['text']; ?></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </section> */ ?>

<?php /* <section class="p-t-0">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="font-normal text-uppercase letter-spacing m-t-0"><?php echo $fields['section_7_title']; ?></h2>
            <?php echo $fields['section_7_description']; ?>
          </div>
        </div>
      </div>
    </section>


    <?php $entity = 'science'; ?>
    <?php $countries = include( TEMPLATE_DIRECTORY . '/include/form/formcontainer.php'); ?>
    <!-- section 10 -->
    <section style="background-image: url('<?php echo $fields['section_10_background_image']; ?>'); background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-lg-offset-6 col-md-8 col-md-offset-4 col-sm-12 col-smoffset-0">
            <h2 class="font-normal text-uppercase letter-spacing m-t-0"><?php echo $fields['section_10_title']; ?></h2>
            <?php echo $fields['section_10_description']; ?>
          </div>
        </div>
      </div>
    </section>
    <!-- section 11 -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="text-uppercase letter-spacing m-t-0"><?php echo $fields['section_11_title']; ?></h2>
            <?php echo $fields['section_11_description']; ?>
            <?php $section_11_columns = $fields['section_11_columns']; ?>
            <?php foreach($section_11_columns as $column) : ?>
              <div class="science-lists">
                <div class="col-sm-2 col-xs-12">
                  <?php $section_11_items = $column['items']; ?>
                  <ul class="big-lists">
                    <?php foreach( $section_11_items as $item ) : ?>
                      <li style="font-size:10px"><?php echo $item['title']; ?></li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </section>
    <!-- section 12 -->
    <section style="background-image: url('<?php echo $fields['section_12_background_image']; ?>'); background-size: cover;" class="experience">
      <div class="container">
        <div class="row text-white">
          <div class="col-xs-12">
            <h2 class="letter-spacing text-uppercase m-b-30 m-t-0"><?php echo $fields['section_12_title']; ?></h2>
            <?php echo $fields['section_12_description']; ?>
          </div>
        </div>
      </div>
    </section>
    <!-- section 13 -->
    <section style="background-image: url('<?php echo $fields['section_13_background_image']; ?>'); background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h2 class="text-uppercase letter-spacing m-t-0 m-b-30"><?php echo $fields['section_13_title']; ?></h2>
            <?php echo $fields['section_13_description']; ?>
          </div>
        </div>
      </div>
    </section>
    <!-- section 14 -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h2 class="science-formula letter-spacing text-uppercase font-normal m-t-0 m-b-30"><?php echo $fields['section_14_title']; ?></h2>
            <div class="col-sm-6 p-0 letter-spacing">
              <p class="text-uppercase text-left"><span class="bold"><?php echo $fields['section_14_description']; ?></p>
            </div>
            <div class="col-sm-6 p-0">
              <ul class="inside_ul blue_ul">
                <?php $section_14_list = $fields['section_14_list']; ?>
                <?php foreach( $section_14_list as $item ) : ?>
                  <li class="f-18">
                    <?php echo $item['title']; ?>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- section 15 -->
    <section style="background-image: url('<?php echo $fields['section_15_background_image']; ?>'); background-size: cover;" >
      <div class="container">
        <div class="row text-white">
          <div class="col-xs-12">
            <h2 class="letter-spacing bold m-t-0 "><?php echo $fields['section_15_title']; ?></h2>
            <h3 class="bold letter-spacing text-white text-uppercase m-b-30"><?php echo $fields['section_15_description']; ?></h3>
            <?php $section_15_columns = $fields['section_15_columns']; ?>
            <?php foreach($section_15_columns as $column ) : ?>
              <div class="col-sm-6">
                <?php $section_15_items = $column['items']; ?>
                <ul class="blue_ul inside_ul">
                  <?php foreach( $section_15_items as $item ) : ?>
                    <li><?php echo $item['title']; ?></li>
                  <?php endforeach; ?>
                </ul>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </section>
    <!-- section 16 -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h4 class="letter-spacing text-uppercase m-t-0 m-b-30"><?php echo $fields['section_16_title']; ?></h4>
            <?php echo $fields['section_16_description']; ?>
            <ul>
              <?php $section_16_items = $fields['section_16_items']; ?>
              <?php foreach($section_16_items as $item) : ?>
                <li class="text-uppercase bold"><?php echo $item['title']; ?></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </section>
    */
    ?>
    <?php get_footer(); ?>
