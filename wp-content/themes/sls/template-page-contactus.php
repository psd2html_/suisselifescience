<?php /* Template Name: Contact Us */ ?>
<?php

get_header();

global $sls_data;

$fields = get_fields( get_the_ID() );

wp_enqueue_script(
    'google-map',
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyA55HS4C36lVMwdXSHgeY-BiVwcsfvsa20",
    array('jquery')
);

//$GeoInfo = $WPGeoplugin->getGeoInfo();


$args=array(
    'post_type' => 'locations',
    'post_status' => 'publish',
    'posts_per_page' => -1
);

//$query = new WP_Query($args);
$wp_query = new \WP_Query();
$posts = $wp_query->query($args);

$locations = array();
foreach ($posts as $post) {
    $locations[] = get_field('marker', $post->ID);
}


	
?>


    <script>
        var main_locations = <?php echo json_encode($locations); ?>;
    </script>
    <section class="about_us_header bg-fixed" <?php if (!empty($fields["section_1_field_background"])) : ?> style="background-image: url('<?php echo $fields["section_1_field_background"]; ?>'); <?php endif; ?> ">
        <div class="container">
            <h2 class="m-t-20"><?php echo $fields["contactus_header_field_1"]; ?></h2>
            <h5 class="font-ligth "><?php echo $fields["contactus_header_field_2"]; ?></h5>
            <h4 class="m-b-60"><?php echo $fields["contactus_header_field_3"]; ?></h4>
        </div>
    </section>

    <section>

        <?php $entity = 'contact_us'; ?>

        <div class="container">
        <div class="row">
            <div class="clearfix">
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <h2 id="contactus_customer_form_field_1" class="form-title"><?=get_field("form1_title",'option'); ?></h2>
                </div>
            </div>
            <div class="col-sm-4 wed_like_to">
                <h5 class="text-left"><?php echo $fields["contactus_customer_form_field_2"]; ?></h5>
            </div>
            <div class="col-sm-8 contact-form">

                <form id="ajax_contact_send" method="POST" class="offer_form about_fix clearfix my_amazing_form" >

                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
				<div class="row">
					<?php foreach(get_field($entity . '_form_fields', 'option') as $field){ ?>
							<?php include("include/form/forminputs.php");?>
					<?};?>
				</div>
				<div class="clearfix"></div>
                <h4 class="text-blue m-t-45"><?=get_field($entity . '_interests_title','option'); ?></h4>
                <div class="form-checkboxes half-width-check">
					<?php include("include/sudscribe/subscribe_checkboxes.php");?>
                </div>

                    <div class="subskribe">
                        <?php $agreements = get_field($entity . '_agreements_form','option'); ?>
                        <?php foreach ( $agreements as $agreement ) :?>
                            <?php $agreement_id = 'agree_' . rand(); ?>
                            <input type="hidden" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                            <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>
                    </div>
					<input type="hidden" name="subject_to_submit" value="<?=get_field("form1_mail_subject",'option'); ?>">
					<?$email="";foreach(get_field($entity . '_email_for_sending', 'option') as $em){?>
						<input type="hidden" name="email_to_submit[]" value="<?=$em["email"]?>">
					<?}?>

                <button class="nbtn nbtn-inverse  m-t-10 pull-right" type="submit"><?=get_field($entity . '_submit_buttom','option'); ?><i class="ico arrow arrow_white"></i></button>
				</form>


                <form id="ajax_contact_send_mobile" method="POST" class="offer_form about_fix clearfix my_amazing_form" >

                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                    <div class="row">
                        <?php foreach(get_field($entity . '_form_fields_mobile', 'option') as $field){ ?>
                            <?php include("include/form/forminputs.php");?>
                        <?};?>
                    </div>
                    <div class="clearfix"></div>

                    <h4 class="text-blue m-t-45"><?=get_field($entity . '_interests_title','option'); ?></h4>
                    <div class="form-checkboxes half-width-check">
                        <?php include("include/sudscribe/subscribe_checkboxes.php");?>
                    </div>

                    <div class="subskribe">
                        <?php $agreements = get_field($entity . '_agreements_form','option'); ?>
                        <?php foreach ( $agreements as $agreement ) :?>
                            <?php $agreement_id = 'agree_' . rand(); ?>
                            <input type="hidden" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                            <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="subject_to_submit" value="<?=get_field("form1_mail_subject",'option'); ?>">
                    <?$email="";foreach(get_field($entity . '_email_for_sending', 'option') as $em){?>
                        <input type="hidden" name="email_to_submit[]" value="<?=$em["email"]?>">
                    <?}?>

                    <button class="nbtn nbtn-inverse  m-t-10 pull-right" type="submit"><?=get_field($entity . '_submit_buttom','option'); ?><i class="ico arrow arrow_white"></i></button>
                </form>


            </div>
            </div>
        </div>

    </section>


    <section class="about_us_header bg-fixed" <?php if (!empty($fields["section_1_field_background"])) : ?> style="background-image: url('<?php echo $fields["section_1_field_background"]; ?>'); <?php endif; ?> ">
        <div class="container">
            <div class="row text-center about_us_boxes">
                <div class="col-sm-6 about_us_box">
                    <h4 class=""><?php echo $fields["contactus_header_field_4"]; ?></h4>

                    <a href="<?php echo $fields["contactus_header_field_5"]['url']; ?>" class="nbtn nbtn-inverse"><?php echo $fields["contactus_header_field_5"]['title']; ?><i class="ico arrow arrow_white"></i></a>
                </div>

                <div class="col-sm-6 about_us_box">
                    <h4 class=""><?php echo $fields["contactus_header_field_6"]; ?></h4>
                    <h5 class=""><?php echo $fields["contactus_header_field_7"]; ?></h5>
                    <h6 class="font-ligth "><?php echo $fields["contactus_header_field_8"]; ?></h6>

                    <a href="<?php echo $fields["contactus_header_field_9"]['url']; ?>" class="nbtn nbtn-inverse"><?php echo $fields["contactus_header_field_9"]['title']; ?><i class="ico arrow arrow_white"></i></a>
                </div>

                <div class="col-sm-6 about_us_box">
                    <h4 class="font-ligth f-42"><?php echo $fields["contactus_header_field_10"]; ?></h4>
                    <h5 class="font-ligth "><?php echo $fields["contactus_header_field_11"]; ?></h5>

                    <a href="<?php echo $fields["contactus_header_field_12"]['url']; ?>" class="nbtn nbtn-inverse"><?php echo $fields["contactus_header_field_12"]['title']; ?><i class="ico arrow arrow_white"></i></a>
                </div>
                <div class="col-sm-6 about_us_box">
                    <h4 class=""><?php echo $fields["contactus_header_field_13"]; ?></h4>
                    <h5 class=""><?php echo $fields["contactus_header_field_14"]; ?></h5>

                    <a id="contactus_header_field_15" href="<?php echo $fields["contactus_header_field_15"]['url']; ?>" class="nbtn nbtn-inverse"><?php echo $fields["contactus_header_field_15"]['title']; ?><i class="ico arrow arrow_white"></i></a>

                </div>

            </div>

        </div>

    </section>


    <section class="prefooter_map_section">
        <div id="map-block" class="map-block">
        </div>
        <div class="container">
            <div class="col-sm-4 pre_map_informer">
                <h4 class="m-b-20"><?php echo $fields["contactus_contact_info_field_1"]; ?></h4>
                <p class="m-t-0">
                <a class="f-36" href="tel:<?php echo $fields["contactus_contact_info_field_2"]; ?>"><b><?php echo $fields["contactus_contact_info_field_2"]; ?></b></a>
</p>
                <h5 class="text-ligth m-b-0 m-t-20"><?php echo $fields["contactus_contact_info_field_3"]; ?></h5>
                <p class="m-t-0">
                <a href="mailto:<?php echo $fields["contactus_contact_info_field_4"]; ?>" class="f-24 bold"><?php echo $fields["contactus_contact_info_field_4"]; ?></a>
</p>
                <a href="skype:<?php echo $sls_data['social_skype']; ?>?call" class="nbtn nbtn-white m-t-25 m-b-50" ><?php echo $fields["contactus_contact_info_field_5"]; ?><i class="ico arrow"></i></a>
                <h4 class="bold m-b-20"><?php echo $fields["contactus_contact_info_field_6"]; ?></h4>
                <p class="m-t-0">
                <a class="f-36" href="tel:<?php echo $fields["contactus_contact_info_field_7"]; ?>"><b><?php echo $fields["contactus_contact_info_field_7"]; ?></b></a></p>
                <p><?php echo $fields["contactus_contact_info_field_8"]; ?><img src="<?php echo $fields["contactus_contact_info_field_flag_image"]; ?>" alt="<?php echo $fields["contactus_contact_info_field_flag_image_alt"]; ?>" /></p>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>

    <section>
        <div class="container">
            <div class="col-md-2">
                <a href="<?php echo $sls_data['social_linkedin']; ?>" class="folow_us" target="_blank">
                    <div class="folow_image">
                        <i class="fa fa-linkedin"></i>
                    </div>
                    <div class="folow_info">
                        <p>FOLLOW US</p>
                        <b>ON linkedin</b>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?php echo $sls_data['social_facebook']; ?>" class="folow_us" target="_blank">
                    <div class="folow_image">
                        <i class="fa fa-facebook"></i>
                    </div>
                    <div class="folow_info">
                        <p>LIKE US</p>
                        <b>ON facebook</b>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?php echo $sls_data['social_instagram']; ?>" class="folow_us" target="_blank">
                    <div class="folow_image">
                        <i class="fa fa-instagram"></i>
                    </div>
                    <div class="folow_info">
                        <p>FOLLOW US</p>
                        <b>ON instagram</b>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?php echo $sls_data['social_youtube']; ?>" class="folow_us" target="_blank">
                    <div class="folow_image">
                        <i class="fa fa-youtube"></i>
                    </div>
                    <div class="folow_info">
                        <p>WATCH US</p>
                        <b>ON youtube</b>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?php echo $sls_data['social_pinterest']; ?>" class="folow_us" target="_blank">
                    <div class="folow_image">
                        <i class="fa fa-pinterest"></i>
                    </div>
                    <div class="folow_info">
                        <p>PIN US</p>
                        <b>ON pinterest</b>
                    </div>
                </a>
            </div>
            <div class="col-md-2">
                <a href="<?php echo $sls_data['social_googleplus']; ?>" class="folow_us" target="_blank">
                    <div class="folow_image">
                        <i class="fa fa-google-plus"></i>
                    </div>
                    <div class="folow_info">
                        <p>FOLLOW US</p>
                        <b>ON google plus</b>
                    </div>
                </a>
            </div>
        </div>

    </section>
<?php get_footer(); ?>
