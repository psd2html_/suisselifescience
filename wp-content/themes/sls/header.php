<?php

if(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') && !strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')){

    $splink = get_option('sls_url');
    $site_link = $_SERVER['HTTP_HOST'];

        if(!isset($_COOKIE['cross_site']) && !isset($_GET['cs'])){
            setcookie ("cross_site", 1);
            header("Location: http://$splink/?cs=1");
            exit();    
        }else{
            if($_GET['cs']==='1'){
                setcookie ("cross_site", 1);
                header("Location: http://$splink/?cs=2");
                exit();
            }elseif($_GET['cs']==='2'){
                setcookie ("cross_site", 1);
                header("Location: http://$splink/?cs=3");
                exit();
            }elseif($_GET['cs']==='3'){
                setcookie ("cross_site", 1);
                header("Location: http://$site_link/");
                exit(); 
            }
        }       
}

global $sls_data;

$geolocate_country_code = Seven_Geolocation::geolocate_ip( '', true, true );
?>
<!DOCTYPE html>
<!--
<?php echo "User test mode country: ". $_COOKIE['test_country'] . "\n";?>
<?php echo "User geolocate country: ". $geolocate_country_code . "\n";?>
-->
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url" content="<?php bloginfo('url'); ?>" />

    <?php if(isset($sls_data['media-favicon']['url']) && ($sls_data['media-favicon']['url'] != "")) { ?><link rel="shortcut icon" href="<?php echo esc_url($sls_data['media-favicon']['url']); ?>"><?php } ?>
    <?php if(isset($sls_data['media-favicon-iphone']['url']) && ($sls_data['media-favicon-iphone']['url'] != "")) { ?><link rel="apple-touch-icon" href="<?php echo esc_url($sls_data['media-favicon-iphone']['url']); ?>"><?php } ?>
    <?php if(isset($sls_data['media-favicon-iphone_retina']['url']) && ($sls_data['media-favicon-iphone-retina']['url'] != "")) { ?><link rel="apple-touch-icon" sizes="120x120" href="<?php echo esc_url($sls_data['media-favicon-iphone-retina']['url']); ?>"><?php } ?>
    <?php if(isset($sls_data['media-favicon-ipad']['url']) && ($sls_data['media-favicon-ipad']['url'] != "")) { ?><link rel="apple-touch-icon" sizes="76x76" href="<?php echo esc_url($sls_data['media-favicon-ipad']['url']); ?>"><?php } ?>
    <?php if(isset($sls_data['media-favicon-ipad-retina']['url']) && ($sls_data['media-favicon-ipad-retina']['url'] != "")) { ?><link rel="apple-touch-icon" sizes="152x152" href="<?php echo esc_url($sls_data['media-favicon-ipad-retina']['url']); ?>"><?php } ?>

    <?php /* RSS и всякое */ ?>
    <link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
    <link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
    <link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,100,300,900&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <title><?php wp_title(''); ?></title>
    <?php wp_head(); ?>
    <script>
        function setTestCountry(e) {
            var date = new Date(new Date().getTime() + 1209600 * 1000);
            document.cookie = "test_country="+e+"; path=/; expires=" + date.toUTCString();
        }
    </script>
    <?php if(!is_store_available()){ ?>
    <style>
    #menu-item-2033{display: none!important;}
    </style>
    <?php } ?>
</head>

<!--Geolocation-->
<? //$Geolocation=new WPGeoplugin();?>
<!--Geolocation-->


<body <?php body_class(); ?>>
<header>
    <div class="top_plashs">
        <div class="container">
            <div class="pre_header text-right font-ligth">

                <?php if ( woo_cart_quantity_user_res() ) : ?>
                    <div class="inline">
                        <a class="cart-contents cart_icon_block" href="<?php echo get_option('woo_secure_iddna_url'); ?>cart/" title=""><span><?php echo woo_cart_quantity_user_res(); ?> item<?php if(woo_cart_quantity_user_res()!=1){ ?>s<?php } ?></span><i></i></a>
                    </div>
                <?php endif; ?>

                <div class="inline search_label">
                    <form action="<?php echo esc_url(home_url());?>">
                        <input type="text" name="s" value="" placeholder="<?php echo __("Search", "sls-theme"); ?>" class="header_search_input" autocomplete="off">
                        <button type="submit"><i class="fa" aria-hidden="true"></i></button>
                    </form>
                </div>
                <?php if ( is_theme_plugin_active('language_switcher/plugin.php') ) : ?>
                <!--Language-->
                <div class="inline link_group">
                    <input type="button" value="Language" id="popup__toggle" />
                </div>
                <div class="popup__overlay">
                    <div class="popup">
                        <a href="#" class="popup__close">X</a>
                        <?php $regions = getAllRegions(); ?>
                        <?php foreach ($regions as $region) { ?>
                            <?php $languages = getLanguagesForRegion($region->languages); ?>
                            <?php if ($languages) { ?>
                                <ul class="region_label">
                                    <li><?php echo $region->name; ?>
                                        <img src="<?php echo getImgSrcByRegion($region->name); ?>" style="display:block;">
                                    </li>

                                    <?php foreach ($languages as $language) { ?>
                                        <li>
                                        <a href="<?php echo get_site_by_language($language->code);?>">
                                            <?php echo $language->english_name; ?>
                                        </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <!--Language-->
                <?php endif; ?>
                <div class="inline mobile_rushalo ">

                    <form method="GET" action="/">
                        <input type="text" placeholder="Search" name="s" class="header_search_input" value="<?if(isset($_GET[" s "])){echo $_GET["s "];};?>"/>
                        <i class="fa fa-search rishalo_btn" aria-hidden="true"></i>
                        <input type="submit" class="hidden" />
                    </form>
                </div>

                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'before-top',
                        'menu_class' => 'header_pre_menu  inline',
                        'container' => '',
                        'container_class' => '',
                        'walker' => new Before_Top_Walker_Nav_Menu()
                    ));
                ?>

                <?php

                global $is_secure_user_login;

                ?>

                <?php if(isset($is_secure_user_login) && $is_secure_user_login): ?>

                    <a title="<?php echo get_secure_username(); ?>" class="log-out login_btn inline">Logout</a>

                <?php else: ?>
                <a href="/login" class="login_btn inline <?php if(get_the_ID()=='502'){ ?> active-login <? } ?>">Sign in</a>
<?php
                    //wp_nav_menu(array(
                        //'items_wrap' => '%3$s',
                        //'theme_location' => 'login-top',
                        //'menu_class' => '',
                        //'container' => '',
                        //'container_class' => '',
                        //'walker' => new Walker_Login_Menu()
                    //));
                    ?>

                <?php endif; ?>


            </div>
        </div>
    </div>
    <div class="container main_header">
        <nav class="navbar  row m-b-0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topnav" aria-expanded="false">
                    <span class="sr-only">Меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php if(isset($sls_data['media-logo']['url']) && ($sls_data['media-logo']['url'] != "")) { ?>
                    <a class="navbar-brand" href="<?php echo esc_url(home_url()); ?>/"><img src="<?php echo esc_url($sls_data['media-logo']['url']); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
                <?php } else { ?>
                    <a href="<?php echo esc_url(home_url()); ?>/"><?php esc_html(bloginfo('name')); ?></a>
                <?php } ?>
            </div>
            <div class="collapse navbar-collapse text-center" id="topnav">
                <div class="text-right">
                    <div class="mobile_plash">
                        <div class="inline search_label">
                            <input type="text" placeholder="Search" class="header_search_input" />
                            <i class="fa" aria-hidden="true"></i>
                        </div>
                    </div>
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'top',
                        'menu_class' => 'nav navbar-nav header_mein_menu',
                        'container' => 'div',
                        'container_class' => 'header_mein_menu',
                        'walker' => new Bootstrap_Walker_Nav_Menu()
                    ));
                    ?>
                    <?php $entity = 'header_footer'; ?>
                    <div class="header_emeil">
                        <div class="input-group">
                            <form id="ajax_header_send" method="POST" action="" >
                                <input type="hidden" name="subject_to_submit" value="<?=get_field($entity . '_mail_subject', 'option');?>">
                                <!--<input type="hidden" name="subscr" value="subscr">-->
                                <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                                <div class="input-group">
                                <?$email="";foreach(get_field($entity . '_email_for_sending', 'option') as $em){?>
                                    <input type="hidden" name="email_to_submit[]" value="<?=$em["email"]?>">
                                <?}?>

                                    <input type="email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" placeholder="<?php echo get_field($entity . '_enter_your_email', 'option'); ?>" required >
                                    <span class="input-group-btn">
                                        <button class="nbtn nbtn-inverse" type="submit"><?php echo get_field($entity . '_submit_buttom', 'option'); ?> <i class="ico arrow arrow_white"></i></button>
                                    </span>
                                </div>
                            </form>
                        </div>
                        <!-- /input-group -->
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
