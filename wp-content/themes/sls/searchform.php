<?php
/**
 * Шаблон формы поиска (searchform.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
?>
<div class="container">
<form role="search" method="get" class="search-form form-inline" action="<?php echo home_url( '/' ); ?>">
	<div class="form-group">
		<label class="sr-only" for="search-field">Search</label>
		<input type="search" class="form-control input-sm" id="search-field" placeholder="Type here ..." value="<?php echo get_search_query() ?>" name="s">
	</div>
	<button type="submit" class="btn nbtn nbtn-inverse btn-default btn-sm">Search</button>
</form>
</div>
