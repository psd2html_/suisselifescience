<?php /* Template Name: Team */ ?>
<?php

    get_header();

    $section_title = get_field( 'section_title', get_the_ID());
?>
        <section class="team_section">
            <div class="container">
                <h2 class="f-42 font-ligth m-b-50 uppercase text-center"><?php echo $section_title; ?></h2>
                <div class="team_block clearfix">
                    <?php
                    $args = array(
                        'posts_per_page'   => 10,
                        'offset'           => 0,
                        'category'         => '',
                        'category_name'    => '',
                        'orderby'          => 'date',
                        'order'            => 'DESC',
                        'include'          => '',
                        'exclude'          => '',
                        'meta_key'         => '',
                        'meta_value'       => '',
                        'post_type'        => 'team_member',
                        'post_mime_type'   => '',
                        'post_parent'      => '',
                        'author'	   => '',
                        'post_status'      => 'publish',
                        'suppress_filters' => true
                    );
                    $team_members = get_posts( $args );
                    ?>

                    <?php foreach( $team_members as $member ) : ?>
                    <?php $fields = get_fields($member->ID); ?>
                    <div class="col-lg-3 col-sm-6">
                        <div class="team_inner clearfix">
                            <div class="team_photo_block">
                                <img src="<?php echo $fields['photo']; ?>" />
                            </div>
                            <div class="col-sm-12">
                                <h4 class="bold"><?php echo $fields['full_name']; ?></h4>
                                <h5 class="text-blue"><?php echo $fields['position']; ?></h5>
                                <hr>
                                <p><?php echo $fields['description']; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php get_footer(); ?>