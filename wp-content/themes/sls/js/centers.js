var map;
var isDraggable = jQuery(document).width() > 480 ? true : false;
console.log(isDraggable)
function initMap() {
	if(document.getElementById('gooMap')){
		map = new google.maps.Map(document.getElementById('gooMap'), {
			//center: {lat: (Geolocation.latitude*1), lng: (Geolocation.longitude*1)},
			zoom: 9,
			maxZoom:10,
			draggable: isDraggable,
			scrollwheel: false
		});
		var bounds = new google.maps.LatLngBounds();
		
		if(office.length){
			jQuery.each(office, function (index,value){
				office[index].loc = new google.maps.LatLng(office[index].lat,office[index].lng);
				createMarker(office[index]);
				bounds.extend(office[index].loc);
			});
			map.fitBounds(bounds);	
		}else{
			geocodeByCountry("europe")
		}
	}	
}


function createMarker(office,bounds) {
  var marker = new google.maps.Marker({
    map: map,
    position: office.loc,
	title:office.name
  });

  attachSecretMessage(marker, office.name);

}

function attachSecretMessage(marker, secretMessage) {
  var infowindow = new google.maps.InfoWindow({
    content: secretMessage
  });

  marker.addListener('click', function() {
    infowindow.open(marker.get('map'), marker);
  });
};

function panMap(lat,lon){
	if (map) {
		var newlatlng = new google.maps.LatLng(lat, lon);
		map.panTo(newlatlng);
	}
}
function geocodeByCountry(address){
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			map.fitBounds(results[0].geometry.viewport);
		}
	});
}
