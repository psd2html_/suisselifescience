jQuery(document).ready(function($) {

//LABEL OF INPUTS
   $('.my_form_input > input').on('focus', function() {
       ($(this).next()).addClass('active');
   });
    $('.my_form_input > input').on('blur', function() {
        console.log($(this).next().find('label').val());
        if($(this).val() === '') {
            $(this).next().removeClass('active');
        }
    });

    //DRAG SOCIAL ICONS IN FOOTER
    if($(window).width() <= 768) {
        $('.ssbp.ssbp-right').insertBefore($('footer .container .row:nth-of-type(4)'));
    }
});
