jQuery(function( $ ) {

    var add_to_cart_btn = $('.add-to-cart');

    $('.program-price-table').each(function() {
        var product_id = $(this).data('id');

        var current_product = {};

        if (product_id && wcc_buy_page_settings.products) {
            current_product = wcc_buy_page_settings.products[product_id];
        }


        if ( current_product._price ) {

            var formatted_price = current_product.currency_symbol+current_product._price;          
        
            $(this).html(formatted_price);

        }


    });

    add_to_cart_btn.on( 'click', function( e ) {
        e.preventDefault();

        var data = {},
            button = $(this);

        data = button.data();

        if ( wcc_buy_page_settings.products && data.product_id in wcc_buy_page_settings.products) {
            var current_product = wcc_buy_page_settings.products[ data.product_id ];
            if (current_product.product_set == wcc_buy_page_settings.program_key ) {
                window.location.href = wcc_buy_page_settings.redirect_url + data.product_id;
                return;
            }
        }

        data.action = "cross_add_to_cart";
        button.addClass('loading');

        $.ajax({
            method: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: data,
            dataType: 'json',
            success: function( response ) {
                cross_add_to_cart_callback.call( button, response );
            }
        });
    });

    function cross_add_to_cart_callback ( response ) {

        var self = this;

        var loader_count = 0;

        console.log( response );

        if ( ! response.cookie_data && ! ! response.host ) {
            this.removeClass('loading');
            return;
        }

        if ( response.woo_user_cart_session ) {
            var cart_session_url = "//" + response.woo_user_cart_session.host
                + "/wp-json2/wp/v2/auth/cart.gif?session_name="
                + encodeURIComponent(response.woo_user_cart_session.value)
                + "&session_expiration=" + encodeURIComponent(response.woo_user_cart_session.expires);

            var img = document.createElement('img');
            img.style = "display:none";
            $(img).on('error', function() {
                console.log('loaded');

                // this.removeClass('loading');
                loader_count++;

                if ( response.items_in_cart && loader_count >= 2 ) {
                    self.removeClass('loading');
                    window.location.href = response.cart_page;
                }
            });
            $(img).load(function() {
                console.log('loaded');

                loader_count++;

                if ( response.items_in_cart && loader_count >= 2 ) {
                    self.removeClass('loading');
                    window.location.href = response.cart_page;
                }
                //this.removeClass('loading');
                //window.location.href = response.cart_page;
            });
            img.src = cart_session_url;
            $("body").append( img );
        }

        var auth_url = "//" + response.host
            + "/wp-json2/wp/v2/auth/woo.gif?customer_id="
            + encodeURIComponent(response.cookie_data.customer_id)
            + "&session_expiration=" + encodeURIComponent(response.cookie_data.session_expiration)
            + "&session_expiring=" + encodeURIComponent(response.cookie_data.session_expiring)
            + "&cookie_hash=" + encodeURIComponent(response.cookie_data.cookie_hash);

        var img = document.createElement('img');
        img.style = "display:none";
        $(img).on('error', function() {
            console.log('loaded');

            // this.removeClass('loading');
            loader_count++;

            if ( response.items_in_cart  && loader_count >= 2 ) {
                self.removeClass('loading');
                window.location.href = response.cart_page;
            }
        });
        $(img).load(function() {
            console.log('loaded');

            loader_count++;

            if ( response.items_in_cart  && loader_count >= 2 ) {
                self.removeClass('loading');
                window.location.href = response.cart_page;
            }
            //this.removeClass('loading');
            //window.location.href = response.cart_page;
        });
        img.src = auth_url;
        $("body").append( img );
    }

    // init

    var selected_products = $('.products option:selected');

    //console.log( selected_products );

    $.each(selected_products, function( index, product ) {

        handle_product( product );

    });

    $('.products').on( 'change', function( e ) {

        handle_product( this );

    });

    function handle_product( product ) {
        var product_holder = $( product ).closest('.buy-order-t-item');
        var product_id = $( product ).val();

        var current_product = {};

        if (product_id && wcc_buy_page_settings.products) {
            current_product = wcc_buy_page_settings.products[product_id];
        }

        var str = current_product.name;
	var str_res = str.replace("iDDNA®", "<short>i</short>DDNA<sup>®</sup>");

        product_holder.find('.product-title').html(str_res);
        product_holder.find('.product-description').html(current_product.description);

        var add_to_cart_button = product_holder.find('.add-to-cart');

        if ( current_product._price ) {

            var formatted_price =  current_product.currency_symbol+current_product._price;
            product_holder.find('.product-price').html( formatted_price );

            add_to_cart_button.attr('data-product_id', current_product.id);
            add_to_cart_button.attr('data-quantity', 1);
            add_to_cart_button.removeAttr('disabled');

        } else {

            var formatted_price = 'no price';
            product_holder.find('.product-price').html( formatted_price );

            add_to_cart_button.removeAttr('data-product_id');
            add_to_cart_button.removeAttr('data-quantity');
            add_to_cart_button.attr('disabled', 'disabled');
        }
    }

    $('.bohb_item_tabs .bohb_item').on('click', function() {
        var current_tab = $(this);
        var current_id = current_tab.data('tab_id');
        var all_tabs = current_tab.closest('.bohb_item_tabs').find('.bohb_item');

        $('.bohb_item_value').val( current_id );
        var table = current_tab.closest('.buy-order-table');

        var packages = table.find('.buy-order-t-container');
        var current_package = table.find('.buy-order-t-container.' + current_id);

        packages.removeClass('active');
        current_package.addClass('active');

        all_tabs.removeClass('active');
        current_tab.addClass('active');
    });

    var current_id = $('.bohb_item_value').val();
    var current_tab = $('.bohb_item_tabs .bohb_item.' + current_id);
    var all_tabs = current_tab.closest('.bohb_item_tabs').find('.bohb_item');

    var table = current_tab.closest('.buy-order-table');

    var packages = table.find('.buy-order-t-container');
    var current_package = table.find('.buy-order-t-container.' + current_id);

    packages.removeClass('active');
    current_package.addClass('active');

    all_tabs.removeClass('active');
    current_tab.addClass('active');
});