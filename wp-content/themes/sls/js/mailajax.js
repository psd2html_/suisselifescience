if (window.attachEvent) {
    window.attachEvent('onload', loadSoeJs);
} else {
    if (window.onload) {
        if (typeof isSeoFunctionLoaded == 'undefined') {
            isSeoFunctionLoaded = false;
        }
        if (!isSeoFunctionLoaded) {
            var curronload = window.onload;
            var newonload = function(evt) {
                //curronload(evt);
                loadSoeJs(evt);
            };
            window.onload = newonload;
            isSeoFunctionLoaded = true;
        }
    } else {
        window.onload = loadSoeJs;
    }
}

function loadSoeJs() {
    var head = document.getElementsByTagName('head').item(0);
    var js = document.createElement('script');
    js.setAttribute('type', 'text/javascript');
    js.setAttribute('src', '//static.scheduleonce.com/mergedjs/ScheduleOnceEmbed.js');
    js.setAttribute('defer', 'true');
    js.async = true;
    head.appendChild(js);
}(function() {
    function SOEScriptLoaded() {
        if (typeof soe != 'undefined') {
            soe.InitButton("//secure.scheduleonce.com/idna?thm=white&bc=006DAF&tc=FFFFFF", 'idna', "Schedule an Appointment", "rgb(0, 109, 175)", "rgb(255, 255, 255)", "false");
            soe.AddEventListners("", "", "", "", "false");
        } else {
            setTimeout(SOEScriptLoaded, 1000);
        }
    }
    setTimeout(SOEScriptLoaded, 1000)
})();

function clearForm(self) {
    self.find("button").removeAttr("disabled");
    var countryElem = self.find(".js-hint>div").detach();
    self.resetForm();
    countryElem.appendTo(self.find(".js-hint"));
    putGeoData(jQuery);
}

function sendAjaxToCurrentServer($, self, registrations) {

    self.find("button").attr("disabled", "disabled");

    // Get page title and page url
    var pageTitle, ogTitle, pageUrl;

    ogTitle = $('meta[property="og:title"]')
    if (ogTitle.length) {
      pageTitle = ogTitle.attr('content');
    } else {
      pageTitle = $('title').text();
    }


    pageUrl = document.location.href;

    self.ajaxSubmit({
      url: '/wp-content/themes/sls/include/ajax/ajax_file.php',
      type: 'post',
      data: {
        countryGoogle: GeolocationUser.getCountry(),
        page_title: pageTitle,
        page_url: pageUrl,
      },
      success: function(json) {
        console.log(json);
        if (!registrations) {
            console.info(json.already_on_db);
            if (json.already_on_db) {
                var copyProtectJqueryModal = $('#clear-modal');
                copyProtectJqueryModal.find('.modal-body').html(json.html);
                copyProtectJqueryModal.find('.modal-title').html(json.title);
                copyProtectJqueryModal.modal();
                setTimeout(function(){
                    console.log('redirect to: ', json.redirect_url);
                    window.location.replace(json.redirect_url);
                }, 5000);
            } else {
                var urlPage = json['thank_page_url'];
                if (json.display_scheduleonce) {
                    soe.toggleLightBox('idna');
                } else {
                    console.log('redirect to: ', json['thank_page_url']);
                    //window.location.href = json['thank_page_url'];
                }
                clearForm(self);
            }
            if (json.status !== true) {
                clearForm(self);
            }
        }
      },
    });
}

jQuery(function($) {
    $iframe = $('#SOI_idna');
    $iframe.load(function () {
      console.log('load iframe');
      $iframe[0].contentWindow.onbeforeunload = function () {
        console.log('unload iframe');
        window.location.href = urlPage;
      };
    });

  var queries = [
    '#ajax_contact_send',
    '#ajax_contact_send_mobile',
    '#ajax_header_send',
    '#ajax_footer_send',
    '#ajax_main_send',
    '#ajax_mainform_send',
    '#ajax_contact_send_second',
	'body.home form',
	'body.page-template-template-page-home-new form',
  ];

  $(queries.join(', ')).submit(function() {
    var self = $(this);
    sendAjaxToCurrentServer($, self);

    return false;
  });

  $("input[name='First name'],input[name='Last name']").on({
    keyup: function() {
      if ($(this).val().indexOf(" ") >= 0) {
        $(this).val($(this).val().replace(' ', ''))
      }
    },
    keydown: function() {
      if ($(this).val().indexOf(" ") >= 0) {
        $(this).val($(this).val().replace(' ', ''))
      }
    },
    onchange: function() {
      if ($(this).val().indexOf(" ") >= 0) {
        $(this).val($(this).val().replace(' ', ''))
      }
    }
  });

  $("input[name='City'],input[name='State']").on({
    keyup: function() {
      if ($(this).val().indexOf("%") >= 0) {
        $(this).val($(this).val().replace('%', ''))
      }
    },
    keydown: function() {
      if ($(this).val().indexOf("%") >= 0) {
        $(this).val($(this).val().replace('%', ''))
      }
    },
    onchange: function() {
      if ($(this).val().indexOf("%") >= 0) {
        $(this).val($(this).val().replace('%', ''))
      }
    }
  });
});

