
jQuery(document).ready(function ($) {

    // custom scripts
    function section_height(){
    var heady = jQuery("header").outerHeight();
    jQuery("section:first-of-type").css("margin-top", heady);
    }
    section_height();
    $( window ).resize(function() {
        section_height();
    });
    if ($('#distributors-slider')) {
        $('#distributors-slider').lightSlider({
            adaptiveHeight: true,
            item: 1,
            slideMargin: 0,
            loop: true,
            pager: false
        });
    }

    var infowindow = new google.maps.InfoWindow();

    $('.languige').ddslick();

    function initMap() {

        var map_1 = new google.maps.Map(document.getElementById('map-block'), {
            zoom: 8,
            center:  new google.maps.LatLng(46.685477, 7.178036),
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        if(main_locations.length){
            var value = main_locations[1];

            var marker = new google.maps.Marker({
                map: map_1,
                position: new google.maps.LatLng(value.lat, value.lng)
            });
        }
    }

    if(document.getElementById('map-block')) {
        initMap();
    }

    var map,
        markers = {};
    function initMapCenter() {
        if(document.getElementById('zglushka')){
            map = new google.maps.Map(document.getElementById('zglushka'), {
                zoom: 4,
                scrollwheel: false,
                disableDefaultUI: true
            });
            var bounds = new google.maps.LatLngBounds();

            if(office.length){
                jQuery.each(office, function (index,value){
                    value.loc = new google.maps.LatLng(value.location.lat, value.location.lng);

                    var marker = new google.maps.Marker({
                        map: map,
                        position: value.loc,
                        title: value.company_name
                    });

                    var contentString = '<div class="info-window-content">'+
                        '<h5 class="first-heading">'+value.company_name+'</h5>'+
                        '<div class="body-content">'+
                        '<p>'+value.address+'</p>';
                    if(value.company_phone) {
                        jQuery.each(value.company_phone, function (index, phone) {
                            contentString += '<p>'+phone.number+'</p>';
                        });
                    }

                    contentString += '</div></div>';

                    marker.addListener('click', function() {
                        infowindow.close();
                        infowindow.setContent( contentString );
                        infowindow.open(marker.get('map'), marker);

                        map.setCenter(marker.getPosition());
                    });

                    markers[value.id] = marker;

                    marker.contentString = contentString;

                    bounds.extend(value.loc);
                });
                map.fitBounds(bounds);

                google.maps.event.addListenerOnce(map, 'zoom_changed', function() {
                    var oldZoom = map.getZoom();
                    map.setZoom(4); //Or whatever
                });
            }

            // Setup the click event listener - zoomIn
            google.maps.event.addDomListener(document.getElementById('btn-zoom-controls-zoom-in'), 'click', function() {
                map.setZoom(map.getZoom() + 1);
            });

            // Setup the click event listener - zoomOut
            google.maps.event.addDomListener(document.getElementById('btn-zoom-controls-zoom-out'), 'click', function() {
                map.setZoom(map.getZoom() - 1);
            });
        }
    }

    function clearOverlays() {
        _.each(markers, function( value ) {
            value.setMap(null);
        });
        markers = {};
    }


    var $find_center_form = $('.find-centers');
    if($find_center_form.length) {
        google.maps.event.addDomListener(window, "load", initMapCenter);
    }

    $find_center_form.find('.find-centers-btn').on('click', function(e) {

        var input = $find_center_form.find('.find-centers-input')[0];

        if( input.validity.valueMissing ){
            input.setCustomValidity("Please enter country, city or zip.");
            return true;
        }

        e.preventDefault();

        $find_center_form.toggleClass('loading');

        var $center_list = $(".center-list");

        $center_list.empty();

        clearOverlays();

        var data = {
            action: 'sls_find_centers',
            search: $(this).closest('.find-centers').find('.find-centers-input').val()
        };

        var template = _.template($("#center_template").html());

        $.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: data,
            dataType: 'json',
            success: function( response ) {

                $find_center_form.toggleClass('loading');

                console.log(response);

                var bounds = new google.maps.LatLngBounds();

                if ( response.centers ) {

                    jQuery.each(response.centers, function (index, value) {
                        value.loc = new google.maps.LatLng(value.location.lat, value.location.lng);

                        var marker = new google.maps.Marker({
                            map: map,
                            position: value.loc,
                            title: value.company_name
                        });

                        var contentString = '<div class="info-window-content">'+
                            '<h5 class="first-heading">'+value.company_name+'</h5>'+
                            '<div class="body-content">'+
                            '<p>'+value.address+'</p>';
                        if(value.company_phone) {
                            jQuery.each(value.company_phone, function (index, phone) {
                                contentString += '<p>'+phone.number+'</p>';
                            });
                        }

                        contentString += '</div></div>';

                        marker.addListener('click', function() {
                            infowindow.close();
                            infowindow.setContent( contentString );
                            infowindow.open(marker.get('map'), marker);

                            map.setCenter(marker.getPosition());
                        });

                        marker.contentString = contentString;

                        markers[value.id] = marker;

                        bounds.extend(value.loc);
                    });
                    map.fitBounds(bounds);

                    google.maps.event.addListenerOnce(map, 'zoom_changed', function() {
                        var oldZoom = map.getZoom();
                        map.setZoom(4); //Or whatever
                    });

                    var items = {
                        resellers: _.filter( response.centers, function( value ) { if(value.role == 'Reseller') return value; } ),
                        distributors: _.filter( response.centers, function( value ) { if(value.role == 'Distributor') return value; } )
                    };

                    $center_list.html( template( { items: items } ));
                }

            }
        });

    });


    function distributorsMapInit() {
        var map = new google.maps.Map(document.getElementById('map-block-distributors'), {
            //center: {lat: (Geolocation.latitude*1), lng: (Geolocation.longitude*1)},
            zoom: 8,
            maxZoom: 10,
            scrollwheel: false
        });
        var bounds = new google.maps.LatLngBounds();

        var template = $("#distributors_template").html();

        if (global_distributors.length) {
            jQuery.each(global_distributors, function (index, value) {
                value.loc = new google.maps.LatLng(value.location.lat, value.location.lng);

                var marker = new google.maps.Marker({
                    map: map,
                    position: value.loc,
                    title: value.company_name
                });

                var contentString = '<div class="info-window-content">'+
                    '<h5 class="first-heading">'+value.company_name+'</h5>'+
                    '<div class="body-content">'+
                    '<p>'+value.address+'</p>';
                jQuery.each(value.company_phone, function (index, phone) {
                    contentString += '<p>'+phone.number+'</p>';
                });
                contentString += '</div></div>';

                var infowindow = new google.maps.InfoWindow();

                infowindow.setContent( contentString );
                infowindow.open(marker.get('map'), marker);

                marker.addListener('click', function() {
                    infowindow.close();
                    infowindow.setContent( contentString );
                    infowindow.open(marker.get('map'), marker);
                });

                marker.contentString = contentString;

                markers[value.id] = marker;

                bounds.extend(value.loc);
            });
            map.fitBounds(bounds);
        }

    }


    // select country distributors page

    if (document.getElementById('map-block-distributors')) {

        google.maps.event.addDomListener(window, "load", distributorsMapInit);

    }


    $('#contactus_header_field_15').on('click', function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("#contactus_customer_form_field_1").offset().top - 225
        }, 2000);
    });
	
	
	$('.home-modal').click(function() {
        $(".homevideo").arcticmodal();
		return false;
    });
	

	if(window.location.hash === '#contact-form'){
		$('html, body').animate({
            scrollTop: $("#contactus_customer_form_field_1").offset().top - 225
        }, 1000);
	}

    $(document).on('click', '.show-on-map-center', function(e) {
        e.preventDefault();

        var $currentCenter = $(this).attr('data-center_id');

        var marker = markers[ $currentCenter ];

        infowindow.close();
        infowindow.setContent( marker.contentString );
        infowindow.open(marker.get('map'), marker);

        map.setCenter(marker.getPosition());

        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    //home-find-centers-btn

    var $home_search_centers_form = $(".home-find-centers");

    $home_search_centers_form.find('.home-find-centers-btn').on("click", function( e ) {

        var $input = $home_search_centers_form.find('input'),
            input = $input[0];

        if( input.validity.valueMissing ){
            input.setCustomValidity("Please enter country, city or zip.");
            return true;
        } else {
            e.preventDefault();
            input.setCustomValidity("");
            window.location.href = '/centers/' + $input.val();
        }

    });

		
});