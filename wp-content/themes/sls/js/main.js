
jQuery(document).ready(function ($) {

    
    jQuery('.rishalo_btn').click(function () {
  jQuery('.mobile_rushalo').toggleClass('active');
  jQuery('.mobile_rushalo .header_search_input').slideToggle()
});
    
    
    
    /* Initialize wow.js */
        // console.log('Initialize wow.js');
        var wow = new WOW({
            //mobile: false
            callback:     function(box) {
                if($(box).hasClass("sls-animation_block")){
                    $(box).addClass("on");
                }
            }
        });
        wow.init();
    // custom scripts
    function section_height(){
    var heady = jQuery("header").outerHeight();
    jQuery("section:first-of-type").css("margin-top", heady);
    }
    section_height();
    $( window ).resize(function() {
        section_height();
    });
    if ($('#distributors-slider')) {
        $('#distributors-slider').lightSlider({
            adaptiveHeight: true,
            item: 1,
            slideMargin: 0,
            loop: true,
            pager: false
        });
    }

    if (typeof google === 'object' && typeof google.maps === 'object') {
        var infowindow = new google.maps.InfoWindow();
    }


    $('.languige').ddslick();
	var isDraggable = jQuery(document).width() > 480 ? true : false;
    function initMap() {

        var map_1 = new google.maps.Map(document.getElementById('map-block'), {
            zoom: 8,
            //center:  new google.maps.LatLng(46.685477, 7.178036),
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: isDraggable,
            //mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        if(main_locations.length){
            var value = main_locations[1];
            var marker = new google.maps.Marker({
                map: map_1,
                position: new google.maps.LatLng(value.lat, value.lng)
            });
        }
    }

    if(document.getElementById('map-block')) {
        initMap();
    }

    var map,
        markers = {};
    function initMapCenter() {
        if(document.getElementById('zglushka')){
            map = new google.maps.Map(document.getElementById('zglushka'), {
                zoom: 8,
                maxZoom:16,
                draggable: isDraggable,
                scrollwheel: false,
                disableDefaultUI: true,
            });

              // Setup the click event listener - zoomIn
            google.maps.event.addDomListener(document.getElementById('btn-zoom-controls-zoom-in'), 'click', function() {
                map.setZoom(map.getZoom() + 1);
            });

            // Setup the click event listener - zoomOut
            google.maps.event.addDomListener(document.getElementById('btn-zoom-controls-zoom-out'), 'click', function() {
                map.setZoom(map.getZoom() - 1);
            });
           
            var bounds = new google.maps.LatLngBounds();
            var boundsRelevant = new google.maps.LatLngBounds();
            if(office.length){
                jQuery.each(office, function (index,value){                   
                    value.loc = new google.maps.LatLng(value.location.lat, value.location.lng);

                    var marker = new google.maps.Marker({
                        map: map,
                        position: value.loc,
                        contentString:value.HTML
                    });

                    var contentString = '<div class="info-window-content">'+
                        '<h5 class="first-heading">'+value.company_name+'</h5>'+
                        '<div class="body-content">'+
                        '<p>'+value.address+'</p>';
//                    if(value.company_phone) {
//                        jQuery.each(value.company_phone, function (index, phone) {
//                            contentString += '<p>'+phone.number+'</p>';
//                        });
//                    }

                    contentString += '</div></div>';

                    marker.addListener('click', function() {
                        infowindow.close();
                        infowindow.setContent( this.contentString );
                        infowindow.open(marker.get('map'), marker);

                        map.setCenter(marker.getPosition());
                    });

                    markers[value.id] = marker;

                    //marker.contentString = contentString;

                    bounds.extend(value.loc);
                });
                 if(window.jsOfficeRelevant !== undefined){
                    jQuery.each(jsOfficeRelevant, function (index,value){
                        jsOfficeRelevant[index].loc = new google.maps.LatLng(value.location.lat, value.location.lng);
                        boundsRelevant.extend(jsOfficeRelevant[index].loc);
                    });
                    //map.fitBounds(boundsRelevant);
		    map.fitBounds(bounds);
                }else{
                    map.fitBounds(bounds);	
                }

            }

         
        }
        
      
    }

    function clearOverlays() {
        _.each(markers, function( value ) {
            value.setMap(null);
        });
        markers = {};
    }


   var $find_center_form = $('.find-centers');
    if($find_center_form.length) {
       google.maps.event.addDomListener(window, "load", initMapCenter);
        
   }

//    $find_center_form.find('.find-centers-btn').on('click', function(e) {
//
//        var input = $find_center_form.find('.find-centers-input')[0];
//
//        if( input.validity.valueMissing ){
//            input.setCustomValidity("Please enter country, city or zip.");
//            return true;
//        }
//
//        e.preventDefault();
//
//        $find_center_form.toggleClass('loading');
//
//        var $center_list = $(".center-list");
//
//        $center_list.empty();
//
//        clearOverlays();
//
//        var data = {
//            action: ajax_method,
//            search: $(this).closest('.find-centers').find('.find-centers-input').val()
//        };
//
//        var template = _.template($("#center_template").html());
//
//        $.ajax({
//            type: 'POST',
//            url: '/wp-admin/admin-ajax.php',
//            data: data,
//            dataType: 'json',
//            success: function( response ) {
//
//                $find_center_form.toggleClass('loading');
//
//                console.log(response);
//
//                var bounds = new google.maps.LatLngBounds();
//
//                if ( response.centers ) {
//
//                    jQuery.each(response.centers, function (index, value) {
//                        value.loc = new google.maps.LatLng(value.location.lat, value.location.lng);
//
//                        var marker = new google.maps.Marker({
//                            map: map,
//                            position: value.loc,
//                            contentString:office.HTML
//                        });
//
//                        var contentString = '<div class="contentString-window-content">'+
//                            '<h5 class="first-heading">'+value.company_name+'</h5>'+
//                            '<div class="body-content">'+
//                            '<p>'+value.address+'</p>';
//                        if(value.company_phone) {
//                            jQuery.each(value.company_phone, function (index, phone) {
//                                contentString += '<p>'+phone.number+'</p>';
//                            });
//                        }
//
//                        contentString += '</div></div>';
//
//                        marker.addListener('click', function() {
//                            infowindow.close();
//                            infowindow.setContent( this.contentString );
//                            infowindow.open(marker.get('map'), marker);
//
//                            map.setCenter(marker.getPosition());
//                        });
//
//                        //marker.contentString = contentString;
//
//                        markers[value.id] = marker;
//
//                        bounds.extend(value.loc);
//                    });
//                    map.fitBounds(bounds);
//
//                    google.maps.event.addListenerOnce(map, 'zoom_changed', function() {
//                        var oldZoom = map.getZoom();
//                        map.setZoom(4); //Or whatever
//                    });
//
//                    var items = {
//                        resellers: _.filter( response.centers, function( value ) { if(value.role == 'Reseller') return value; } ),
//                        distributors: _.filter( response.centers, function( value ) { if(value.role == 'Distributor') return value; } )
//                    };
//
//                    $center_list.html( template( { items: items } ));
//                }
//
//            }
//        });
//
//    });
//

    function distributorsMapInit() {
      
        var map = new google.maps.Map(document.getElementById('map-block-distributors'), {
            //center: {lat: (Geolocation.latitude*1), lng: (Geolocation.longitude*1)},
            zoom: 8,
            maxZoom:16,
            draggable: isDraggable,
            scrollwheel: false,
            disableDefaultUI: true,
        });
        
         // Setup the click event listener - zoomIn
        google.maps.event.addDomListener(document.getElementById('btn-zoom-controls-zoom-in'), 'click', function() {
            map.setZoom(map.getZoom() + 1);
        });

        // Setup the click event listener - zoomOut
        google.maps.event.addDomListener(document.getElementById('btn-zoom-controls-zoom-out'), 'click', function() {
            map.setZoom(map.getZoom() - 1);
        });
        
        
        var bounds = new google.maps.LatLngBounds();
        var boundsRelevant = new google.maps.LatLngBounds();
        var template = $("#distributors_template").html();

        if (global_distributors.length) {
            jQuery.each(global_distributors, function (index, value) {
                value.loc = new google.maps.LatLng(value.location.lat, value.location.lng);
                var marker = new google.maps.Marker({
                    map: map,
                    position: value.loc,
                    contentString:value.HTML
                });

                var contentString = '<div class="contentString-window-content">'+
                    '<h5 class="first-heading">'+value.company_name+'</h5>'+
                    '<div class="body-content">'+
                    '<p>'+value.address+'</p>';
//                if(value.company_phone) {
//                jQuery.each(value.company_phone, function (index, phone) {
//                    contentString += '<p>'+phone.number+'</p>';
//                });
//                }
                contentString += '</div></div>';

                // var infowindow = new google.maps.InfoWindow();

                // infowindow.setContent( contentString );
                // infowindow.open(marker.get('map'), marker);

                marker.addListener('click', function() {
                    infowindow.close();
                    infowindow.setContent( this.contentString );
                    infowindow.open(marker.get('map'), marker);
                });

                //marker.contentString = contentString;

                markers[value.id] = marker;
                //markers.push(marker);
                bounds.extend(value.loc);
            });
            map.fitBounds(bounds);

//            google.maps.event.addListenerOnce(map, 'zoom_changed', function() {
//                var oldZoom = map.getZoom();
//                map.setZoom(3); //Or whatever
//                map.setCenter(new google.maps.LatLng(46.685477, 7.178036));
//            });
           
            
        }
        
       

    }


    // select country distributors page

    if (document.getElementById('map-block-distributors')) {

        google.maps.event.addDomListener(window, "load", distributorsMapInit);

    }


    $('#contactus_header_field_15').on('click', function(e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("#contactus_customer_form_field_1").offset().top - 225
        }, 2000);
    });


	$('.home-modal').click(function() {
        $(".homevideo").arcticmodal();
		return false;
    });


	if(window.location.hash === '#contact-form'){
		$('html, body').animate({
            scrollTop: $("#contactus_customer_form_field_1").offset().top - 225
        }, 1000);
	}

    $(document).on('click', '.show-on-map-center', function(e) {
        e.preventDefault();

        var $currentCenter = $(this).attr('data-center_id');

        var marker = markers[ $currentCenter ];

        var map = marker.get('map');

        infowindow.close();
        infowindow.setContent( marker.contentString );
        infowindow.open(marker.get('map'), marker);

        map.setCenter(marker.getPosition());
        map.setZoom(12);
        $("html, body").animate({ scrollTop: 0 }, "slow");
    
      

    });

    //home-find-centers-btn

    var $home_search_centers_form = $(".home-find-centers");

    $home_search_centers_form.find('.home-find-centers-btn').on("click", function( e ) {

        var $input = $home_search_centers_form.find('input'),
            input = $input[0];

        if( input.validity.valueMissing ){
            input.setCustomValidity("Please enter country, city or zip.");
            return true;
        } else {
            e.preventDefault();
            input.setCustomValidity("");
            window.location.href = '/centers/' + $input.val();
        }

    });

    // var $elements = $('input,select,textarea');

    // $($elements).each( function( index, item ) {
    //     item.addEventListener('invalid',function(){
    //             this.scrollIntoView(false);
    //     });
    // });

    webshim.activeLang('en');
    webshims.polyfill('forms');
    webshims.cfg.no$Switch = true;

    // var carousel = $(".carousel-inner");

    // if(carousel.length) {
    //     //Enable swiping...
    //     carousel.swipe({
    //         //Generic swipe handler for all directions
    //         swipeLeft: function (event, direction, distance, duration, fingerCount) {
    //             $(this).parent().carousel('next');
    //         },
    //         swipeRight: function () {
    //             $(this).parent().carousel('prev');
    //         },
    //         //Default is 75px, set to 0 for demo so any distance triggers swipe
    //         threshold: 0
    //     });
    // }
    $(document).on('click', 'form [type="submit"]', function(e) {
        var that = $(this);
        setTimeout(function(){
            if(that.parents('form').find('.user-error').length){
                var scrolltop = that.parents('form').find('.user-error').offset().top;
                var body = $("html, body");
                body.stop().animate({scrollTop: scrolltop-200}, '500');
            }
        }, 500)
    });

});

