window.GeolocationUser = {
	init:function(){
		self=this;
		if(!self.getCountry()){
			self.getGeolocationUserLatLng();
		}
	},
	getGeolocationUserLatLng : function(){
		var self = this;
		return jQuery.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCXHoJ82R_UPczlOjEmVXpZ7-bqA4FzCy8&v=3").done(function(data){
			self.getGeolocationUserAddress(data).done(function(resultGeocode){
				self.setUserInfo(resultGeocode);
			})
		});
	},
	getGeolocationUserAddress:function(data){
		return jQuery.post("http://maps.googleapis.com/maps/api/geocode/json?latlng="+data.location.lat+","+data.location.lng+"&language=en&v=3");
		
	},
	setUserInfo :function(resultGeocode){
		var self = this;
		console.log(resultGeocode.results[0]);
		for(i = 0; i < resultGeocode.results[0]["address_components"].length; i++){
			if(resultGeocode.results[0]["address_components"][i]["types"][0]=="country"){
				var country = resultGeocode.results[0]["address_components"][i]["long_name"];
				self.setCountry(country);
			}else if(resultGeocode.results[0]["address_components"][i]["types"][0]=="locality"){
				var city = resultGeocode.results[0]["address_components"][i]["long_name"];
				self.setCity(city);
			}else if(resultGeocode.results[0]["address_components"][i]["types"][0]=="administrative_area_level_1"){
				var state = resultGeocode.results[0]["address_components"][i]["long_name"];
				self.setState(state);
			}
		}
	},
	setCountry:function(country){
		jQuery.cookie('geo_country', country, { expires: 0.4 });
	},
	setState:function(state){
		jQuery.cookie('geo_state', state, { expires: 0.4 });
	},
	setCity:function(city){
		jQuery.cookie('geo_city', city, { expires: 0.4 });
	},
	setLatLng:function(latLng){
		jQuery.cookie('latlng', "{'lat':'"+latLng.lat+"','lng':'"+latLng.lng+"'}", { expires: 0.4 });
	},
	getCountry:function(){
		if(jQuery.cookie("geo_country")){
			return jQuery.cookie("geo_country");
		}else{
			return false;
		}
	},
	getState:function(){
		if(jQuery.cookie("geo_state")){
			return jQuery.cookie("geo_state");
		}else{
			return false;
		}
	},
	getCity:function(){
		if(jQuery.cookie("geo_city")){
			return jQuery.cookie("geo_city");
		}else{
			return false;
		}
	},
	getUserGeoInfo:function(){
		var self = this;
		if(this.getCountry()){
			var geoInfo={
				"country": this.getCountry(),
				"state": this.getState(),
				"city": this.getCity()
			}
			return geoInfo;
		}else{
			self.getGeolocationUserLatLng();
		}
	}
}
GeolocationUser.init();