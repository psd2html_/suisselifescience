jQuery(function($){
    //console.log(GeolocationUser.getState());
	function timeOutRequest(){
		if (GeolocationUser.getCountry()){
            putGeoData($);
		}else{
			setTimeout(function(){
				timeOutRequest();
			},1000);
		}
	}
	timeOutRequest();
});

function putGeoData($) {
    var html = '<span class="req">required</span>';
    $("input[name='First name']").prop('pattern', '.{2,}');
    $("input[name='Last name']").prop('pattern', '.{2,}');
    if (GeolocationUser.getCity()) {
        $("input[name='City']").val(GeolocationUser.getCity());
    }
    if (GeolocationUser.getCountry()) {
        $("select[name='Country']").each(function(){
            $(this).siblings('input').remove();
            $(this).find("option[value='"+GeolocationUser.getCountry()+"']").first().prop('selected', true);
            $(this).selectToAutocomplete();
            $(this).siblings('input').show();
        });
    }
    if (GeolocationUser.getState()) {
        var country = $("select[name='Country']").val();
        //country = 'United States';
        if ((country === 'United States') || (country === 'Canada')) {
            $("input[name='State']").val(GeolocationUser.getState());
            $("input[name='State']").prop('required',true);
            $("input[name='State']").parent().append(html);
        } else {
            $("input[name='State']").prop("disabled", true);
            $("input[name='State']").parent().parent().hide();
            $("input[name='State']").val('');
        }
    }
}
