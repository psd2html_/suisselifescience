<?php /* Template Name: Centers */ ?>
<?php

get_header();
$fields = get_fields(get_the_ID());
?>

<?php

echo <<<CT
            <script type="text/template" id="center_template">
                <% if(items.resellers.length) { %>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <h4 class="uppercase text-blue m-t-0"><b><short>i</short>DDNA<sup>®</sup> ANTI-AGING CENTERS </b></h4></div>
                                </div>
                                <div class="row">
                                    <% _.each(items.resellers, function( center ) { %>
                                        <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3">
                                            <h4 class=""><%= center.country %></h4>
                                            <h5 class="m-b-5 bold"><%= center.company_name %></h5>
                                            <p class="f-14">
                                                <%= center.address %>
                                                    <br>
                                                    <%if(center.zip){%>
                                                        <%= center.zip %>,
                                                            <%}%>
                                                                <%= center.city %>
                                            </p>
                                            <h5 class="m-t-15 font-light"><a class="show-on-map-center" data-center_id="<%= center.id %>">Show on map</a></h5>
                                        </div>
                                        <% }); %>
                                </div>
                            </div>
                        </div>
                    </div>
                <% } %>
                <% if( 0 < items.resellers.length && 0 < items.distributors.length) { %>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <hr>
                        </div>
                    </div>
                </div>
                <% } %>
                <% if(items.distributors.length) { %>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <% _.each(items.distributors, function( center ) { %>
                                        <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3">
                                            <h4 class=""><%= center.country %></h4>
                                            <h5 class="m-b-5 bold"><%= center.company_name %></h5>
                                            <p class="f-14">
                                                <%= center.address %>
                                                    <br>
                                                    <%if(center.zip){%>
                                                        <%= center.zip %>,
                                                            <%}%>
                                                                <%= center.city %>
                                            </p>
                                            <h5 class="m-t-15 font-light"><a class="show-on-map-center" data-center_id="<%= center.id %>">Show on map</a></h5>
                                        </div>
                                        <% }); %>
                                </div>
                            </div>
                        </div>
                    </div>
                <% } %>
            </script>
CT;
?>

<svg xmlns="http://www.w3.org/2000/svg" style="display:none">
    <symbol id="loading-circle" viewBox="0 0 18 18">
        <circle opacity=".1" stroke-width="2" stroke-miterlimit="10" cx="9" cy="9" r="8" fill="none"></circle>
        <circle stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" stroke-dasharray="15,100" cx="9" cy="9"
                r="8" fill="none" transform="rotate(78.4817 9 9)">
            <animateTransform attributeName="transform" begin="0s" dur="1s" type="rotate" from="0 9 9" to="360 9 9"
                              repeatCount="indefinite"></animateTransform>
        </circle>
    </symbol>
</svg>

<section class="map-section full_height">
    <div class="map-block">
        <div class="top_h1">
            <h3 class="text-white font-ligth m-b-20"><?php echo $fields['section_1_field_1']; ?></h3>
        </div>
        <div id="zglushka" class="zglushka">
        </div>
        <div class="centers_finder_block">
            <div class="container">
                <div class="btn-zoom-controls">
                    <button id="btn-zoom-controls-zoom-in" class="btn-zoom-controls-zoom-in"><i class="fa fa-plus"></i>
                    </button>
                    <button id="btn-zoom-controls-zoom-out" class="btn-zoom-controls-zoom-out"><i
                            class="fa fa-minus"></i></button>
                </div>
                <h2 class="form-title"><?php echo $fields['section_1_field_2']; ?></h2>
                <div class="maper_searcher">
                    <form class="find-centers form-inline inline vt f-0 col-sm-12" action="#map-section" method="POST">
                        <div class="input-group input-group-xs">
                            <input required type="text" class="find-centers-input m-0 "
                                   placeholder="<?php echo $fields['section_1_field_3']; ?>" name="search"
                                   value="<?php //if(isset($_POST['search'])){echo $_POST['search'];}?>">

                            <div class="input-group-btn">
                                <button type="submit"
                                        class="nbtn nbtn-white find-centers-btn"><?php echo $fields['section_1_field_4']; ?></i></button>
                                <svg class="loading-icon">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#loading-circle"></use>
                                </svg>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php

global $sls_data;
wp_enqueue_script('google-map', "https://maps.googleapis.com/maps/api/js?key=AIzaSyA55HS4C36lVMwdXSHgeY-BiVwcsfvsa20");
wp_enqueue_script('underscore');

global $wpdb;

$jsOfficeRelevant = [];
$jsOffice = [];

$db_table_name = SECURE_CENTERS_TABLE;

$mypostsSearch = array();
$search_count = 0;
$lat_lon = Seven_Geolocation::geolocate_coordinates('', true, true);
$query_select_order_by = "";
$query_select_columns = "select * from `{$db_table_name}` ";
if (!empty($lat_lon)) {
    if (array_key_exists("latitude", $lat_lon) && array_key_exists("longitude", $lat_lon)) {
        $latitude = $lat_lon['latitude'];
        $longitude = $lat_lon['longitude'];
        $earthRadius = '99999999999.0'; //in miles.
        $query_select_columns = "select *,ROUND(
                {$earthRadius} * ACOS(
                    SIN({$latitude} * PI() / 180) * SIN(latitude * PI() / 180) + COS({$latitude} * PI() / 180)
                    * COS(latitude * PI() / 180) * COS((longitude * PI() / 180) - ({$longitude} * PI() / 180))
                  ),
                  1
              ) AS DISTANCE from `{$db_table_name}` ";
        $query_select_order_by = " ORDER BY DISTANCE ";
    }
}


if (!isset($_REQUEST['search'])) {
    $country_code = Seven_Geolocation::geolocate_ip('', true, true);
    //$country_code = "AD";    
    $country_zone = get_country_zone_by_code($country_code);
    $country_name = get_country_name_by_code($country_code);
    //print_r(" country_code  " . $country_code);
    if (!empty($country_name)) {
        $args = $query_select_columns
            . " where town_city='{$country_name}'"
            . " OR country='{$country_name}'"
            . " OR state='{$country_name}'"
            . " OR zip='{$country_name}'"
            . $query_select_order_by;

        $mypostsSearch = $wpdb->get_results($args);
        if (count($searchRelevant) == 0) {
            $zone_countries = get_zone_countries($country_zone);
            if (count($zone_countries) > 0) {
                $args = $query_select_columns
                    . " where town_city IN ( '" . implode($zone_countries, "', '") . "')"
                    . " OR country IN ( '" . implode($zone_countries, "', '") . "')"
                    . " OR state IN ( '" . implode($zone_countries, "', '") . "')"
                    . " OR zip IN ( '" . implode($zone_countries, "', '") . "')"
                    . $query_select_order_by;

                $mypostsSearch = $wpdb->get_results($args);

            }
        }

    }
} else {
    $args = $query_select_columns
        . " where town_city='{$_REQUEST['search']}'"
        . " OR country='{$_REQUEST['search']}'"
        . " OR state='{$_REQUEST['search']}'"
        . " OR zip='{$_REQUEST['search']}'"
        . $query_select_order_by;
    $mypostsSearch = $wpdb->get_results($args);
    //print_r("mypostsSearch : ". count($mypostsSearch));
};


$searchRelevant = array();
if (isset($mypostsSearch) && count($mypostsSearch) > 0) {
    foreach ($mypostsSearch as $post) {
        $searchRelevant[] = $post->id;
    };
};
$search_count = count($mypostsSearch);
if (!empty($mypostsSearch) && count($mypostsSearch) > 0) {
    include('include/centers/center-block.php');
}

$args = $query_select_columns . " where id NOT IN ( '" . implode($searchRelevant, "', '") . "' )" . $query_select_order_by;

$myposts = $wpdb->get_results($args);
//print_r("centers count : " . count($centers));

//print_r(" jsOffice : " . json_encode($jsOffice));


$dist_centers = array();
$res_centers = array();

foreach ($mypostsSearch as $center) {
    if (strtolower($center->user_role) == 'partner') {
        $res_centers[] = $center;
    } else if (strtolower($center->user_role) == "distributor" || strtolower($center->user_role) == "master-distributor") {
        $dist_centers[] = $center;
    }

    include('include/centers/centers-marker-html.php');
    $center_details = array(
        'id' => $center->id,
        'location' => array("lat" => $center->latitude, "lng" => $center->longitude),
        'company_name' => (strtolower($center->user_role) == "partner" ? $center->brand_name : $center->company_name),
        'company_phone' => $center->company_phone,
        'city' => $center->town_city,
        'zip' => $center->zip,
        'state' => $center->state,
        'address' => $center->street_address,
        'country' => $center->country,
        'HTML' => $markerHTML
    );

    $jsOfficeRelevant[] = $center_details;
    $jsOffice[] = $center_details;
}

foreach ($myposts as $center) {

    if ($center->user_role == 'partner' && isset($center->company_name) && !empty($center->company_name)) {
        $res_centers[] = $center;
    } else if (isset($center->company_name) && !empty($center->company_name)) {
        $dist_centers[] = $center;
    }

    include('include/centers/centers-marker-html.php');
    $center_details = array(
        'id' => $center->id,
        'location' => array("lat" => $center->latitude, "lng" => $center->longitude),
        'company_name' => (strtolower($center->user_role) == "partner" ? $center->brand_name : $center->company_name),
        'company_phone' => $center->company_phone,
        'city' => $center->town_city,
        'zip' => $center->zip,
        'state' => $center->state,
        'address' => $center->street_address,
        'country' => $center->country,
        'HTML' => $markerHTML
    );

    $jsOffice[] = $center_details;
}


?>

<section class="login_bg centers_res">
    <? if ($search_count == 0 && isset($_REQUEST['search'])) { ?>
        <p style="color:red;">We were unable to find centers by your query. Please refine it or see the full list of
            centers below.</p>
    <? }; ?>

    <div class="top_h1">
        <h3 class="text-white font-ligth m-b-20"><b>FIND AN iDDNA</b><sub>®</sub> ANTI-AGING CENTER CLOSE TO YOU</h3>
    </div>

    <div class="center-list">
        <div class="container">
            <div class="row">
                <?php if (!empty($res_centers)) : ?>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-lg-12 ">
                                <h4 class="uppercase text-blue m-t-0"><b>
                                        <short>i</short>
                                        DDNA<sup>®</sup> ANTI-AGING CENTERS </b></h4>
                            </div>
                        </div>
                        <div class="row">
                            <?php foreach ($res_centers as $center): ?>
                                <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3">
                                    <!--                                                <h4 class="uppercase"></h4>-->
                                    <h5 class="uppercase"><b><?= $center->brand_name; ?></b></h5>
                                    <?php if ($center->center_name && !empty($center->center_name)) : ?>
                                        <h5 class="uppercase"><b><?= $center->center_name; ?></b></h5>
                                    <?php endif; ?>
                                    <?php include("include/centers/display_block_center.php"); ?>

                                    <h5 class="m-t-15 font-light"><a class="show-on-map-center"
                                                                     data-center_id="<?php echo $center->id; ?>"><?php echo __('Show on map', 'sls-theme'); ?></a>
                                    </h5>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php if (!empty($res_centers) && !empty($dist_centers)) : ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 ">
                        <hr>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="container">
            <div class="row">
                <?php if (!empty($dist_centers)) : ?>
                    <div class="col-md-12">
                        <div class="row">
                            <?php foreach ($dist_centers as $center): ?>
                                <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3">
                                    <h5 class="uppercase"><b><?= $center->company_name; ?></b></h5>
                                    <?php include("include/centers/display_block_center.php"); ?>
                                    <h5 class="m-t-15 font-light"><a class="show-on-map-center"
                                                                     data-center_id="<?php echo $center->id; ?>"><?php echo __('Show on map', 'sls-theme'); ?></a>
                                    </h5>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section class="b-t-grey ">
    <?php $entity = 'centers'; ?>
    <div class="container">
        <div class="clearfix">
            <div class="col-sm-4">
            </div>
            <div class="col-sm-8">
                <h2 class=" form-title"><?= get_field($entity . '_title', 'option'); ?></h2>
                <p class="form-subtitle"><?= get_field($entity . '_subtitle', 'option'); ?></p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <h4 class="text-blue m-t-0 text-left"
                    style="font-size: 20px;"><?= get_field($entity . '_descriptions_1', 'option'); ?></h4>
                <br>
                <h3 class="text-left bold"><?= get_field($entity . '_descriptions_2', 'option'); ?></h3>
            </div>
            <div class="col-sm-8 contact-form">
                <form id="ajax_contact_send" method="POST" action=""
                      class="form-checkboxes half-width-check ffer_form clearfix my_amazing_form about_fix">
                    <div class="row ">
                        <?php foreach (get_field($entity . '_form_fields', 'option') as $field) { ?>
                            <?php include("include/form/forminputs.php"); ?>
                        <? }; ?>
                    </div>
                    <div class="clearfix "></div>
                    <h4 class="text-blue m-t-45"><?php echo get_field($entity . '_interests_title', 'option'); ?></h4>
                    <div class="form-checkboxes double_row">
                        <?php include("include/sudscribe/subscribe_checkboxes.php"); ?>
                    </div>

                    <div class="subskribe">
                        <?php $agreements = get_field($entity . '_agreements_form', 'option'); ?>
                        <?php foreach ($agreements as $agreement) : ?>
                            <?php $agreement_id = 'agree_' . rand(); ?>
                            <input type="checkbox" id="<?php echo $agreement_id; ?>"
                                   required <?php echo ($agreement['checked']) ? 'checked' : ''; ?>>
                            <label for="<?php echo $agreement_id; ?>"
                                   class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="subject_to_submit"
                           value="<?php echo get_field($entity . '_mail_subject', 'option'); ?>">

                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                    <? foreach (get_field($entity . '_email_for_sending', 'option') as $em) { ?>
                        <input type="hidden" name="email_to_submit[]" value="<?= $em["email"] ?>">
                    <? } ?>
                    <div class="text-center">
                        <button type="submit" class="nbtn nbtn-inverse pull-right">
                            <?php echo get_field($entity . '_submit_buttom', 'option'); ?><i
                                class="ico arrow arrow_white"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>

    <?if (count($jsOfficeRelevant) > 0){?>
    var jsOfficeRelevant = <?=json_encode($jsOfficeRelevant);?>;

    <?}?>
    var ajax_method = 'sls_find_centers';
    var office = <?=json_encode($jsOffice);?>;


</script>

<?php get_footer(); ?>
