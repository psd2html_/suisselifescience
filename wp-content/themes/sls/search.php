<?php
/**
 * Шаблон поиска (search.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); ?>

<?php

global $wpdb;

// If you use a custom search form
// $keyword = sanitize_text_field( $_POST['keyword'] );

// If you use default WordPress search form
$keyword = get_search_query();
$keywords = explode(' ', $keyword);
$keywords = array_filter($keywords, function($word){ return (strlen($word) > 3); });
$keywordsStr = implode('|', $keywords);
$keyword = '%' . $wpdb->esc_like( $keyword ) . '%';

// Search in all custom fields
$post_ids_meta = $wpdb->get_col( $wpdb->prepare( "
    SELECT DISTINCT `post_id` FROM `{$wpdb->postmeta}`
    WHERE `meta_value` LIKE '%s'
", $keyword ) );

// Search in post_title and post_content
$post_ids_post = $wpdb->get_col( $wpdb->prepare( "
    SELECT DISTINCT ID FROM `{$wpdb->posts}`
    WHERE `post_title` LIKE '%s'
    OR `post_content` LIKE '%s'
", $keyword, $keyword) );
//
// Search in all custom fields
$post_ids_meta_all = $wpdb->get_col( $wpdb->prepare( "
    SELECT DISTINCT `post_id` FROM `{$wpdb->postmeta}`
    WHERE `meta_value` REGEXP '%s'
", $keywordsStr ) );

// Search in post_title and post_content
$post_ids_post_all = $wpdb->get_col( $wpdb->prepare( "
    SELECT DISTINCT ID FROM `{$wpdb->posts}`
    WHERE `post_title` REGEXP '%s'
    OR `post_content` REGEXP '%s'
", $keywordsStr, $keywordsStr ) );

$post_ids = array_merge( $post_ids_meta, $post_ids_post, $post_ids_meta_all, $post_ids_post_all );
$post_ids = array_unique($post_ids);

foreach( $post_ids as $key => $value) {
	$post_ids[$key] = (int) $value;
}

$page = (int)$_GET['page'];
// Query arguments
$args = array(
    'post_type'   => array(
        'post',
        'page',
        //'acf-field-group'
    ),
	'post_status' => 'publish',
	'post__in'    => $post_ids,
    'orderby'     => 'post__in',
    'post_per_page' => 10,
    'offset'      => $page,
);

//$args['paged'] = max( get_query_var( 'paged' ), 1 );

$query = (empty($post_ids)) ? new WP_Query() : new WP_Query( $args );
include_once('search/paginator.class.php');
?>
<section>
<?php get_search_form( true ); ?>
	<div class="container search-result">
		<div class="row">
			<div class="<?php content_class_by_sidebar(); ?>">
				<h1><?php printf('Search: %s', get_search_query()); ?></h1>
                <?php if ( $query->have_posts() ): ?>
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
					    <?php include(locate_template('loop-search.php')); ?>
                    <?php endwhile; ?>
                <?php else: echo '<p>Not found.</p>'; endif; ?>	 
                <div class="paginator">
<?php
    $total = $query->found_posts;

    $paginator = new CustomPaginator( $page, 9, $total );
    echo $paginator->createLinks( 3, 'search' );
?>
			    </div>
			</div>
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>
