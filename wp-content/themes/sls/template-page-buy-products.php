<?php /* Template Name: Buy Products */ ?>
<?php get_header(); ?>

<?php

$fields = get_fields( get_the_ID() );

$survey_url = get_option('wcc_survey_url', 'http://stagingidna.7demo.org.ua/survey/?product_id=');

if (!empty($fields['survey_url'])) {
    $survey_url = $fields['survey_url'];
}

class Buy_Products_Page_Handler {


    public static $product_map = array();
    public static $barcode_product_map = array();
    public static $products = array();
    public static $categories = array();
    public static $ACTIVE_CATEGORY = 'skin-care';
    public static $DEFAULT_CATEGORY = 'skin-care';

    public static function get_products_from_store( $product_barcodes = [] ) {

        if ( empty($product_barcodes) ) {
            return false;
        }

        $args = [
        'store_url' => get_option('woo_secure_iddna_url'),
            'consumer_key' => get_option('woo_secure_iddna_consumer_key'), //'ck_4cceaf150ebf1eb29a011f7cf5703866b7d8545a',
            'consumer_secret' => get_option('woo_secure_iddna_consumer_secret') //'cs_b9dc4b9ab60e21fce9cd6cc3579f1a6d4681c142',
            ];

            $woocommerce = new Automattic\WooCommerce\Client(
                $args['store_url'],
                $args['consumer_key'],
                $args['consumer_secret'],
                [
                'version' => 'wc/v1',
                'wp_api' => true,
                'timeout'     => 45,
                ]
                );

            $parameters = [
            'barcodes' => implode( ',', $product_barcodes )
            ];

            if ( isset($_COOKIE['secure_iddna_login'])) {
                $username = get_username_from_cookie($_COOKIE['secure_iddna_login']);

                if (!empty($username)) {
                    $parameters['username'] = $username;
                }
            }

            $country_code = Seven_Geolocation::geolocate_ip( '', true, true );

            $parameters['country_code'] = $country_code;
            if ( isset($_COOKIE['test_country']) ) {
                $parameters['test_country'] = $_COOKIE['test_country'];
            }

            try {
                $_response = $woocommerce->get("products/", $parameters);
            } catch ( Exception $e ) {
            //to long waiting...
            //var_dump( $e );
                return [];
            }

            return $_response;
        }

        public static function get_all_barcodes( $categories ) {

            $barcodes = array();

            foreach ( $categories as $category ) {
                foreach ( $category['packages'] as $package ) {
                    foreach( $package['products'] as $product ) {
                        $barcodes[] = $product['barcode'];
                    }
                }
            }

            $barcodes = array_unique( $barcodes );

            return $barcodes;
        }

        public static function fill_maps() {
            foreach ( self::$products as $product ) {
                self::$product_map[ $product['id'] ] = $product;
                self::$barcode_product_map[  $product['barcode'] ] = $product;
            }
        }

        public static function set_current_category( $current_category ) {

            if ( empty( $current_category ) ) {
                return self::$DEFAULT_CATEGORY;
            }

            foreach ( self::$categories as $category ) {
                $current_category = sanitize_title( $current_category );
                if ( sanitize_title( $category['title'] ) == $current_category ) {
                    return $current_category;
                }
            }

            return self::$DEFAULT_CATEGORY;
        }

        public static function init() {
            global $survey_url;

            wp_register_script( 'buy-product', get_template_directory_uri() . '/js/buy-product.js', array('jquery'), '', true);
            wp_enqueue_script( 'buy-product' );

            $current_category = get_query_var('current_category');

            $page_configs = get_fields( get_the_ID() );

            self::$categories = $page_configs['section_2_categories'];

            if ( empty( self::$categories ) ) {
                return;
            }

            self::$ACTIVE_CATEGORY = self::set_current_category( $current_category );

            $barcodes = self::get_all_barcodes( self::$categories );

            self::$products = self::get_products_from_store( $barcodes );

            self::fill_maps();


            $wcc_buy_page_settings = array(
                'products' => self::$product_map,
                'program_key' => get_option('wcc_program_key', 'program'),
                'redirect_url' => $survey_url
                );
            wp_localize_script( 'buy-product', 'wcc_buy_page_settings', $wcc_buy_page_settings);
        }
    }

    Buy_Products_Page_Handler::init();

    ?>
    <style>
        .buy-order-t-container {
            display: none;
        }
        .buy-order-t-container.active {
            display: block;
        }
        p {
            padding-top: 7px;
        }
    </style>

    <section style="background: transparent url(/wp-content/themes/sls/img/buy-programs-bg.jpg)" class="cover">
        <div class="container">

            <div class="programs-box">
                <h1><?= $fields['buy_table_programs_title']; ?></h1>
                <?= $fields['buy_table_programs_description']; ?>
                <div class="programs-wrap">
                    <?php $products_table = []; ?>
                    <?php foreach( Buy_Products_Page_Handler::$categories as $key => $category ) : ?>
                        <?php $sanitize_title = sanitize_title( $category['title'] ); ?>
                        <?php foreach( $category['packages'] as $package ) : ?>
                            <?php foreach( $package['products'] as $item => $product ) : ?>
                                <?php $current_product = Buy_Products_Page_Handler::$barcode_product_map[ $product['barcode'] ]; ?>
                                <?php $barcodes = array('07iDDNAuc30Fd', '07iDDNAuc30sk', '01DNA'); ?>
                                <?php if (in_array($product['barcode'], $barcodes) && !in_array($product['barcode'], $products_table)): ?>
                                    <?php if ($product['barcode'] == '01DNA') {
                                        $product_type = 'dna_age';
                                        array_push($products_table, $product['barcode']);
                                        $buy_url = '#';
                                        $add_to_cart_class = 'add-to-cart';
                                    } else if ($product['barcode'] == '07iDDNAuc30sk') {
                                        $product_type = 'ultra_custom';
                                        array_push($products_table, $product['barcode']);
                                        $buy_url = $survey_url . $current_product['id'];
                                        $add_to_cart_class = '';
                                    } else if ($product['barcode'] == '07iDDNAuc30Fd') {
                                        $product_type = 'ultra_custom_diet';
                                        array_push($products_table, $product['barcode']);
                                        $buy_url = $survey_url . $current_product['id'];
                                        $add_to_cart_class = '';
                                    }
                                    ?>
                                    <div class="program-item">
                                        <div class="program-info active">
                                            <?php $rp_title = str_replace("iDDNA®", "<short>i</short>DDNA<sup>®</sup>", $current_product['name']); ?>
                                            <p class="program-title"><?php echo $rp_title; ?></p>
                                            <?php $field = 'buy_table_' . $product_type . '_title' ?>
                                            <p class="programs-desc"><?= $fields[$field]; ?></p>
                                            <div>
                                                <p class="program-price program-price-table" data-id="<?=$current_product['id']?>"></p>
                                                <a data-quantity="1" data-product_id="<?=$current_product['id']?>" href="<?= $buy_url ?>" class="nbtn nbtn-inverse <?php echo $add_to_cart_class; ?>"><span></span><?= $fields['buy_table_button_label']; ?></a>
                                            </div>
                                        </div>
                                        <?php for ($i=1; $i<=25; $i++): ?>
                                            <?php $field = 'buy_table_' . $product_type . '_line_'. $i ?>
                                            <?php if ($fields[$field] != '') : ?>
                                                <p class="program-text"><?= $fields[$field]; ?></p>
                                            <?php endif; ?>
                                        <?php endfor; ?>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <p class="h1 text-uppercase font-normal m-t-0  letter-spacing text-center"><?= $fields['ask_your_dna_title_buy'] ?></p>
            <p class="letter-spacing f-23 text-uppercase m-t-0 m-b-30 bold text-center"><?= $fields['join_iddna_title_buy'] ?></p>
            <div class="flex-center">
                <div class="vb inline text-center"> <img src="/wp-content/themes/sls/img/buy-svg-png-1.png" />
                    <?= $fields['choose_option_1_text_buy'] ?>
                    </div>
                    <div class="vb inline text-center"> <span class="or_borded">or</span> </div>
                    <div class="vb inline text-center"> <img src="/wp-content/themes/sls/img/buy-svg-png-2.png" />
                        <?= $fields['choose_option_2_text_buy'] ?>
                        </div>
                    </div>
                    <?= $fields['pre_order_text_buy'] ?>
                        <div class="buy-order-table">
                            <div class="buy-order-t-header clearfix">
                                <div class="buy-ordeer-head_blocks pst">
                                    <div class="bohb_item">PRODUCT SELECTION</div>
                                </div>
                                <div class="buy-ordeer-head_blocks bohb_item_tabs">
                                    <input type="hidden" class="bohb_item_value" value="<?php echo Buy_Products_Page_Handler::$ACTIVE_CATEGORY; ?>">
                                    <?php foreach( Buy_Products_Page_Handler::$categories as $key => $category ) : ?>
                                        <?php $sanitize_title = sanitize_title( $category['title'] ); ?>
                                        <div data-tab_id="<?php echo $sanitize_title; ?>" class="bohb_item <?php echo $sanitize_title; ?>"><?php echo $category['title']; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <?php if ( ! empty( Buy_Products_Page_Handler::$categories ) ) :?>
                                <?php foreach( Buy_Products_Page_Handler::$categories as $key => $category ) : ?>
                                    <?php $sanitize_title = sanitize_title( $category['title'] ); ?>
                                    <div class="buy-order-t-container <?php echo $sanitize_title; ?> <?php echo (Buy_Products_Page_Handler::$ACTIVE_CATEGORY == $sanitize_title ) ? 'active' : ''; ?>">
                                        <?php foreach( $category['packages'] as $package ) : ?>
                                            <div class="buy-order-t-item">
                                                <div class="boti-image"> <img src="<?php echo $package['image']; ?>"> </div>
                                                <div class="boti-info">
                                                    <h4 class="product-title"></h4>

                                                    <p class="product-description"></p>

                                                    <div class="boti-info-footer row">
                                                        <div class="col-md-5">
                                                            <select name="products-select" class="products user-success">
                                                                <?php foreach( $package['products'] as $item => $product ) : ?>
                                                                    <?php $current_product = Buy_Products_Page_Handler::$barcode_product_map[ $product['barcode'] ]; ?>
                                                                    <?php $rp_title = str_replace("iDDNA®", "<short>i</short>DDNA<sup>®</sup>", $current_product['name']); ?>
                                                                    <option value="<?php echo $current_product['id']; ?>"><?php echo $rp_title; ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <p class="product-price"></p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <button class="nbtn nbtn-inverse add-to-cart">Add to cart</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>


                        <h2 class="font-normal letter-spacing"><?= $fields['order_proces_buy'] ?></h2>
                        <?= $fields['content_processing_content_buy']; ?>
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h2 class="m-t-50 letter-spacing font-normal"><?= $fields['payment_methods_title_buy'] ?></h2>
                                        <p class="bold m-b-30"> <?= $fields['payment_methods_header_buy'] ?> <img class="m-l-20 payment-variants" src="/wp-content/themes/sls/img/payment-variants.png"/></p>

                                        <?= $fields['payment_methods_text_buy'] ?>
                                    </div>
                                </div>
                                 
                        <h2 class="text-uppercase m-t-50 letter-spacing font-normal"><?= $fields['shipping_title_buy'] ?></h2>
                        <?= $fields['shipping_text_buy'] ?>
        </div>

    </section>

    <section class="cover text-white experience" style="background-image: url('/wp-content/themes/sls/img/buy-bg-layout.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="m-t-0 m-b-30 bold text-uppercase f-31"><?= $fields['suggestion_title_buy'] ?></h2></div>
                    <div class="col-md-12">
                        <?= $fields['suggestion_content_buy'] ?>
                    </div>
                </div>
            </div>
        </section>
        <?php get_footer(); ?>