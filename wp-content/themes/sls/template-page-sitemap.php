<?php /* Template Name: Sitemap */ ?>
    <?php get_header(); ?>
<section>
	<div class="container sitemap-container">	
		<div>
			<h1 class="m-b-65 text-uppercase"><?=get_field("title")?></h1>
			
		</div>
		<div class="col-sm-6 sitemap-url">
			<ul class="main-sitemap-url">
				<?foreach(get_field("left_1st_link_menu") as $main){?>
					<li><a href="<?=$main["url"]?>"><?=$main["text"]?></a>
						<?if($main["2nd_link_menu"]){?>
							<ul>
								<?foreach($main["2nd_link_menu"] as $sub){?>
									<li><a href="<?=$sub["url"]?>"><?=$sub["text"]?></a></li>
								<?};?>
							</ul>
						<?};?>
					</li> 
				<?};?>
			</ul>
		</div>
		<div class="col-sm-6 sitemap-url">
			<ul class="main-sitemap-url">
				<?foreach(get_field("right_1st_link_menu") as $main){?>
					<li><a href="<?=$main["url"]?>"><?=$main["text"]?></a>
						<?if($main["2nd_link_menu"]){?>
							<ul>
								<?foreach($main["2nd_link_menu"] as $sub){?>
									<li><a href="<?=$sub["url"]?>"><?=$sub["text"]?></a></li>
								<?};?>
							</ul>
						<?};?>
					</li> 
				<?};?>
			</ul>
		</div>
</div>
</section>

	<?php get_footer(); ?>