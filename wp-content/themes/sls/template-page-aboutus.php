<?php /* Template Name: About Us */ ?>
<?php get_header();

    $fields = get_fields( get_the_ID() );

?>
        <section class="about_us_new bg-fixed"
                 <?php if (!empty($fields["aboutus_section_1_background"])) : ?>
                 style="background-image: url('<?php echo $fields["aboutus_section_1_background"]; ?>');
                 <?php endif; ?>
        ">
            <div class="container">
                <h2 class="f-52 font-ligth"><?php echo $fields["aboutus_section_1_field_1"]; ?></h2>
                <h2 class="f-52"><?php echo $fields["aboutus_section_1_field_2"]; ?></h2>
            </div>
        </section>

        <section class="">
            <div class="container text-center">
                <h2 class="m-t-20 "><?php echo $fields["aboutus_section_2_field_1"]; ?></h2>
                <h5 class="m-t-20 m-b-20"><?php echo $fields["aboutus_section_2_field_2"]; ?></h5>
                <h4 class="text-blue "><?php echo $fields["aboutus_section_2_field_3"]; ?></h4>
            </div>
        </section>

        <section class="about_us_second bg-fixed" <?php if (!empty($fields["aboutus_section_3_background"])) : ?>
                 style="background-image: url('<?php echo $fields["aboutus_section_3_background"]; ?>');
                 <?php endif; ?>
        ">
            <div class="container">
                <h2 class="m-t-10"><?php echo $fields["aboutus_section_3_field_1"]; ?></h2>
                <h5 class="m-b-50"><?php echo $fields["aboutus_section_3_field_2"]; ?></h5>
                <h4 class="m-b-20"><?php echo $fields["aboutus_section_3_field_3"]; ?></h4>
                <h5 class="m-b-15"><?php echo $fields["aboutus_section_3_field_4"]; ?></h5>
            </div>
        </section>

        <section class="about_flag">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-4 tutka">
                        <p class="m-b-15"><?php echo $fields["aboutus_section_4_field_1"]; ?></p>

                        <p class="m-b-15"><?php echo $fields["aboutus_section_4_field_2"]; ?></p>
                        <p class="m-b-15"><?php echo $fields["aboutus_section_4_field_3"]; ?></p>
                        <p class="m-b-5"><?php echo $fields["aboutus_section_4_field_4"]; ?></p>
                        <h4 class=" m-b-20"><?php echo $fields["aboutus_section_4_field_5"]; ?></h4>
                        <h4 class="text-blue bold"><?php echo $fields["aboutus_section_4_field_6"]; ?></h4>
                        <!--<img src="<?php echo bloginfo('template_url'); ?>/img/flag-about.png" />-->
                        <p class="m-t-20"><?php echo $fields["aboutus_section_4_field_7"]; ?></p>
                    </div>
                </div>
            </div>
        </section>
        <?php get_footer(); ?>