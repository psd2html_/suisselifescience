<?php /* Template Name: Providers */ ?>
<?php get_header();?>
<!-- <script src="<?php echo bloginfo('template_url'); ?>/js/jquery.touchSwipe.min.js"></script> -->
<?$items = get_field("section_1_slider_items"); ?>
<section class="cover mobile_baner baner_h624" style="background: rgba(0, 0, 0, 0) url('<?php echo $items[0]['image'];?>') repeat  center;">
	<div class="container">
		<h2 class="slide_title"><?php echo $items[0]['title'];?></h2>
		<div class="slide_text">
			<?php echo $items[0]['html_description']; ?>
		</div>
	</div>
</section>
<section class="">
	<div class="container">
		<h1 class="m-t-0 m-b-30"><?=get_field("section_2_title")?></h1>
		<div class="row">
			<div class="col-md-12">
				<p class="m-b-30"><?=get_field("section_2_paragraph_1")?></p>
				<p class="m-b-30"><?=get_field("section_2_paragraph_2")?></p>
				<p class="m-b-30 text-blue f-18"><?=get_field("section_2_paragraph_3_blue")?></p>
				<ul class="blue_ul inside_ul">
					<?foreach(get_field("section_2_list_items") as $key=>$item){?>
						<li><?=$item["description"]?></li>
					<?};?>
				</ul>
				<p class="m-t-30"><?=get_field("section_2_description")?></p>
			</div>
		</div>
	</div>
</section>
<section class="cover mobile_baner baner_h646" style="background-image: url('<?=get_field("section_3_background_image")?>');">
	<div class="container ">
		<div class="row">
			<div class="col-lg-7 col-lg-offset-5 col-md-9 col-md-offset-3">
				<div class="m-b-60 mobile-m-b">
					<h2 class="m-t-0"><?=get_field("section_3_title_1_line")?></h2>
				</div>
				<div class="bold text-uppercase m-b-60 mobile-m-b">
					<p class="f-23"><?=get_field("section_3_description_bold")?></p>
				</div>
				<ul class="blue_ul inside_ul">
					<?foreach(get_field("section_3_list_items") as $key=>$item){?>
						<li><?=$item["description"]?></li>
						<?};?>
					</ul>
				</div>
			</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-4 m-b-20">
				<div class="text-white blue_bg contact_sls">
					<p class="text-left m-b-20"><?=get_field("section_4_left_section_paragraph_1")?></p>
					<p class="text-left m-b-50"><?=get_field("section_4_left_section_paragraph_2")?></p>
					<div class="contacts bold text-uppercase m-b-30">
						<a href="mailto:<?=get_field("section_4_email")?>"><span class="contact-icon"><i class="fa fa-envelope"></i></span><?=get_field("section_4_email")?></a>
						<a href="whatsapp://send?abid=<?=get_field("section_4_whatsapp")?>"><span class="contact-icon"><i class="fa fa-whatsapp"></i></span>WHATSAPP</a>
						<a href="skype:<?=get_field("section_4_skype")?>?call"><span class="contact-icon"><i class="fa fa-skype"></i></span><?=get_field("section_4_skype")?></a>
						<a href="tel:<?=get_field("section_4_telephone")?>"><span class="contact-icon"><i class="fa fa-phone"></i></span><?=get_field("section_4_telephone")?></a>
					</div>
					<div class="social-icons m-b-25 text-center">
						<?foreach(get_field("section_4_social_media") as $key=>$item){?>
							<div class="social-icon social-big"><a href="<?=$item["url"]?>" target="_blank" title=""><i class="fa <?=$item["icon_class"]?>"></i></a></div>
						<?};?>
					</div>
					<p class="p-l-10"><?=get_field("section_4_working_time")?></p>
				</div>
			</div>
                    <?php $entity = 'prividers'; ?>
			<div class="col-xs-12 col-lg-8">
				<h2 class="form-title"><?php echo get_field($entity . '_title', 'option'); ?></h2>
				<div class="contact-form">
					<form id="ajax_register_provider" method="POST" action="" class="offer_form clearfix my_amazing_form about_fix">
						<?php  wp_nonce_field( 'registration_secure_user', 'cross-domain-register-nonce', false ); ?>
						<input type="hidden" name="user_role" value="prospect_partner">

                                                <div class="row">
							<?php foreach(get_field($entity . '_form_fields', 'option') as $field){ ?>
								<?php include("include/form/forminputs.php");?>
							<?};?>
						</div>
						<div class="clearfix "></div>
						<h4 class="text-blue m-t-45"><?php echo get_field($entity . '_interests_title', 'option'); ?></h4>
						<div class="form-checkboxes half-width-check">
							<?php include("include/sudscribe/subscribe_checkboxes.php");?>
						</div>
                            <div class="subskribe">
                                <?php $agreements = get_field($entity . '_agreements', 'option'); ?>
                                <?php foreach ( $agreements as $agreement ) :?>
                                    <?php $agreement_id = 'agree_' . rand(); ?>
                                    <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                                    <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                                <?php endforeach; ?>
                                <div class="clearfix"></div>
                            </div>

                        <input type="hidden" name="subject_to_submit" value="<?php echo get_field($entity . '_mail_subject', 'option'); ?>">

                        <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                            <?php foreach( get_field($entity . '_email_for_sending', 'option') as $email ) : ?>
                                <input type="hidden" name="email_to_submit[]" value="<?php echo $email["email"]; ?>">
                            <?php endforeach; ?>
					<div class="row">
						<div class="text-center">
							<button type="submit" class="nbtn nbtn-inverse pull-right"><?php echo get_field($entity . '_submit_button', 'option'); ?></button>
						</div>
					</div>
					<div class="row m-t-10">
						<div class="col-xs-12 text-center">
							<div class="alert alert-success text-center notification hidden" role="alert">
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
	<section class="cover experience" style="background-image: url('<?=get_field("section_5_background_image")?>');">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p class="font-ligth f-48 text-uppercase text-white letter-spacing"><?=get_field("section_5_left_side_title")?></p>
				</div>
				<div class="col-md-6">
					<p class="f-18 bold text-uppercase text-white"><?=get_field("section_5_right_side_description")?></p>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="m-t-0  letter-spacing text-uppercase"><?=get_field("section_6_title_1")?></h2>
					<p class="f-23 m-b-30"><?=get_field("section_6_description")?></p>
					<div class="list">

						<?
						$sitems = get_field("section_6_list_items_1");
						$part = round(count($sitems)/4)+2;
						$chanks = array_chunk($sitems, $part);
						foreach($chanks as $key=>$item){?>
							<div class="col">
							<?foreach($item as $k=>$title){?>
								<span><?=$title['description']?></span>
							<? } ?>
							</div>
							<? } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
		<section>
		<div class="container">
			<div class="row">
			<div class="col-sm-12">
				<h2 class="m-t-0 m-b-30 letter-spacing text-uppercase"><?=get_field("section_6_title_2")?></h2></div>
				<div class="col-md-9">
					<ul class="blue_ul inside_ul">
						<?foreach(get_field("section_6_list_items_2") as $key=>$item){?>
							<li><?=$item["description"]?></li>
						<?};?>
					</ul>
				</div>
				<div class="col-md-3">
					<img class="pull-right" src="<?=get_field("section_6_right_side_icon")?>" alt="picture">
				</div>
			</div>
		</div>
	</section>
	<section style="background-image: url('<?=get_field("section_7_background_image")?>');" class="cover text-white experience">
		<div class="container">
			<div class="row">
			<div class="col-sm-12">
				<h2 class="m-t-0 m-b-30 bold font-normal letter-spacing text-uppercase"><?=get_field("section_7_title")?></h2></div>
				<div class="col-md-6">
					<ul class="blue_ul inside_ul">
						<?foreach(get_field("section_7_list_items") as $key=>$item){if($key<4){?>
							<li><?=$item["description"]?></li>
						<?}};?>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="blue_ul inside_ul mobile-top-p">
						<?foreach(get_field("section_7_list_items") as $key=>$item){if($key>=4){?>
							<li><?=$item["description"]?></li>
						<?}};?>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
			<div class="col-sm-12">
				<h2 class="m-t-0 text-blue font-normal m-b-30 letter-spacing text-uppercase">EXCLUSIVE PERSONALIZED MEDICINE  </br>THAT WORKS</h2></div>
				<div class="row desktop-science-formula">
					<div class="col-lg-7 col-xs-12 vertical-center">
						<div class="figure-circle-features skin-care-circler">
							<div class="figure-circle-feature-item-bg wow fadeIn" data-wow-duration="0.2s" data-wow-delay="1.7s">
								<img src="<?php bloginfo('template_url');?>/img/anim-circles/ab8.png" alt="The_science_of_skin_care_personalized_from_DNA_iddna_14" title="The science of skin care, personalized from DNA|iDDNA®_14" />
							</div>
							<div>
								<? readfile($_SERVER["DOCUMENT_ROOT"]."/wp-content/themes/sls/img/circle-styled.svg"); ?></div>
							</div>

							<div class=" arrow-animate text-center wow fadeIn hidden-xs hidden-sm" data-wow-duration="0.2s" data-wow-delay="1.8s">
								<img src="<?php bloginfo('template_url');?>/img/anim-circles/arrow-right.png" alt="arrow right" />
							</div>
							<div>
								<p class="wow fadeIn m-b-35" data-wow-duration="0.2s" data-wow-delay="2.3s">made &#8211; for &#8211; <b>YOU</b></p>
								<img class="display-vb wow fadeInRight" data-wow-duration="0.2s" data-wow-delay="2.5s" src="<?php bloginfo('template_url');?>/img/anim-circles/ab11.png" alt="The_science_of_skin_care_personalized_from_DNA_iddna_15" title="The science of skin care, personalized from DNA|iDDNA®_15" />
								<img class="display-vb wow fadeInRight" data-wow-duration="0.2s" data-wow-delay="2.7s" src="<?php bloginfo('template_url');?>/img/anim-circles/ab12.png" alt="The_science_of_skin_care_personalized_from_DNA_iddna_16" title="The science of skin care, personalized from DNA|iDDNA®_16" />
								<img class="display-vb wow fadeInRight" data-wow-duration="0.2s" data-wow-delay="2.9s" src="<?php bloginfo('template_url');?>/img/anim-circles/ab13.png" alt="The_science_of_skin_care_personalized_from_DNA_iddna_17" title="The science of skin care, personalized from DNA|iDDNA®_17" />
								<img class="display-vb wow fadeInRight" data-wow-duration="0.2s" data-wow-delay="3.1s" src="<?php bloginfo('template_url');?>/img/anim-circles/ab14.png" alt="The_science_of_skin_care_personalized_from_DNA_iddna_18" title="The science of skin care, personalized from DNA|iDDNA®_18" />
								<p class="m-t-35 combination-string text-center">
									<b class="wow fadeIn" data-wow-duration="0.2s" data-wow-delay="3.4s">1</b>
									<span class="wow fadeIn" data-wow-duration="0.2s" data-wow-delay="3.6s">in</span>
									<b class="wow fadeIn " data-wow-duration="0.2s" data-wow-delay="3.8s">86,980,460,544</b>
									<p class="wow fadeIn text-center" data-wow-duration="0.2s" data-wow-delay="4s"><short>i</short>DDNA<sup>®</sup>programs</p>
								</p>
							</div>
						</div>
						<div class="col-lg-4 col-lg-offset-1 col-xs-12 col-xs-offset-0 color-gray">
							<p class="f-23 m-b-30"><span class="text-uppercase">No more</span> trial and error.</p>
							<p class="f-23 m-b-30" ><span class="text-uppercase">No more</span> time-consuming bunch of data.</p>
							<p class="f-23 m-b-60">The client’s tailored protocol delivered with <span class="text-uppercase">true actionable information</span></p>
							<p class="text-uppercase bold f-31">THE LUXURY OF MADE TO MEASURE</p>
						</div>
					</div>

				</div>
				<div class="mobile-science-formula">
					<img class="center-block" src="<?php bloginfo('template_url');?>/img/anim-circles/anim-circle-mobile.jpg" alt="">
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="m-t-0 m-b-0 font-normal  letter-spacing text-uppercase"><?=get_field("section_8_sub_title_1_line")?></h2>
					<p class="f-31 text-uppercase letter-spacing text-blue bold"><?=get_field("section_8_sub_title_2_line")?></p>
				</div>
			</div>
			<div class="row">
				<?foreach(get_field("section_8_list_items") as $key=>$item){?>
						<div class="col-md-6">
							<div class="row">
							<h3 class="col-xs-12" style="font-size: 18px;"><short><?=$item["title"]?></short></h3>
							<?foreach($item["item"] as $item_child){?>
								<div class="col-md-6">
									<img src='<?=$item_child["image"]?>' class="full-width">
									<p><?=$item_child["description"]?></p>
								</div>
								<?};?>
							</div>
						</div>
						<?};?>
					</div>
			</div>
	</section>
	<section >
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p class="f-18">Your <short>i</short>DDNA<sup>®</sup> Experiense</p>
					<h2 class="text-uppercase f-31 bold m-t-0">how it works</h2>
				</div>
				<div class="how_it_works col-xs-12">
					<div class="col-md-6 left-block">
						<p class="f-18 italic c-111">Your DNA sample collection made simple.</p>
						<div class="item m-b-10 clearfix wow fadeInLeft" data-wow-duration="0.4s" data-wow-delay="0.4s">
							<div class="col-xs-12"><h4 class="text-uppercase bold f-18 letter-spacing">1. tell us about your lifestyle</h4></div>
							<div class="row">
								<div class="col-sm-4 text-center ">
									<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-01.png' class="">
								</div>
								<div class="col-sm-8">
									<p class="m-b-10">Go to <b class="text-blue">www.idna.works</b></p>
									<p class="p-w-ls-0">Log in with your user/password from the <span class="like_btn">Login</span> button on the top menu.</p>
								</div>
								<div class="clearfix"></div>
								<div class="col-sm-4 text-center ">
									<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-02.png' class="">
								</div>
								<div class="col-sm-8">
									<p class="p-w-l-h m-b-10">Enter your <b>unique [iD] code </b> (located on your [iD] PASSPORT card).</p>
									<p class="p-w-l-h m-b-10">Choose your DNA-based service.</p>
									<p class="p-w-l-h">Fill out your epigenetics online survey (it takes 4 minutes).</p>
								</div>
							</div>
							<p class="rect">Every question represents a personal datapoint.
								The answers you provide allow you to earn actionable insights about getting the best from your genes.
								More answers mean more accuracy for your <short>i</short>DDNA<sup>®</sup>.<br>
								Without this simple information, by law we must dispose of your sample – which means we cannot
								perform our service.</p>
							</div>
							<div class="item m-b-10 clearfix wow fadeInLeft" data-wow-duration="0.4s" data-wow-delay="0.8s">
								<div class="col-xs-12"><h4 class="text-uppercase bold f-18 letter-spacing">2. Get your dna sample collected</h4></div>
								<div class="row">
									<div class="col-sm-4 text-center">
										<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-03.png' class="">
									</div>
									<div class="col-sm-8">
										<p class="m-b-10"><b>Do NOT eat, drink, smoke or chew gum for 30
											minutes before collecting oral sample.</b></p>
											<p class="m-b-10">Follow the directions in your Quick Start Guide.</p>
											<p class="m-b-10"><b>Your <short>i</short>DDNA<sup>®</sup></b> is secure, personal and confidential.</p>
											<p class="m-b-10">Each kit uses a unigue numerical identifer and does not identify you by your name.</p>
											<p>All DNA samples are destroyed after analysis.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 right-block row">
								<p class="f-18 italic c-111">Science at work for you</p>
								<div class="item m-b-10 clearfix wow fadeInRight" data-wow-duration="0.4s" data-wow-delay="1.2s">
									<div class="col-xs-12"><h4 class="text-uppercase bold f-18 letter-spacing">3. Your dna is sequenced</h4></div>
									<div class="row">
										<div class="col-sm-8">
											<p>Your de-identified sample is sent to Suisse Life
												Science’s lab in the US where we sequence your SNPs.</p>
											</div>
											<div class="col-sm-4 text-center top-minus">
												<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-04.png' class="">
											</div>
										</div>
									</div>
									<div class="item m-b-10 clearfix wow fadeInRight" data-wow-duration="0.4s" data-wow-delay="1.4s">
										<div class="col-xs-12"><h4 class="text-uppercase bold f-18 letter-spacing">4. genetics + epigenetics interpretation</h4></div>
										<div class="row">
											<div class="col-sm-8">
												<p>Your genetic raw data flies to Switzerland where it is run through our extensive knowledge base, focusing
													on the interplay between your genetics, environment
													and lifestyle.</p>
												</div>
												<div class="col-sm-4 text-center">
													<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-05.png' class="">
												</div>
											</div>
										</div>
										<div class="item m-b-10 clearfix wow fadeInRight" data-wow-duration="0.4s" data-wow-delay="1.8s">
											<div class="col-xs-12"><h4 class="text-uppercase bold f-18 letter-spacing">5. your treatment is designed</h4></div>
											<div class="col-sm-8">
												<p class="p-w-ls-0">Our Science of [DNA]f(x) algorithm uses your genes
													to map the best ingredients in the world and delivers
													the latest research-based recommendations to reach
													your goals.</p>
												</div>
												<div class="col-sm-4 text-center ">
													<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-06.png' class="">
												</div>
											</div>
											<div class="item m-b-10 clearfix wow fadeInRight" data-wow-duration="0.4s" data-wow-delay="2.2s">
												<div class="col-xs-12"><h4 class="text-uppercase bold f-18 letter-spacing">6. your <short>i</short>DDNA<sup>®</sup> ultra-custom</h4></div>
												<div class="col-sm-8">
													<p class="p-w-ls-0">Your made to measure (30- or 60-day) treatment is
														delivered to your door. Science-based personal
														recommendations to activate your genes’ potential
														are delivered to your concierge private area online
														and your mobile app. And you’ll receive lifelong
														custom updates.</p>
													</div>
													<div class="col-sm-4 text-center top-minus m-b-30">
														<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-07.png' class="">
													</div>
													<div class="col-sm-8">
														<p class="text-uppercase m-b-10">Simple <br> NOn-invasive <br>long-lasting, natural results</p>
														<p class="text-uppercase bold">Say hello to ypur best life.</p>
													</div>
													<div class="col-sm-4 text-center p-t-20">
														<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/pic-08.png' class="">
													</div>
												</div>
											</div>
											<div class="how_it_works_bg wow fadeIn" data-wow-duration="0.4s" data-wow-delay="2.6s">
												<img src='<?php echo bloginfo('template_url'); ?>/img/how_it_works/how_it_works_bg.png' class="">
											</div>
											<div class="clearfix"></div>
										</div>
										<!--<img src='<?//php echo bloginfo('template_url'); ?>/img/how_it_works.jpg' class="full-width"> -->
									</div>
								</div>
							</section>
							<section>
								<div class="container">
									<div class="row">
									<div class="col-sm-12">
										<h2 class="form-title"><?php echo get_field('prividers2_title', 'option'); ?></h2>
										<p class="form-subtitle"><?php echo get_field('prividers2_title_subtitle', 'option'); ?></p>
                                        <?php $entity = 'prividers2'; ?>
										<form class="offer_form clearfix my_amazing_form about_fix">
											<div class="row">
                                            <?php foreach(get_field($entity . '_form_fields', 'option') as $field){ ?>
                                                <?php include("include/form/forminputs.php");?>
                                            <?};?>
											</div>
											<div class="subskribe clearfix">
						<h4 class="text-blue m-t-45"><?php echo get_field($entity . '_interests_title', 'option'); ?></h4>
						<div class="form-checkboxes half-width-check">
							<?php include("include/sudscribe/subscribe_checkboxes.php");?>
						</div>
                            <div class="subskribe">
                                <?php $agreements = get_field($entity . '_agreements', 'option'); ?>
                                <?php foreach ( $agreements as $agreement ) :?>
                                    <?php $agreement_id = 'agree_' . rand(); ?>
                                    <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                                    <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                                <?php endforeach; ?>
                                <div class="clearfix"></div>
                            </div>

                        <input type="hidden" name="subject_to_submit" value="<?php echo get_field($entity . '_mail_subject', 'option'); ?>">

                        <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                            <?php foreach( get_field($entity . '_email_for_sending', 'option') as $email ) : ?>
                                <input type="hidden" name="email_to_submit[]" value="<?php echo $email["email"]; ?>">
                            <?php endforeach; ?>
						<div class="text-center">
							<button type="submit" class="nbtn nbtn-inverse pull-right"><?php echo get_field($entity . '_submit_button', 'option'); ?></button>
						</div>
											</form>
										</div>
										</div>
									</div>
								</section>
								<?php get_footer(); ?>
