<?php

global $sls_data;

$args=array(
    'post_type' => 'locations',
    'post_status' => 'publish',
    'posts_per_page' => 2,
);

//$query = new WP_Query($args);
$wp_query = new \WP_Query();
$posts = $wp_query->query($args);

$locations = array();
foreach ($posts as $post) {
    $locations[] = get_fields($post->ID);
}

?>
    <footer>
        <div class="container">
            <?php if( $sls_data['switch-top-footer-widgets'] ): ?>
            <div class="row">
                <div class="col-md-12 m-t-60 clearfix">
                    <div class="row">
                    <div class="col-xs-12 col-lg-8 footer_menu">
                        <?php
                            $columns = $sls_data['top-footer-widgets-columns'];
                            $class = (int) (12 / $columns) + 1;
                          //  for( $i = 1; $i <= $columns - 2; $i++):
                        ?>
                            <!-- old yours <div class="col-sm-<?php echo $class; ?>">-->

                        <?php// endfor; ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 m-b-20">
                            <?php dynamic_sidebar( 'sls-top-footer-widget-area-1' ); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-2 m-b-20">
                            <?php dynamic_sidebar( 'sls-top-footer-widget-area-2' ); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 m-b-20">
                            <?php dynamic_sidebar( 'sls-top-footer-widget-area-3' ); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 m-b-20">
                            <?php dynamic_sidebar( 'sls-top-footer-widget-area-4' ); ?>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                        <div class="col-sm-12">
                            <h4><?php echo __('Locations', 'sls-theme'); ?></h4>
                        </div>
                            <? $new_location = array_reverse($locations); ?>

                            <? foreach ($new_location as $location) { ?>
                                <div class="col-sm-6 col-xs-6  ">
                                                    <ul>
                                                <li><? echo $location['company']; ?></li>
                                                <li><? echo $location['street']; ?></li>
                                                <li><? echo $location['city']; ?></li>
                                                <li><? echo $location['country']; ?></li>
                                    </ul>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if( $sls_data['switch-bottom-footer-widgets'] ): ?>
            <div class="row mini-mimi">
                <?php
                  //  $columns = $sls_data['top-footer-widgets-columns'];
                  //  $class = 12 % $columns;
                  //  for( $i = 1; $i <= $columns; $i++):
                ?>
                    <div class="col-sm-<?php echo $class; ?>">
                        <?php //dynamic_sidebar( 'sls-bottom-footer-widget-area-' . $i ); ?>
                    </div>
                <?php// endfor; ?>
            </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-12 m-t-15">
                    <div class="row">
                    <div class="col-lg-8 col-xs-12">
                        <div class="col-sm-6 clearfix p-t-10 p-l-0">
                            <div class="col-sm-7 col-xs-6 no-padding footer-logo">
                                <a href="<?php echo esc_url(home_url()); ?>/"><img src="<?php echo esc_url($sls_data['media-footer-logo']['url']); ?>" alt="<?php esc_attr(bloginfo('name')); ?>"/></a>
                            </div>
                            <!--
                            <ul class="col-sm-5 col-xs-6">
                                <li>via Zorzi, 4</li>
                                <li>6900 Lugano</li>
                                <li>Switzerland</li>
                            </ul>
    -->
                        </div>
                        <div class="col-sm-6 p-t-40">
                            <?php dynamic_sidebar( 'sls-bottom-footer-widget-area-1' ); ?>
                        </div>
                    </div>
                    <?php $entity = 'header_footer'; ?>
                    <div class="col-lg-4 col-xs-12">
                        <div class=" header_emeil footer_mail">
                            <div class="input-group m-t-30">
                            <form id="ajax_footer_send" method="POST" action="" >
								<input type="hidden" name="subject_to_submit" value="<?=get_field($entity . '_mail_subject', 'option');?>">
								<!--<input type="hidden" name="subscr" value="subscr">-->
                                <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                                <div class="input-group">
								<?$email="";foreach(get_field($entity . '_email_for_sending', 'option') as $em){?>
									<input type="hidden" name="email_to_submit[]" value="<?=$em["email"]?>">
								<?}?>
                                
                                    <input type="email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" placeholder="<?php echo get_field($entity . '_enter_your_email', 'option'); ?>" required >
                                    <span class="input-group-btn"> 
                                        <button class="nbtn nbtn-inverse" type="submit"><?php echo get_field($entity . '_submit_buttom', 'option'); ?> <i class="ico arrow arrow_white"></i></button>
                                    </span>
                                </div>
                            </form>
                            </div>
                            <!-- /input-group -->
                        </div>
                    </div>
                    </div>
                </div>
        </div>
            <?php if( has_nav_menu('footer-horizontal')) : ?>
            <div class="row">
                <div class="com-md-12 clearfix">
                    <?php wp_nav_menu( array(
                        'theme_location' => 'footer-horizontal',
                        'container_class' => 'foot_horizont_menu clearfix'
                    ) );?>
                </div>
            </div>
            <?php endif; ?>
            <?php if( $sls_data['switch-copyright-area'] ) : ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="copyright"><?php echo __($sls_data['top-copyright-text'], 'sls-theme'); ?></div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="copyright_info">
                            <?php echo __($sls_data['bottom-copyright-text'], 'sls-theme'); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </footer>

    <div style="display: none;">
        <div class="box-modal thanq">
            <div class="modal-header">
              <i class="fa fa-times-circle arcticmodal-close" aria-hidden="true"></i>
              <h4 class="modal-title" id="myModalLabel">Thank you</h4>
            </div>
            <div class="contentn-modal">
                <div class="js-cont"></div>
            </div>
        </div>
    </div>

    <?php wp_footer(); ?>
</body>
</html>
