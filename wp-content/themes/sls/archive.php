<?php
get_header(); ?>
	<section>
		<div class="container">
			<div class="row">
				<div class="<?php content_class_by_sidebar(); ?>">
					<h1><?php
						if (is_day()) : printf(__('Daily Archives: %s', 'sls-theme'), get_the_date());
						elseif (is_month()) : printf(__('Monthly Archives: %s', 'sls-theme'), get_the_date('F Y'));
						elseif (is_year()) : printf(__('Yearly Archives: %s', 'sls-theme'), get_the_date('Y'));
						else : __('Archives', 'sls-theme');
						endif; ?></h1>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part('loop'); ?>
					<?php endwhile;
					else: echo '<p>'.__('No posts found','sls-theme').'</p>'; endif; ?>
					<?php pagination(); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>