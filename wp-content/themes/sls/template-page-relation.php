<?php /* Template Name: Relation */ ?>
<?php get_header();?>
<!-- <script src="<?php echo bloginfo('template_url'); ?>/js/jquery.touchSwipe.min.js"></script> -->
<div class="corporate-relation-page">
	<section class="full-width-slider" style="margin-top: 148px;">
			<img class="responsive-img" src="<?=get_field("section_1_background_image")?>" alt="image">
	</section>
<!--   <section class="full-width-slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src='<?php echo bloginfo('template_url'); ?>/img/sls-relation-bg.jpeg' style="background-size: cover;" alt="First slide">
          <div class="container relative">
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
    </div>
  </section> -->
	<section class="">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?foreach(get_field("section_2_paragraph_of_the_article_1") as $item){?>
						<p class="m-b-30"><?=$item["paragraph"]?></p>
					<?};?>
  				<!-- <div class="p-l-30"> -->
  					<h3 class="light letter-spacing m-b-30"><?=get_field("section_2_sub_title_1");?></h3>
  					<?foreach(get_field("section_2_paragraph_of_the_article_2") as $item){?>
  						<p class="m-b-30"><?=$item["paragraph"]?></p>
  					<?};?>
  					<h4 class="text-blue bold letter-spacing text-uppercase"><?=get_field("section_2_title_items");?></h4>
  					<ul class="blue_ul inside_ul m-t-30">
  						<?foreach(get_field("section_2_list_items") as $item){?>
  							<li><?=$item["title_description"]?></li>
  						<?};?>
  					</ul>
  				 <!-- </div> -->
        </div>
			</div>
		</div>
	</section>
	<section style="background-image: url('<?=get_field("section_3_background_image");?>'); background-size: cover;" >
		<div class="container text-white">
			<h2 class="m-t-0 font-normal letter-spacing"><?=get_field("section_3_title");?></h2>
			<h4 class="bold m-t-40 letter-spacing text-uppercase"><?=get_field("section_3_sub_title");?></h4>
			<h4 class="light text-uppercase tal letter-spacing"><?=get_field("section_3_description");?></h4>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="science-formula letter-spacing text-uppercase font-normal m-t-0 m-b-40"><?=get_field("section_4_title");?></h2>
					<div class="col-sm-6 p-0">
						<h4 class="text-uppercase"><?=get_field("section_4_left_side_descrition");?></h4>
					</div>
					<div class="col-sm-6 p-0">
						<ul class="inside_ul blue_ul">
							<?foreach(get_field("section_4_right_side_items") as $item){?>
								<li class="f-18">
									<span class="bold text-uppercase"><?=$item["bold_title"]?> </span> <br> <?=$item["description"]?>
								</li>
							<?};?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="bg-grey">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h3 class="f-23 text-uppercase text-blue m-t-0 m-b-30"><span class="bold f-23"><?=get_field("section_5_title_1_line")?></span><br> <?=get_field("section_5_title_2_line")?></h3>
					<ul class="inside_ul blue_ul">
						<?foreach(get_field("section_5_list_items") as $item){?>
							<li><?=$item["decription"]?></li>
						<?};?>
					</ul>
					<h2 class="bold f-23 text-blue text-uppercase m-t-30 m-b-0"><?=get_field("section_5_description_bold")?></h2>
				</div>
			</div>
		</div>
	</section>
	<section class="experience" style="background-image: url('<?=get_field("section_6_background_image")?>'); background-size: cover;">
		<div class="container">
			<div class="row text-white">
			<div class="col-xs-12">
				<h2 class="font-normal text-center letter-spacing m-t-0"><?=get_field("section_6_title_1_line")?></h2>
				<h3 class="bold text-uppercase text-center f-18"><?=get_field("section_6_title_2_line")?></h3>
				<h2 class="font-light text-uppercase letter-spacing m-b-40 f-31 m-t-40 text-center"><?=get_field("section_6_title_3")?></h2>
				</div>
				<div class="col-sm-12 advantage_relation">
					<?foreach(get_field("section_6_list_items") as $item){?>
						<div class="col-sm-3 m-t-40">
							<div class="icon">
								<img class="center-block" src="<?=$item["icon"]?>" alt="">
							</div>
							<p class="text-center m-t-30"><?=$item["description"]?></p>
						</div>
					<?};?>
				</div>
				<div class="col-sm-12">
					<p class="m-t-40 text-center"><?=get_field("section_6_description")?></p>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
                <!--
				<img class="center-block" src="<?=get_field("section_7_big_image")?>" alt="">
-->
					<!-- New svg is wrong... -->
                <? readfile($_SERVER["DOCUMENT_ROOT"]."/wp-content/themes/sls/img/HIW_info.svg"); ?>
                
                <?/*php include("include/animate-3-block.php") */?>
			</div>
		</div>
	</section>
	<section class="p-t-0">
		<div class="container">
			<div class="row">
				<h2 class="text-blue m-t-0 m-b-0"><?=get_field("section_8_title")?></h2>
			</div>
		</div>
	</section>
	<section   class="p-0">
	<? readfile($_SERVER["DOCUMENT_ROOT"]."/wp-content/themes/sls/img/sls-relation-map.svg"); ?>
	</section>
	<section>
		<div class="container">
			<div class="row">
        <div class="col-xs-12">
  				<h2 class="m-t-0 m-b-30 letter-spacing"><?=get_field("section_9_title")?></h2>
  				<?foreach(get_field("section_9_paragraph_of_the_article") as $item){?>
  					<p><?=$item["paragraph"];?></p>
  				<?};?>
        </div>
			</div>
		</div>
	</section>
	<section style="background-image: url('<?=bloginfo('template_url'); ?>/img/sls-relation-bg-with-letters.jpeg'); background-size: cover;">
		<div class="container">
			<div class="row">
				<h2 class="letter-spacing m-t-0 m-b-40"><?=get_field("section_10_title")?></h2>
				<div class="col-sm-5 col-xs-12">
					<img src="<?=bloginfo('template_url'); ?>/img/diagramm.png" alt="">
				</div>
				<div class="col-sm-6 col-md-offset-1 col-xs-12" style="padding-top: 100px">
					<?foreach(get_field("section_10_list_items") as $key=>$item){?>
						<h4 class="text-uppercase bold f-23"><?=$item["title"]?></h4>
						<p class="<?if(next(get_field("section_10_list_items"))){?>m-b-40<?};?> tal"><?=$item["description"]?></p>
					<?};?>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<h2 class="letter-spacing m-t-0 m-b-25"><?=get_field("section_11_title")?></h2>
				<div class="col-sm-3 col-xs-12 hidden-xs">
					<ul class="relation-overview-listLeft ">
						<li><?=get_field("section_11_text_company")?></li>
						<li><?=get_field("section_11_text_headquarters")?></li>
						<li><?=get_field("section_11_text_industry")?></li>
						<li class="m-b-100"><?=get_field("section_11_text_segments")?></li>
						<li class="m-b-60"><?=get_field("section_11_text_usp")?></li>
						<li><?=get_field("section_11_text_main_markets")?></li>
						<li><?=get_field("section_11_text_incorporated")?></li>
						<li><?=get_field("section_11_text_status")?></li>
						<li><?=get_field("section_11_text_capital_stock")?></li>
						<li class="m-b-75"><?=get_field("section_11_text_subsidiaries")?></li>
						<li><?=get_field("section_11_text_websites")?></li>
					</ul>
				</div>
				<div class="col-sm-8 col-sm-offset-1 col-xs-12 hidden-xs">
					<ul class="relation-overview-listRight">
						<li><?=get_field("section_11_value_company")?></li>
						<li><?=get_field("section_11_value_headquarters")?></li>
						<li><?=get_field("section_11_value_industry")?></li>
						<li><?=get_field("section_11_value_segments")?></li>
						<li class="bold"><?=get_field("section_11_value_usp")?></li>
						<li><?=get_field("section_11_value_main_markets")?></li>
						<li><?=get_field("section_11_value_incorporated")?></li>
						<li><?=get_field("section_11_value_status")?></li>
						<li><?=get_field("section_11_value_capital_stock")?></li>
						<li><?=get_field("section_11_value_subsidiaries")?></li>
						<li>
							<?foreach(get_field("section_11_value_websites") as $item){?>
								<a href="//<?=$item["url"]?>"><?=$item["url"]?></a> <br>
							<?};?>
						</li>
					</ul>
				</div>
			</div>
			<div class="row hidden-sm hidden-md hidden-lg">
				<div class="col-xs-12">
					<ul class="relation-overview-listLeft ">
						<li><?=get_field("section_11_text_company")?></li>
						<li><?=get_field("section_11_value_company")?></li>
						<li><?=get_field("section_11_text_headquarters")?></li>
						<li><?=get_field("section_11_value_headquarters")?></li>
						<li><?=get_field("section_11_text_industry")?></li>
						<li><?=get_field("section_11_value_industry")?></li>
						<li class="m-b-100"><?=get_field("section_11_text_segments")?></li>
						<li><?=get_field("section_11_value_segments")?></li>
						<li class="m-b-60"><?=get_field("section_11_text_usp")?></li>
						<li class="bold"><?=get_field("section_11_value_usp")?></li>
						<li><?=get_field("section_11_text_main_markets")?></li>
						<li><?=get_field("section_11_value_main_markets")?></li>
						<li><?=get_field("section_11_text_incorporated")?></li>
						<li><?=get_field("section_11_value_incorporated")?></li>
						<li><?=get_field("section_11_text_status")?></li>
						<li><?=get_field("section_11_value_status")?></li>
						<li><?=get_field("section_11_text_capital_stock")?></li>
						<li><?=get_field("section_11_value_capital_stock")?></li>
						<li class="m-b-75"><?=get_field("section_11_text_subsidiaries")?></li>
						<li><?=get_field("section_11_value_subsidiaries")?></li>
						<li><?=get_field("section_11_text_websites")?></li>
						<li>
							<?foreach(get_field("section_11_value_websites") as $item){?>
								<a href="//<?=$item["url"]?>"><?=$item["url"]?></a> <br>
							<?};?>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<section class="p-t-0">
		<div class="container">
				<h3 class="bold f-31 m-t-0 m-b-0"><?=get_field("section_11_text_after_company_info")?></h3>
				<a href="mailto:<?=get_field("section_11_email_button_email_our_corporate_relations")?>"><button type="submit" class="nbtn nbtn-inverse m-t-30 p-r-50"><?=get_field("section_11_text_button_email_our_corporate_relations")?><i class="ico arrow arrow_white"></i></button></a>
		</div>
	</section>
</div>
<?php get_footer(); ?>