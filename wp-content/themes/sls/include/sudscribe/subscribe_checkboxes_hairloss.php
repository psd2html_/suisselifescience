<ul class="clearfix">
<?php foreach(get_field('hairlos_interests', 'option') as $key => $interest) : ?>
	<?php $interest_id = $interest['value_of_mailchimp_interest'] . '_' .rand();?>
	<li class="col-md-3">
		<input
			id="<?php echo $interest_id; ?>"
			type="checkbox"
			name="interest[]"
			value="<?php echo $interest['value_of_mailchimp_interest']; ?>"
			<?php echo ( $interest['checked'] ) ? 'checked' : '';  ?>
		/>
		<label for="<?php echo $interest_id; ?>"><?php echo $interest['name']; ?></label>
	</li>
<?php endforeach; ?>

</ul>
