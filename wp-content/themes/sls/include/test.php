<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );




function test_get_country_code($country) {

    $countries_list = get_field('countries_list', 'option');

    $country_code = null;
    foreach ($countries_list as $countries) {
        if ( strtoupper($countries['country']) === strtoupper($country) ) {
            $country_code = $countries['code'];
        }
    }

    return $country_code;
}



function test_get_all_centers_from_secure() {
    global $wpdb;
    $db_table_name = 'wp_secure_centers';

    $args = array(
        'timeout' => 120,
        'httpversion' => '1.1',
    );

    $table_columns = array(
        'first_name',
        'last_name',
        'user_email',
        'zip',
        'user_role',
        'company_name',
        'company_desc',
        'street_address',
        'town_city',
        'state',
        'country',
        'company_phone',
        'company_website',
        'company_logo',
        'latitude',
        'longitude',
        'company_email',
        'booking_email',
        'secure_user_id',
         'brand_name',
	'center_name',
        'status',
    );

    $response = wp_remote_get('http://secure.idna.works/wp-json/v3/findcenter', $args);
    
    if (is_wp_error($response)) return false;
    $body = (string) $response['body'];
    $centers = json_decode($body);
    $new_counties = [];   
    $wpdb->query('START TRANSACTION');
    try {
        $wpdb->query("TRUNCATE TABLE `{$db_table_name}`");
        foreach ($centers->data as $center) {
            $new_line = array();
            foreach ($center as $key=>$column) {
                if (in_array($key, $table_columns)) {
                    $new_line[$key] = $column;
                }
                 if ($key === 'user_id') {                   
                    $new_line['secure_user_id'] = $column;                   
                }
//                if ($key === 'country') {
//                    $country_code = test_get_country_code($column);
//                    $new_line['country_code'] = $country_code;
//                    if (empty($country_code)) {
//                        $new_counties[] = $column;
//                    }
//                }
            }
            $wpdb->insert($db_table_name, $new_line);
        }
        //$countries_list = get_field('countries_list', 'option');
    } catch (Exception $e) {
        $wpdb->query('ROLLBACK');        
    }
    $wpdb->query('COMMIT');
    
}

test_get_all_centers_from_secure();


//$lat_lon = Seven_Geolocation::geolocate_coordinates( '', true, true  );
//var_dump($lat_lon);
//$partner = array_shift(select_closest_distributors($lat_lon));
//var_dump($partner);

//$lat_lon = array("latitude"=>"51.84107040","longitude"=>"5.87152390","state"=>"Netherlands");
//$partner = array_shift(select_closest_distributors($lat_lon,"Netherlands"));
//var_dump($partner);