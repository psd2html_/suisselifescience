<?
$args = array(
	'numberposts' => -1,
	'post_type' => 'centers',
	's'=>$_POST['search'],
	'suppress_filters'=>0
);
$search_posts = get_posts( $args );

?>
<pre><?print_r($search_posts)?></pre>
<div class="centers_block clearfix">
	<?$jsOffice=[];?>
	<?foreach($search_posts as $post){?>
		<?$jsOffice[]=array(
		"name"=>$post->post_title,
		"lat"=>get_field("location",$post->ID)["lat"],
		"lng"=>get_field("location",$post->ID)["lng"],
	);?>
	<div class="js-office col-sm-3">
		<h2 class="js-office-title"><?=$post->post_title;?></h2>
		<h3 class=""><?=get_field("address",$post->ID);?></h3>
		<?foreach(get_field("telephones",$post->ID) as $tel){?>
			<h4>
				<a href="tel:<?=$tel['number'];?>">
					<?=$tel["number"];?>
				</a>
			</h4>
		<?};?>
		<h4><a href="javascript:void(0)" onclick="panMap(<?=get_field("location",$post->ID)["lat"]?>, <?=get_field("location",$post->ID)["lng"]?>)">SHOW ON MAP</a></h4>
	</div>
	<?}?>
	<script>
		var office = <?=json_encode($jsOffice);?>;
	</script>
</div>