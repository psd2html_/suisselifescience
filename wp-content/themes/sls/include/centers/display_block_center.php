<?php if(isset($center)) {?>
  <? $country = isset($center->country)? $center->country :''?>
  <?php $country_zone = get_country_zone_by_name($country); ?>
<? $city = isset($center->town_city)? $center->town_city :''?>
<? $zip = isset($center->zip)? $center->zip :''?>
<?if($center->street_address){?>
	<p><?=trim($center->street_address);?>	
	<br>
        <?if($country_zone=="Europe"){?>	
            <?echo $zip;?>
	<?} else if($country_zone=="United States"){?>		
            <?if($city && !empty($zip)){echo $city.", ".$zip;}?>
            <?if($city && empty($zip)){echo $city;}?>
	<?}else{?>             
            <?if($city && !empty($zip)){echo $zip.", ".$city;}?>
            <?if($city && empty($zip)){echo $city;}?>
	<?};?>
        <?if($country=="United States" || $country=="Canada"){?>		
            <?if($center->state){?>
                , <?=trim($center->state);?>
            <?};?> 
	<?}?>
	</p>
<?};?>
<?php } elseif(isset($post)) {?>
  <? $country = isset($post->country)? $post->country :''?>
  <?php $country_zone = get_country_zone_by_name($country); ?>
<? $city = isset($post->town_city)? $post->town_city :''?>
<? $zip = isset($post->zip)? $post->zip :''?>
<?if($post->street_address){?>
	<p><?=trim($post->street_address);?>	
	<br>
        <?if($country_zone=="Europe"){?>	
            <?echo $zip;?>
	<?} else if($country_zone=="United States"){?>		
            <?if($city && !empty($zip)){echo $city.", ".$zip;}?>
            <?if($city && empty($zip)){echo $city;}?>
	<?}else{?>             
            <?if($city && !empty($zip)){echo $zip.", ".$city;}?>
            <?if($city && empty($zip)){echo $city;}?>
	<?};?>
        <?if($country=="United States" || $country=="Canada"){?>		
            <?if($post->state){?>
                , <?=trim($post->state);?>
            <?};?> 
	<?}?>
	</p>
<?};?>
<?php } ?>
