<?
global $WPGeoplugin;
$GeoInfo = $WPGeoplugin->getGeoInfo()?>
<?
    $geoCat = get_term_by( 'slug', $WPGeoplugin->getCountryTaxonomy(), "zones");
    //$geoCat = get_term_by( 'slug', 'united-states', "zones");
    //$geoCat = get_term_by( 'slug', 'belarus', "zones");
	
	$myposts = get_posts(array(
        'showposts' => -1,
        'post_type' => 'centers',
        'orderby' => 'title',
        'order' => 'ASC'
        )
	);

    $terms = get_terms( array(
        'taxonomy' => 'zones',
        //'parent' => 269,
        //'hide_empty' => false,
    ) );

    $zonesTermsId = array();
    foreach ($terms as $term) {
        if ($term->parent == $geoCat->term_taxonomy_id) $zonesTermsId[] = $term->term_id;
    }

	$centerPosts = get_posts(array(
        'showposts' => -1,
        'post_type' => 'centers',
        'tax_query' => array(
            array(
                'taxonomy' => 'zones',
                //'field' => 'parent',
                //'terms' => '365',
                'terms' => $zonesTermsId,
            )
        ),
        'orderby' => 'title',
        'order' => 'ASC'
        )
	);


    //echo '<pre>';
    //var_dump($terms);
    //var_dump($geoCat->term_taxonomy_id);
    //var_dump($WPGeoplugin->getCountryTaxonomy());
    //var_dump($centerPosts);
    //var_dump($myposts);
    //echo '</pre>';
    //exit;
?>
			<?if($WPGeoplugin->getCountryTaxonomy()=="Global"){
            ?>
				<script>
					var panCountry = "<?=$WPGeoplugin->getCountry()?>";
				</script>
			<?}else{
            ?>
				<script>
					var panCountry = "<?=$WPGeoplugin->getCountryTaxonomy()?>";
				</script>
			<?}?>

<?if (isset($_REQUEST['search']) && empty($myposts)){?>
	<section class="bg-grey">
		<div class="container">
			<?if($WPGeoplugin->getCountryTaxonomy()=="Global"){
				$args = array( 'post_type' => 'centers', 'numberposts'=> -1);
				$myposts = get_posts( $args );?>
				<script>
					var panCountry = "<?=$WPGeoplugin->getCountry()?>";
				</script>
			<?}else{
				$args = array( 'post_type' => 'centers', 'zones' => $WPGeoplugin->getCountryTaxonomy());
				$myposts = get_posts($args);?>
				<script>
					var panCountry = "<?=$WPGeoplugin->getCountryTaxonomy()?>";
				</script>
			<?}?>
		</div>
	</section>
<?};?>

<?foreach($myposts as $post){?>
	<?if(get_field("location",$post->ID)){?>
		<?include('centers-marker-html.php');?>
		<?$jsOffice[]=array("name"=>$post->post_title,"lat"=>get_field("location",$post->ID)["lat"],"lng"=>get_field("location",$post->ID)["lng"],'HTML'=>$markerHTML)?>
	<?};?>
<?};?>
<?foreach($centerPosts as $post){?>
		<?$jsCenterOffice[]=array("name"=>$post->post_title,"lat"=>get_field("location",$post->ID)["lat"],"lng"=>get_field("location",$post->ID)["lng"],'HTML'=>$markerHTML)?>
<?};?>
<script>
	var office = <?=json_encode($jsOffice);?>;
	var centerOffice = <?=json_encode($jsCenterOffice);?>;
    console.log('centerOffice: ', centerOffice);
    console.log('getCountryTaxonomy: ', <?php echo json_encode($WPGeoplugin->getCountryTaxonomy()); ?>);
    console.log('term_taxonomy_id: <?php echo $geoCat->term_taxonomy_id; ?>');
    console.log('centerPosts: ', <?php echo json_encode($centerPosts); ?>);
</script>
