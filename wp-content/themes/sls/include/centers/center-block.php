 <?php     
//    if(isset($searchRelevant)) {
//        $args = "select * from `{$db_table_name}` where id NOT IN ( '" . implode($searchRelevant, "', '") . "' )";
//        $mycenters = $wpdb->get_results($args);  
//    }

   $blogs = array();
        $press_releases = array();
if(isset($mypostsSearch) && count($mypostsSearch)>0){
            $center_index = -1;
            foreach($mypostsSearch as $key=>$post){
                    if(strtolower($post->user_role)=="partner"){
                        $center = $post;                          
                        include('centers-marker-html.php');    
                        $center_details = array(
                            'id' => $center->id,
                            'location' => array("lat"=>$center->latitude,"lng"=>$center->longitude),
                            'company_name' => $center->brand_name,
                            'company_phone' => $center->company_phone,
                            'city' => $center->town_city,
                            'zip' => $center->zip,
                            'state' => $center->state,
                            'address' => $center->street_address,
                            'country' => $center->country,
                            'HTML'=>$markerHTML
                        );
                        $jsOfficeRelevant[]=$center_details;
                        $jsOffice[]=$center_details;
                        unset($mypostsSearch[$key]);                        
                        break;
                    }
            }       
//         if(!isset($center)){
//                foreach($mypostsSearch as $key=>$post){
//                       if((strtolower($post->user_role)=="distributor" || strtolower($post->user_role)=="master-distributor")){
//                           $center = $post;  
//                            include('centers-marker-html.php');                     
//                            $center_details = array(
//                              'id' => $center->id,
//                              'location' => array("lat"=>$center->latitude,"lng"=>$center->longitude),
//                              'company_name' => $center->company_name,
//                              'company_phone' => $center->company_phone,
//                              'city' => $center->town_city,
//                              'zip' => $center->zip,
//                              'state' => $center->state,
//                              'address' => $center->street_address,
//                              'country' => $center->country,
//                              'HTML'=>$markerHTML
//                          );
//                          $jsOfficeRelevant[]=$center_details;
//                          $jsOffice[]=$center_details;
//                          unset($mypostsSearch[$key]);
//                          break;
//                       }
//               }          
//         }
//       if(!isset($center)){  
//            include('centers-marker-html.php');                     
//            $jsOfficeRelevant[]=array("name"=>$center->company_name,"lat"=>$center->latitude,"lng"=>$center->longitude,'HTML'=>$markerHTML);
//            $jsOffice[]=array("name"=>$center->company_name,"lat"=>$center->latitude,"lng"=>$center->longitude,'HTML'=>$markerHTML);
//            
//            array_splice($mypostsSearch, array_search($center,$mypostsSearch),1);
//       }
}

?>

<?php if (isset($center)) : ?>

<?php

$secure_user_id = $center->secure_user_id;
$center_posts = get_posts(
        array('post_type' => 'centers',
            'meta_query' => array( 
                   array('key' => 'secure_user_id','value' => $center->secure_user_id)
                ),
            'posts_per_page'=>1       
           )         
        );
if(count($center_posts) > 0) {
    $center_post = $center_posts[0];   
$blogs_query = 
    array(
      'post_type' => 'post',
      'meta_query' => array(
        array(
          'key' => 'post_types',
          'value' => 'blog_post',
        ),
        array(
          'key' => 'centers',
          'value'   => array('','0'),
          'compare' => 'NOT IN'
        )
      ),
       'orderby' => 'modified',
       'order'          => 'DESC'       
    ); 

      
        $blogs_data = get_posts($blogs_query);
       //print_r(count($center_blogs_data));
         
        foreach ($blogs_data as $blog) {              
            $blog_array = get_field("centers",$blog->ID);             
            for($i = 0;$i<count($blog_array);$i++) {
                 if($center_post->ID == $blog_array[$i]["centers_id"])
                 {
                     $blogs[] = $blog;
                 }                 
            }                  
        }
        //print_r($blogs);   
        
    $press_release_query = 
    array(
      'post_type' => 'post',
      'meta_query' => array(
        array(
          'key' => 'post_types',
          'value' => 'press_pelease',
        ),
        array(
          'key' => 'centers',
          'value'   => array('','0'),
          'compare' => 'NOT IN'
        )
      ),
       'orderby' => 'modified',
       'order'          => 'DESC'       
    ); 

        
        $press_releases_data = get_posts($press_release_query);
       //print_r(count($center_blogs_data));
         
        foreach ($press_releases_data as $press_release) {             
            $press_release_array = get_field("centers",$press_release->ID);             
            for($i = 0;$i<count($press_release_array);$i++) {
                 if($center_post->ID== $press_release_array[$i]["centers_id"])
                 {
                     $press_releases[] = $press_release;
                 }                 
            }                  
        }
        //print_r($press_releases);   
}
?>

<section class="bg-dark-grey " style="padding: 40px 0;background: #27292b none repeat scroll 0 0;">
        <div class="container text-white">  
             <div class="row">
                  <?php if(count($blogs) == 0 && count($press_releases)==0) :?> 
                   <div class="col-md-6 col-sm-offset-3 col-xs-6" style="background: #44464a;padding: 0 25px 25px 25px;">  
                        <h4 class="uppercase"><b><?=$center->country?></b></h4>                      
                       <?php if((strtolower($center->user_role)=="partner")){?>
                            <h5 class="m-b-20 gb"><b><?=$center->brand_name;?></b><br></h5>   
                        <?php } else { ?>
                              <h5 class="m-b-20 gb"><b><?=$center->company_name;?></b><br></h5>   
                        <?php } ?>
                        <h4 class="m-b-20 gl">
                        <?php include('display_block_center.php');?>
                        </h4>    
                        <h5 class="m-t-15 font-light"><a  href="javascript:void(0)"  id="center-block-show-map" class="show-on-map-center active" data-center_id="<?php echo $center->id; ?>"><?php echo __('Show on map', 'sls-theme'); ?></a></h5>
                            <br>
                            <button class="nbtn nbtn-inverse" onclick="document.location.href='<?php echo WP_SITEURL ?>/anti-aging-from-DNA/contact-us'">Book A Consultation</button>
                 </div>
                  <?php else :?>
                 <div class="col-md-6 col-sm-offset-0 col-xs-6" style="background: #44464a;padding: 0 25px 25px 25px;">  
                        <h4 class="uppercase"><b><?=$center->country?></b></h4>                      
                       <?php if((strtolower($center->user_role)=="partner")){?>
                            <h5 class="m-b-20 gb"><b><?=$center->brand_name;?></b><br></h5>   
                        <?php } else { ?>
                              <h5 class="m-b-20 gb"><b><?=$center->company_name;?></b><br></h5>   
                        <?php } ?>
                        <h4 class="m-b-20 gl">
                        <?php include('display_block_center.php');?>
                        </h4>    
                        <h5 class="m-t-15 font-light"><a  href="javascript:void(0)"  id="center-block-show-map" class="show-on-map-center active" data-center_id="<?php echo $center->id; ?>"><?php echo __('Show on map', 'sls-theme'); ?></a></h5>
                            <br>
                            <button class="nbtn nbtn-inverse" onclick="document.location.href='<?php echo WP_SITEURL ?>/anti-aging-from-DNA/contact-us'">Book A Consultation</button>
                 </div>
                 <div class="col-md-6 col-sm-offset-0 col-xs-6" style="padding-left: 20px;">  
                     <ul id="center-block-tab" class="nav nav-tabs center-block-tab">
                         <li class="active"><a data-toggle="tab" style="font-size: 15px;font-weight: 500;" href="#center-blog-post-content">BLOG POSTS</a></li>
                         <li><a data-toggle="tab" style="font-size: 15px;font-weight: 500;" href="#center-press-release-content">PRESS RELEASES</a></li>                          
                      </ul>

                      <div class="tab-content center-block-content">
                            <div id="center-blog-post-content" class="tab-pane fade in active">                              
                                <div id="center-block-blogs-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                            <?php foreach($blogs as $index=>$blog) : ?>
                                                <li data-target="#center-block-blogs-carousel" data-slide-to="<?php echo $index; ?>" class="<?php if($index==0)echo 'active' ?>"></li>
                                            <?php endforeach; ?>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                      <?php foreach($blogs as $index=>$blog) : ?>
                                         <div class="item <?php if($index==0)echo 'active' ?>">                                             
                                            <div class="post">
							<div class="post-img float_left">
                                                            <img class="center-blog-post-img" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($blog->ID), 'medium')[0]; ?>" alt="">
                                                        </div>
							<div class="info-post">
								<?php $blog_categories = get_the_category($blog); ?>
								<div class="post-title" id="<?php echo $blog->ID; ?>"><?php echo get_the_title($blog); ?></div>
								<h6 class="post-info custom"><?php echo get_post_time('d/m/Y', false, $blog); ?> <br> Categories:
                                                                    <?php
                                                                        $blog_category_names = "";
									 foreach ($blog_categories as $blog_category){
										 $blog_category_names .= esc_html($blog_category->name);
									}
                                                                     echo wp_trim_words(esc_html($blog_category_names),5); 
                                                                  ?>      
								</h6>
                                                                <hr class="white_hr">
								<p class="post-text post_text_margin letter_spacing">
									<?php $blog_excerpt = !empty(get_the_excerpt($blog)) ? get_the_excerpt($blog) : wp_trim_words($blog->post_content, 20); ?>
                                    <?php echo strip_tags($blog_excerpt); ?>
								</p>
								<div class="btns">
									<a href="<?php echo get_permalink($blog); ?>" class="read-more">Read more</a>
									<!--<a href="#" class="our-blog">our blog</a>-->
								</div>
							</div>
						</div>                                      
                                            
                                         </div>
                                      <?php endforeach; ?>  
                                    </div>

                                    <!-- Left and right controls -->

                                  </div>
                            </div>
                            <div id="center-press-release-content" class="tab-pane fade">
                             <div id="center-block-press-release-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                            <?php foreach($press_releases as $index=>$press_release) : ?>
                                                <li data-target="#center-block-press-release-carousel" data-slide-to="<?php echo $index; ?>" class="<?php if($index==0)echo 'active' ?>"></li>
                                            <?php endforeach; ?>
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                      <?php foreach($press_releases as $index=>$press_release) : ?>
                                         <div class="item <?php if($index==0)echo 'active' ?>">                                             
                                            <div class="post">
							<div class="post-img float_left">
                                                            <img class="center-blog-post-img" src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id($press_release->ID), 'medium')[0]; ?>" alt="">
                                                        </div>
							<div class="info-post">
								<?php $blog_categories = get_the_category($press_release); ?>
								<div class="post-title" id="<?php echo $press_release->ID; ?>"><?php echo get_the_title($press_release); ?></div>
								<div class="post-info custom"><?php echo get_post_time('d/m/Y', false, $press_release); ?> <br> Categories:	
                                                                     <?php
                                                                        $blog_category_names = "";
									 foreach ($blog_categories as $blog_category){
										 $blog_category_names .= esc_html($blog_category->name);
									}
                                                                     echo wp_trim_words(esc_html($blog_category_names),5); 
                                                                  ?>                                                                 
                                                                    
								</div>
                                                                <hr class="white_hr">
								<p class="post-text post_text_margin letter_spacing">
									<?php $blog_excerpt = !empty(get_the_excerpt($press_release)) ? get_the_excerpt($press_release) : wp_trim_words($press_release->post_content, 20); ?>
                                    <?php echo strip_tags($blog_excerpt); ?>
								</p>
								<div class="btns">
									<a href="<?php echo get_permalink($press_release); ?>" class="read-more">Read more</a>
									<!--<a href="#" class="our-blog">our blog</a>-->
								</div>
							</div>
						</div>                                      
                                            
                                         </div>
                                      <?php endforeach; ?>   
                                    </div>

                                    <!-- Left and right controls -->

                                  </div>
                            </div>     
                      </div>
                 </div>
                 <?php endif; ?>
             </div>
        </div>      
 </section>

<script>
  
    jQuery(document).ready(function($) {        
         function set_center_block_interval(){             
                $('.show-on-map-center.active').trigger('click');
        }        
        //setTimeout( set_center_block_interval, 5000); 
     })         
</script>

<?php endif; ?>
