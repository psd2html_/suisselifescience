<?

if(isset($_REQUEST['search'])){
	$args = array( 'post_type' => 'centers', 'numberposts'=> -1,
	'meta_query'	=> array(
		'relation'		=> 'OR',
		array(
			'key'		=> 'city',
			'value'		=> $_REQUEST['search'],
			'compare'	=> 'LIKE'
		),
		array(
			'key'		=> 'country',
			'value'		=> $_REQUEST['search'],
			'compare'	=> 'LIKE'
		),
		array(
			'key'		=> 'zip',
			'value'		=> $_REQUEST['search'],
			'compare'	=> 'LIKE'
		),
		array(
			'key'		=> 'state',
			'value'		=> $_REQUEST['search'],
			'compare'	=> 'LIKE'
		)
	));
}else{
	$args = array( 'post_type' => 'centers', 'numberposts'=> -1);
}
$jsOffice=[];
$myposts = get_posts( $args );
if(!is_page()){
	$myposts = $posts;
}?>


<?if (isset($_REQUEST['search']) && empty($myposts)){?>
	<section class="bg-grey">
		<div class="container">
			<?if($WPGeoplugin->getCountryTaxonomy()=="Global"){
				$args = array( 'post_type' => 'centers', 'numberposts'=> -1);
				$myposts = get_posts( $args );?>
				<script>
					var panCountry = "<?=$WPGeoplugin->getCountry()?>";
				</script>
			<?}else{
				$args = array( 'post_type' => 'centers', 'zones' => $WPGeoplugin->getCountryTaxonomy());
				$myposts = get_posts($args);?>
				<script>
					var panCountry = "<?=$WPGeoplugin->getCountryTaxonomy()?>";
				</script>
			<?}?>
		</div>
	</section>
<?};?>

<?foreach($myposts as $post){?>
	<?if(get_field("location",$post->ID)){?>
			<?include('centers-marker-html.php');?>
		<?$jsOffice[]=array("name"=>$post->post_title,"lat"=>get_field("location",$post->ID)["lat"],"lng"=>get_field("location",$post->ID)["lng"],'HTML'=>$markerHTML)?>
	<?};?>
<?};?>
<script>
	var office = <?=json_encode($jsOffice);?>;
</script>