<?php
    global $WPGeoplugin;
    global $wpdb;

    $GeoInfo = $WPGeoplugin->getGeoInfo();
    $jsOfficeRelevant =[];
    $jsOffice=[];
    $db_table_name = SECURE_CENTERS_TABLE;
if(!isset($_REQUEST['search'])){	
        
    $country_code=Seven_Geolocation::geolocate_ip( '', true, true );   
    //$country_code = "TH";
    $country_zone = get_country_zone_by_code($country_code);
    $country_name = get_country_name_by_code($country_code);  
    if(!empty($country_name)){ 
         $args = "select * from `{$db_table_name}` where "
                 . " town_city='{$country_name}'"
                 . " OR country='{$country_name}'"
                 . " OR state='{$country_name}'"
                 . " OR zip='{$country_name}'";     
         $mycenters = $wpdb->get_results($args);         
          if(count($mypostsSearch) == 0) {                
                $zone_countries = get_zone_countries($country_zone);
                if(count($zone_countries) > 0) {
                    $args = "select * from `{$db_table_name}` where "
                         . " town_city IN ( '" . implode($zone_countries, "', '") . "')"
                         . " OR country IN ( '" . implode($zone_countries, "', '") . "')"
                         . " OR state IN ( '" . implode($zone_countries, "', '") . "')"
                         . " OR zip IN ( '" . implode($zone_countries, "', '") . "')";           

                    //print_r($args);
                    $mycenters = $wpdb->get_results($args);                     
                 }
         }            
         $centerPosts = $mycenters;
         //print_r("mypostsSearch : ". count($mypostsSearch));
    }   
} else {
     $args = "select * from `{$db_table_name}` where "
                 . " town_city='{$_REQUEST['search']}'"
                 . " OR country='{$_REQUEST['search']}'"
                 . " OR state='{$_REQUEST['search']}'"
                 . " OR zip='{$_REQUEST['search']}'";   
        $mycenters = $wpdb->get_results($args);           
}

include('center-block.php');
?>

<section class="bg-grey p-b-0">
			<div class="container">
<?if(count($mycenters)<1 && isset($_REQUEST['search'])){?>
	<p style="color:red;">We were unable to find centers by your query, please refine it. Below is the list of centers close to your location.</p>
<?};?>
<?$markersCount=isset($center)?1:0;?>
<?$forCount=0;$titleRole=0;?>
<?if($mycenters){?>
<?foreach($mycenters as $post){?>
	<?if(strtolower($post->user_role)=="partner"){?>
		<?if($titleRole==0){$titleRole=1;?>
			<h2 class="m-t-0 m-b-40"><short>i</short>DDNA<sup>®</sup> ANTI-AGING CENTERS</h2>
			<hr>
		<?}?>
		
		<?if($forCount==0){?>
			<div class="row">
		<?};?>
		<?include('centers-marker-html.php');?>
                 <?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
                 <?$markersCount++;?>
		<div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?php echo $post->user_role?>">
                            <h4 class="uppercase"><?php echo $post->country?></h4>
                            <h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
                            <?include('display_block_center.php');?>
                            <h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?php echo $post->latitude?>, <?php echo $post->longitude?>, <?=$markersCount-1?>)">Show on map</a></h6>
		</div>
		<?if($forCount==2){$forCount=0;?>
			</div>
		<?}else{
			$forCount++;
		};?>
	<?};?>
<?};?>
<?};?>
<?if($forCount!=0){?>
	</div>
<?}?>
<?$forCount=0;?>
<?$titleRole=0;?>
<?if($mycenters){?>
<?foreach($mycenters as $key=>$post){?>
	<?if((strtolower($post->user_role)=="distributor" || strtolower($post->user_role)=="master-distributor") && (isset($post->company_name) && !empty($post->company_name))){?>
		<?if($post->latitude && !empty($post->latitude) && $post->longitude && !empty($post->longitude)){?>
			<?if($titleRole==0){$titleRole=1;?>
				<!--<h2 class="m-t-0 m-b-40">iDDNA® ANTI-AGING CENTERS</h2>-->
				<hr>
			<?}?>
		
			<?if($forCount==0){?>
				<div class="row">
			<?};?>
			<?include('centers-marker-html.php');?>     
                         <?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
                          <?$markersCount++;?>
                        <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?php echo $post->user_role?>">
                            <h4 class="uppercase"><?php echo $post->country?></h4>
                            <h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
                                <?include('display_block_center.php');?>
                                <h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?php echo $post->latitude?>, <?php echo $post->longitude?>, <?php echo $markersCount-1?>)">Show on map</a></h6>
                        </div>
			<?if($forCount==2){$forCount=0;?>
				</div>
			<?}else{
				$forCount++;
			};?>
		<?};?>
	<?};?>
<?};?>
<?};?>
<?if($forCount!=2){$forCount=0;?>
	</div>
<?}?>

<?if(count($mycenters)<1){?>
<? $args = "select * from `{$db_table_name}`";
    $mycenters = $wpdb->get_results($args)
 ?>
<?foreach($mycenters as $post){?>
	<?if($post->latitude && !empty($post->latitude) && $post->longitude && !empty($post->longitude)){?>
		<?include('centers-marker-html.php');?>                        
                 <?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
	<?};?>
<?};?>
<?if($centerPosts){?>
<?foreach($centerPosts as $post){?>
		<?$jsCenterOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML)?>
<?};?>
<?};?>

<?};?>


<script>
    var office = <?php echo json_encode($jsOffice);?>;
    var centerOffice = <?php echo json_encode($jsCenterOffice);?>;
    console.log('centerOffice: ', centerOffice);
    console.log('getCountryTaxonomy: ', <?php echo json_encode($WPGeoplugin->getCountryTaxonomy()); ?>);   
    console.log('centerPosts: ', <?php echo json_encode($centerPosts); ?>);    
</script>

</div>
</section>
