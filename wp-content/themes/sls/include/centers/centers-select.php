<?$args=array(
	'taxonomy' => 'zones',
	'hide_empty' => false,
	'orderby' =>'term_group',
	'exclude_tree'=>306
  );
$zones=get_categories($args);
?>
<div class="col-sm-6 full-inner">
	<select onchange='location.href="/age-management-center/"+this.options[this.selectedIndex].value+"/";' autocomplete="off">
		<?foreach ($zones as $zone){
			if(wp_get_term_taxonomy_parent_id($zone->term_id,"zones")!=269){
				if(get_query_var('zones') == strtolower($zone->slug)){
					$selected = "selected='selected'";
				}else if(!get_query_var('zones') && strtolower($currentGeolocation["country"]["name_en"]) == strtolower($zone->slug)){
					$selected = "selected='selected'";
				}else{
					$selected = "";
				};
				if($zone->category_parent==0){
					$option = '<option class="selectZone" value="'.strtolower($zone->slug).'" '.$selected.'><b>';
					$option .= $zone->cat_name;
					$option .= '</b></option>';
				}else{
					$option = '<option class="selectCountry" value="'.strtolower($zone->slug).'" '.$selected.'>&nbsp;&nbsp;&nbsp;';
					$option .= $zone->cat_name;
					$option .= '</option>';
				}
			
				echo $option;
			};	
		}?>
	</select>
</div>
<?if(get_query_var('zones')=="united-states"){?>
	<?$argsUSA=array(
		'taxonomy' => 'zones',
		'hide_empty' => false,
		'orderby' =>'term_group',
		'child_of'=>269
	  );
	$zonesUSA=get_categories($argsUSA);
	?>
	<div class="col-sm-6 full-inner">
		<select onchange='location.href="/age-management-center/united-state/"+this.options[this.selectedIndex].value+"/";' autocomplete="off">
			<option value="">--Change State--</option>
			<?foreach ($zonesUSA as $zone){
				if(get_query_var('zones') == strtolower($zone->slug)){
					$selected = "selected='selected'";
				}else if(!get_query_var('zones') && strtolower($currentGeolocation["country"]["name_en"]) == strtolower($zone->slug)){
					$selected = "selected='selected'";
				}else{
					$selected = "";
				};
				if($zone->category_parent==0){
					$option = '<option class="selectZone" value="'.strtolower($zone->slug).'" '.$selected.'><b>';
					$option .= $zone->cat_name;
					$option .= '</b></option>';
				}else{
					$option = '<option class="selectCountry" value="'.strtolower($zone->slug).'" '.$selected.'>&nbsp;&nbsp;&nbsp;';
					$option .= $zone->cat_name;
					$option .= '</option>';
				}
			
				echo $option;
			}?>
		</select>
	</div>
<?};?>