<?php

//global $WPGeoplugin;
global $wpdb;

$jsOfficeRelevant =[];
$jsOffice=[];

$db_table_name = SECURE_CENTERS_TABLE;


if(!isset($_REQUEST['search'])){        
    $country_code=Seven_Geolocation::geolocate_ip( '', true, true );   
    //$country_code = "AD";
    
    $country_zone = get_country_zone_by_code($country_code);
    $country_name = get_country_name_by_code($country_code);  
    //print_r(" country_code  " . $country_code);
    if(!empty($country_name)){ 
         $args = "select * from `{$db_table_name}` where "
                 . " town_city='{$country_name}'"
                 . " OR country='{$country_name}'"
                 . " OR state='{$country_name}'"
                 . " OR zip='{$country_name}'";           
                 
         $mypostsSearch = $wpdb->get_results($args);
         if(count($mypostsSearch) == 0) {                
                $zone_countries = get_zone_countries($country_zone);
                if(count($zone_countries) > 0) {
                    $args = "select * from `{$db_table_name}` where "
                         . " town_city IN ( '" . implode($zone_countries, "', '") . "')"
                         . " OR country IN ( '" . implode($zone_countries, "', '") . "')"
                         . " OR state IN ( '" . implode($zone_countries, "', '") . "')"
                         . " OR zip IN ( '" . implode($zone_countries, "', '") . "')";           

                    //print_r($args);
                    $mypostsSearch = $wpdb->get_results($args);  
                   
                 }
         }
         
         $centerPosts = $mypostsSearch;
         
         
    } 
    else
    {     
        $args = "select * from `{$db_table_name}`";
        $myposts = $wpdb->get_results($args); 
   }
?>
	<script>
		panCountry = "";
	</script>
<?php 
 }
  else {

       $args = "select * from `{$db_table_name}` where "
                 . " town_city='{$_REQUEST['search']}'"
                 . " OR country='{$_REQUEST['search']}'"
                 . " OR state='{$_REQUEST['search']}'"
                 . " OR zip='{$_REQUEST['search']}'";                 
         $mypostsSearch = $wpdb->get_results($args);         
         //print_r("mypostsSearch : ". count($mypostsSearch));
};

    
$searchRelevant=array();
if(isset($mypostsSearch) && count($mypostsSearch)>0){
	foreach($mypostsSearch as $post){
		$searchRelevant[]=$post->id;
	};
};  
//print_r(" mypostsSearch count  " . count($mypostsSearch));
//print_r(" myposts count  " . count($myposts));
include('center-block.php');
?>
<section class="bg-grey p-b-0">
<div class="container">
<div class="row">

<?if(count($mypostsSearch) < 1 && isset($_REQUEST['search'])){?>
		<p style="color:red;">We were unable to find centers by your query. Please refine it or see the full list of centers below.</p>
<?};?>
<?
$args = "select * from `{$db_table_name}` where id NOT IN ( '" . implode($searchRelevant, "', '") . "' )";
$myposts = $wpdb->get_results($args);
?>

<?$markersCount=isset($center)?1:0;?>
<?if(isset($mypostsSearch) && count($mypostsSearch)>0){?>

	<?$forCount=0;$titleRole=0;?>
	<?foreach($mypostsSearch as $post){?>
		<?if(strtolower($post->user_role)=="partner" && isset($post->company_name) && !empty($post->company_name)){?>

			<?if($titleRole==0 && !isset($aging)){$titleRole=1;?>
				<h2 class="uppercase m-t-0 m-b-40"><short>i</short>DDNA<sup>®</sup> CENTERS</h2>
				<hr>
			<?}elseif($titleRole==0){$titleRole=1;?>
				<h2 class="uppercase m-t-0 m-b-40"><short>i</short>DDNA<sup>®</sup> ANTI-AGING CENTERS</h2>
				<hr>
			<?};?>
			<?if($forCount==0){?>
				<div class="row">
			<?};?>
			<?include('centers-marker-html.php');?>                        
			<?$jsOfficeRelevant[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
			<?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
			<?$markersCount++;?>
			<div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?=$post->user_role?>">
                            <h4 class="uppercase"><?=$post->country?></h4>
				<h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
				<?include('display_block_center.php');?>
				<h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?=$post->latitude?>, <?=$post->longitude?>, <?=$markersCount-1?>)">Show on map</a></h6>
			</div>
			<?if($forCount==2){$forCount=0;?>
				</div>
			<?}else{
				$forCount++;
			};?>
		<?};?>
	<?};?>
	<?foreach($myposts as $post){?>
		<?if(strtolower($post->user_role)=="partner" && isset($post->company_name) && !empty($post->company_name)){?>
			<?if($titleRole==0 && !isset($aging)){$titleRole=1;?>
				<h2 class="uppercase m-t-0 m-b-40"><short>i</short>DDNA<sup>®</sup> CENTERS</h2>
				<hr>
			<?}elseif($titleRole==0){$titleRole=1;?>
				<h2 class="uppercase m-t-0 m-b-40"><short>i</short>DDNA<sup>®</sup> ANTI-AGING CENTERS</h2>
				<hr>
			<?};?>
			<?if($forCount==0){?>
				<div class="row">
			<?};?>
			<?include('centers-marker-html.php');?>
			<?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
			<?$markersCount++;?>
			<div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?=$post->user_role?>">
                            <h4 class="uppercase"><?=$post->country?></h4>
				<h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
				<?include('display_block_center.php');?>
				<h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?=$post->latitude?>, <?=$post->longitude?>, <?=$markersCount-1?>)">Show on map</a></h6>
			</div>
			<?if($forCount==2){$forCount=0;?>
				</div>
			<?}else{
				$forCount++;
			};?>
		<?};?>
	<?};?>
	<?if($forCount!=0){?>
		</div>
	<?}?>
	<?$forCount=0;$titleRole=0;?>

	<?foreach($mypostsSearch as $key=>$post){?>
		<?if((strtolower($post->user_role)=="distributor" || strtolower($post->user_role)=="master-distributor") && (isset($post->company_name) && !empty($post->company_name))){?>
			<?if($post->latitude && !empty($post->latitude) && $post->longitude && !empty($post->longitude)){?>
				<?if($titleRole==0){$titleRole=1;?>
					<!--<h2 class="uppercase m-t-0 m-b-40">Distributors</h2>-->
					<hr>
				<?}?>

				<?if($forCount==0){?>
					<div class="row">
				<?};?>
				<?include('centers-marker-html.php');?>                        
                                <?$jsOfficeRelevant[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
                                <?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
                                <?$markersCount++;?>
                                <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?=$post->user_role?>">
                                    <h4 class="uppercase"><?=$post->country?></h4>
                                     <h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
                                        <?include('display_block_center.php');?>
                                        <h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?=$post->latitude?>, <?=$post->longitude?>, <?=$markersCount-1?>)">Show on map</a></h6>
                                </div>
				<?if($forCount==2){$forCount=0;?>
					</div>
				<?}else{
					$forCount++;
				};?>
			<?};?>
		<?};?>
	<?};?>
	<?foreach($myposts as $key=>$post){?>
		<?if((strtolower($post->user_role)=="distributor" || strtolower($post->user_role)=="master-distributor") && (isset($post->company_name) && !empty($post->company_name))){?>
			<?if($post->latitude && !empty($post->latitude) && $post->longitude && !empty($post->longitude)){?>
				<?if($titleRole==0){$titleRole=1;?>
					<!--<h2 class="uppercase m-t-0 m-b-40">Distributors</h2>-->
					<hr>
				<?}?>

				<?if($forCount==0){?>
					<div class="row">
				<?};?>
				<?include('centers-marker-html.php');?>                        
                                <?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
                                <?$markersCount++;?>
                                <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?=$post->user_role?>">
                                    <h4 class="uppercase"><?=$post->country?></h4>
                                      <h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
                                        <?include('display_block_center.php');?>
                                        <h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?=$post->latitude?>, <?=$post->longitude?>, <?=$markersCount-1?>)">Show on map</a></h6>
                                </div>
				<?if($forCount==2){$forCount=0;?>
					</div>
				<?}else{
					$forCount++;
				};?>
			<?};?>
		<?};?>
	<?};?>
	<?if($forCount!=2){$forCount=0;?>
		</div>
	<?}?>

<?}else{?>

	<?//------------------------//?>

	<?$forCount=0;$titleRole=0;?>
	<?foreach($myposts as $post){?>
		<?if(strtolower($post->user_role=="partner") && isset($post->company_name) && !empty($post->company_name)){?>
			<?if($titleRole==0 && !isset($aging)){$titleRole=1;?>
				<h2 class="uppercase m-t-0 m-b-40"><short>i</short>DDNA<sup>®</sup> CENTERS</h2>
				<hr>
			<?}elseif($titleRole==0){$titleRole=1;?>
				<h2 class="uppercase m-t-0 m-b-40"><short>i</short>DDNA<sup>®</sup> ANTI-AGING CENTERS</h2>
				<hr>
			<?};?>


			<?if($forCount==0){?>
				<div class="row">
			<?};?>
			<?include('centers-marker-html.php');?>
			<?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
			<?$markersCount++;?>
			<div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?=$post->user_role?>">
                            <h4 class="uppercase"><?=$post->country?></h4>
				<h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
				<?include('display_block_center.php');?>
				<h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?=$post->latitude?>, <?=$post->longitude?>, <?=$markersCount-1?>)">Show on map</a></h6>
			</div>
			<?if($forCount==2){$forCount=0;?>
				</div>
			<?}else{
				$forCount++;
			};?>
		<?};?>
	<?};?>
	<?if($forCount!=0){?>
		</div>
	<?}?>
	<?$forCount=0;$titleRole=0;?>

	<?foreach($myposts as $key=>$post){?>
		<?if((strtolower($post->user_role)=="distributor" || strtolower($post->user_role)=="master-distributor") && (isset($post->company_name) && !empty($post->company_name))){?>
			<?if($post->latitude && !empty($post->latitude) && $post->longitude && !empty($post->longitude)){?>
				<?if($titleRole==0){$titleRole=1;?>
					<!--<h2 class="uppercase m-t-0 m-b-40">Distributors</h2>-->
					<hr>
				<?}?>

				<?if($forCount==0){?>
					<div class="row">
				<?};?>
				<?include('centers-marker-html.php');?>
                                <?$jsOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML);?>
                                <?$markersCount++;?>
                                <div class="col-md-4 col-sm-offset-0 map_find_elem col-xs-6 col-xs-offset-3 search-block-res" data-role="<?=$post->user_role?>">
                                    <h4 class="uppercase"><?=$post->country?></h4>
                                      <h5 class="m-b-20"><b><?=$post->company_name;?></b><br><p><?=$post->first_name." ".$post->last_name; ?></p></h5>
                                        <?include('display_block_center.php');?>
                                        <h6 class="m-t-20"><a class="bold" href="javascript:void(0)" onclick="panMap(<?=$post->latitude?>, <?=$post->longitude?>, <?=$markersCount-1?>)">Show on map</a></h6>
                                </div>
				<?if($forCount==2){$forCount=0;?>
					</div>
				<?}else{
					$forCount++;
				};?>
			<?};?>
		<?};?>
	<?};?>
	<?if($forCount!=2){$forCount=0;?>
		</div>
	<?}?>
<?};?>
<?if($centerPosts){?>
<?foreach($centerPosts as $post){?>
		<?$jsCenterOffice[]=array("name"=>$post->company_name,"lat"=>$post->latitude,"lng"=>$post->longitude,'HTML'=>$markerHTML)?>
<?};?>
<?};?>
    <?php /*var_dump($jsOffice); exit;*/ ?>
<script>
	<?if (count($jsOfficeRelevant)>0){?>
		var jsOfficeRelevant = <?=json_encode($jsOfficeRelevant);?>;
    <?}?>
	var office = <?=json_encode($jsOffice);?>;
	var centerOffice = <?=json_encode($jsCenterOffice);?>;
        console.log('office: ', office);
        console.log('centerOffice: ', centerOffice);
        console.log('centerPosts: ', <?php echo json_encode($centerPosts); ?>);
</script>

</div>
</div>
</section>