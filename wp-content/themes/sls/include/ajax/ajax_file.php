<?php
/*** WordPress ***/
error_reporting(0);
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/custom_mailchimp_api/include/mailchimp.php');

add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));

if ($_POST['page_title']) {
    $page_title = wp_slash($_POST['page_title']);
} else {
    $page_title = '';
}

if ($_POST['page_url']) {
    $page_url = wp_slash($_POST['page_url']);
} else {
    $page_url = '';
}

// Data for form_results
$form_data = [];
$form_data['page_title'] = $page_title;
$form_data['page_url'] = $page_url;
if (isset($_POST['Email'])) {
    $form_data['email'] = $_POST['Email'];
} elseif (isset($_POST['email'])) {
    $form_data['email'] = $_POST['email'];
}

$entity = isset($_POST['entity']) ? $_POST['entity'] : 0;

$thankId = $entity . '_thank_you_html';
$interests_field = $entity . '_interests';
//var_dump($thankId);
//$interests_data = array();
$interests_data = get_field($interests_field, 'option');
//var_dump($interests_data);

$email_content_key = $thankId.'_email';
$email_content = trim(get_field($email_content_key, 'option'));
if (!$email_content) {
    $email_content = trim(get_field($thankId, 'option'));
}

$mc = new \VPS\MailChimp(get_field('api_key', 'option'));

// If user alread subscribed
//$resp = $mc->get('/lists/'.get_field('id_subscribe_list', 'option').'/members/' . md5(strtolower($_POST["Email"])));

$already_contact_redirect = $thankId . '_redirect';
$redirect = get_field($already_contact_redirect, 'option');

wp_reset_postdata();
if ($redirect) {
    $the_query = get_posts( array(
        'numberposts'     => -1,
        'post_type'       => 'form_results',
        'post_status'     => 'private',
        'meta_key'        => 'email',
        'meta_value'      => $form_data['email'],
) );
    $already_in_db = (count($the_query) > 0) ? true : false;
}


if ($already_in_db && $redirect) {
    wp_send_json([
        'status' => false,
        'title' => get_field('title_modal_window_already_contact_us', 'option'),
        'redirect_url' => get_field('redirect_url_modal_window_already_contact_us', 'option'),
        'already_on_db' => $already_in_db,
        'html' => get_field('you_already_contacted_us', 'option'),
    ]);
}

$post = $_POST;
if (isset($post['countryGoogle'])) {
    $post['Country_Geolocation'] = $post['countryGoogle'];
    unset($post['countryGoogle']);
}


if (1==1) {
    // Форма для підписки
    $message = "";

    $subject = $_POST["subject_to_submit"];

    $fields = [];
    foreach ($post as $key => $value) {
        // Фільтрація полів
        // Все крім истемних полів попадає в лист
        if ($key != "email_to_submit"
            && $key != "interest"
            && $key != "subject_to_submit"
            && $key != "entity"
            && $key != "cross-domain-register-nonce"
            && $key != "subscr"
        ) {
            $keys = ucfirst(str_replace("_", " ", $key));
            if ($post[$key]) {
                $message .= "<b>".$keys.":</b> ".stripslashes($post[$key])."<br>";
            }
            $fields[$key] = $value;
        } else {
            if (isset($post["interest"]) && $key == "interest") {

                $interestsMail = [];

                foreach ($_POST["interest"] as $keyInterest) {
                    foreach ($interests_data as $interest) {
                        if ($keyInterest === $interest['value_of_mailchimp_interest']) {
                            $interestsMail[] = $interest['name'];
                        }
                    }
                }
                if (!empty ($interestsMail)) {
                    $message .= "<b>Interests:</b> ".implode('; ', $interestsMail)."<br>";
                }
            }
        }
    };

    // Підписка на MailChimp якщо є вибрані товари
    // Skin care, diet and sport
    $check_user_subscribe = get_field($thankId . '_subscribe', 'option');
    if (isset($_POST["interest"]) && $_POST["interest"] && isset($form_data['email']) && $form_data['email'] && $check_user_subscribe) {
        $interests = [];
        foreach ($_POST["interest"] as $key) {
            $interests[$key] = true;
        }

        $resp = $mc->post('/lists/'.get_field('id_subscribe_list', 'option').'/members', [
            'email_address' => $form_data['email'],
            'interests' => $interests,
            'status' => 'subscribed',
        ]);
    }



    $email_admin_content_key = $thankId.'_email_admin';
    $email_admin_content = trim(get_field($email_admin_content_key, 'option'));

    // Формування заголовка листа
    $country = ($_POST["Country"]) ? $_POST["Country"] : (($_POST["countryGoogle"]) ? $_POST["countryGoogle"] : '');
    //$subject = str_replace("{country}", $country, $subject);


    $interest_list = ($interestsMail) ? implode(", ", $interestsMail) : '';

    $form_data['subject'] = $subject;
    $form_data['message'] = $message;

    $from = get_field($entity . '_mail_from_distributor', 'option');
    $headers = 'From: ' . $from . "\r\n";

    $message_body = get_field($entity . '_thank_you_html_email_admin', 'option');
    $messageObj = new EditMessage;
    $messageObj->setBody($message_body)
                ->setSubject($subject)
                ->setHeaders($headers)
                ->addReplace('{field_list}', $message)
                ->addReplace('{country}', $country)
                ->addReplace('{interestedproduct}', $interest_list)
                ;

    foreach ($fields as $field=>$value) {
        $messageObj->addReplace('{field_' . $field . '}', $value);
    }

    $send_to_distributor = get_field($entity . '_forms_notifications_country_distributor', 'option');
    $email_to_send = [];

    // Getting coordinates for current user from API.
    $lat_lon = Seven_Geolocation::geolocate_coordinates( '', true, true  );
    $partner = array_shift(select_closest_distributors($lat_lon, $country));

    if (!empty($partner) && isset($partner->user_email) && !empty($partner->user_email) && $send_to_distributor) {

        $email_to_send[] = $partner->user_email;
        foreach ($_POST["email_to_submit"] as $mailSub) {
            $headers .= 'BCC:' . $mailSub . "\r\n";
        }
        $messageObj->setHeaders($headers);
    } else {
        foreach ($_POST["email_to_submit"] as $mailSub) {
            $email_to_send[] = $mailSub;
        }
    }
    //
    // Send email to closest partner on the same region as the user or to Company.
    foreach ($email_to_send as $mailSub) {
        try {
            $messageObj->send($mailSub);
        } catch (Exception $e) {		
            wp_send_json([
                'status' => false,
                'message'=> $e->getMessage(),
            ]);
        }
    }

    $from = get_field($entity . '_mail_from_client', 'option');
    $mail_title = get_field($entity . '_mail_title_client', 'option');
    $headers = 'From: ' . $from . "\r\n";

    $messageObj = new EditMessage;
    $messageObj->setBody($email_content)
                ->setSubject($mail_title)
                ->setHeaders($headers)
                ;
    foreach ($fields as $field=>$value) {
        $messageObj->addReplace('{field_' . $field . '}', $value);
    }

    try {
        $messageObj->send($form_data['email']);
    } catch (Exception $e) {	
        wp_send_json([
            'status' => false,
            'message'=> $e->getMessage(),	
        ]);
    }

}

remove_filter('wp_mail_content_type', 'set_html_content_type');
function set_html_content_type()
{
    return 'text/html';
}

if (function_exists('frp_insert_form_result')) {
    frp_insert_form_result($form_data);
}

wp_send_json([
    'status' => true,
    'display_scheduleonce' => get_field($entity . '_scheduleonce', 'option'),
    'thank_page_url' => '/thank-you/?source='.urlencode($thankId),
]);
