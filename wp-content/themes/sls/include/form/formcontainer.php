    <!-- section 9 -->
    <section>
        <?php /*$form_section = get_fields('option');*/ ?>
        <div class="container">
            <h2 class="form-title"><?php echo get_field($entity . '_title', 'option'); ?></h2>
        <?php if (!empty(get_field($entity . "_title_subtitle", 'option'))) : ?>
        <p class="form-subtitle"><?=get_field($entity . "_title_subtitle",'option');?></p>
            <!--
                <div class="row m-b-40 ">
                    <div class="col-lg-12">
                        <h5 class="font-ligth  m-t-20 m-b-0"><?=get_field($entity . "_title_subtitle",'option'); ?></h5>
                    </div>
                </div>
            -->
        <?php endif; ?>
            <div class="row">
                <?php echo !empty($sidebar_content) ? $sidebar_content : ''; ?>
                <div class="col-xs-12 <?php echo !empty($sidebar_class) ? $sidebar_class : ''; ?>">
                    <div class="contact-form">
                        <form id="ajax_contact_send" method="POST" action="" class="clearfix m-t-40 my_amazing_form">

                            <div class="row clearfix">
                                <?php
                                foreach(get_field($entity . '_form_fields', 'option') as $field){ ?>

                                    <?if($field["type"]=="textarea"){?>
                                        <div class="col-lg-6">
                                            <div class="my_form_input">
                                                <textarea name="<?=$field["name"];?>" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?>></textarea>
                                            </div>
                                        </div>
                                    <?}else if($field["type"]=="Geolocation-Country"){?>
                                        <div class="col-lg-6">
                                            <div class="my_form_input">
                                                <div style="position:relative js-hint">
                                                    <?php $countries = include( TEMPLATE_DIRECTORY . '/include/countries.php'); ?>

                                                    <select class="full-width cnt-ss" id="country-selector" autocorrect="off" autocomplete="off" name="<?=$field["name"];?>" <?if ($field["required"]==1){echo "required";};?>>
                                                        <option value=""><?=$field["placeholder"];?></option>
                                                        <?php foreach( $countries as $country_code => $country_name ) : ?>
                                                            <option value="<?php echo $country_name; ?>"><?php echo $country_name; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?}else if($field["type"]=="Geolocation-State"){;?>
                                        <div class="col-lg-6">
                                            <div class="my_form_input">
                                                <input name="State" type="text" class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?> />
                                                <?if ($field["required"]==1){?>
                                                    <span class="req">required</span>
                                                    <span class="valid">Valid</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <? }else if($field["type"]=="Geolocation-City"){; ?>
                                        <div class="col-lg-6">
                                            <div class="my_form_input">
                                                <input name="City" type="text" class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field[ "required"]==1){echo "required";};?> />
                                                <?if ($field["required"]==1){?>
                                                    <span class="req">required</span>
                                                    <span class="valid">Valid</span>
                                                            <? } ?>
                                                    </div>
                                        </div>
                                    <? }else if($field["type"]=="select"){; ?>
                                        <?php $k = explode('|', $field["select_value"]);?>
                                        <div class="col-lg-6">
                                            <select name="<?php echo $field["name"]; ?>">
                                                <?php foreach($k as $result){ ?>
                                                    <option value="<?php echo $result; ?>">
                                                        <?php echo $result; ?>
                                                    </option>
                                                <?}?>
                                            </select>
                                        </div>
                                    <? }else{ ?>
                                        <div class="col-lg-6">
                                            <div class="my_form_input">
                                                <input name="<?=$field["name"];?>" <?php if($field["type"]=='zipcode' ){ ?>type="text" maxlength="10"<?php }elseif($field["type"]=='tel'){ ?>type="tel" pattern="^[0-9-+s()]*$"<?php }else{ ?>type="<?=$field["type"];?>"<?php } ?> class="full-width" placeholder="<?=$field["placeholder"];?>"<?if ($field["required"]==1){echo "required";};?> value="" />
                                                <?if ($field["required"]==1){?>
                                                    <span class="req">required</span>
                                                    <span class="valid">Valid</span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <? }; ?>

                                <? }; ?>
                            </div>

                            <h4 class="text-blue m-t-50 m-b-30"><?php echo get_field($entity . '_interests_title', 'option'); ?></h4>

                            <div class="form-checkboxes prov-checkbox">

                                <ul class="clearfix">

                                    <?php foreach( get_field($entity . '_interests', 'option') as $key => $interest) : ?>
                                        <?php $interest_id = $interest['value_of_mailchimp_interest'] . '_' .rand();?>
                                        <li class="col-md-3">
                                            <input
                                                id="<?php echo $interest_id; ?>"
                                                type="checkbox"
                                                name="interest[]"
                                                value="<?php echo $interest['value_of_mailchimp_interest']; ?>"
                                                <?php echo ( $interest['checked'] ) ? 'checked' : '';  ?>
                                            />
                                            <label for="<?php echo $interest_id; ?>"><?php echo $interest['name']; ?></label>
                                        </li>
                                    <?php endforeach; ?>

                                </ul>

                            </div>

                            <div class="subskribe">
                                <?php $agreements = get_field($entity . '_agreements', 'option'); ?>
                                <?php foreach ( $agreements as $agreement ) :?>
                                    <?php $agreement_id = 'agree_' . rand(); ?>
                                    <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                                    <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                                <?php endforeach; ?>
                                <div class="clearfix"></div>
                            </div>

                            <input type="hidden" name="subject_to_submit" value="<?php echo get_field($entity . '_mail_subject', 'option'); ?>">

                            <input type="hidden" name="entity" value="<?php echo $entity; ?>">

                            <?php foreach( get_field($entity . '_email_for_sending', 'option') as $email ) : ?>
                                <input type="hidden" name="email_to_submit[]" value="<?php echo $email["email"]; ?>">
                            <?php endforeach; ?>

                            <button class="nbtn nbtn-inverse  m-t-0 pull-right width" type="submit"><?php echo get_field($entity . '_submit_button', 'option'); ?></button>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
