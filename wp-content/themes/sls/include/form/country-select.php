<style>

</style>

<?php

$countries = include( TEMPLATE_DIRECTORY . '/include/countries.php');

?>

<select class="full-width cnt-ss" id="country-selector" autocorrect="off" autocomplete="off" name="<?=$field["name"];?>" <?if ($field["required"]==1){echo "required";};?>>
	<option value=""><?=$field["placeholder"];?></option>
	<?php foreach( $countries as $country_code => $country_name ) : ?>
		<option value="<?php echo $country_name; ?>"><?php echo $country_name; ?></option>
	<?php endforeach; ?>
</select>