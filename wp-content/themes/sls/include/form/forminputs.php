<?if($field["type"]=="textarea"){?>
	<div class="col-lg-<?php echo $field['width']; ?>">
		<div class="my_form_input">
			<textarea name="<?=$field["name"];?>" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?>></textarea>
		</div>
	</div>
	<?}else if($field["type"]=="Geolocation-Country"){?>
		<div class="col-lg-<?php echo $field['width']; ?>">
			<div class="my_form_input">
				<div  class="js-hint">
					<div style="position:relative" class="sel-rel">
						<?php include("country-select.php");?>
						<?php /*if ($field["required"]==1){?>
							<span class="req">required</span>
							<span class="valid c-s-v">Valid</span>
						<?php } */?>
					</div>
				</div>
			</div>
		</div>
		<?}else if($field["type"]=="Geolocation-State"){;?>
			<div class="col-lg-<?php echo $field['width']; ?>">
				<div class="my_form_input">
					<input value="<?=$GeoInfo["region"]["name_en"]?>" name="State" type="text" class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?> />
					<?if ($field["required"]==1){?>
						<span class="req uppercase">required</span>
						<span class="valid">Valid</span>
					<?php } ?>
				</div>
			</div>
			<? }else if($field["type"]=="Geolocation-City"){; ?>
				<div class="col-lg-<?php echo $field['width']; ?>">
					<div class="my_form_input">
						<input value="<?=$GeoInfo["city"]["name_en"]?>" name="City" type="text" class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?> />
						<?if ($field["required"]==1){?>
							<span class="req uppercase">required</span>
							<span class="valid">Valid</span>
							<?php } ?>
					</div>
				</div>
				<? }else if($field["type"]=="select"){; ?>
					<?php $k = explode('|', $field["select_value"]);?>
				<div class="col-lg-<?php echo $field['width']; ?>">
					<select name="<?php echo $field["name"]; ?>">
						<?php foreach($k as $result){ ?>
							<option value="<?php echo $result; ?>">
								<?php echo $result; ?>
							</option>
							<?}?>
					</select>
				</div>
				<? } else if( $field['type']=="email" ) { ?>
					<div class="col-lg-<?php echo $field['width']; ?>">
						<div class="my_form_input">
							<input name="<?=strtolower($field["name"]);?>" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?> value="" />
<div class="cover_label">
                                    <label for="last_name"><?=$field["placeholder"];?></label>
                                </div>
						<?if ($field["required"]==1){?>
							<span class="req uppercase">required</span>
							<span class="valid">Valid</span>
							<?php } ?>
						</div>
					</div>
				<? } else if( $field['type']=="url" ) { ?>
					<div class="col-lg-<?php echo $field['width']; ?>">
						<div class="my_form_input">
							<input name="<?=$field["name"];?>" type="text" class="full-width" pattern="^((https?|ftp)://)?www\.([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?> value="" />
<div class="cover_label">
                                    <label for="last_name"><?=$field["placeholder"];?></label>
                                </div>
						<?if ($field["required"]==1){?>
							<span class="req uppercase">required</span>
							<span class="valid">Valid</span>
							<?php } ?>
						</div>
					</div>
				<? } else { ?>
					<div class="col-lg-<?php echo $field['width']; ?>">
						<div class="my_form_input">
							<input name="<?=$field["name"];?>" <?php if($field["type"]=='zipcode' ){ ?>type="text" maxlength="10"<?php }elseif($field["type"]=='tel'){ ?>type="tel" pattern="^[0-9-+s()]*$"<?php }else{ ?>type="<?=$field["type"];?>"<?php } ?> class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?> value="" />
				<div class="cover_label">
                                    <label for="last_name"><?=$field["placeholder"];?></label>
                                </div>
							<?if ($field["required"]==1){?>
								<span class="req uppercase">required</span>
								<span class="valid">Valid</span>
							<?php } ?>
						</div>
					</div>
				<? } ?>
