<?php

/*
 * Redux Configuration
 */

if( ! class_exists( 'Redux' ) ) {
    return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "sls_data";

$theme = wp_get_theme();

$args = array(
    'opt_name'          => $opt_name,            // This is where your data is stored in the database and also becomes your global variable name.
    'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
    'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
    'menu_type'         => 'submenu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu'    => false,                    // Show the sections below the admin menu item or not
    'menu_title'        => __('Theme Options', 'sls-theme'),
    'page_title'        => __('Theme Options', 'sls-theme'),
    'save_defaults'     => true,
    'async_typography'  => true,                    // Use a asynchronous font on the front end or font string
    'admin_bar'         => false,                    // Show the panel pages on the admin bar
    'global_variable'   => $opt_name,                      // Set a different name for your global variable other than the opt_name
    'dev_mode'          => false,                    // Show the time the page took to load, etc
    'customizer'        => false,                    // Enable basic customizer support
    'page_slug'         => 'sls-options',
    'system_info'       => false,
    'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
);

Redux::setArgs( $opt_name, $args );

/* Set Extensions /-------------------------------------------------- */
Redux::setExtensions( $opt_name, dirname( __FILE__ ) . '/extensions/' );

/*
function sls_theme_remove_ad() {
    return '';
}
add_filter('redux/' . $args['opt_name'] . '/aDBW_filter', 'sls_theme_remove_ad');
add_filter('redux/' . $args['opt_name'] . '/aNF_filter', 'sls_theme_remove_ad');
add_filter('redux/' . $args['opt_name'] . '/aNFM_filter', 'sls_theme_remove_ad');
add_filter('redux/' . $args['opt_name'] . '/aURL_filter', 'sls_theme_remove_ad');
*/

Redux::setSection( $opt_name, array(
    'title'     => __('General', 'sls-theme'),
    'icon'      => 'el-icon-home',
    'fields'    => array(
        // Favicons
        array(
            'id'        => 'general-favicons',
            'type'      => 'info',
            'desc'      => __('Favicons', 'sls-theme')
        ),
        array(
            'id'        => 'media-favicon',
            'type'      => 'media',
            'url'       => false,
            'title'     => __('Favicon Upload (16x16)', 'sls-theme'),
            'subtitle'  => __('Upload your Favicon', 'sls-theme')
        ),
        array(
            'id'       => 'media-favicon-iphone',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Apple iPhone Icon Upload (57x57)', 'sls-theme'),
            'subtitle' => __('Upload your Apple Touch Icon (57x57px png)', 'sls-theme'),
        ),
        array(
            'id'       => 'media-favicon-iphone-retina',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Apple iPhone Retina Icon Upload (120x120)', 'sls-theme'),
            'subtitle' => __('Upload your Apple Touch Retina Icon (120x120 png)', 'sls-theme'),
        ),
        array(
            'id'       => 'media-favicon-ipad',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Apple iPad Mini Icon Upload (76x76)', 'sls-theme'),
            'subtitle' => __('Upload your Apple iPad Mini Icon (76x76px png)', 'sls-theme'),
        ),
        array(
            'id'       => 'media-favicon-ipad-retina',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Apple iPad Retina Icon Upload (152x152)', 'sls-theme'),
            'subtitle' => __('Upload your Apple Touch Retina Icon (152x152 png)', 'sls-theme'),
        ),
        // Logo
        array(
            'id'        => 'general-logo',
            'type'      => 'info',
            'desc'      => __('Logo', 'sls-theme')
        ),
        array(
            'id'       => 'media-logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Logo Upload', 'sls-theme'),
        ),
        array(
            'id'       => 'media-logo-retina',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Retina Logo Upload', 'sls-theme'),
            'subtitle' => __('Upload your Retina Logo. This should be your Logo in double size (If your logo is 100 x 20px, it should be 200 x 40px)', 'sls-theme'),
        ),
        array(
            'id'       => 'media-logo-alt',
            'type'     => 'text',
            'title'    => __('Logo Alt', 'sls-theme'),
            'subtitle' => __('Enter Alt text for your logo', 'sls-theme'),
            'default'  => '',
        ),
    )
));

Redux::setSection( $opt_name, array(
    'title'     => __('API', 'sls-theme'),
    'icon'      => 'el-icon-fork',
    'fields'    => array(
            // API
        array(
            'id'        => 'general-api',
            'type'      => 'info',
            'desc'      => __('API', 'sls-theme')
        ),
        array(
            'id'       => 'iddna_centers_api_url',
            'type'     => 'text',
            'title'    => __('Iddna Centers', 'sls-theme'),
            'subtitle' => __('Enter URL to iDDna centers', 'sls-theme'),
            'default'  => '',
        ),
        array(
            'id'       => 'iddna_ajax_api_url',
            'type'     => 'text',
            'title'    => __('Iddna Ajax API', 'sls-theme'),
            'subtitle' => __('Enter URL to iDDna wp-admin/admin-ajax.php', 'sls-theme'),
            'default'  => '',
        ),
        array(
            'id'       => 'googleapis_api_key',
            'type'     => 'text',
            'title'    => __('Googleapis Api key', 'sls-theme'),
            'subtitle' => __('Enter your google api key', 'sls-theme'),
            'default'  => '',
        ),
    )
));

Redux::setSection( $opt_name, array(
    'title'     => __('Social Media', 'sls-theme'),
    'icon'      => 'el-icon-twitter',
    'fields'    => array(
        array(
            'id'        => 'social-media',
            'type'      => 'info',
            'desc'      => __('Social Media Icons', 'sls-theme')
        ),
        /*
        array(
            'id'       => 'accordion-social-links',
            'type'     => 'repeater',
            'url'      => false,
            'title'    => __('Social links', 'sls-theme'),
            'fields'     => array(
                'icon' => array(
                    'type'        => 'select',
                    'description' => esc_html__( 'Select a social network to automatically add its icon', 'Avada' ),
                    'default'     => 'none',
                    //'choices'     => Avada_Data::fusion_social_icons( true, false ),
                ),
                'url' => array(
                    'type'        => 'text',
                    'label'       => esc_html__( 'Link (URL)', 'Avada' ),
                    'description' => esc_html__( 'Insert your custom link here', 'Avada' ),
                    'default'     => '',
                ),
                'custom_title' => array(
                    'type'        => 'text',
                    'label'       => esc_html__( 'Custom Icon Title', 'Avada' ),
                    'description' => esc_html__( 'Insert your custom link here', 'Avada' ),
                    'default'     => '',
                ),
                'custom_source' => array(
                    'type'        => 'media',
                    'label'       => esc_html__( 'Link (URL) of the image you want to use as the icon', 'Avada' ),
                    'description' => esc_html__( 'Upload your custom icon', 'Avada' ),
                    'default'     => '',
                ),
            ),
        ),
        */
        array(
            'id'       => 'social_dribbble',
            'type'     => 'text',
            'title'    => __('Dribbble', 'sls-theme'),
            'subtitle' => __('Enter URL to your Dribbble Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_facebook',
            'type'     => 'text',
            'title'    => __('Facebook', 'sls-theme'),
            'subtitle' => __('Enter URL to your Facebook Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_foursquare',
            'type'     => 'text',
            'title'    => __('Foursquare', 'sls-theme'),
            'subtitle' => __('Enter URL to your Foursquare Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_flickr',
            'type'     => 'text',
            'title'    => __('Flickr', 'sls-theme'),
            'subtitle' => __('Enter URL to your Flickr Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_github',
            'type'     => 'text',
            'title'    => __('Github', 'sls-theme'),
            'subtitle' => __('Enter URL to your Github Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_googleplus',
            'type'     => 'text',
            'title'    => __('Google+', 'sls-theme'),
            'subtitle' => __('Enter URL to your Google+ Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_instagram',
            'type'     => 'text',
            'title'    => __('Instagram', 'sls-theme'),
            'subtitle' => __('Enter URL to your Instagram Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_linkedin',
            'type'     => 'text',
            'title'    => __('LinkedIn', 'sls-theme'),
            'subtitle' => __('Enter URL to your LinkedIn Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_pinterest',
            'type'     => 'text',
            'title'    => __('Pinterest', 'sls-theme'),
            'subtitle' => __('Enter URL to your Pinterest Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_renren',
            'type'     => 'text',
            'title'    => __('Renren', 'sls-theme'),
            'subtitle' => __('Enter URL to your Renren Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_rss',
            'type'     => 'text',
            'title'    => __('RSS', 'sls-theme'),
            'subtitle' => __('Enter URL to your RSS Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_skype',
            'type'     => 'text',
            'title'    => __('Skype', 'sls-theme'),
            'subtitle' => __('Enter your Skype Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_soundcloud',
            'type'     => 'text',
            'title'    => __('Soundcloud', 'sls-theme'),
            'subtitle' => __('Enter URL to your Soundcloud Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_stackoverflow',
            'type'     => 'text',
            'title'    => __('Stack Overflow', 'sls-theme'),
            'subtitle' => __('Enter URL to your Stack Overflow Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_twitter',
            'type'     => 'text',
            'title'    => __('Twitter', 'sls-theme'),
            'subtitle' => __('Enter URL to your Twitter Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_tumblr',
            'type'     => 'text',
            'title'    => __('Tumblr', 'sls-theme'),
            'subtitle' => __('Enter URL to your Tumblr Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_vimeo',
            'type'     => 'text',
            'title'    => __('Vimeo', 'sls-theme'),
            'subtitle' => __('Enter URL to your Vimeo Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_vk',
            'type'     => 'text',
            'title'    => __('VK', 'sls-theme'),
            'subtitle' => __('Enter URL to your VK Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_weibo',
            'type'     => 'text',
            'title'    => __('Weibo', 'sls-theme'),
            'subtitle' => __('Enter URL to your Weibo Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_xing',
            'type'     => 'text',
            'title'    => __('Xing', 'sls-theme'),
            'subtitle' => __('Enter URL to your Xing Account', 'sls-theme'),
            'default'  => '',
        ),

        array(
            'id'       => 'social_youtube',
            'type'     => 'text',
            'title'    => __('YouTube', 'sls-theme'),
            'subtitle' => __('Enter URL to your YouTube Account', 'sls-theme'),
            'default'  => '',
        ),
    )
));

Redux::setSection( $opt_name, array(
    'title'     => __('Footer', 'sls-theme'),
    'icon'      => 'el-icon-file-edit',
    'fields'    => array(
        array(
            'id'        => 'footer-footer',
            'type'      => 'info',
            'desc'      => __('Footer Widget', 'sls-theme')
        ),
        array(
            'id'        => 'switch-top-footer-widgets',
            'type'      => 'switch',
            'title'     => __('Top Footer Widget Area', 'sls-theme'),
            'subtitle'  => __('Turn on to display top footer widgets area', 'sls-theme'),
            'default'   => true
        ),
        array(
            'id'        => 'top-footer-widgets-columns',
            'type'      => 'slider',
            'title'     => __('Top Footer Widget Columns', 'sls-theme'),
            'subtitle' => __('Controls the number of columns in the footer.', 'sls-theme'),
            'default'   => 1,
            'min'       => 1,
            'step'      => 1,
            'max'       => 6,
            'display_value'     => 'text',
            'required'  => array('switch-top-footer-widgets', '=', true)
        ),
        array(
            'id'        => 'switch-bottom-footer-widgets',
            'type'      => 'switch',
            'title'     => __('Bottom Footer Widget Area', 'sls-theme'),
            'subtitle'  => __('Turn on to display bottom footer widgets area', 'sls-theme'),
            'default'   => true
        ),
        array(
            'id'        => 'bottom-footer-widgets-columns',
            'type'      => 'slider',
            'title'     => __('Bottom Footer Widget Columns', 'sls-theme'),
            'subtitle' => __('Controls the number of columns in the footer.', 'sls-theme'),
            'default'   => 1,
            'min'       => 1,
            'step'      => 1,
            'max'       => 6,
            'display_value'     => 'text',
            'required'  => array('switch-bottom-footer-widgets', '=', true)
        ),
        array(
            'id'        => 'footer-copyright',
            'type'      => 'info',
            'desc'      => __('Copyright', 'sls-theme')
        ),
        array(
            'id'        => 'switch-copyright-area',
            'type'      => 'switch',
            'title'     => __('Copyright Area', 'sls-theme'),
            'subtitle'  => __('Turn on to display copyright area', 'sls-theme'),
            'default'   => true
        ),
        array(
            'id'        => 'top-copyright-text',
            'type'      => 'textarea',
            'title'     => __('Top Copyright Text', 'sls-theme'),
            'default'   => '',
            'args'      => array(
                'wpautop'       => false,
                'media_buttons' => false,
                'textarea_rows' => 5,
                'teeny'         => false,
                'quicktags'     => false,
            ),
            'required' => array('switch-copyright-area', '=', true),
        ),
        array(
            'id'        => 'bottom-copyright-text',
            'type'      => 'textarea',
            'title'     => __('Bottom Copyright Text', 'sls-theme'),
            'default'   => '',
            'args'      => array(
                'wpautop'       => false,
                'media_buttons' => false,
                'textarea_rows' => 5,
                'teeny'         => false,
                'quicktags'     => false
            ),
            'required' => array('switch-copyright-area', '=', true),
        ),
            // Logo
        array(
            'id'        => 'footer-logo',
            'type'      => 'info',
            'desc'      => __('Footer Logo', 'sls-theme')
        ),
        array(
            'id'       => 'media-footer-logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Footer Logo Upload', 'sls-theme'),
        ),
        array(
            'id'       => 'media-footer-logo-alt',
            'type'     => 'text',
            'title'    => __('Footer Logo Alt', 'sls-theme'),
            'subtitle' => __('Enter Alt text for your footer logo', 'sls-theme'),
            'default'  => '',
        ),
    )
));