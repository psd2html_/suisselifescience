<?php

// Exit if accessed directly
	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

// Don't duplicate me!
	if ( ! class_exists( 'ReduxFramework_extension_accordion' ) ) {


		/**
		 * Main ReduxFramework_extension_accordion extension class
		 *
		 * @since       1.0.0
		 */
		class ReduxFramework_extension_accordion {

			public static $version = '1.0.1';

			// Protected vars
			protected $parent;
			public $extension_url;
			public $extension_dir;
			public static $theInstance;
			public static $ext_url;
			public $field_id = '';
			private $class_css = '';
			public $field_name = 'accordion';

			/**
			 * Class Constructor. Defines the args for the extions class
			 *
			 * @since       1.0.0
			 * @access      public
			 *
			 * @param       array $parent Parent settings.
			 *
			 * @return      void
			 */
			public function __construct( $parent ) {

				$avadaredux_ver = ReduxFramework::$_version;

				// Set parent object
				$this->parent = $parent;

				// Set extension dir
				if ( empty( $this->extension_dir ) ) {
					$this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
					$this->extension_url = trailingslashit( get_template_directory_uri() ) . 'admin/extensions/accordion/';
					// $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
					self::$ext_url       = $this->extension_url;
				}

				// Set instance
				self::$theInstance = $this;

				// Uncomment when customizer works - kp
				//include_once($this->extension_dir . 'multi-media/inc/class.customizer.php');
				//new AvadaReduxColorSchemeCustomizer($parent, $this->extension_dir);

				// Adds the local field
				add_filter( 'redux/' . $this->parent->args['opt_name'] . '/field/class/' . $this->field_name, array(
					&$this,
					'overload_field_path'
				) );
			}

			static public function getInstance() {
				return self::$theInstance;
			}

			static public function getExtURL() {
				return self::$ext_url;
			}

			// Forces the use of the embeded field path vs what the core typically would use
			public function overload_field_path( $field ) {
				return dirname( __FILE__ ) . '/' . $this->field_name . '/field_' . $this->field_name . '.php';
			}
		}
	}
