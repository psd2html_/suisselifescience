<?php

class SLS_Sidebars {

    public function __construct()
    {
        add_action( 'widgets_init', array( $this, 'widgets_init') );
    }

    public function widgets_init()
    {
        global $sls_data;

        $columns = (int) $sls_data['top-footer-widgets-columns'];

        for ( $i = 1; $i <= $columns; $i++ ) {
            register_sidebar( array(
                'name'          => sprintf( __('Top Footer Widget Area %s', 'sls-theme'), $i),
                'id'            => 'sls-top-footer-widget-area-' . $i,
                'before_widget' => '<div id="%1$s" class="sls-top-footer-widget-column widget %2$s">',
                'after_widget'  => '<div style="clear:both;"></div></div>',
                'before_title'  => '<h4 class="widget-title">',
                'after_title'   => '</h4>',
            ) );
        }

        $columns = (int) $sls_data['bottom-footer-widgets-columns'];
        for ( $i = 1; $i <= $columns; $i++ ) {
            register_sidebar( array(
                'name'          => sprintf( __('Bottom Footer Widget Area %s', 'sls-theme'), $i),
                'id'            => 'sls-bottom-footer-widget-area-' . $i,
                'before_widget' => '<div id="%1$s" class="sls-bottom-footer-widget-column widget %2$s">',
                'after_widget'  => '<div style="clear:both;"></div></div>',
                'before_title'  => '<h4 class="widget-title">',
                'after_title'   => '</h4>',
            ) );
        }
    }

}