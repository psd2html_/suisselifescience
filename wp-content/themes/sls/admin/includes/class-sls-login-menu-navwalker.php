<?php

class Walker_Login_Menu extends Walker {

    // Tell Walker where to inherit it's parent and id values
    var $db_fields = array(
        'parent' => 'menu_item_parent',
        'id'     => 'db_id'
    );

    /**
     * At the start of each element, output a <li> and <a> tag structure.
     *
     * Note: Menu objects include url and title properties, so we will use those.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        $output .= sprintf( "\n<a href='%s'%s>%s</a>\n",
            $item->url,
            ( $item->current ) ? ' class="login_btn inline active-login"' : ' class="login_btn inline"',
            $item->title
        );
    }

}