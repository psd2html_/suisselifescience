<?php

class Before_Top_Walker_Nav_Menu extends Walker_Nav_Menu {

    public function walk( $elements, $max_depth )
    {
        $list = array ();

        foreach ( $elements as $item )
        {
            $list[] = sprintf( "<li><a href='%s'%s>%s</a></li>",
                $item->url,
                ( $item->current ) ? ' class="current"' : '',
                $item->title
            );
        }

        return join( "", $list );
    }
}