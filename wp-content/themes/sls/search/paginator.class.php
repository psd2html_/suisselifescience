<?php
 
class CustomPaginator
{
 
    private $_conn;
    private $_search;
    private $_page;
    private $_perPage;
    private $_total;

    /**
     * __construct
     *
     * @param mixed $total
     */
    public function __construct( $page, $perPage, $total ) {
         
        $this->_page = $page;
        $this->_perPage = $perPage;
        $this->_total = $total;
        $this->_search = rawurlencode(get_search_query()); 
    }

    /**
     * createLinks
     *
     * @param mixed $links
     * @param mixed $list_class
     */
    public function createLinks( $links, $list_class ) {
     
        $last       = ceil( $this->_total / $this->_perPage);
     
        $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
        $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
     
        $html       = '<ul class="' . $list_class . '">';
     
        $class      = ( $this->_page == 1 ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="?s=' . $this->_search . '&page=' . ( $this->_page - 1 ) . '">&laquo;</a></li>';
     
        if ( $start > 1 ) {
            $html   .= '<li><a href="?s=' . $this->_search . '&page=1">1</a></li>';
            $html   .= '<li class="disabled"><span>...</span></li>';
        }
     
        for ( $i = $start ; $i <= $end; $i++ ) {
            $class  = ( $this->_page == $i ) ? "active" : "";
            $html   .= '<li class="' . $class . '"><a href="?s=' . $this->_search . '&page=' . $i . '">' . $i . '</a></li>';
        }
     
        if ( $end < $last ) {
            $html   .= '<li class="disabled"><span>...</span></li>';
            $html   .= '<li><a href="?s=' . $this->_search . '&page=' . $last . '">' . $last . '</a></li>';
        }
     
        $class      = ( $this->_page == $last ) ? "disabled" : "";
        $html       .= '<li class="' . $class . '"><a href="?s=' . $this->_search . '&page=' . ( $this->_page + 1 ) . '">&raquo;</a></li>';
     
        $html       .= '</ul>';
     
        return $html;
    }

}
