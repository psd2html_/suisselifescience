<?php /* Template Name: IDDNA */ ?>
<?php get_header();

$fields = get_fields( get_the_ID() );

?>

<?php // section 1 ?>
<section class="cover" style="margin-top: 149px; background: url(&quot;http://suisselifescience.com/wp-content/themes/sls/img/sls-iddna-banner.jpg&quot;) center center repeat rgba(0, 0, 0, 0);">
  <div class="container">
    <h2 class="slide_title">ANTI-AGING SCIENCE </h2>
    <div class="slide_text">
      <p class="bold m-b-25 f-23">You’ve tried skincare, anti-aging lotions, <br> diets, vitamins, training plans,</p>
      <p class=" bold h3 letter-spacing text-uppercase text-blue m-t-0">but never one like this one</p>
    </div>
  </div>
</section>

<?php // section 2 ?>
<section class="">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p class="m-b-0">Every day, new research reveals contradictory advice.</p>
        <p class="m-b-0">It’s complicated, confusing, overwhelming.</p>
        <p class="m-b-0">Potion, lotions, diets, powders, blogs …all claiming they know what is best for you.</p>
        <h2 class="letter-spacing text-uppercase m-t-20 m-b-20 font-light">
          Anti-aging science
        </h2>
        <p class="text-uppercase bold">YOUR DNA MAKES YOU ONE OF A KIND.</p>
        <p>
          We are the same, but we are all different.Decode the 0.1% that makes you unique and learn how your genes can help you look and feel better.
        </p>
      </div>
    </div>
  </div>
</section>

<?php // section 3 ?>
<section class="p-0">
  <div class="container">
    <div class="row ">
      <div class="sls-animation_block wow">
        <div class="animation_item step-1 flex-center">
          <div class="h_line delay2"></div>
          <div class="v_line right delay2"></div>
          <div class="col-sm-2">
            <img src="http://suisselifescience.com/wp-content/themes/sls/img/amerika-for-sls.png" alt="" class="img-70">
          </div>
          <div class="col-sm-9">
            <p class="light text-uppercase f-23 step">STEP 1</p>
            <h2 class="m-b-20 m-t-20 text-blue bold letter-spacing f-31 text-uppercase">DNA STUDY</h2>
            <p class="text-uppercase bold ">YOUR DNA IS SEQUENCED.</p>
            <p class="font-normal">Your de-identified sample is sent to Suisse Life Science’s lab in the US where we sequence your SNPs (only those scientifically-accepted to affect the aging concerns you want to target).</p>
          </div>
        </div>
        <div class="animation_item step-2 flex-center">
          <div class="h_line delay2"></div>
          <div class="v_line left delay2"></div>
          <div class="col-sm-2 col-sm-offset-1">
            <img src="http://suisselifescience.com/wp-content/themes/sls/img/plus-2-for-sls.png" alt="" class="img-70">
          </div>
          <div class="col-sm-9">
            <p class="light text-uppercase f-23 step">STEP 2</p>
            <h2 class="m-b-20 m-t-20 text-blue bold letter-spacing f-31 text-uppercase">MADE TO MEASURE TREATMENT DESIGN</h2>
            <p class="text-uppercase bold ">GENETICS + EPIGENETICS INTERPRETATION.</p>
            <p class="font-normal m-b-30">
              Your genetic raw data flies to Switzerland where it is run through our extensive knowledge base, <br>
              focusing on the unique interplay between your genetics, environment and lifestyle out of hundreds of billion potential variables.
            </p>
            <p class="text-uppercase bold ">YOUR TAILOR-MADE PROGRAM IS DESIGNED</p>
            <p class="font-normal">
              Our Science of [DNA] <span class="sls-iddna-func">f(x)</span><sup>™</sup> algorithm uses your genes to map the best ingredients in the world and delivers the latest research-based recommendations to reach your goals.
            </p>
          </div>
        </div>
        <div class="animation_item step-3 flex-center">
          <div class="h_line delay2"></div>
          <div class="col-sm-2">
            <img src="http://suisselifescience.com/wp-content/themes/sls/img/mac-for-sls.png" alt="" class="img-70">
          </div>
          <div class="col-sm-9">
            <p class="light text-uppercase f-23 step">STEP 3</p>
            <h2 class="m-b-20 m-t-20 text-blue bold letter-spacing f-31 text-uppercase">EXPERIENCE YOUR <short>i</short>DDNA<sup>&reg;</sup></h2>
            <p>Your made to measure (30- or 60-day) treatment is delivered to your door. Science-based personal recommendations to activate your genes’ potential are delivered to your concierge private area online and your mobile app. And you’ll receive lifelong custom updates. </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php // section 4 ?>
<section style="background: rgba(0, 0, 0, 0) url('http://devsls.7demo.org.ua/wp-content/themes/sls/img/about-you-for-sls.jpg') repeat scroll 0 0;" class="cover">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-6 col-xs-12 mobile-white">
        <h2 class="text-uppercase bold f-31 m-t-0 m-b-60">SIMPLE <br> NON-INVASIVE <br> LONG-LASTING NATURAL RESULTS</h2>
        <h2 class="text-uppercase m-b-60"><short>i</short>DDNA<sup>&reg;</sup> is about you</h2>
        <p>Finally, a smart anti-aging that knows you, understands you and follows the changes of your life.</p>
        <h4 class="text-uppercase bold f-16 m-t-0">A new discreet life companion.</h4>
      </div>
    </div>
  </div>
</section>
<?php // section 5 ?>
<section>
  <?php $entity = 'hairlos'; ?>
  <?php $form_section = get_fields('option'); ?>
  <style>
  </style>
  <div class="container">
    <h2 class="m-t-0 bold text-blue letter-spacing text-uppercase m-b-0">WANT TO LOOK AMAZING?</h2>
    <h2 class="m-t-0 text-blue letter-spacing text-uppercase">Be among the first to join our exclusive program:</h2>
    <h4 class="bold f-16 m-b-40">We’d like to hear from you:</h4>
    <div class="row">
      <div class="col-xs-12">
        <div class="contact-form">
          <form id="ajax_contact_send" method="POST" action="" class="clearfix my_amazing_form">

            <div class="row clearfix">
              <?php
              foreach($form_section['science_form_fields'] as $field){ ?>

                <?if($field["type"]=="textarea"){?>
                  <div class="col-lg-6">
                    <div class="my_form_input">
                      <textarea name="<?=$field["name"];?>" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?>></textarea>
                    </div>
                  </div>
                  <?}else if($field["type"]=="Geolocation-Country"){?>
                    <div class="col-lg-6">
                      <div class="my_form_input">
                        <div style="position:relative">
                          <?php $countries = include( TEMPLATE_DIRECTORY . '/include/countries.php'); ?>

                          <select class="full-width cnt-ss" id="country-selector" autocorrect="off" autocomplete="off" name="<?=$field["name"];?>" <?if ($field["required"]==1){echo "required";};?>>
                            <option value=""><?=$field["placeholder"];?></option>
                            <?php foreach( $countries as $country_code => $country_name ) : ?>
                            <option value="<?php echo $country_name; ?>"><?php echo $country_name; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <?}else if($field["type"]=="Geolocation-State"){;?>
                    <div class="col-lg-6">
                      <div class="my_form_input">
                        <input name="State" type="text" class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field["required"]==1){echo "required";};?> />
                        <?if ($field["required"]==1){?>
                          <span class="req">required</span>
                          <span class="valid">Valid</span>
                          <?php } ?>
                        </div>
                      </div>
                      <? }else if($field["type"]=="Geolocation-City"){; ?>
                        <div class="col-lg-6">
                          <div class="my_form_input">
                            <input name="City" type="text" class="full-width" placeholder="<?=$field["placeholder"];?>" <?if ($field[ "required"]==1){echo "required";};?> />
                            <?if ($field["required"]==1){?>
                              <span class="req">required</span>
                              <span class="valid">Valid</span>
                              <?php } ?>
                            </div>
                          </div>
                          <? }else if($field["type"]=="select"){; ?>
                            <?php $k = explode('|', $field["select_value"]);?>
                            <div class="col-lg-6">
                              <select name="<?php echo $field["name"]; ?>">
                                <?php foreach($k as $result){ ?>
                                  <option value="<?php echo $result; ?>">
                                    <?php echo $result; ?>
                                  </option>
                                  <?}?>
                                </select>
                              </div>
                              <? }else{ ?>
                                <div class="col-lg-6">
                                  <div class="my_form_input">
                                    <input name="<?=$field["name"];?>" <?php if($field["type"]=='zipcode' ){ ?>type="text" maxlength="10"<?php }elseif($field["type"]=='tel'){ ?>type="tel" pattern="^[0-9-+s()]*$"<?php }else{ ?>type="<?=$field["type"];?>"<?php } ?> class="full-width" placeholder="<?=$field["placeholder"];?>"<?if ($field["required"]==1){echo "required";};?> value="" />
                                    <?if ($field["required"]==1){?>
                                      <span class="req">required</span>
                                      <span class="valid">Valid</span>
                                      <?php } ?>
                                    </div>
                                  </div>
                                  <? }; ?>

                                  <? }; ?>
                                </div>

                                <h4 class="text-blue m-t-45"><?php echo $form_section["science_interests_title"]; ?></h4>

						<div class="form-checkboxes half-width-check">
							<?php include("include/sudscribe/subscribe_checkboxes.php");?>
						</div> 

                                </div>

                                <div class="subskribe">
                                  <?php $agreements = $form_section['science_agreements']; ?>
                                  <?php foreach ( $agreements as $agreement ) :?>
                                    <?php $agreement_id = 'agree_' . rand(); ?>
                                    <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                                    <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                                  <?php endforeach; ?>
                                </div>

                                <input type="hidden" name="subject_to_submit" value="<?php echo $form_section["science_mail_subject"]; ?>">


                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                                <?php foreach( $form_section['science_email_for_sending'] as $email ) : ?>
                                  <input type="hidden" name="email_to_submit[]" value="<?php echo $email["email"]; ?>">
                                <?php endforeach; ?>

                                <button class="nbtn nbtn-inverse  m-t-10 pull-right" type="submit"><?php echo $form_section["science_submit_button"]; ?></button>

                              </form>

                            </div>
                          </div>
                        </div>
                      </section>
                      <?php // section 6 ?>
                      <section class="find_center " style="background-image: url('http://devsls.7demo.org.ua/wp-content/uploads/2016/05/bg_texture_HD_05.jpg');">
                        <div class="container">
                          <div class="text-center">
                            <h2 class="font-ligth  m-t-0"><b>FIND</b> A CENTER</h2>
                            <h5 class="text-white">Only selected anti-aging clinics and centers can provide your exclusive iDDNA<sup>®</sup>. Find one near you.</h5>

                            <form class="home-find-centers" method="POST">
                              <div class="m-t-20 p-b-0">
                                <div class="input-group section_form">
                                  <input required="" type="text" class="form-control" placeholder="Enter country, city or ZIP">
                                  <span class="input-group-btn">
                                    <button type="submit" class="btn  btn-default home-find-centers-btn" style="height: 45px; padding: 13px;">FIND A CENTER<i class="ico arrow"></i></button>
                                  </span>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </section>
                      <section>
                        <div class="container">
                          <div class="col-sm-12">
                            <h4 class="bold uppercase">You’re in <b>good company</b></h4>
                          </div>
                          <div class="col-sm-12 col-lg-6">
                            <div class="blog_pre">
                              <div class="row">
                                <div class="col-lg-6"><h4 class="text-left p-b-15 font-ligth  m-t-10 ">Listen to your DNA:</h4></div>
                                <div class="col-lg-6 full-inner">
                                  <a class="nbtn nbtn-inverse pull-right m-b-10 " href="/contact-us/">our blog<i class="ico arrow"></i></a>
                                </div>
                              </div>


                            </div>

                            <div class="">
                              <div class="blog clearfix">
                                <div class="col-sm-6 no-padding">
                                  <img class="full-width" src="http://devsls.7demo.org.ua/wp-content/themes/sls/img/blog-1.jpg">
                                </div>
                                <div class="col-sm-6 blog-info">
                                  <h5 class="blog_title bold ">iDDNA<sup>®</sup> launch in <br>Hong Kong</h5>
                                  <div class="post-info">
                                    <span>03/06/2016   |  Categories: Luxury anti-aging, Skin care, Luxury lifestyle</span>
                                  </div>
                                  <p class="blog_text">After the big success in The Netherlands, we are glad to announce that the new, exclusive iDDNA® membership will be soon available in Hong Kong and China. We are currently planning a very exclusive Hong Kong launch for September, and then roll out in other selected 50 anti-aging clinics in South Asia.</p>
                                  <!-- <a class="nbtn pull-left" href="">LEARN MORE<i class="ico arrow"></i></a>-->
                                </div>
                              </div>
                            </div>

                    <!-- poku ne potribno
                    <div class="col-sm-6 ">
                        <div class="blog">
                            <div class="">
                                <img class="full-width" src="http://devsls.7demo.org.ua/wp-content/themes/sls/img/blog-2.jpg" />
                            </div>
                            <div class="blog-info">
                                <h4 class="blog_title bold">Title About Some Really</h4>
                                <div class="post-info">
                                    <span>By <a href="">Admin</a>  |  October 29th, 2012   |  Categories: Inspiration</span>
                                </div>
                                <p class="blog_text">At vero eos et accusamus et iusto odios unsip dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un deleniti atquestepi sitesdeleniti atquestepi sites excep non providentsim ...</p>
                                <a href="" class="nbtn">Read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="blog">
                            <div class="">
                                <img class="full-width" src="http://devsls.7demo.org.ua/wp-content/themes/sls/img/blog-3.jpg" />
                            </div>
                            <div class="blog-info">
                                <h4 class="blog_title bold">Title About Some Really</h4>
                                <div class="post-info">
                                    <span>By <a href="">Admin</a>  |  October 29th, 2012   |  Categories: Inspiration</span>
                                </div>
                                <p class="blog_text">At vero eos et accusamus et iusto odios unsip dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un deleniti atquestepi sitesdeleniti atquestepi sites excep non providentsim ...</p>
                                <a href="" class="nbtn">Read more</a>
                            </div>
                        </div>
                    </div>
                  -->
                </div>
                <div class="col-sm-12 col-lg-6">
                  <h4 class="text-lenter p-b-15 font-ligth">What people are saying:</h4>
                  <div class="social_posts">
                    <?php $posts = seven_get_social_media_feed(); ?>

                    <?foreach($posts as $post){?>
                      <div class="social_post row p-b-20">
                        <div class="col-sm-3">
                          <?php if (!empty($post->image)): ?>
                          <img class="full-width" src="<?=$post->image?>" />
                        <?php endif; ?>
                      </div>
                      <div class="col-sm-9">
                        <h5 class="m-t-0 m-b-0 bold blog_title "><?=wp_trim_words($post->text, 5, "...")?></h5>
                        <div class="post-info elipsis">
                        </div>
                        <p class="blog_text"><?=wp_trim_words($post->text, 32, "...")?></p>
                        <div class="row m-t-10 ">
                          <div class="inline">
                            <a target="_blank" href="<?php echo $post->link; ?>" class="share_btn  nbtn">Like<i class="ico arrow"></i></a>
                          </div>
                          <div class="inline">
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post->link; ?>" class="share_btn  nbtn">Share<i class="ico arrow"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?};?>
                  </div>
                </div>
              </div>
            </section>
            <?php get_footer(); ?>
