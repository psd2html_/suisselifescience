<?php /* Template Name: Home */ ?>

<?php get_header();

    $fields = get_fields( get_the_ID() );

$barcodes = [
    get_field("health_barcode"),
    get_field("skin_barcode"),
];
$unique_barcodes = array_unique( $barcodes );
$products = get_products_from_store( $unique_barcodes );
$product_id_barcode_map = [];

if ( ! empty ($products) ) {
    foreach ($products as $product) {
        if ( in_array($product['barcode'], $barcodes ) ) {
            $product_id_barcode_map[ $product['barcode'] ] = $product['id'];
        }
    }
}
function get_product_id( $barcode ) {
    global $product_id_barcode_map;

    if ( isset($product_id_barcode_map[ $barcode ]) ) {
        return $product_id_barcode_map[ $barcode ];
    }
}


$skin_product = get_product_id(get_field('skin_barcode'));
$health_product = get_product_id(get_field("health_barcode"));
?>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

<?php $form_section = get_fields('option'); ?>
    <section class="paralax_block mobile_baner" <?php if (!empty($fields["section_1_field_background"])) : ?>
             style="background-image: url('<?php echo $fields["section_1_field_background"]; ?>');
             <?php endif; ?>
                 ">
        <div class="">
            <div class="container ">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="m-b-0"><?php echo $fields["section_1_field_1"]; ?></h1>
                                <div class="paralax_info_block mobile_p0">
                                    <!--<h2>A unique <b>anti-aging lifestyle</b> program<br>Exclusively designed from <b>your DNA</b></h2>-->
                                    <h2 class="m-b-0"><?php echo $fields["section_1_field_2"]; ?></h2>
                                    <h4 class="text-blue   m-t-0"><?php echo $fields["section_1_field_2_1"]; ?></h4>

                                    <p class="font-normal m-t-30 otst f-20 text-left"><?php echo $fields["section_1_field_2_2"]; ?></p>
                                    <h4 class="text-blue" ><?php echo $fields["section_1_field_2_3"]; ?></h4>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>

            </div>
            <div class="container watcher_header">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="row">
                            <div class="col-lg-6">							
                                <a href="#" target="_blank" class="nbtn  pull-left new-btn home-modal">
                                    <?php echo $fields["section_1_field_4"]; ?><i class="ico arrow"></i></a>
                            </div>
                            <div class="col-lg-6">
                                <?php if ( is_store_available() ) :?>
                                    <a href="<?php echo $fields["section_1_field_3_shop"]['url']; ?>" class="nbtn nbtn-inverse pull-left">
                                    <?php echo $fields["section_1_field_3_shop"]['title']; ?><i class="ico arrow"></i></a>
                                <?php else : ?>
                                    <a href="<?php echo $fields["section_1_field_3"]['url']; ?>" class="nbtn nbtn-inverse pull-left">
                                        <?php echo $fields["section_1_field_3"]['title']; ?><i class="ico arrow"></i></a>
                                <?php endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



        <section class="covered_section cover experience " <?php if (!empty($fields["section_2_field_background"])) : ?>
                 style="background-image: url('<?php echo $fields["section_2_field_background"]; ?>');
                 <?php endif; ?>
                     ">
            <div class="container">
                <div class="text-center">
                    <!--<h5 class="text-white m-t-0 m-b-20 text-left font-thin uppercase"><?php echo $fields["home_experience_field_1"]; ?></h5>-->
                    <h4 class="text-white m-t-0 m-b-0  text-left text-center"><?php echo $fields["home_experience_field_2"]; ?></h4>


                    <h5 class="text-white m-t-10 m-b-0 "><?php echo $fields["home_experience_field_3"]; ?></h5>

                    <h2 class="text-white m-t-45 m-b-20 font-ligth"><?php echo $fields["home_experience_field_4"]; ?></h2>
                    <div class="m-t-20 m-b-20 clearfix">
                        <div class="input-group section_form">
						
						
                            <?php $entity = 'join_now'; ?>					
						
                            <form method="GET" action="http://stagingidna.7demo.org.ua/anti-aging-from-DNA/luxury-anti-aging-get-started/survey" >
                                <input type="hidden" name="product_id" value="39398">
                               <input type="hidden" name="entity" value="join_now">
				<input required type="email" class="form-control" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="<?php echo $form_section[$entity.'_enter_your_email']; ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><?php echo $form_section[$entity.'_submit_buttom']; ?> 
                                        <i class="ico arrow arrow_white"></i>
                                    </button>
                                </span>					
                            </form>	
		  
                        </div>
                    </div>
                    <h5 class="text-white f-42 m-t-20 m-b-0 uppercase font-ligth"><?php echo $fields["home_experience_field_7"]; ?></h5>

                    <h5 class="text-white m-t-0 m-b-0"><?php echo $fields["home_experience_field_8"]; ?></h5>
                </div>
            </div>
        </section>

        <section class="">
        <div class="container">
            <div class="text-center">
                
                <p class="text-light m-t-0 m-b-20"><?php echo $fields["home_about_iddna_field_2"]; ?></p>
                <p class="text-light m-b-20"><?php echo $fields["home_about_iddna_field_3"]; ?></p>
                <h4 class="m-b-0 m-t-40 text-left bold"><?php echo $fields["home_about_iddna_field_1"]; ?></h4>
                <p class="text-justify"><?php echo $fields["home_about_iddna_field_4"]; ?></p>

                <h3 class="text-blue bold"><?php echo $fields["home_about_iddna_field_5"]; ?></h3>
            </div> 
            <div class="pocket_block">
                <div class="col-sm-4 m-b-40 col-xs-offset-1 col-xs-10 col-sm-offset-0">
                    <div class="pocket">
                        <div class="pocket_info">
                            <a href="<?php echo $fields["home_about_iddna_field_9"]['url']; ?>">
								<img src="<?php echo $fields["home_about_iddna_field_6_background"]; ?>" alt="<?php echo $fields["home_about_iddna_field_6_background_alt"]; ?>" />
								<div class="pocket_name">
									<h2><?php echo $fields["home_about_iddna_field_6"]; ?></h2>
								</div>
							</a>
                        </div>
                        <div class="pocket_text text-left col-sm-12">
                            <h4 class="bold uppercase"><?php echo $fields["home_about_iddna_field_7"]; ?></h4>
                            <p><?php echo $fields["home_about_iddna_field_8"]; ?></p>
                        </div>
                        <div class="pocket_footer row clearfix">
                            <div class="col-md-12 full-inner col-lg-6">
                            <a href="<?php echo $fields["home_about_iddna_field_9"]['url']; ?>" class="nbtn pull-left"><?php echo $fields["home_about_iddna_field_9"]['title']; ?><i class="ico arrow"></i></a>
                            </div>
                            <div class="col-md-12 full-inner col-lg-6">
                            <?php
                            if (!empty($fields['skin_barcode'])):
                                $url = $fields['survey_url'] . $skin_product;
                            ?>
                                <a href="<?php echo $url; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_10_shop"]['title']; ?><i class="ico arrow"></i></a>
                            <?php else: ?>
                                <?php if ( is_store_available() ) : ?>
                                    <a href="<?php echo $fields["home_about_iddna_field_10_shop"]['url']; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_10_shop"]['title']; ?><i class="ico arrow"></i></a>
                                <?php else: ?>
                                    <a href="<?php echo $fields["home_about_iddna_field_10"]['url']; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_10"]['title']; ?><i class="ico arrow"></i></a>
                                <?php endif; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
	
				
                <div class="col-sm-4 m-b-40 col-xs-offset-1 col-xs-10 col-sm-offset-0">
                    <div class="pocket">
                        <div class="pocket_info">
							<a href="<?php echo $fields["home_about_iddna_field_14"]['url']; ?>">
								<img src="<?php echo $fields["home_about_iddna_field_11_background"]; ?>" alt="<?php echo $fields["home_about_iddna_field_11_background_alt"]; ?>"/>
								<div class="pocket_name">
									<h2><?php echo $fields["home_about_iddna_field_11"]; ?></h2>
								</div>
							</a>
                        </div>
                        <div class="pocket_text text-left col-sm-12">
                            <h4 class="bold uppercase"><?php echo $fields["home_about_iddna_field_12"]; ?></h4>
                            <p><?php echo $fields["home_about_iddna_field_13"]; ?></p>
                        </div>
                        <div class="pocket_footer row clearfix">
                            <div class="col-md-12 full-inner col-lg-6">
                            <a href="<?php echo $fields["home_about_iddna_field_14"]['url']; ?>" class="nbtn pull-left"><?php echo $fields["home_about_iddna_field_14"]['title']; ?><i class="ico arrow"></i></a>
                            </div>
                            <div class="col-md-12 full-inner col-lg-6">
                            <?php
                            if (!empty($fields['health_barcode'])):
                                $url = $fields['survey_url'] . $health_product;
                                ?>
                                <a href="<?php echo $url; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_10_shop"]['title']; ?><i class="ico arrow"></i></a>
                            <?php else: ?>
                                <?php if ( is_store_available() ) : ?>
                                    <a href="<?php echo $fields["home_about_iddna_field_15_shop"]['url']; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_15_shop"]['title']; ?><i class="ico arrow"></i></a>
                                <?php else: ?>
                                    <a href="<?php echo $fields["home_about_iddna_field_15"]['url']; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_15"]['title']; ?><i class="ico arrow"></i></a>
                                <?php endif; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 m-b-40 col-xs-offset-1 col-xs-10 col-sm-offset-0">
                    <div class="pocket">
                        <div class="pocket_info">
							<a href="<?php echo $fields["home_about_iddna_field_16_1"]['url']; ?>">
								<img src="<?php echo $fields["home_about_iddna_field_16_background"]; ?>" alt="<?php echo $fields["home_about_iddna_field_16_background_alt"]; ?>"/>
								<div class="pocket_name">
									<h2><?php echo $fields["home_about_iddna_field_16"]; ?></h2>
								</div>
							</a>	
                        </div>
                        <div class="pocket_text text-left col-sm-12">
                            <h4 class="bold uppercase"><?php echo $fields["home_about_iddna_field_17"]; ?></h4>
                            <p><?php echo $fields["home_about_iddna_field_18"]; ?></p>
                        </div>
                        <div class="pocket_footer row  clearfix">
                            <div class="col-md-12 full-inner col-lg-6">
                            <a href="<?php echo $fields["home_about_iddna_field_16_1"]['url']; ?>" class="nbtn pull-left"><?php echo $fields["home_about_iddna_field_16_1"]['title']; ?><i class="ico arrow"></i></a>
                                </div>
                            <div class="col-md-12 full-inner col-lg-6">
                            <?php if ( is_store_available() ) :?>
                                <a href="<?php echo $fields["home_about_iddna_field_16_2_shop"]['url']; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_16_2_shop"]['title']; ?><i class="ico arrow"></i></a>
                            <?php else: ?>
                                <a href="<?php echo $fields["home_about_iddna_field_16_2"]['url']; ?>" class="nbtn nbtn-inverse pull-right"><?php echo $fields["home_about_iddna_field_16_2"]['title']; ?><i class="ico arrow"></i></a>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="find_center " <?php if (!empty($fields["section_4_field_background"])) : ?>
             style="background-image: url('<?php echo $fields["section_4_field_background"]; ?>');
             <?php endif; ?>
                 ">
        <div class="container">
            <div class="text-center">
                <h2 class="font-ligth  m-t-0"><?php echo $fields["home_find_center_field_1"]; ?></h2>
                <h5 class="text-white"><?php echo $fields["home_find_center_field_2"]; ?></h5>

                <form class="home-find-centers" method="POST">
                    <div class="m-t-20 p-b-0">
                        <div class="input-group section_form">
                            <input required type="text" class="form-control" placeholder="<?php echo $fields["home_find_center_field_3"]; ?>">
                            <span class="input-group-btn">
                                <button type="submit" class="btn  btn-default home-find-centers-btn" style="height: 45px; padding: 13px;"><?php echo $fields["home_find_center_field_4"]; ?><i class="ico arrow"></i></button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div> 
    </section>

        <!--
        <section>
            <div class="container">
                <div class="text-center">
                    <h2>Want to look amaizing? </h2>
                    <h3>Fill out the form to ask for specialist invitation</h3>

                    <div class="p-t-20 p-b-20">
                        <form class="form-inline">
                            <div class="form-group">
                                <input type="" class="form-control" id="inputPassword2" placeholder="Enter your name">
                            </div>
                            <div class="form-group">
                                <input type="" class="form-control" id="inputPassword2" placeholder="Enter your name">
                            </div>
                            <button type="submit" class="btn btn-default">Continue</button>

                        </form>
                        <h3>By submitting, you agree to Suisse Life Science's Terms & Conditions and Privacy Policy</h3>
                    </div>
                </div>
            </div>
        </section>
-->
    <section class="covered_section cover amazing baner_h630 mobile_baner" <?php if (!empty($fields["section_5_field_background"])) : ?>
             style="background-image: url('<?php echo $fields["section_5_field_background"]; ?>');
             <?php endif; ?>
                 ">
        <div class="container m-t-100 m-b-100 mobile_m0">
            <div class="col-sm-6 col-sm-offset-6">
            <div class="row">
                <?php $entity = 'amazing'; ?>	
                
                <h4 class="uppercase"><?php echo get_field($entity . '_title', 'option'); ?></h4>
                <h5><?php echo get_field($entity . '_subtitle', 'option'); ?></h5>
                <form id="ajax_mainform_send" method="POST" action="" class="amazing_form blue_inputs my_amazing_form">

                    <div class="row clearfix">
                            <?php foreach($form_section[$entity . '_form_fields'] as $field){ ?>
                                    <?php include("include/form/forminputs.php");?>
                            <?};?>
                    </div>					

                    <div class="form-checkboxes prov-checkbox">
                            <?php include("include/sudscribe/subscribe_checkboxes.php");?>
                    </div>

                    <div class="clearfix">
                            <?php $agreements = $form_section[$entity . '_agreements_form']; ?>
                            <?php foreach ( $agreements as $agreement ) :?>
                                    <?php $agreement_id = 'agree_' . rand(); ?>
                                    <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                                    <label for="<?php echo $agreement_id; ?>" class="small_check one_line" style="float: none;"><?php echo $agreement["name"]; ?></label>
                            <?php endforeach; ?>
                    </div>

                    <input type="hidden" name="subject_to_submit" value="<?=$form_section[$entity . '_mail_subject']; ?>">

                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">

                    <?php foreach($form_section[$entity . '_email_for_sending'] as $email):?>
                            <input type="hidden" name="email_to_submit[]" value="<?=$email["email"]; ?>">
                    <?php endforeach; ?>
                    <div class="clearfix">
                            <button class="nbtn nbtn-inverse m-t-20 m-b-20" type="submit"><?=$form_section[$entity . '_submit_buttom']; ?><i class="ico arrow arrow_white"></i></button>
                    </div>

            </form>
            </div>
        </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="bold uppercase"><?php echo $fields["home_good_company_field_1"]; ?></h4>
                </div>
                <div class="col-sm-12 col-lg-6">
                    <?php if (function_exists(get_idna_posts)) : ?>
                        <?php $blog_posts = get_idna_posts(); ?>
                    <?php endif; ?>
                    <?php if ($blog_posts) : ?>
                    <?php $blog_page = array_shift($blog_posts); ?>
                        <?php $date = new DateTime($blog_page->date); ?>
                        <div class="blog_pre">
                            <div class="row">
                                <div class="col-lg-6"><h4 class="text-left p-b-15 font-ligth  m-t-10 "><?php echo $fields["home_good_company_field_2"]; ?></h4></div>
                                <div class="col-lg-6 full-inner">
                                    <a class="nbtn nbtn-inverse pull-right m-b-10 " href="<?php echo $blog_page->guid->rendered; ?>"><?php echo $fields["home_good_company_field_3"]; ?><i class="ico arrow"></i></a>
                                </div>
                            </div>
                            

                        </div>

                        <div class="">
                            <div class="blog clearfix">
                                <div class="col-sm-6 no-padding">
                                    <?php $blog_thumbnail = get_idna_media($blog_page->featured_media); ?>
                                    <img class="full-width" src="<?php echo $blog_thumbnail->media_details->sizes->medium_large->source_url; ?>" />
                                </div>
                                <div class="col-sm-6 blog-info">
                                    <h5 class="blog_title bold "><?php echo $blog_page->title->rendered; ?></h5>
                                    <div class="post-info">
                                    <span><?php echo $date->format('d/m/Y'); ?>   |  Categories:<br>
                                        <?php $blog_categories = get_idna_categories($blog_page->id); ?>
                                        <?php foreach ($blog_categories as $blog_category) : ?>
                                            <?php $category_name[] = $blog_category->name; ?>
                                        <?php endforeach; ?>
                                        <?php echo esc_html(implode(', ', $category_name)); ?>
                                    </span>
                                    </div>
                                            <?php $blog_excerpt = !empty($blog_page->excerpt->rendered) ? $blog_page->excerpt->rendered : wp_trim_words($blog_page->content->rendered, 20); ?>
                                    <p class="blog_text"><?php echo strip_tags($blog_excerpt); ?></p>
                                    <!-- <a class="nbtn pull-left" href="">LEARN MORE<i class="ico arrow"></i></a>-->
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- poku ne potribno
                    <div class="col-sm-6 ">
                        <div class="blog">
                            <div class="">
                                <img class="full-width" src="<?php echo bloginfo('template_url'); ?>/img/blog-2.jpg" />
                            </div>
                            <div class="blog-info">
                                <h4 class="blog_title bold">Title About Some Really</h4>
                                <div class="post-info">
                                    <span>By <a href="">Admin</a>  |  October 29th, 2012   |  Categories: Inspiration</span>
                                </div>
                                <p class="blog_text">At vero eos et accusamus et iusto odios unsip dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un deleniti atquestepi sitesdeleniti atquestepi sites excep non providentsim ...</p>
                                <a href="" class="nbtn">Read more</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="blog">
                            <div class="">
                                <img class="full-width" src="<?php echo bloginfo('template_url'); ?>/img/blog-3.jpg" />
                            </div>
                            <div class="blog-info">
                                <h4 class="blog_title bold">Title About Some Really</h4>
                                <div class="post-info">
                                    <span>By <a href="">Admin</a>  |  October 29th, 2012   |  Categories: Inspiration</span>
                                </div>
                                <p class="blog_text">At vero eos et accusamus et iusto odios unsip dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un dignissimos ducimus qui blan ditiis prasixers esentium voluptatum un deleniti atquestepi sitesdeleniti atquestepi sites excep non providentsim ...</p>
                                <a href="" class="nbtn">Read more</a>
                            </div>
                        </div>
                    </div>
                -->
            </div>
            <div class="col-sm-12 col-lg-6">
                <h4 class="text-lenter p-b-15 font-ligth"><?php echo $fields["home_good_company_field_5"]; ?></h4>
                <div class="social_posts">
                    <?php $posts = seven_get_social_media_feed(); ?>

                    <?foreach($posts as $post){?>
                        <div class="social_post row p-b-20">
                            <div class="col-sm-3">
                                <?php if (!empty($post->image)): ?>
                                <img class="full-width" src="<?=$post->image?>" />
                            <?php endif; ?>
                        </div>
                        <div class="col-sm-9">
                            <h5 class="m-t-0 m-b-0 bold blog_title "><?=wp_trim_words($post->text, 5, "...")?></h5>
                            <div class="post-info elipsis">
                            </div>
                            <p class="blog_text"><?=wp_trim_words($post->text, 32, "...")?></p>
                            <div class="row m-t-10 ">
                                <div class="inline">
                                    <a target="_blank" href="<?php echo $post->link; ?>" class="share_btn  nbtn">Like<i class="ico arrow"></i></a>
                                </div>
                                <div class="inline">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $post->link; ?>" class="share_btn  nbtn">Share<i class="ico arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?};?>
                </div>
            </div>
        </div>
    </div>
</section>




    <!-- Modal HTML -->
	<div style="display: none;">
		<div class="box-modal homevideo">

			<div class="contentn-modal-home">
				<i class="fa fa-times-circle arcticmodal-close" aria-hidden="true"></i>
				<div class="js-cont">
                    <embed id="playerid" width="800px" height="450px" allowfullscreen="true" allowscriptaccess="always" quality="high" bgcolor="#000000" name="playerid" style="" src="http://www.youtube.com/v/<?php echo $fields["section_1_field_5"]; ?>?enablejsapi=1&version=3&playerapiid=ytplayer&autoplay=1" type="application/x-shockwave-flash"></embed>
					
					</div>
			</div>
		</div>
	</div>
	
        <?php get_footer(); ?>
