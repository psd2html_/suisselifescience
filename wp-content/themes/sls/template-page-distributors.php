<?php /* Template Name: Distributors */ ?>
<?php

get_header();

$fields = get_fields( get_the_ID() );

global $sls_data;

//wp_enqueue_script(
//    'google-map',
//	"https://maps.googleapis.com/maps/api/js?key=" . $sls_data['googleapis_api_key']
//);

wp_enqueue_script(
    'google-map',
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyA55HS4C36lVMwdXSHgeY-BiVwcsfvsa20"
);

wp_enqueue_script('underscore');
//wp_enqueue_script('customwindow', get_template_directory_uri().'/js/customwindow.js', array('main', 'google-map', 'jquery'),'',true);

//$GeoInfo = $WPGeoplugin->getGeoInfo();


 
global $wpdb;
$db_table_name = SECURE_CENTERS_TABLE;

    $lat_lon = Seven_Geolocation::geolocate_coordinates( '', true, true  );
    $query_select_order_by = "";
    $query_select_columns = "select * from `{$db_table_name}` ";
    if(!empty($lat_lon)){
        if(array_key_exists("latitude", $lat_lon) && array_key_exists("longitude", $lat_lon))
        {
             $latitude   = $lat_lon['latitude'];
             $longitude  = $lat_lon['longitude'];         
             $earthRadius = '99999999999.0'; //in miles.
             $query_select_columns = "select *,ROUND(
                {$earthRadius} * ACOS(
                    SIN({$latitude} * PI() / 180) * SIN(latitude * PI() / 180) + COS({$latitude} * PI() / 180)
                    * COS(latitude * PI() / 180) * COS((longitude * PI() / 180) - ({$longitude} * PI() / 180))
                  ),
                  1
              ) AS DISTANCE from `{$db_table_name}` "; 
             $query_select_order_by = " ORDER BY DISTANCE ";  
        } 
    }


   $args = $query_select_columns." where user_role IN ('distributor', 'master-distributor')".$query_select_order_by;    
              
//print_r($args);
$mypostsSearch = $wpdb->get_results($args); 
$jsOffice = array();


foreach ( $mypostsSearch as $center ) {
    include('include/centers/centers-marker-html.php');
    $jsOffice[] = array(
        'id' => $center->id,
        'location' => array("lat"=>$center->latitude,"lng"=>$center->longitude),
        'company_name' => (strtolower($center->user_role)=="partner" ?$center->brand_name: $center->company_name),
        'company_phone' => $center->company_phone,
        'city' => $center->town_city,
        'zip' => $center->zip,
        'state' => $center->state,
        'address' => $center->street_address,
        'country' => $center->country,
        'HTML'=>$markerHTML
    );
}


?>
<script>
	var global_distributors = <?php echo json_encode($jsOffice); ?>;
</script>
<script type="text/template" id="distributors_template">
</script>

<!--
<section class="full-width-slider">
	<ul id="distributors-slider">
		<?php foreach($fields['slider'] as $slide): ?>
		<li>
			<img src="<?php echo $slide['url']; ?>" />
		</li>
		<?php endforeach; ?>
	</ul>
</section>
-->

<section class="distri_map">
	<div class="top_h1">
		<h3 class="text-white font-ligth m-b-20"><?php echo $fields["distributors_section_1_field_1"]; ?></h3>
	</div>
	<div id="map-block-distributors" class="map-block-distributors" data-new="new"></div>
	<div class="dist-btn-zoom-controls">
		<button id="btn-zoom-controls-zoom-in" class="btn-zoom-controls-zoom-in"><i class="fa fa-plus"></i></button>
		<button id="btn-zoom-controls-zoom-out" class="btn-zoom-controls-zoom-out"><i class="fa fa-minus"></i></button>
	</div>
        <?php if(count($mypostsSearch) > 0) :?>
        <a  href="javascript:void(0)" style="display: none;" id="center-block-show-map" class="show-on-map-center active" data-center_id="<?php echo $mypostsSearch[0]->id; ?>"></a>
        <?php endif; ?>
</section>
<section>
	<div class="container text-center">
		<h2 class="m-t-0 text-blue"><?php echo $fields["distributors_section_2_field_1"]; ?></h2>
		<p class="m-b-20"><?php echo $fields["distributors_section_2_field_2"]; ?></p>
		<p class="m-b-20"><?php echo $fields["distributors_section_2_field_3"]; ?></p>
		<p class="m-b-20"><?php echo $fields["distributors_section_2_field_4"]; ?></p>

		<h4 class="m-t-0 m-b-20"><?php echo $fields["distributors_section_2_field_5"]; ?></h4>
		<p class="m-b-20"><?php echo $fields["distributors_section_2_field_6"]; ?></p>
		<p class="m-b-20"><?php echo $fields["distributors_section_2_field_7"]; ?></p>
	</div>

	<div class="container">
		<h2 class="form-title"><?=get_field("form3_title",'option'); ?></h2>
		<p class="form-subtitle"><?=get_field("distributors_title_subtitle",'option'); ?></p>
		<div class="row">
			<div class="col-lg-12">
				<form id="ajax_register_distributor" method="POST" action="" class="form-checkboxes prov-checkbox offer_form f-w-form clearfix m-t-40 my_amazing_form about_fix">
					<?php  wp_nonce_field( 'registration_secure_user', 'cross-domain-register-nonce', false ); ?>
                    <?php $entity = 'distributors'; ?>
                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
					<input type="hidden" name="user_role" value="prospect_distributor">

					<input type="hidden" name="subject_to_submit" value="<?php echo get_field($entity . '_mail_subject', 'option'); ?>">
					<?foreach(get_field($entity . '_email_for_sending', 'option') as $em){?>
						<input type="hidden" name="email_to_submit[]" value="<?=$em["email"]?>">
					<?}?>

					<div class="row">
					<?php foreach(get_field($entity . '_form_fields', 'option') as $field) : ?>
							<?php include("include/form/forminputs.php");?>
					<? endforeach; ?>
					</div>
					<div class="clearfix"></div>
					<h4 class="text-blue m-t-45"><?=get_field($entity . '_title','option'); ?></h4>
					<div class="form-checkboxes">
					   <?php include("include/sudscribe/subscribe_checkboxes.php");?>
					</div>
                    <div class="subskribe">
                        <?php $agreements = get_field($entity . '_agreements','option'); ?>
                        <?php foreach ( $agreements as $agreement ) :?>
                            <?php $agreement_id = 'agree_' . rand(); ?>
                            <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                            <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>
                    </div>

					<div class="row">
						<div class="col-xs-12 text-center">
							<button type="submit" class="nbtn nbtn-inverse pull-right">
								<?=get_field($entity . '_submit_buttom','option'); ?><i class="ico arrow arrow_white"></i>
							</button>
						</div>
					</div>

					<div class="row m-t-10">
						<div class="col-xs-12 text-center">
							<div class="alert alert-success text-center notification hidden" role="alert">
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<script>
  
    jQuery(document).ready(function($) {        
         function set_center_block_interval(){             
                $('.show-on-map-center.active').trigger('click');
        }        
        //setTimeout( set_center_block_interval, 5000); 
     })         
</script>

<?php get_footer(); ?>
