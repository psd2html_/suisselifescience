jQuery(document).ready(function($){


	$(document).on('click','.scroll-down', function(event){
		var firstScreenHeight = $('.first-screen').height();
		var firstScreenPadding = parseInt($('.first-screen').css('padding-top'));

		$('body').animate({ scrollTop: firstScreenHeight + firstScreenPadding }, 'slow')
	});
	$(document).on('click , touchstart','.close-menu , .nav-toggle', function(){
		$('.sidemenu').toggleClass('off');
	});
	// $('.carousel').swiperight(function() {
	// 	$(this).carousel('prev');
	// });
	// $('.carousel').swipeleft(function() {
	// 	$(this).carousel('next');
	// });
	$('.ourBlog--carousel').slick({
		slidesToShow:3,
		infinite: true
	});
});
// 	$(window).resize(function(event) {
// 		animationsOn()
// 	});
// 	animationsOn();

// 	/**
// 	 * Disable overflow for wow animations
// 	 */
// 	function animationsOn() {
// 		var mq = window.matchMedia( "(max-width: 768px)" );
// 		if (mq.matches) {
// 			$('html').css('overflow', 'initial');
// 		}
// 	};
// });
