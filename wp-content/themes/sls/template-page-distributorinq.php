<?php /* Template Name: Distributorinq */ ?>
<?php get_header();?>
<?php $fields = get_fields( get_the_ID() ); ?>
        <?php $form_section = get_fields('option'); ?>

<section class="full-width-slider">
	<div id="myCarousel" class="carousel slide">
		<div class="carousel-inner" role="listbox">
			<?foreach(get_field("section_1_slider_items") as $key=>$item){?>
				<div class="item <?if($key==0){?>active<?};?>">
					<img class="first-slide" src='<?=$item["image"]?>' alt="First slide">
				</div>
			<?};?>
		</div><!--
		<div class="container btn_holder">
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
		</div> -->
	</div>
</section>
<section class="less_h1">
	<div class="container">
		<h1 class="m-t-0 m-b-30 letter-spacing"><?php echo $fields['section_2_title']; ?></h1>
		<div class="row">
			<div class="col-md-10">
				<p class="m-b-30"><?=get_field("section_2_description_1")?></p>
				<p class="m-b-30 text-blue f-18">
					<b class="text-uppercase"><?=get_field("section_2_list_title")?></b>
				</p>
				<!-- <ul class="blue_ul inside_ul">
					<?foreach(get_field("section_2_list_items") as $key=>$item){?>
						<li><?=$item["list_item"]?></li>
					<?};?>
				</ul>  -->
				<ul class="blue_ul inside_ul">
					<li><b class="text-uppercase">Medical devices</b> distributors</li>
					<li>Pharma or <b class="text-uppercase">top-brands cosmetic</b> distributors</li>
				</ul>
				<p class="m-b-30  m-t-30 f-18">with</p>


				<ul class="blue_ul inside_ul">
					<li>A strong focus on <b class="text-uppercase">education, customer service and innovation</b></li>
					<li><b class="text-uppercase">Access and established distribution</b> to anti-aging physicians, age-management practitioners, cosmetic doctors, dermatologists, plastic surgeons and clinics, nutritionists, high-end medispas and pharmacies.</li>
				</ul>
				<p class="m-t-30 bold f-23 text-uppercase"><?=get_field("section_2_sub_title")?></p>
				<p class="m-t-30" id="form_hash"><?=get_field("section_2_description_2")?></p>
				</div>
			</div>
		</div>
	</section>
	<section>
    <?php $entity = 'distributors_inquiries'; ?>
	<div class="container">
		<h2 class="form-title"><?=$form_section["distributors_inquiries_title"];?></h2>
		<p class="form-subtitle"><?=$form_section["distributors_inquiries_subtitle"];?></p>
		<div class="row">
			<div class="col-lg-12">
				<form id="ajax_register_distributor_inq" method="POST" action="" class="f-w-form clearfix my_amazing_form">
					<?php  wp_nonce_field( 'registration_secure_user', 'cross-domain-register-nonce', false ); ?>
					<input type="hidden" name="user_role" value="prospect_distributor">

					<input type="hidden" name="subject_to_submit" value="<?php echo get_field('distributors_inquiries_mail_subject', 'option'); ?>">
                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
					<?$email="";foreach(get_field('distributors_inquiries_email_for_sending', 'option') as $em){?>
						<input type="hidden" name="email_to_submit[]" value="<?=$em["email"]?>">
					<?}?>
					<div class="row">
						<?php foreach(get_field('distributors_inquiries_form_fields', 'option') as $field) : ?>

							<?php include("include/form/forminputs.php");?>

						<? endforeach; ?>
					</div>
					<div class="clearfix"></div>
					<h4 class="text-blue m-t-40"><?=$form_section["distributors_inquiries_interests_title"];?></h4>
					<div class="form-checkboxes prov-checkbox">
					   <?php include("include/sudscribe/subscribe_checkboxes.php");?>
					</div>
					<div class="row">
						<div class="subskribe clearfix">
                            <?php $agreements = $form_section['distributors_agreements_form']; ?>
                            <?php foreach ( $agreements as $agreement ) :?>
                                <?php $agreement_id = 'agree_' . rand(); ?>
                                <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                                <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                            <?php endforeach; ?>
						</div>

						<div class="row">
							<div class="col-xs-12 text-center">
								<button type="submit" class="nbtn nbtn-inverse pull-right req-demo">
									<?php echo $form_section['distributors_inquiries_submit_button']; ?><i class="ico arrow arrow_white"></i>
								</button>
							</div>
						</div>

						<div class="row m-t-10">
							<div class="col-xs-12 text-center">
								<div class="alert alert-success text-center notification hidden" role="alert">
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</section>
<section class="text-center text-white cover" style="background-image: url('<?=get_field("section_4_background_image")?>');">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<p class="m-t-0 m-b-30 f-18 text-center text-uppercase bold"><?=get_field("section_4_title_1")?></p>
				<p class="f-23 text-center text-uppercase letter-spacing"><?=get_field("section_4_title_2")?></p>
				<div class="col-sm-12 m-t-60  m-b-40 text-white block_icons">
					<div class="row">
						<?foreach(get_field("section_4_list_items") as $key=>$item){?>
							<div class="col-sm-4">
								<div class="icon vertical-center"><img class="center-block" src='<?=$item["icon"]?>' alt=""></div>
								<p class="text-center m-t-40"><?=$item["title"]?></p>
							</div>
						<?};?>
					</div>
				</div>
				<p class="text-center"><?=get_field("section_4_description_bottom_list_items")?></p>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<?=get_field("section_5_description")?>
	</div>
</section>
<section  class="cover" style="background-image: url('<?=get_field("section_6_background_image")?>');">
	<div class="container text-white">
		<div class="row">
			<div class="col-lg-10">
				<h2 class="letter-spacing text-uppercase m-b-30"><?=get_field("section_6_title_list_items")?></h2>
				<ul class="blue_ul inside_ul">
					<?foreach(get_field("section_6_list_items") as $item){?>
						<li>
							<b><?=$item["desription_item"]?></b>
						</li>
					<?};?>
				</ul>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-10">
				<h3 class="f-31 m-b-30 m-t-0 letter-spacing text-uppercase"><?=get_field("section_7_title_list_items")?></h3>
				<ul class="blue_ul inside_ul">
					<?foreach(get_field("section_7_list_items") as $item){?>
						<li><?=$item["desription_item"]?></li>
					<?};?>
				</ul>
				<p class="text-blue text-uppercase m-t-30 letter-spacing f-23"><?=get_field("section_7_sub-title_sounds_great_it_really_is")?> <a href="<?=get_field("section_7_button_url_book_a_demo_right_now")?>" class="nbtn nbtn-inverse long_btn m-t-10 p-r-70"><?=get_field("section_7_button_text_book_a_demo_right_now")?> <i class="ico arrow arrow_white"></i></a></p>
			</div>
		</div>
	</div>
</section>
<section class="text-white cover" style="background-image: url('<?=get_field("section_8_background_image")?>');">
	<div class="container">
			<div class="row">
			<div class="col-xs-12">
				<h2 class="bold   letter-spacing text-uppercase m-t-0 m-b-30"><?=get_field("section_8_title")?></h2>
				</div>
				<div class="col-md-6">
					<ul class="blue_ul inside_ul">
						<?foreach(get_field("section_8_list_items") as $key=>$item){?>
							<?if($key<5){?>
								<li><?=$item["description_item"]?></li>
							<?};?>
						<?};?>
					</ul>
				</div>
			<div class="col-md-6">
				<ul class="blue_ul inside_ul">
					<?foreach(get_field("section_8_list_items") as $key=>$item){?>
						<?if($key>=5){?>
							<li><?=$item["description_item"]?></li>
						<?};?>
					<?};?>
				</ul>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-10">
				<h3 class="f-31 m-b-30 m-t-0 letter-spacing text-uppercase"><?=get_field("section_9_title")?></h3>
				<ul class="blue_ul inside_ul">
					<?foreach(get_field("section_9_list_items") as $key=>$item){?>
						<li><?=$item["title"]?></li>
					<?};?>
				</ul>
				<p class="m-t-30 bold"><?=get_field("section_9_description_bottom_list_items")?></p>
			</div>
		</div>
	</div>
</section>
<section class="footer_slide">
		<div class="photo_galery">
			<?foreach(get_field("section_10_photo_items") as $key=>$item){?>
			<img src='<?=$item["image"]?>' alt="photo">
			<?};?>
		</div>
	<div class="clearfix"></div>
</section>
<?php get_footer(); ?>
<script>
jQuery(document).ready(function($){
  $('.photo_galery').owlCarousel({
   margin:0,
   nav: true,
   loop:true,
   autoWidth:true,
   items:10
  });
 });
</script>
