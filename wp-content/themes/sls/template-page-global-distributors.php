<?php /* Template Name: Global distributors */ ?>
<?php get_header(); ?>
<?php

get_header();
$fields = get_fields( get_the_ID() );
?>

<section class="distri_map">
    <div class="top_h1">
        <h3 class="text-white font-ligth m-b-20"><?php echo $fields['section_1_field_1']; ?></h3>
    </div>
    <div id="map-block-distributors" class="map-block-distributors"></div>
    <div class="dist-btn-zoom-controls">
        <button id="btn-zoom-controls-zoom-in" class="btn-zoom-controls-zoom-in"><i class="fa fa-plus"></i></button>
        <button id="btn-zoom-controls-zoom-out" class="btn-zoom-controls-zoom-out"><i class="fa fa-minus"></i></button>
    </div>
</section>



<?php


global $sls_data;

//wp_enqueue_script(
//    'google-map',
//    "https://maps.googleapis.com/maps/api/js?key=" . $sls_data['googleapis_api_key']
//);

wp_enqueue_script(
    'google-map',
    "https://maps.googleapis.com/maps/api/js?key=AIzaSyA55HS4C36lVMwdXSHgeY-BiVwcsfvsa20"
);

wp_enqueue_script('underscore');

global $wpdb;
$db_table_name = SECURE_CENTERS_TABLE;

    $lat_lon = Seven_Geolocation::geolocate_coordinates( '', true, true  );
    $query_select_order_by = "";
    $query_select_columns = "select * from `{$db_table_name}` ";
    if(!empty($lat_lon)){
        if(array_key_exists("latitude", $lat_lon) && array_key_exists("longitude", $lat_lon))
        {
             $latitude   = $lat_lon['latitude'];
             $longitude  = $lat_lon['longitude'];
             $earthRadius = '99999999999.0'; //in miles.
             $query_select_columns = "select *,ROUND(
                {$earthRadius} * ACOS(
                    SIN({$latitude} * PI() / 180) * SIN(latitude * PI() / 180) + COS({$latitude} * PI() / 180)
                    * COS(latitude * PI() / 180) * COS((longitude * PI() / 180) - ({$longitude} * PI() / 180))
                  ),
                  1
              ) AS DISTANCE from `{$db_table_name}` ";
             $query_select_order_by = " ORDER BY DISTANCE ";
        }
    }


   $args = $query_select_columns." where user_role IN ('distributor', 'master-distributor')".$query_select_order_by;

//print_r($args);
$mypostsSearch = $wpdb->get_results($args);
$jsOffice = array();

if(!empty($mypostsSearch) && count($mypostsSearch) > 0) {
    //include('include/centers/center-block.php');
}


foreach ( $mypostsSearch as $center ) {
    include('include/centers/centers-marker-html.php');
    $jsOffice[] = array(
        'id' => $center->id,
        'location' => array("lat"=>$center->latitude,"lng"=>$center->longitude),
        'company_name' => (strtolower($center->user_role)=="partner" ?$center->brand_name: $center->company_name),
        'company_phone' => $center->company_phone,
        'city' => $center->town_city,
        'zip' => $center->zip,
        'state' => $center->state,
        'address' => $center->street_address,
        'country' => $center->country,
        'HTML'=>$markerHTML
    );
}



?>
    <script>
        var global_distributors = <?php echo json_encode($jsOffice); ?>;
    </script>

    <section class="login_bg overflow-hidden">
        <div class="center-list">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 ">
                        <h4 class="uppercase text-blue m-t-0"><?php echo $fields['section_2_title']; ?></h4>
                    </div>
                </div>
            </div>
            <div class="global-distributor-list m-t-35">
                <?php if( ! empty($mypostsSearch)) : ?>
                        <?php $i = 0; $j = 1; ?>
                    <?php foreach( $mypostsSearch as $center): ?>
                        <?php if ($i % 3 == 0) : ?>
                            <?php $j = 1; ?>
                            <div class="global-distributor-item">
                                <div class="container">
                                    <div class="row">
                        <?php endif; ?>
                                <div class=" col-md-4 col-sm-offset-0 col-xs-12">
                                       <?php if((strtolower($center->user_role)=="partner")){?>
                                            <h5 class="uppercase"><b><?=$center->brand_name;?></b></h5>
                                               <?php if($center->center_name && !empty($center->center_name)) : ?>
                                                <h5 class="uppercase"><b><?=$center->center_name; ?></b></h5>
                                             <?php endif; ?>
                                        <?php } else { ?>
                                                 <h5 class="uppercase"><b><?=$center->company_name;?></b></h5>
                                       <?php } ?>
                                    <?php include("include/centers/display_block_center.php");?>
                                    <h5 class="m-t-15 font-light"><a class="show-on-map-center" data-center_id="<?php echo $center->id; ?>"><?php echo __('Show on map', 'sls-theme'); ?></a></h5>
                                </div>
                        <?php if ($j == 3) : ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php $i++; $j++; ?>
                        <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="b-t-grey ">
            <?php $form_section = get_fields('option'); ?>
            <style>
            </style>
            <div class="container">
                <div class="row  clearfix">
                    <div class="col-sm-12">
                        <h2 class="form-title"><?php echo $form_section['global_distributors_title']; ?></h2>
                        <p class="form-subtitle"><?php echo $form_section['global_distributors_subtitle']; ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 contact-form">
                        <form id="ajax_contact_send" method="POST" action="" class="clearfix my_amazing_form">

                    <?php $entity = 'global_distributors'; ?>
                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
                            <div class="row clearfix">
                                <?php foreach($form_section['global_distributors_form_fields'] as $field){ ?>
                                    <?php include("include/form/forminputs.php");?>

                                <? }; ?>
                            </div>

                            <h4 class="text-blue m-t-45"><?php echo $form_section["global_distributors_interests_title"]; ?></h4>

                            <div class="form-checkboxes prov-checkbox">


					   <?php include("include/sudscribe/subscribe_checkboxes.php");?>

                            </div>

                            <div class="subskribe  clearfix col-xs-12 ">
                                <?php $agreements = $form_section['global_distributors_agreements']; ?>
                                <?php foreach ( $agreements as $agreement ) :?>
                                    <?php $agreement_id = 'agree_' . rand(); ?>
                                    <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                                    <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                                <?php endforeach; ?>
                            </div>

                            <input type="hidden" name="subject_to_submit" value="<?php echo $form_section["global_distributors_mail_subject"]; ?>">

                            <?php foreach( $form_section['global_distributors_email_for_sending'] as $email ) : ?>
                                <input type="hidden" name="email_to_submit[]" value="<?php echo $email["email"]; ?>">
                            <?php endforeach; ?>
<div class="clearfix">
                            <button class="nbtn nbtn-inverse  m-t-10 pull-right" type="submit"><?php echo $form_section["global_distributors_submit_button"]; ?></button>
                            </div>

                        </form>

                    </div>
                </div>
        </section>

<script>

    jQuery(document).ready(function($) {
         function set_center_block_interval(){
             if($('.show-on-map-center').length > 0){
                 $('.show-on-map-center').first().trigger('click');
             }

        }
        //setTimeout( set_center_block_interval, 5000); 
     })
</script>

<?php get_footer(); ?>
