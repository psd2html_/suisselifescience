<?php
/**
 * Template Name: Thanks page
 */

$fields = get_fields(get_the_ID());
?>

<?php get_header(); ?>

    <section class="thanks-page">
        <div class="container">
            <div class="col-lg-12">
                <?php if (have_posts()): ?>

                    <?php while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php endif; ?>

                <!--<a href="/" class="black-btn m-t-60">my dashboard</a>-->
            </div>
        </div>
    </section>

<?php get_footer(); ?>
