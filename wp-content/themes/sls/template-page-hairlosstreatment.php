<?php /* Template Name: Hairlosstreatment */ ?>
<?php get_header();

$fields = get_fields( get_the_ID() );

wp_enqueue_script(
	'touchSwipe',
	get_template_directory_uri() . "/js/jquery.touchSwipe.min.js",
	array('jquery'),
	false,
	true
);

?>
<?php // section 1 ?>
<?php $slides = $fields['section_1_slider']; ?> 
	<section class="cover mobile_baner baner_h630" style="background: rgba(0, 0, 0, 0) url('<?php echo $slides[0]['background_image'];?>') repeat  center;">
		<div class="container">
			<h2 class="slide_title"><?php echo $slides[0]['slide_title']; ?></h2>
			<div class="slide_text">
				<?php echo $slides[0]['slide_description']; ?>
			</div>
		</div>
	</section> 
<!-- <section class="full-width-slider">
	<?php $slides = $fields['section_1_slider']; ?>
	<div id="myCarousel" class="carousel slide"> 
		<div class="carousel-inner" role="listbox">
			<?php $first = true; ?>
			<?php foreach( $slides as $slide ) :?>
			<?php $class = ($first) ? 'item active' : 'item' ?>
			<?php $first = false; ?>
			<div class="<?php echo $class; ?>">
				<img class="first-slide" src='<?php echo $slide['background_image']; ?>' alt="<?php echo $slide['background_image_alt']; ?>">
				<div class="container relative">
					<div class="carousel-caption">
						<h2 class="slide_title"><?php echo $slide['slide_title']; ?></h2>
						<div class="slide_text">
							<?php echo $slide['slide_description']; ?>
						</div>
					</div> 
				</div>
			</div>
			<?php endforeach; ?>
		</div>
		<div class="container btn_holder">
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"></a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"></a>
		</div> 
	</div>
</section> -->
<?php // section 2 ?>
<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="m-t-0 letter-spacing text-uppercase"><?php echo $fields['section_2_title']; ?></h1>
				<?php echo $fields['section_2_description']; ?>
			</div>
		</div>
	</div>
</section>
<?php // section 3 ?>
<section class="cover mobile_baner baner_h646" style="background: url('<?php echo $fields['section_3_background_image']; ?>') no-repeat  center;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="m-t-0 m-b-40 letter-spacing text-uppercase"><?php echo $fields['section_3_title']; ?></h2>
				<p class="f-23 bold m-b-40  letter-spacing"><?php echo $fields['section_3_description']; ?></p>
				<p class=" f-23 text-uppercase m-b-40 letter-spacing"><?php echo $fields['section_3_sub_description']; ?></p>
			</div>
			<div class="col-md-4 mobile-m-b">
				<p class="m-b-40 text-left font-normal"><?php echo $fields['section_3_left_column_description_1']; ?></p>
				<p class="f-23 text-nowrap text-blue text-left letter-spacing text-uppercase thin"><?php echo $fields['section_3_left_column_description_2']; ?></p>
			</div>
			<?php $right_column_items = $fields['section_3_right_column_items']; ?>
			<div class="col-md-4 pull-right wow fadeInRight" data-wow-duration="0.4s" data-wow-delay="0.5s">
				<div class="mobile-p0 p-l-70 m-b-40">
					<p class="italic m-b-10"><?php echo $right_column_items[0]['title']; ?></p>
					<p class="text-uppercase text-blue p-b-10 bold b-b b-blank m-b-10"><?php echo $right_column_items[0]['description']; ?></p>
					<p class="bold"><?php echo $right_column_items[0]['tags']; ?></p>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-3 wow fadeInRight" data-wow-duration="0.4s" data-wow-delay="1s">
				<div class="mobile-p0 p-l-70">
					<p class="italic m-b-10"><?php echo $right_column_items[1]['title']; ?></p>
					<p class="text-uppercase text-blue p-b-10 bold b-b b-blank m-b-10"><?php echo $right_column_items[1]['description']; ?></p>
					<p class="bold"><?php echo $right_column_items[1]['tags']; ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php // section 4 ?>
<section class="cover experience" style="background-image: url('<?php echo $fields['section_4_background_image']; ?>');">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="bold text-uppercase letter-spacing f-23 m-t-0 text-white text-center"><?php echo $fields['section_4_title']; ?></h2>
				<?php $advantages = $fields['section_4_advantages']; ?>
				<div class="col-sm-12 m-t-60 text-white advantage">
					<div class="row">
						<?php foreach( $advantages as $advantage ) : ?>
						<div class="col-sm-6 col-md-3">
							<div class="icon"><img class="center-block" src="<?php echo $advantage['image']; ?>" alt="<?php echo $advantage['image_alt']; ?>"></div>
							<p class="text-center m-t-40"><?php echo $advantage['description']; ?></p>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php // section 5 ?>
<section>
    <?php $entity = 'hairlos'; ?>
	<?php $form_section = get_fields('option'); ?>
	
	<div class="container">
		<h2 class="text-left text-blue m-t-0 m-b-0 font-ligth uppercase"><?=$form_section[$entity . '_title']; ?></h2>
		<p class="bold m-t-20"><?=$form_section[$entity . '_subtitle']; ?></p>
		<div class="row">
		<div class="col-sm-12">
			<div class="contact-form">
				<form id="ajax_contact_send" method="POST" action="" class="clearfix m-t-40 my_amazing_form">

				<div class="row clearfix">
					<?php foreach($form_section['hairlos_form_fields'] as $field){ ?>
						<?php include("include/form/forminputs.php");?>
					<?};?>
				</div>

				<h4 class="text-blue m-t-45"><?php echo $form_section["hairlos_interests_title"]; ?></h4>

				<div class="form-checkboxes prov-checkbox">
					<?php include("include/sudscribe/subscribe_checkboxes.php");?>
				</div>

				<div class="subskribe col-xs-12 clearfix">
                    <?php $agreements = $form_section['hairlos_agreements']; ?>
                    <?php foreach ( $agreements as $agreement ) :?>
                        <?php $agreement_id = 'agree_' . rand(); ?>
                        <input type="checkbox" id="<?php echo $agreement_id; ?>" required <?php echo ( $agreement['checked'] ) ? 'checked' : '';  ?>>
                        <label for="<?php echo $agreement_id; ?>" class="small_check one_line"><?php echo $agreement["name"]; ?></label>
                    <?php endforeach; ?>
				</div>

				<input type="hidden" name="subject_to_submit" value="<?=$form_section["hairlos_mail_subject"]; ?>">

                <input type="hidden" name="entity" value="<?php echo $entity; ?>">

				<?php foreach($form_section['hairlos_email_for_sending'] as $email):?>
					<input type="hidden" name="email_to_submit[]" value="<?=$email["email"]; ?>">
				<?php endforeach; ?>
				<div class="clearfix">
					<button class="nbtn nbtn-inverse  m-t-10 pull-right" type="submit"><?=$form_section["hairlos_submit_button"]; ?><i class="ico arrow arrow_white"></i></button>
				</div>

			</form>

		</div>
		</div>
	</div>

</section>
<?php // section 6 ?>
<section class="cover" style="background: url('<?php echo $fields['section_6_background_image']; ?>') no-repeat  center;">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-4 col-md-8 col-xs-offset-0 col-xs-12 text-white">
                <?php $entity = 'join_now'; ?>
				<?php echo $fields['section_6_right_content_description']; ?>

				
				<form method="GET" class="m-t-30 col-md-8" action="http://stagingidna.7demo.org.ua/anti-aging-from-DNA/luxury-anti-aging-get-started/survey" >
					<input type="hidden" name="product_id" value="39398">
					<div class="input-group input-group-xs">
						<input required="" type="email" class="join-input" name="email" placeholder="<?php echo $form_section[$entity.'_enter_your_email']; ?>" aria-invalid="true">
						<div class="input-group-btn">
							<button type="submit" class="nbtn nbtn-white find-centers-btn"><?php echo $form_section[$entity.'_submit_buttom']; ?></button>
						</div>
					</div>					
				</form>	
				
				<!--<form id="ajax_main_send" class="m-t-30 col-md-8" method="POST" action="">
					<input type="hidden" name="subject_to_submit" value="<?php echo $form_section["join_now_mail_subject"]; ?>">
					<input type="hidden" name="subscr" value="subscr">
                    <input type="hidden" name="entity" value="<?php echo $entity; ?>">
					<?php foreach( $form_section['join_now_email_for_sending'] as $email ) : ?>
						<input type="hidden" name="email_to_submit[]" value="<?php echo $email["email"]; ?>">
					<?php endforeach; ?>
					<div class="input-group input-group-xs">
						<input required="" type="email" class="join-input" name="email" placeholder="<?php echo $form_section["join_now_enter_your_email"]; ?>" aria-invalid="true">
						<div class="input-group-btn">
							<button type="submit" class="nbtn nbtn-white find-centers-btn"><?php echo $form_section["join_now_submit_buttom"]; ?></button>
						</div>
					</div>
				</form>-->
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
