<?php /* Template Name: Log in */ ?>
<?php

get_header();

wp_enqueue_script('jquery');

$fields = get_fields( get_the_ID() );
?>

    <section class="login_sect" <?php if (!empty($fields["section_2_background"])) : ?>
             style="background-image: url('<?php echo $fields["section_2_background"]; ?>');
             <?php endif; ?>
                 ">
                  <div class="container text-center text-white">
            <h2 class=" font-ligth m-b-30 m-t-0"><?php echo $fields["login_loginform_field_1"]; ?></h2>
            <h4 class="text-blue m-b-0"><?php echo $fields["login_loginform_field_2"]; ?></h4>
            <h5 class=" m-b-0 m-t-10"><?php echo $fields["login_loginform_field_3"]; ?></h5>
            <!--a href="<?php echo $fields["login_loginform_field_4"]['url']; ?>" class="nbtn nbtn-inverse m-t-20" <?php echo $fields["login_loginform_field_4"]['target'] ? 'target="_blank"' : '' ;?> -->
            <a href="http://suisselifescience.com/anti-aging-from-DNA/providers" class="nbtn nbtn-inverse m-t-20" <?php echo $fields["login_loginform_field_4"]['target'] ? 'target="_blank"' : '' ;?> >    
            <?php echo $fields["login_loginform_field_4"]['title']; ?>
                <i class="ico arrow arrow_white"></i>
            </a>
</section>
        </div>
</section>
<section class="login_bg ">
    <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-height col-sm-top overflow">
                    <div class="login_block clearfix text-center">
                        <h3 class="uppercase m-b-10"><?php echo $fields["login_loginform_field_5"]; ?></h3>
                        <h5 class="m-b-20 m-t-0"><?php echo $fields["login_loginform_field_6"]; ?></h5>
                        <h4 class=" text-blue m-t-20 m-b-20"><?php echo $fields["login_loginform_field_7"]; ?></h4>
                        <p class="m-b-5 f-14 text-center"><?php echo $fields["login_loginform_field_8"]; ?></p>
                        <form class="loginform" id="loginform" action="https://secure.idna.works/login" method="post" >
                            <input name="log" id="user_login" class="full-width b-grey m-t-15 m-b-20" type="text" placeholder="<?php echo $fields["login_loginform_field_9"]; ?>" required>
                            <div class="error_message error_username"></div>
                            <input class="full-width b-grey m-t-20 m-b-20" type="password" name="pwd" id="user_pass"  value="" autocomplete="off" type="password" placeholder="<?php echo $fields["login_loginform_field_10"]; ?>" required>
                            <div class="error_message error_password"></div>
							 <button class="nbtn nbtn-inverse m-t-10 m-b-10 bold" type="submit">
                                <?php echo $fields["login_loginform_field_11"]; ?>
                                <i class="ico arrow"></i>
                            </button>
                          <!--  <button class="nbtn nbtn-inverse m-t-20" type="button" name="wp-submit">
                                <?php echo $fields["login_loginform_field_11"]; ?>
                                <i class="ico arrow arrow_white"></i>
                            </button>-->
                            <br>
                            <h6>
                                <a href="https://secure.idna.works/"  class="f-14">
                                    <?php echo $fields["login_loginform_field_12"]; ?>
                                </a>
                            </h6>
							<input name="redirect_to" value="https://secure.idna.works/" type="hidden">
							<input name="instance" value="" type="hidden">
							<input name="action" value="login" type="hidden">
                        </form>
					</div>
                </div>
                <div class="col-sm-6 col-sm-height col-sm-top overflow">
                    <div class="login_block clearfix text-center">
                        <h3 class="uppercase m-b-10"><?php echo $fields["login_loginform_field_13"]; ?></h3>
                        <h5 class="m-b-20 m-t-0"><?php echo $fields["login_loginform_field_14"]; ?></h5>
                        <h4 class=" text-blue m-t-20 m-b-20"><?php echo $fields["login_loginform_field_15"]; ?></h4>
                        <p class="m-b-5 f-14 text-center"><?php echo $fields["login_loginform_field_16"]; ?></p>
                        <form class="loginform" id="loginform" action="https://secure.idna.works/login" method="post" >
                            <input class="full-width b-grey m-t-15 m-b-20" name="log" id="user_login" type="text" placeholder="<?php echo $fields["login_loginform_field_17"]; ?>" required>
                            <div class="error_message error_username"></div>
                            <input class="full-width b-grey m-t-20 m-b-20" type="password" name="pwd" id="user_pass"  value="" autocomplete="off" type="password" type="text" placeholder="<?php echo $fields["login_loginform_field_18"]; ?>" required>
                            <div class="error_message error_password"></div>
  							 <button class="nbtn nbtn-inverse m-t-10 m-b-10 bold" type="submit">
                                <?php echo $fields["login_loginform_field_19"]; ?>
                                <i class="ico arrow"></i>
                            </button>
                          <!--  <button class="nbtn nbtn-inverse m-t-20" type="button">
                                <?php echo $fields["login_loginform_field_19"]; ?>
                                <i class="ico arrow arrow_white"></i>
                            </button> -->
                            <br>
                            <h6>
                                <a href="https://secure.idna.works/" class="f-14">
                                <?php echo $fields["login_loginform_field_20"]; ?>
                            </a>
							</h6>
							<input name="redirect_to" value="https://secure.idna.works/" type="hidden">
							<input name="instance" value="" type="hidden">
							<input name="action" value="login" type="hidden">
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="about_us_header" <?php if (!empty($fields["section_2_background"])) : ?>
             style="background-image: url('<?php echo $fields["section_2_background"]; ?>');
             <?php endif; ?>
                 ">
        <div class="container">
            <h2 class="m-t-20"><?php echo $fields["login_get_in_touch_field_1"]; ?></h2>
            <h5><?php echo $fields["login_get_in_touch_field_2"]; ?></h5>
            <h4 class="m-b-60"><?php echo $fields["login_get_in_touch_field_3"]; ?></h4>
        </div>
        <div class="container">
            <div class="row text-center about_us_boxes">
                <div class="col-sm-6 about_us_box">
                    <h4 class="f"><?php echo $fields["login_get_in_touch_field_4"]; ?></h4>

                    <a href="<?php echo $fields["login_get_in_touch_field_5"]['url']; ?>" class="nbtn nbtn-inverse" <?php echo $fields["login_get_in_touch_field_5"]['target'] ? 'target="_blank"' : '' ;?>>
                        <?php echo $fields["login_get_in_touch_field_5"]['title']; ?>
                        <i class="ico arrow arrow_white"></i>
                    </a>
                </div>

                <div class="col-sm-6 about_us_box">
                    <h4 class="font-ligth f-42"><?php echo $fields["login_get_in_touch_field_6"]; ?></h4>
                    <h5 class="bold"><?php echo $fields["login_get_in_touch_field_7"]; ?></h5>
                    <h6 class="font-ligth "><?php echo $fields["login_get_in_touch_field_8"]; ?></h6>

                    <a href="<?php echo $fields["login_get_in_touch_field_9"]['url']; ?>" class="nbtn nbtn-inverse" <?php echo $fields["login_get_in_touch_field_9"]['target'] ? 'target="_blank"' : '' ;?>>
                        <?php echo $fields["login_get_in_touch_field_9"]['title']; ?>
                        <i class="ico arrow arrow_white"></i>
                    </a>
                </div>

                <div class="col-sm-6 about_us_box">
                    <h4 class="font-ligth f-42"><?php echo $fields["login_get_in_touch_field_10"]; ?></h4>
                    <h5 class="font-ligth "><?php echo $fields["login_get_in_touch_field_11"]; ?></h5>

                    <a href="<?php echo $fields["login_get_in_touch_field_12"]['url']; ?>" class="nbtn nbtn-inverse" <?php echo $fields["login_get_in_touch_field_12"]['target'] ? 'target="_blank"' : '' ;?>>
                        <?php echo $fields["login_get_in_touch_field_12"]['title']; ?>
                        <i class="ico arrow arrow_white"></i>
                    </a>
                </div>
                <div class="col-sm-6 about_us_box">
                    <h4 class="font-ligth f-42"><?php echo $fields["login_get_in_touch_field_13"]; ?></h4>
                    <h5 class="font-ligth "><?php echo $fields["login_get_in_touch_field_14"]; ?></h5>

                    <a href="<?php echo $fields["login_get_in_touch_field_15"]['url']; ?>" class="nbtn nbtn-inverse" <?php echo $fields["login_get_in_touch_field_15"]['target'] ? 'target="_blank"' : '' ;?>>
                        <?php echo $fields["login_get_in_touch_field_15"]['title']; ?>
                        <i class="ico arrow arrow_white"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
